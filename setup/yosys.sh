# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


echo "Setting up Yosys directory"

if ! dpkg -s bison >& /dev/null; then
    sudo apt install -y bison
fi
if ! dpkg -s flex >& /dev/null; then
    sudo apt install -y flex
fi
if ! dpkg -s libreadline-dev >& /dev/null; then
    sudo apt install -y libreadline-dev
fi
if ! dpkg -s gawk >& /dev/null; then
    sudo apt install -y gawk
fi
if ! dpkg -s tcl-dev >& /dev/null; then
    sudo apt install -y tcl-dev
fi
if ! dpkg -s tclsh >& /dev/null; then
    sudo apt install -y tclsh
fi
if ! dpkg -s libffi-dev >& /dev/null; then
    sudo apt install -y libffi-dev
fi
if ! dpkg -s graphviz >& /dev/null; then
    sudo apt install -y graphviz
fi
if ! dpkg -s xdot >& /dev/null; then
    sudo apt install -y xdot
fi
if ! dpkg -s pkg-config >& /dev/null; then
    sudo apt install -y pkg-config
fi
if ! dpkg -s libboost-all-dev >& /dev/null; then
    sudo apt install -y libboost-all-dev
fi
if ! dpkg -s zlib1g-dev >& /dev/null; then
    sudo apt install -y zlib1g-dev
fi
if ! dpkg -s python-dev >& /dev/null; then
    sudo apt install -y python-dev
fi
if ! dpkg -s jpegoptim >& /dev/null; then
    sudo apt install -y jpegoptim
fi
cd ..
cd run
if [ -d "yosys" ]; then
    rm -r -f yosys
fi
mkdir yosys
#cd yosys
#git clone https://github.com/YosysHQ/yosys.git
#mv -f yosys archive
#cd ..
cp misc/yosys.py yosys/yosys.py
cp misc/getTop.sh yosys/getTop.sh
cd ..
echo "Finished setting up Yosys directory"