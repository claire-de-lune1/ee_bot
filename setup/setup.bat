::==================================================================================
:: BSD 3-Clause License
::
:: Copyright (c) 2021, Tom Schmitz
:: All rights reserved.
:: 
:: Redistribution and use in source and binary forms, with or without
:: modification, are permitted provided that the following conditions are met:
:: 
:: 1. Redistributions of source code must retain the above copyright notice, this
::    list of conditions and the following disclaimer.
:: 
:: 2. Redistributions in binary form must reproduce the above copyright notice,
::    this list of conditions and the following disclaimer in the documentation
::    and/or other materials provided with the distribution.
:: 
:: 3. Neither the name of the copyright holder nor the names of its
::    contributors may be used to endorse or promote products derived from
::    this software without specific prior written permission.
:: 
:: THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
:: AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
:: IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
:: DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
:: FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
:: DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
:: SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
:: CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
:: OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
:: OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
::==================================================================================


reg query "hkcu\software\Python"
if ERRORLEVEL 1 GOTO NOPYTHON
goto :HASPYTHON
:NOPYTHON
	ECHO "Please Install Python"
	goto DONE
:HASPYTHON
	cd ..
	cd "EE_Bot_DB\EE_Bot_DB\src"
	IF exist sqlite ( rmdir sqlite /s /q )
	curl.exe --output sqlite.zip --url https://sqlite.org/2021/sqlite-amalgamation-3350200.zip
	tar -xf sqlite.zip
	del sqlite.zip
	ren sqlite-amalgamation-3350200 sqlite
	cd sqlite
	del shell.c
	del sqlite3ext.h
	cd ..
	cd ..
	cd ..
	pip3 install .
	IF ERRORLEVEL 1 GOTO FAILED
	cd ..
	pytest -v
	cd run
	IF exist sqlite ( rmdir kicost /s /q )
	mkdir kicost
	cd ..
	ECHO "Finished setting up EE Bot's Dependencies"
	goto DONE

:FAILED
	ECHO "Please make sure you have Visual Studio Build Tools installed"
	goto DONE

:DONE
PAUSE
