# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from EE_Bot_DB.EE_Bot_DB import EE_DB
from EE_Bot_DB.EE_Bot_DB import searchWarnings
from EE_Bot_DB.EE_Bot_DB import cogEnum
from EE_Bot_DB.EE_Bot_DB import channelType
from EE_Bot_DB.EE_Bot_DB import quizDifficulty
from EE_Bot_DB.EE_Bot_DB import serverData
from datetime import datetime
from sys import platform
import pytest
import os


Pairs = [[1, 'emoji1'], [2, 'emoji2'], [3, 'emoji3']]


windows = platform == 'win32'


@pytest.fixture(scope='session', autouse=True)
def db_fixture():
    if os.path.exists('test.db'):
        os.remove('test.db')
    yield
    #For some reason, windows causes race conditions
    if not windows:
        os.remove('test.db')


def test_creation():
    db = EE_DB('test.db', 1, False)
    assert isinstance(db, EE_DB)
    db.setupGuildFlags([1])
    archive, purge = db.getGuildFlags()
    assert archive is False and purge is False
    del db


def test_insert1():
    db = EE_DB('test.db', 1, False)
    db.insertUser(1, False)
    assert db.getError() != 0
    del db


def test_insert2():
    db = EE_DB('test.db', 1, True)
    db.insertUser(1, False)
    assert db.getError() == 0
    del db


def test_insert3():
    db = EE_DB('test.db', 1, False)
    db.insertUser(1, False)
    assert db.getError() != 0
    del db


def test_insert4():
    db = EE_DB('test.db', 1, False)
    db.insertUser(2, False)
    assert db.getError() == 0
    db.insertUser(3, False)
    assert db.getError() == 0
    del db


#Test guild configurations
def test_configureGuild():
    db = EE_DB('test.db', 1)
    db.configureGuild(1, 2, 3, 4, 5)
    assert db.getError() == 0
    db.configureGuild(1, 2, 3, 4, 5)
    assert db.getError() != 0
    ID = db.getLoggingID(channelType.log)
    assert ID == 1
    ID = db.getLoggingID(channelType.role)
    assert ID == 2
    ID = db.getLoggingID(channelType.level)
    assert ID == 3
    ID = db.getLoggingID(channelType.shame)
    assert ID == 4
    ID = db.getLoggingID(channelType.watch)
    assert ID == 5
    del db


def test_reconfigureGuild():
    db = EE_DB('test.db', 1)
    db.reconfigureGuild(2, 1, 1, 1, 1)
    assert db.getError() == 0
    db.configureGuild(1, 2, 3, 4, 5)
    assert db.getError() != 0
    ID = db.getLoggingID(channelType.log)
    assert ID == 2
    ID = db.getLoggingID(channelType.role)
    assert ID == 1
    ID = db.getLoggingID(channelType.level)
    assert ID == 1
    ID = db.getLoggingID(channelType.shame)
    assert ID == 1
    ID = db.getLoggingID(channelType.watch)
    assert ID == 1
    del db


def test_updatePosts():
    db = EE_DB('test.db', 1)
    rankUp, rank = db.updateUserPosts(1)
    assert rankUp is True and rank == 1
    rankUp, rank = db.updateUserPosts(1)
    assert rankUp is False and rank == 1
    rankUp, rank = db.updateUserPosts(1)
    assert rankUp is False and rank == 1
    rankUp, rank = db.updateUserPosts(1)
    assert rankUp is False and rank == 1
    rankUp, rank = db.updateUserPosts(1)
    assert rankUp is False and rank == 1
    rankUp, rank = db.updateUserPosts(1)
    assert rankUp is False and rank == 1
    rankUp, rank = db.updateUserPosts(1)
    assert rankUp is True and rank == 2
    rankUp, rank = db.updateUserPosts(1)
    assert rankUp is False and rank == 2
    rank, progress = db.checkUserRank(1)
    assert db.getError() == 0
    assert rank == 2
    assert progress > 0.0 and progress < 100.0
    del db


def test_leaderboard():
    db = EE_DB('test.db', 1)
    leaders, rank = db.getLeaderBoard(1)
    assert db.getError() == 0
    assert len(leaders) == 3
    assert rank == 1
    del db


def test_userDelete():
    db = EE_DB('test.db', 1)
    db.insertUser(4)
    db.deleteUser(4)
    assert db.getError() == 0
    db.updateUserPosts(4)
    assert db.getError() != 0
    del db


def test_updateFilter():
    db = EE_DB('test.db', 1)
    assert db.checkUserFilter(2)
    db.updateUserFilter(False, 2)
    assert db.checkUserFilter(2) is False
    del db


def test_notif():
    db = EE_DB('test.db', 1)
    rankUp, rank = db.updateUserPosts(2)
    assert rankUp and rank == 1
    db.updateNotifications(2, 0)
    rankUp, rank = db.updateUserPosts(2)
    assert rankUp is False and rank == 1
    rankUp, rank = db.updateUserPosts(2)
    assert rankUp is False and rank == 1
    rankUp, rank = db.updateUserPosts(2)
    assert rankUp is False and rank == 1
    rankUp, rank = db.updateUserPosts(2)
    assert rankUp is False and rank == 1
    rankUp, rank = db.updateUserPosts(2)
    assert rankUp is False and rank == 1
    rankUp, rank = db.updateUserPosts(2)
    assert rankUp is False and rank == 2
    del db


def test_adjustRank():
    db = EE_DB('test.db', 1)
    rank = db.adjustRank(2)
    assert db.getError() == 0
    assert rank == 2
    del db


def test_channel_create():
    db = EE_DB('test.db', 1)
    db.createChannel('123', 2)
    assert db.getError() != 0
    db.createChannel('123', 2, True)
    assert db.getError() == 0
    db.createChannel('123', 2, True)
    assert db.getError() != 0
    del db


def test_channel_delete():
    db = EE_DB('test.db', 1)
    db.deleteChannel('123')
    assert db.getError() == 0
    db.createChannel('123', 2, True)
    assert db.getError() == 0
    del db


def test_createPoll():
    db = EE_DB('test.db', 1)
    global Pairs
    db.createPoll(1, Pairs)
    assert db.getError() == 0
    del db


def test_retrievePoll():
    db = EE_DB('test.db', 1)
    global Pairs
    msgId, Pairs1 = db.retrievePoll(1)
    assert db.getError() == 0
    assert msgId == 1
    assert len(Pairs) == len(Pairs1)
    assert Pairs[0] in Pairs1
    assert Pairs[1] in Pairs1
    assert Pairs[2] in Pairs1
    del db


def test_checkPollId():
    db = EE_DB('test.db', 1)
    isActive = db.checkPoll_id(1)
    assert db.getError() == 0
    assert isActive
    isActive = db.checkPoll_id(2)
    assert db.getError() == 0
    assert isActive is False
    del db


def test_insertNewPair():
    db = EE_DB('test.db', 1)
    db.addNewPair(1, 4, 'emoji4')
    assert db.getError() == 0
    Pair = [4, 'emoji4']
    msgId, Pairs = db.retrievePoll(1)
    assert len(Pairs) == 4
    assert Pair in Pairs
    del db


def test_deletePair():
    db = EE_DB('test.db', 1)
    db.deletePair(1, 4, 'emoji4')
    assert db.getError() == 0
    Pair = [4, 'emoji4']
    msgId, Pairs = db.retrievePoll(1)
    assert len(Pairs) == 3
    assert Pair not in Pairs
    del db


def test_deletePoll():
    db = EE_DB('test.db', 1)
    db.deletePoll(1)
    assert db.getError() == 0
    msgId, Pairs = db.retrievePoll(1)
    assert db.getError() == 0
    assert len(Pairs) == 0
    del db


def test_warnings():
    db = EE_DB('test.db', 1)
    db.addWarning(2)
    assert db.getError() == 0
    warnings = db.addWarning(2)
    assert warnings == 2
    warnings = db.removeWarning(2)
    assert db.getError() == 0
    assert warnings == 1
    db.clearWarnings(2)
    assert db.getError() == 0
    warnings = db.retrieveWarnings(2)
    assert db.getError() == 0
    assert warnings == 0
    db.addWarning(2)
    warnings = db.retrieveWarnings(2)
    db.clearWarnings(2)
    assert warnings == 1
    del db


def test_poliMsg():
    db = EE_DB('test.db', 1)
    db.setPoliMsg(1)
    assert db.getError() == 0
    assert db.getPoliMsg() == 1
    db.clearPoliMsg()
    assert db.getError() == 0
    assert db.getPoliMsg() == 0
    del db


def test_poliRules():
    db = EE_DB('test.db', 1)
    db.setPoliRules([1, 2])
    assert db.getError() == 0
    rules = db.getPoliRules()
    assert db.getError() == 0
    assert len(rules) == 2
    assert 1 in rules
    assert 2 in rules
    rules.append(3)
    db.setPoliRules(rules)
    assert db.getError() == 0
    rules = db.getPoliRules()
    assert len(rules) == 3
    assert 1 in rules
    assert 2 in rules
    assert 3 in rules
    db.clearPoliRules()
    assert db.getError() == 0
    rules = db.getPoliRules()
    assert len(rules) == 0
    del db


def test_poliChannel():
    db = EE_DB('test.db', 1)
    db.setPoliRulesChannel(1)
    assert db.getError() == 0
    assert db.getPoliRulesChannel() == 1
    db.clearPoliRulesChannel()
    assert db.getError() == 0
    assert db.getPoliRulesChannel() == 0
    del db


def test_prefix():
    db = EE_DB('test.db', 1)
    db.setChannelPrefix('ece')
    assert db.getError() == 0
    assert db.getChannelPrefix() == 'ece'
    db.clearChannelPrefix()
    assert db.getError() == 0
    assert db.getChannelPrefix() == 'ee'
    del db


def test_watchlist():
    db = EE_DB('test.db', 1)
    assert db.inWatchlist(3) is False
    assert db.getError() == 0
    exists, poliBan, monitored, override = db.retrieveWatchlistFlags(3)
    assert exists is False
    assert db.inWatchlist(2)
    exists, poliBan, monitored, override = db.retrieveWatchlistFlags(2)
    assert exists and poliBan is False and monitored is False and override is False
    db.insertIntoWatchlist(3)
    assert db.getError() == 0
    assert db.inWatchlist(3)
    db.insertIntoWatchlist(2)
    assert db.getError() == 0
    db.addWarning(2)
    db.addWarning(2)
    db.addWarning(2)
    exists, poliBan, monitored, override = db.retrieveWatchlistFlags(2)
    assert exists and poliBan is False and monitored and override is False
    db.removeWarning(2)
    exists, poliBan, monitored, override = db.retrieveWatchlistFlags(2)
    assert exists and poliBan is False and monitored is False and override is False  #Uncomment this when issue #130 is resolved
    db.updateMonitor(3, True)
    db.updateOverride(3, True)
    db.updatePoliBan(3, True)
    exists, poliBan, monitored, override = db.retrieveWatchlistFlags(3)
    assert exists and poliBan and monitored and override
    db.updateOverride(2, True)
    db.updateMonitor(2, False)
    db.addWarning(2)
    exists, poliBan, monitored, override = db.retrieveWatchlistFlags(2)
    assert exists and poliBan is False and monitored is False and override
    del db


def test_logMessages():
    db = EE_DB('test.db', 1)
    db.addMessage(1, 1)
    assert db.getError() == 0
    db.addMessage(2, 1)
    db.addMessage(3, 1)
    db.addMessage(4, 1)
    db.addMessage(1, 2)
    messages = db.getMessages(1)
    assert db.getError() == 0
    assert len(messages) == 4
    assert 1 in messages and 2 in messages and 3 in messages and 4 in messages
    db.markMsgDeleted(3, 1)
    assert db.getError() == 0
    messages = db.getMessages(1)
    assert len(messages)
    assert 1 in messages and 2 in messages and 3 not in messages and 4 in messages
    db.bulkMsgDelete(1)
    assert db.getError() == 0
    messages = db.getMessages(1)
    assert len(messages) == 0
    messages = db.getMessages(2)
    assert len(messages) == 1
    assert 1 in messages
    del db


def test_guildFlags():
    db = EE_DB('test.db', 1)
    db.addGuild(1)
    assert db.getError() != 0
    db.addGuild(2)
    assert db.getError() == 0
    db.setGuildFlags(True, True)
    assert db.getError() == 0
    flag1, flag2 = db.getGuildFlags()
    assert db.getError() == 0
    assert flag1 and flag2
    db.resetGuildFlags()
    assert db.getError() == 0
    flag1, flag2 = db.getGuildFlags()
    assert not flag1 and not flag2
    db.setArchiveFlag(True)
    assert db.getError() == 0
    assert db.getArchiveFlag()
    assert db.getError() == 0
    db.setPurgeRoleFlag(True)
    assert db.getError() == 0
    assert db.getPurgeRolesFlag()
    assert db.getError() == 0
    db.setServerConfigured(True)
    assert db.getError() == 0
    assert db.checkServerConfiguredFlag()
    assert db.getError() == 0
    del db


def test_botFlags():
    db = EE_DB('test.db', 1)
    assert not db.getRunFlag()
    assert db.getError() == 0
    db.setRunFlag(True)
    assert db.getError() == 0
    assert db.getRunFlag()
    assert db.getInstances() == 0
    assert db.getError() == 0
    assert db.addInstance()
    assert db.getError() == 0
    assert db.getInstances() == 1
    assert not db.addInstance()
    assert db.getError() == 0
    assert db.removeInstance()
    assert db.getError() == 0
    assert db.getInstances() == 0
    assert not db.removeInstance()
    assert db.getCleanExitFlag()
    assert db.getError() == 0
    db.setCleanExitFlag(False)
    assert db.getError() == 0
    assert not db.getCleanExitFlag()
    assert db.getError() == 0
    del db


def test_toggleLevelSystem():
    db = EE_DB('test.db', 1)
    assert db.levelSystemStatus()
    assert db.getError() == 0
    db.toggleLevelSystem(False)
    assert db.getError() == 0
    assert not db.levelSystemStatus()
    del db


def test_cogs():
    db = EE_DB('test.db', 1)
    db.configureGuild(1, 1, 1, 1, 1)
    assert db.checkCog(cogEnum.Moderator)
    assert db.getError() == 0
    assert db.checkCog(cogEnum.Games)
    assert not db.checkCog(cogEnum.Hardware)
    assert db.checkCog(cogEnum.Yosys)
    db.enableCog(cogEnum.Hardware)
    assert db.getError() == 0
    assert db.checkCog(cogEnum.Hardware)
    db.disableCog(cogEnum.Moderator)
    assert db.getError() == 0
    assert not db.checkCog(cogEnum.Moderator)
    del db


def test_demote():
    db = EE_DB('test.db', 1)
    db.demoteMember(1, 1)
    assert db.getError() == 0
    rank, progress = db.checkUserRank(1)
    assert db.getError() == 0
    assert rank == 1 and progress >= 0.0 and progress < 100.0
    db.demoteMember(1, 5)
    assert db.getError() == 0
    rank, progress = db.checkUserRank(1)
    assert rank == 0 and progress >= 0.0 and progress < 100.0
    del db


def test_banMsg():
    db = EE_DB('test.db', 1)
    msg = db.getBanMsg()
    assert db.getError() == 0
    assert msg == ''
    db.setBanMsg('Test')
    assert db.getError() == 0
    msg = db.getBanMsg()
    assert msg == 'Test'
    db.clearBanMsg()
    assert db.getError() == 0
    assert db.getBanMsg() == ''
    del db


def test_logWarnings():
    db = EE_DB('test.db', 1)
    db.logWarning(1, 'test', 1, 'test', 2, 'admin', 1, 'test')
    assert db.getError() == 0
    db.logWarning(1, 'test', 3, 'test1', 2, 'admin', 1, 'test')
    db.logWarning(1, 'test1', 1, 'test', 1, 'admin1', 4, 'test2')
    db.logWarning(2, 'test', 1, 'test', 2, 'admin', 1, 'test')
    data1 = [1, 'test', 1, 'test', 2, 'admin', 1, 'test', '%d/%d/%d' % (datetime.now().month, datetime.now().day, datetime.now().year)]
    data2 = [1, 'test', 3, 'test1', 2, 'admin', 1, 'test', '%d/%d/%d' % (datetime.now().month, datetime.now().day, datetime.now().year)]
    data3 = [1, 'test1', 1, 'test', 1, 'admin1', 4, 'test2', '%d/%d/%d' % (datetime.now().month, datetime.now().day, datetime.now().year)]
    data4 = [2, 'test', 1, 'test', 2, 'admin', 1, 'test', '%d/%d/%d' % (datetime.now().month, datetime.now().day, datetime.now().year)]
    # Test getting by guild
    data = db.getWarningData(searchWarnings.GUILD, 1, True, ['1'])
    assert db.getError() == 0
    assert len(data) == 3
    assert data1 in data
    assert data2 in data
    assert data3 in data
    data = db.getWarningData(searchWarnings.GUILD, 2, True, ['2'])
    assert len(data) == 1
    assert data4 in data
    # Test getting by channel name
    data = db.getWarningData(searchWarnings.CHANNEL, 1, True, ['test'])
    assert db.getError() == 0
    assert len(data) == 3
    assert data1 in data
    assert data2 in data
    assert data4 in data
    data = db.getWarningData(searchWarnings.CHANNEL, 1, True, ['test1'])
    assert len(data) == 1
    assert data3 in data
    # Test getting by user id
    data = db.getWarningData(searchWarnings.USER_ID, 1, True, ['1'])
    assert db.getError() == 0
    assert len(data) == 3
    assert data1 in data
    assert data3 in data
    assert data4 in data
    data = db.getWarningData(searchWarnings.USER_ID, 1, True, ['3'])
    assert len(data) == 1
    assert data2 in data
    # Test getting by user name
    data = db.getWarningData(searchWarnings.USER_NAME, 1, True, ['test'])
    assert db.getError() == 0
    assert len(data) == 3
    assert data1 in data
    assert data3 in data
    assert data4 in data
    data = db.getWarningData(searchWarnings.USER_NAME, 1, True, ['test1'])
    assert len(data) == 1
    assert data2 in data
    # Test getting by admin ID
    data = db.getWarningData(searchWarnings.ADMIN_ID, 1, True, ['2'])
    assert db.getError() == 0
    assert len(data) == 3
    assert data1 in data
    assert data2 in data
    assert data4 in data
    data = db.getWarningData(searchWarnings.ADMIN_ID, 1, True, ['1'])
    assert len(data) == 1
    assert data3 in data
    # Test getting by admin name
    data = db.getWarningData(searchWarnings.ADMIN_NAME, 1, True, ['admin'])
    assert db.getError() == 0
    assert len(data) == 3
    assert data1 in data
    assert data2 in data
    assert data4 in data
    data = db.getWarningData(searchWarnings.ADMIN_NAME, 1, True, ['admin1'])
    assert len(data) == 1
    assert data3 in data
    # Test getting by rule violated
    data = db.getWarningData(searchWarnings.RULE, 1, True, ['1'])
    assert db.getError() == 0
    assert len(data) == 3
    assert data1 in data
    assert data2 in data
    assert data4 in data
    data = db.getWarningData(searchWarnings.RULE, 1, True, ['4'])
    assert len(data) == 1
    assert data3 in data
    # Test getting by reason
    data = db.getWarningData(searchWarnings.REASON, 1, True, ['test'])
    assert db.getError() == 0
    assert data1 in data
    assert data2 in data
    assert data4 in data
    data = db.getWarningData(searchWarnings.REASON, 1, True, ['test2'])
    assert len(data) == 1
    assert data3 in data
    # Test getting by date
    data = db.getWarningData(searchWarnings.DATE, 1, True, ['%d/%d/%d' % (datetime.now().month, datetime.now().day, datetime.now().year)])
    assert db.getError() == 0
    assert len(data) == 4
    assert data1 in data and data2 in data and data3 in data and data4 in data
    # Test getting all info
    data = db.getWarningData(searchWarnings.ALL, 1, True, [])
    assert db.getError() == 0
    assert len(data) == 4
    assert data1 in data and data2 in data and data3 in data
    del db


def test_quizData():
    db = EE_DB('test.db', 1)
    db.insertQuizUser(10)
    assert db.getError() == 0
    db.insertQuizUser(10)
    assert db.getError() == 0
    db.updateQuizData(10, quizDifficulty.veasy, False)
    assert db.getError() == 0
    db.updateQuizData(10, quizDifficulty.veasy, True)
    db.updateQuizData(10, quizDifficulty.easy, False)
    assert db.getError() == 0
    db.updateQuizData(10, quizDifficulty.easy, True)
    db.updateQuizData(10, quizDifficulty.normal, False)
    assert db.getError() == 0
    db.updateQuizData(10, quizDifficulty.normal, True)
    db.updateQuizData(10, quizDifficulty.hard, False)
    assert db.getError() == 0
    db.updateQuizData(10, quizDifficulty.hard, True)
    db.updateQuizData(10, quizDifficulty.vhard, False)
    assert db.getError() == 0
    db.updateQuizData(10, quizDifficulty.vhard, True)
    data1 = [10, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2]
    data2 = [9, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0]
    qData = db.fetchQuizEntry(10)
    assert qData == data1
    db.insertQuizUser(9)
    db.updateQuizData(9, quizDifficulty.veasy, False)
    db.updateQuizData(9, quizDifficulty.veasy, False)
    db.updateQuizData(9, quizDifficulty.veasy, True)
    qData = db.fetchQuizEntries()
    assert data1 in qData and data2 in qData
    del db


def test_requestServerData():
    db = EE_DB('test.db', 1)
    data = db.requestServerData(serverData.USERS)
    assert db.getError() == 0
    data1 = '1,1,0,1,8,1,0,0'
    data2 = '2,2,2,0,7,0,3,7'
    data3 = '3,3,0,1,0,1,0,0'
    assert data1 in data and data2 in data and data3 in data
    data = db.requestServerData(serverData.CHANNELS)
    assert db.getError() == 0
    data4 = '2,123'
    assert data4 in data
    data = db.requestServerData(serverData.POLL)
    assert db.getError() == 0
    assert data[0] == ''
    db.createPoll(1, Pairs)
    data5 = '5,1,1,emoji1'
    data6 = '6,1,2,emoji2'
    data7 = '7,1,3,emoji3'
    data = db.requestServerData(serverData.POLL)
    assert data5 in data and data6 in data and data7 in data
    data = db.requestServerData(serverData.WATCHLIST)
    assert db.getError() == 0
    data8 = '1,2,0,0,1'
    data9 = '2,3,1,1,1'
    assert data8 in data and data9 in data
    data = db.requestServerData(serverData.MESSAGES)
    assert db.getError() == 0
    assert data
    del db


def test_file_save():
    db = EE_DB('test.db', 1)
    fp1 = bytes('test1', 'ascii')
    fp2 = bytes('test2', 'ascii')
    assert not db.fetchEasterEggs()
    assert not db.getError()
    assert db.createEasterEgg('test1.jpg', 'test1', False, fp1)
    assert not db.getError()
    assert db.createEasterEgg('test2.jpg', 'test2', True, fp2)
    assert not db.createEasterEgg('test3.jpg', 'test1', False, fp2)
    eggs = db.fetchEasterEggs()
    print(eggs)
    print(len(eggs))
    assert len(eggs) == 2
    assert eggs[0]['phrase'] == 'test1'
    assert eggs[1]['phrase'] == 'test2'
    assert eggs[0]['filename'] == 'test1.jpg'
    assert eggs[1]['filename'] == 'test2.jpg'
    assert not eggs[0]['Limited']
    assert eggs[1]['Limited']
    assert eggs[0]['file'] == fp1
    assert eggs[1]['file'] == fp2
    assert db.removeEasterEgg('test2')
    assert not db.getError()
    assert not db.removeEasterEgg('test2')
    assert len(db.fetchEasterEggs()) == 1


def test_role_colors():
    db = EE_DB('test.db', 1)
    rgb1 = (64, 127, 254)
    rgb2 = (476, 51234, 60)
    rgb2_results = (476 & 0xFF, 51234 & 0xFF, 60 & 0xFF)
    db.setRoleColor(rgb1[0], rgb1[1], rgb1[2])
    assert db.getError() == 0
    assert db.getRoleColor() == rgb1
    assert db.getError() == 0
    db.setRoleColor(rgb2[0], rgb2[1], rgb2[2])
    assert db.getRoleColor() == rgb2_results
    assert db.getError() == 0
    db.clearRoleColor()
    assert db.getError() == 0
    db.getRoleColor()
    assert db.getError() != 0
    del db
