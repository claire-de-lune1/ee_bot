# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


try:
    from distutils.core import setup
    from distutils.core import Extension
except Exception:
    raise RuntimeError('\n\nUnable to load libraries\n')

cEE_Bot_DB = Extension('_EE_Bot_DB', sources=['EE_Bot_DB/src/EE_Bot_DB.cpp', 'EE_Bot_DB/src/pyEE_Bot_DB.cpp',
                                              'EE_Bot_DB/src/sqlite/sqlite3.c', 
                                              'EE_Bot_DB/src/utility.cpp'], extra_compile_args=['-std=c++11'])

setup(
    name='EE_Bot_DB',
    version='v3.6.0',
    description='',
    author='Thomas C Schmitz',
    packages=['EE_Bot_DB'],
    ext_modules=[cEE_Bot_DB],
    install_requires=['py-cord', 'numpy', 'emoji', 'rsa', 'kicost', 'pytest', 'scikit-rf'],
    python_requires='>=3.8',
    license='BSD 3')
