# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from enum import IntEnum
from datetime import datetime
try:
    import _EE_Bot_DB as DB
    #import asyncio
    #from copy import deepcopy
except (Exception):
    raise RuntimeError('\n\nUnable to load libraries\n')


__version__ = DB.__version__()


def base_repr(num: int, base: int):
    return DB.base_repr(num, base)


class searchWarnings(IntEnum):
    """Enumerated types for the warning table in C++"""
    NONE = 0
    GUILD = 1
    CHANNEL = 2
    USER_ID = 4
    USER_NAME = 8
    ADMIN_ID = 16
    ADMIN_NAME = 32
    RULE = 64
    REASON = 128
    DATE = 256
    ALL = 512


class cogEnum(IntEnum):
    """Enumerated types for the Cogs in C++"""
    NONE = 0x0
    Moderator = 0x1
    Games = 0x2
    Hardware = 0x4
    Yosys = 0x8
    RF = 0x10
    Schedule = 0x20
    ALL = 0x3F
    DEFAULT = 0x3B


class channelType(IntEnum):
    """Enumerated types for channel types"""
    log = 0
    role = 1
    level = 2
    shame = 3
    watch = 4


class quizDifficulty(IntEnum):
    """Enumerated type for quiz Difficulties"""
    veasy = 0
    easy = 1
    normal = 2
    hard = 3
    vhard = 4


class serverData(IntEnum):
    """Enumerated types for server data tables"""
    USERS = 0
    CHANNELS = 1
    POLL = 2
    WATCHLIST = 3
    MESSAGES = 4


#Error code 1: General error
#Error code 6: No filename
class EE_DB:
    # Initialize database object
    def __init__(self, filename: str, guild_id: int, createTables: bool = True):
        if filename is None:
            self.ok = 6
            return
        # Construct DB in C++
        if guild_id != -1:
            self.__dbCapsule, self.ok = DB.construct(filename, str(guild_id), int(createTables))
        self.guild_id = guild_id
        self.maxInstances = 1
        return

    # Sets up new guilds upon start up
    # list needs to be a list of integers
    def setupGuildFlags(self, guilds: list) -> None:
        if len(guilds) == 0:
            return
        DB.setupGuildFlags(self.__dbCapsule, guilds)
        return

    # Attempts to insert user into DB
    def insertUser(self, id=None, suppressError: bool = False) -> None:
        if id is None:  #Return if no id was given
            return
        # Insert user id into database
        self.ok = DB.insertUser(self.__dbCapsule, str(id), int(suppressError))
        return

    # Attempts to delete user from DB
    def deleteUser(self, id=None) -> None:
        if id is None:
            return
        #Delete user by user id from database
        self.ok = DB.deleteUser(self.__dbCapsule, str(id))
        return

    # Attempts to update user posts from DB
    # Returns (bool, int)
    def updateUserPosts(self, id=None) -> (bool, int):
        if id is None:
            return
        if not self.levelSystemStatus():
            return False, 0
        rankUp, rank, self.ok = DB.updateUserPosts(self.__dbCapsule, str(id))
        rankUp = bool(rankUp)
        return rankUp, rank

    #Function to update the filter
    def updateUserFilter(self, filter=True, id=None) -> None:
        if id is None:
            return
        #Convert parameters to proper data types and execute C++ function
        self.ok = DB.updateUserFilter(self.__dbCapsule, str(id), int(filter))
        return

    #Function to check the User's rank and progress
    #Returns (int, float)
    def checkUserRank(self, id=None) -> (int, float):
        if id is None:
            return
        #Passes in user ID and retrieves the rank as an integer and
        #the progress as a float
        rank, progress, self.ok = DB.retreiveUserRank(self.__dbCapsule,
                                                      str(id))
        return rank, progress

    #Function to check if the filter is active
    #Returns (bool)
    def checkUserFilter(self, id=None) -> bool:
        if id is None:
            return
        #Retrieve filter from database
        Filter, self.ok = DB.checkUserFilter(self.__dbCapsule, str(id))
        return bool(Filter)

    #Function that updates the notif flag in database
    def updateNotifications(self, id=None, notif=1) -> None:
        if id is None:
            return
        self.ok = DB.updateNotifications(self.__dbCapsule, str(id), notif)
        return

    #Function that updates the rank given an ID
    #Returns (int)
    def adjustRank(self, id=None) -> int:
        if id is None:
            return
        rank, self.ok = DB.adjustLevel(self.__dbCapsule, str(id))
        return rank

    #Function that checks if channel is in database
    #Updates error flag if it is in the database
    def createChannel(self, channel=str(), id=None, admin=False) -> None:
        if id is None:
            return
        self.ok = DB.createChannel(self.__dbCapsule, str(id), channel,
                                   int(admin))
        return

    #Function that deletes channel from database
    #Updates error flag if unable to update
    def deleteChannel(self, channel: str) -> None:
        if channel == "":
            return
        self.ok = DB.deleteChannel(self.__dbCapsule, channel)
        return

    #Function that stores important polls into database
    def createPoll(self, msgID=None, matched=[[]]) -> None:
        if msgID is None:
            return
        if self.ok == 0:
            for x in matched:
                #Stores the matched roleID and emoji into database as strings
                self.ok = DB.createNewPoll(self.__dbCapsule, str(msgID),
                                           str(x[0]), str(x[1]))
        return

    #Function that retrieves polls from the database
    #Returns (int, [[]])
    def retrievePoll(self, msgID=None) -> (int, list):
        if msgID is None:
            return
        #Load C++ list with Poll and retrieve length
        Pairs, self.ok = DB.retrievePoll(self.__dbCapsule, str(msgID))
        MsgID = 0

        #Check if database is ok and the length is not 0
        if self.ok == 0 and (len(Pairs) != 0):
            #Get the message ID
            MsgID = DB.getMsgID(self.__dbCapsule)
        return int(MsgID), Pairs

    #Function that deletes and important poll
    def deletePoll(self, msgID=None) -> None:
        if msgID is None:
            return
        self.ok = DB.deletePoll(self.__dbCapsule, str(msgID))
        return

    #Function to check if messageID matches to a saved poll
    #Returns (bool)
    def checkPoll_id(self, msgID: int) -> bool:
        if msgID == 0:
            return False
        isActivePoll, self.ok = DB.checkPoll_id(self.__dbCapsule, str(msgID))
        return bool(isActivePoll)

    #Function that adds new role/emoji pair to database
    def addNewPair(self, msgID: int, roleID: int, emoji: str) -> None:
        if not self.checkPoll_id(msgID):
            return
        #Add entry to table
        self.ok = DB.createNewPoll(self.__dbCapsule, str(msgID), str(roleID),
                                   str(emoji))
        return

    #Function that deletes role/emoji pair from database
    def deletePair(self, msgID: int, roleID: int, emoji: str) -> None:
        if not self.checkPoll_id(msgID):  #Check msgID
            return
        #delete entry from table
        self.ok = DB.deletePoll_Entry(self.__dbCapsule, str(msgID),
                                      str(roleID), str(emoji))
        return

    #Function that adds a warning to a user in the database
    #Returns (int)
    def addWarning(self, userID: int) -> int:
        if userID == 0:
            return 0
        #Add user to watchlist
        self.insertIntoWatchlist(userID)
        if self.ok != 0:
            return -1
        #Add a warning to the database
        self.ok = DB.addWarning(self.__dbCapsule, str(userID))
        warnings = 0
        if self.ok == 0:
            #retrieve number of warnings
            warnings, self.ok = DB.retrieveWarnings(self.__dbCapsule,
                                                    str(userID))
        return warnings

    #Function that retrieves number of warnings from database
    #Returns (int)
    def retrieveWarnings(self, id: int) -> int:
        if id == 0:
            return 0
        #Retrieve warnings from database
        warnings, self.ok = DB.retrieveWarnings(self.__dbCapsule, str(id))
        return warnings

    #Function that deletes a warning from database
    #Returns (int)
    def removeWarning(self, id: int) -> int:
        if id == 0:
            return 0
        #Remove warning from database
        self.ok = DB.removeWarning(self.__dbCapsule, str(id))
        warnings = 0
        if self.ok == 0:
            warnings, ok = DB.retrieveWarnings(self.__dbCapsule, str(id))
        return warnings

    #Function that clears a user's warnings from database
    def clearWarnings(self, id: int) -> None:
        if id == 0:
            return
        #Clear warning flags
        self.ok = DB.clearWarnings(self.__dbCapsule, str(id))
        return

    #Function that saves settings into database
    def configureGuild(self, log: int, roleOptIn: int, level: int,
                       shame: int, watch: int) -> None:
        if (log == 0 or roleOptIn == 0 or level == 0 or
            shame == 0 or watch == 0):
            return
        #Add configurations to database
        self.ok = DB.configureGuild(self.__dbCapsule, str(log),
                                    str(roleOptIn), str(level), str(shame),
                                    str(watch))
        return

    #Function that overwrites settings in database
    def reconfigureGuild(self, log: int, roleOptIn: int, level: int,
                         shame: int, watch: int) -> None:
        if (log == 0 or roleOptIn == 0 or level == 0 or
            shame == 0 or watch == 0):
            return
        #Overwrite configurations in database
        self.ok = DB.reconfigureGuild(self.__dbCapsule, str(log),
                                      str(roleOptIn), str(level),
                                      str(shame), str(watch))
        return

    #Function that retrieves settings from database
    #  0: Log channel
    #  1: Role opt in channel
    #  2: Level notification channel
    #  3: shame channel
    #  4: watch channel
    def getLoggingID(self, logIndex: channelType) -> int:
        channelID, self.ok = DB.getLogsID(self.__dbCapsule, logIndex.real)
        try:
            channelID = int(channelID)
        except Exception:
            channelID = 0
        return channelID

    #set Poli message in database
    def setPoliMsg(self, msgID: int) -> None:
        if msgID == 0:
            return
        self.ok = DB.setPoliMsg(self.__dbCapsule, str(msgID))
        return

    #get poli message id from database
    def getPoliMsg(self) -> int:
        msgID, self.ok = DB.getPoliMsg(self.__dbCapsule)
        try:
            return int(msgID)
        except Exception:
            return 0

    #clear poli message from database
    def clearPoliMsg(self) -> None:
        self.ok = DB.clearPoliMsg(self.__dbCapsule)
        return

    #Set poli rules
    def setPoliRules(self, rules: list) -> bool:
        for i in range(0, len(rules)):
            if type(rules[i]) != int:
                return False
        Rules = ''
        for i in range(0, len(rules)):
            Rules += str(rules[i])
            if (i + 1) == len(rules):
                break
            Rules += ' '
        self.ok = DB.setPoliRules(self.__dbCapsule, Rules)
        return True

    #Get poli rules
    def getPoliRules(self) -> list:
        Rules, self.ok = DB.getPoliRules(self.__dbCapsule)
        Rules = Rules.split(' ')
        if len(Rules) == 1 and not Rules[0].isdigit():
            return []
        try:
            for i in range(0, len(Rules)):
                Rules[i] = int(Rules[i])
        except (Exception):
            Rules = []
        return Rules

    #Clear poli rules
    def clearPoliRules(self) -> None:
        self.ok = DB.clearPoliRules(self.__dbCapsule)
        return

    #Set Poli rules channel
    def setPoliRulesChannel(self, channelID: int) -> None:
        if channelID == 0:
            return
        self.ok = DB.setPoliRulesChannel(self.__dbCapsule, str(channelID))
        return

    #Get poli rules channel
    def getPoliRulesChannel(self) -> int:
        channel, self.ok = DB.getPoliRulesChannel(self.__dbCapsule)
        try:
            channel = int(channel)
        except (Exception):
            channel = 0
        finally:
            return channel

    #Clear poli rules channel
    def clearPoliRulesChannel(self) -> None:
        self.ok = DB.clearPoliRulesChannel(self.__dbCapsule)
        return

    #Set channel prefix for guild
    def setChannelPrefix(self, prefix: str) -> None:
        if prefix == '':
            return
        self.ok = DB.setChannelPrefix(self.__dbCapsule, prefix)
        return

    #Get channel prefix from database
    def getChannelPrefix(self) -> str:
        prefix, self.ok = DB.getChannelPrefix(self.__dbCapsule)
        if prefix == '':
            prefix = 'ee'
        return prefix

    #Clear channel prefix from database
    def clearChannelPrefix(self) -> None:
        self.ok = DB.clearChannelPrefix(self.__dbCapsule)
        return

    #Function that inserts new user into watchlist
    def insertIntoWatchlist(self, userid: int) -> None:
        if userid == 0:
            return
        #Check if user is already in watchlist
        if (not self.inWatchlist(userid)) and self.ok == 0:
            self.ok = DB.insertIntoWatchlist(self.__dbCapsule, str(userid))
        return

    #Function that checks if user is in watchlist
    def inWatchlist(self, userid: int) -> bool:
        if userid == 0:
            return False
        watched, self.ok = DB.inWatchlist(self.__dbCapsule, str(userid))
        return bool(watched)

    #Function that retrieves watchlist flags from database
    def retrieveWatchlistFlags(self, userid: int) -> (bool, bool, bool):
        if userid == 0:
            return False, False, False, False
        (poliBan, monitored, override, exists, 
        self.ok) = DB.watchlistStatus(self.__dbCapsule, str(userid))
        return bool(exists), bool(poliBan), bool(monitored), bool(override)

    #Function that updates the monitor flag
    def updateMonitor(self, userid: int, monitor: bool) -> None:
        if userid == 0:
            return
        self.ok = DB.updateMonitor(self.__dbCapsule, str(userid), int(monitor))
        return

    #Function that updates the poliban flag
    def updatePoliBan(self, userid: int, ban: bool) -> None:
        if userid == 0:
            return
        self.ok = DB.updatePoliBan(self.__dbCapsule, str(userid), int(ban))
        return

    #Function that updates the override flag
    def updateOverride(self, userid: int, override: bool) -> None:
        if userid == 0:
            return
        self.ok = DB.updateOverride(self.__dbCapsule, str(userid),
                                    int(override))
        return

    #Function that adds a message to the database
    def addMessage(self, msgID: int, channelID: int) -> None:
        self.ok = DB.insertMsg(self.__dbCapsule, str(channelID), str(msgID))
        return

    #Function that marks a message as deleted in database
    def markMsgDeleted(self, msgID: int, channelID: int) -> None:
        self.ok = DB.markMsgDeleted(self.__dbCapsule, str(channelID), 
                                    str(msgID))
        return

    # Function that gets message IDs from the database
    # Returns (list)
    def getMessages(self, channelID: int) -> list:
        msgIDs, self.ok = DB.getMessages(self.__dbCapsule, str(channelID))
        return msgIDs

    #Function that bulk deletes messages from the database
    def bulkMsgDelete(self, channelID: int) -> None:
        self.ok = DB.msgBulkDelete(self.__dbCapsule, str(channelID))
        return

    #Function that adds a guild to the guild flags table
    def addGuild(self, guildID: int) -> None:
        if guildID == 0:
            return
        self.ok = DB.addGuild(self.__dbCapsule, guildID)
        return

    #Function that resets flags of all guilds
    #Should be called whenever bot restarts
    def resetGuildFlags(self) -> None:
        self.ok = DB.resetGuildFlags(self.__dbCapsule)
        return

    #Function that sets guild flags
    def setGuildFlags(self, archive: bool, purgeRoles: bool) -> None:
        # Encode flags
        flags = int(archive)
        flags = flags | (int(purgeRoles) << 1)
        #call cpp function
        self.ok = DB.setFlags(self.__dbCapsule, flags)
        return

    #Function that gets guild flags
    def getGuildFlags(self) -> (bool, bool):
        #call cpp function
        flags, self.ok = DB.getFlags(self.__dbCapsule)
        #decode flags
        archive = bool(flags & 1)
        purgeRoles = bool((flags & 2) >> 1)
        return archive, purgeRoles

    #Function that sets archive flag
    def setArchiveFlag(self, archive: bool) -> None:
        self.ok = DB.setArchiveFlag(self.__dbCapsule, int(archive))
        return

    #Function that gets archive flag
    def getArchiveFlag(self) -> bool:
        flags, self.ok = DB.getArchiveFlag(self.__dbCapsule)
        return flags != 0

    #Function that set purge role flag
    def setPurgeRoleFlag(self, purgeRoles: bool) -> None:
        self.ok = DB.setPurgeRolesFlag(self.__dbCapsule, int(purgeRoles))
        return

    #Function that gets purge roles flag
    def getPurgeRolesFlag(self) -> bool:
        flags, self.ok = DB.getPurgeRolesFlag(self.__dbCapsule)
        return flags != 0

    #Function to check if bot is running
    def getRunFlag(self) -> bool:
        flag, self.ok = DB.getStartedFlag(self.__dbCapsule)
        return bool(flag)

    #Function to set if the bot is running
    def setRunFlag(self, running: bool) -> None:
        self.ok = DB.setStartedFlag(self.__dbCapsule, int(running))
        return

    #Function to get number of bot instances
    def getInstances(self) -> int:
        instances, self.ok = DB.getInstances(self.__dbCapsule)
        return instances

    #Function that adds a bot instance
    # returns success as a bool
    def addInstance(self) -> bool:
        instances = self.getInstances()
        if instances < self.maxInstances:
            instances += 1
            self.ok = DB.setInstances(self.__dbCapsule, instances)
            return True
        return False

    #Function that removes an instance from the database
    def removeInstance(self) -> bool:
        instances = self.getInstances()
        if instances > 0:
            instances -= 1
            self.ok = DB.setInstances(self.__dbCapsule, instances)
            return True
        return False

    #Function that gets the clean exit flag
    def getCleanExitFlag(self) -> bool:
        flag, self.ok = DB.getCleanExit(self.__dbCapsule)
        return bool(flag)

    #Function that sets the clean exit flag
    def setCleanExitFlag(self, cleanExit: bool) -> None:
        self.ok = DB.setCleanExit(self.__dbCapsule, int(cleanExit))
        return

    # Function that gets the leader board
    def getLeaderBoard(self, requesterID: int) -> (list, int):
        Leaders, requesterRank, self.ok = DB.getRankLeaders(self.__dbCapsule, str(requesterID))
        return Leaders, requesterRank

    #Function that stores a warning in the database
    def logWarning(self, guildID: int, channelName: str, userID: int, userName: str, adminID: int, adminName: str, rule: int, reason: str) -> None:
        if guildID == 0 or userID == 0 or adminID == 0 or rule == 0:
            self.ok = 1
            return
        # Get current date
        date = datetime.now()
        #convert date into string
        date = "%d/%d/%d" % (date.month, date.day, date.year)
        self.ok = DB.logWarnings(self.__dbCapsule, guildID, channelName, userID, userName, adminID, adminName, rule, reason, date)
        return

    #Function that gets all the warnings from the database given a certain parameter
    def getWarningData(self, searchParam: searchWarnings, guildID: int, owner: bool, args: list) -> list:
        if not owner and not bool(searchParam & searchWarnings.GUILD):
            searchParam = searchParam & (~searchWarnings.ALL)
            searchParam = searchParam | searchWarnings.GUILD
            args.insert(0, str(guildID))
        data, self.ok = DB.getWarningData(self.__dbCapsule, searchParam.real, tuple(args))
        return data

    #Function that turns on or off the level system
    def toggleLevelSystem(self, levelSys: bool) -> None:
        self.ok = DB.toggleLevelSystem(self.__dbCapsule, int(levelSys))
        return

    #Function that checks the level system status
    def levelSystemStatus(self) -> bool:
        status, self.ok = DB.levelSystemStatus(self.__dbCapsule)
        return bool(status)

    #Function that enables cogs for a guild
    def enableCog(self, cog: cogEnum) -> None:
        self.ok = DB.enableCog(self.__dbCapsule, cog.real)
        return

    #Function that disables cogs for a guild
    def disableCog(self, cog: cogEnum) -> None:
        self.ok = DB.disableCog(self.__dbCapsule, cog.real)
        return

    #Function that checks if cog is enabled
    def checkCog(self, cog: cogEnum) -> bool:
        if self.guild_id != -1:
            if self.checkServerConfiguredFlag():
                cogFlags, self.ok = DB.checkCogs(self.__dbCapsule)
            else:
                cogFlags = cogEnum.DEFAULT.real
        else:
            cogFlags = cogEnum.ALL.real
        return (cogFlags & cog.real) != 0

    #Function that set the configured flag
    def setServerConfigured(self, flag: bool) -> None:
        self.ok = DB.setServerConfiguredFlag(self.__dbCapsule, int(flag))
        return

    #Function that get the configured flag
    def checkServerConfiguredFlag(self) -> bool:
        flag, self.ok = DB.getServerConfiguredFlag(self.__dbCapsule)
        return bool(flag)

    #Function that demotes members
    def demoteMember(self, memberID: int, numLevels: int) -> None:
        self.ok = DB.demoteMember(self.__dbCapsule, str(memberID), numLevels)
        return

    #Function that sets the ban message
    def setBanMsg(self, msg: str) -> None:
        self.ok = DB.setBanMsg(self.__dbCapsule, msg)
        return

    #Function that clear the ban message
    def clearBanMsg(self) -> None:
        self.ok = DB.clearBanMsg(self.__dbCapsule)
        return

    #Function that gets the ban message
    def getBanMsg(self) -> str:
        msg, self.ok = DB.getBanMsg(self.__dbCapsule)
        return msg

    #Function that inserts user into quiz table
    def insertQuizUser(self, userID: int) -> None:
        self.ok = DB.insertQuizUser(self.__dbCapsule, str(userID))
        return

    #Function that updates user data in quiz table
    def updateQuizData(self, userID: int, difficulty: quizDifficulty, correct: bool) -> None:
        qData = difficulty.real
        if correct:
            qData = qData | 0x8
        self.ok = DB.updateQuizData(self.__dbCapsule, str(userID), qData)
        return

    #Function that fetches the given user's entry
    def fetchQuizEntry(self, userID: int) -> list:
        qData, self.ok = DB.fetchQuizEntry(self.__dbCapsule, str(userID))
        return qData

    #Funtion that fetches all the quiz data
    def fetchQuizEntries(self) -> list:
        qData, self.ok = DB.fetchQuizEntries(self.__dbCapsule)
        return qData

    #Function that requests data from the database
    def requestServerData(self, table: serverData) -> list:
        data, self.ok = DB.requestServerData(self.__dbCapsule, table.real)
        return data

    #Function that creates and saves an easter egg in the database
    def createEasterEgg(self, filename: str, phrase: str, Filter: bool, fp: bytes) -> bool:
        if list(filter(lambda Phrase: Phrase['phrase'] == phrase, self.fetchEasterEggs())):
            return False
        self.ok = DB.createEasterEgg(self.__dbCapsule, phrase, filename, int(Filter), fp)
        return True

    #Function that removes an easter egg from the database
    def removeEasterEgg(self, phrase: str) -> bool:
        if not list(filter(lambda Phrase: Phrase['phrase'] == phrase, self.fetchEasterEggs())):
            return False
        self.ok = DB.removeEasterEgg(self.__dbCapsule, phrase)
        return True

    def fetchEasterEggs(self) -> list:
        EasterEggs, self.ok = DB.fetchEasterEggs(self.__dbCapsule)
        return EasterEggs

    #Function that sets the role color
    def setRoleColor(self, R: int, G: int, B: int) -> None:
        self.ok = DB.setRoleColor(self.__dbCapsule, R, G, B)
        return

    #Function that requests the role color from the database
    def getRoleColor(self) -> (int, int, int):
        R, G, B, self.ok = DB.getRoleColor(self.__dbCapsule)
        return (int(R), int(G), int(B))

    def clearRoleColor(self) -> None:
        self.ok = DB.clearRoleColor(self.__dbCapsule)
        return

    #Function that determines if there was an error in the database, 
    #returns 0 if no error occured
    #Returns (int)
    def getError(self) -> int:
        return self.ok

    #Function that deallocates memory of database object
    def __delete__(self):
        DB.destroyDB(self.__dbCapsule)
