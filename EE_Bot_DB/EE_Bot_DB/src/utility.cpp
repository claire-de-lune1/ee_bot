/******************************************************************************
 * BSD 3 - Clause License
 *
 * Copyright(c) 2021, Tom Schmitz
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include "utility.h"

void reverse(char *str, uint64_t length)
{
    uint64_t start = 0;
    uint64_t end = length - 1;
    char temp;

    while (start < end)
    {
        temp = str[start];
        str[start++] = str[end];
        str[end--] = temp;
    }
}

char *itoa(int64_t num, char *buffer, int32_t radix)
{
    uint64_t i = 0;
    uint8_t negative = 0;
    s_us_union Num;
    Num.signed_num = num;

    if (radix < 2 || radix > 36)
    {
        if (radix == 0)
        {
            buffer[i++] = '0';
            buffer[i] = 0;
            return buffer;
        }
        return NULL;
    }

    if (num == 0)
    {
        buffer[i++] = '0';
        buffer[i] = 0;
        return buffer;
    }

    if (num < 0 && radix == 10)
    {
        Num.signed_num *= -1;
        negative = 1;
    }

    while (Num.unsigned_num != 0)
    {
        uint8_t rem = Num.unsigned_num % radix;
        buffer[i++] = (rem > 9) ? 'a' + (rem - 10) : '0' + rem;
        Num.unsigned_num /= radix;
    }

    if (negative)
        buffer[i++] = '-';

    buffer[i] = 0;

    reverse(buffer, i);

    return buffer;
}