/******************************************************************************
 * BSD 3 - Clause License
 *
 * Copyright(c) 2021, Tom Schmitz
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef EE_BOT_DB_H
#define EE_BOT_DB_H

#if _WIN32
#pragma warning(disable : 4018)
#pragma warning(disable : 4091)
#pragma warning(disable : 4267)
#pragma warning(disable : 4996)
#endif

// Include database library
extern "C"
{
#include "sqlite/sqlite3.h"
};

// Include necessary std-c++ libraries
#include <cmath>
#include <cstdarg>
#include <cstdint>
#include <cstring>
#include <iostream>
#include <list>
#include <string>
#include <vector>

using std::cerr;
using std::endl;
using std::floor;
using std::list;
using std::log;
using std::string;
using std::to_string;
using std::vector;

// data structure that contains user data
typedef struct userData
{
    string id;
    unsigned int rank;
    unsigned int posts;
    bool filter;
    bool notif;
    unsigned int warnings;
    unsigned int xp;
} userData;

// data structure that contains watchlist data
typedef struct watchlist_data
{
    string id;
    int monitor;
    int poli_ban;
    int Override;
} WatchlistData;

typedef struct message_data
{
    int id;
    string channel_id;
    string message_id;
    int deleted;
} msgData;

typedef struct guildFlags
{
    int archive;
    int purge_roles;
    int configured;
};

typedef struct warningMsgData
{
    long long guildID;
    string channel;
    long long userID;
    string userName;
    long long adminID;
    string adminName;
    unsigned int ruleViolated;
    string reason;
    string date;
};

typedef struct quizData_t
{
    long long userID;
    unsigned int veryEasyCorrect;
    unsigned int veryEasyQuestions;
    unsigned int easyCorrect;
    unsigned int easyQuestions;
    unsigned int normalCorrect;
    unsigned int normalQuestions;
    unsigned int hardCorrect;
    unsigned int hardQuestions;
    unsigned int veryHardCorrect;
    unsigned int veryHardQuestions;
} quizData_t;

typedef struct
{
    string phrase;
    string filename;
    size_t filesize;
    void *fileContents;
    bool filter;
    vector<unsigned char> safeFileContents;
} Blob_data_t;

enum
{
    GUILD = 1,
    CHANNEL = 2,
    USER_ID = 4,
    USER_NAME = 8,
    ADMIN_ID = 16,
    ADMIN_NAME = 32,
    RULE = 64,
    REASON = 128,
    DATE = 256,
    ALL = 512 //,
              // LAST_WARNING_DATA, //Bound for warning data enums
              ////Put more types here later if applicable
              // END //Always last item
};

enum Tables
{
    USERS,
    CHANNELS,
    POLL,
    WATCHLIST,
    MESSAGES
};

// Database handler class, ported into python
class EE_Bot_DB
{
  public:
    // Constructor and destructor
    EE_Bot_DB(const char *database, int &s_ok, const char *guild,
              int createTables);
    ~EE_Bot_DB();

    // Initialization function
    void initTables(int createTables);
    void setupFlags(list<long long> guildIDs);

    // User table functions
    void insertUser(string id, int suppressError, int &ok);
    void deleteUser(string id, int &ok);

    bool updateUserPosts(string id, int &rank, int &ok);
    void updateUserFilter(string id, bool filter, int &ok);

    bool checkUserFilter(string id, int &ok);

    int checkUserRank(string id, float &progress, int &ok);

    void Level_Notifications(string id, bool notify, int &ok);

    void adjust_level(string id, int &rank, int &ok);

    list<userData> getAllUsers(int *ok);

    void demoteUser(string id, unsigned int numLevels, int *ok);

    // Channel functions
    void createChannel(string id, string channel, bool admin, int &error);
    void deleteChannel(string channel, int &ok);

    // Poll functions
    int createNewPoll(string msgID, string roleID, string emoji);
    int retrievePoll(string msgID, int *ok);
    const char *getMsgID(void);
    const char *getRoll_id(void);
    const char *getEmoji_id(void);
    void clearEntry(void);
    void deletePoll(string msgID, int *ok);

    // Edit Poll functions
    bool checkPoll_id(string msgID, int *ok);
    int deletePoll_Entry(string msgID, string role, string emoji);

    // warning functions
    void addWarning(string id, int *ok);
    int retrieveWarnings(string id, int *ok);
    void removeWarning(string id, int *ok);
    void clearWarnings(string id, int *ok);
    int logWarning(warningMsgData data);
    list<warningMsgData> getWarnings(int *ok, int searchParam,
                                     vector<string> values);

    // guild setting functions
    int configure_guild(string logID, string roleOptIn, string levelID,
                        string shame, string watch);
    int reconfigure_guild(string logID, string roleOptIn, string levelID,
                          string shame, string watch);
    const char *get_channel_id(int channel, int *ok);
    int set_poli_msg(string msgID);
    const char *get_poli_msg(int *ok);
    int clear_poli_msg(void);
    int setPoliRules(string rules);
    const char *getPoliRules(int *ok);
    int clearPoliRules(void);
    int setPoliChannel(string channelID);
    const char *getPoliChannel(int *ok);
    int clearPoliChannel(void);
    int setChannelPrefix(string prefix);
    const char *getChannelPrefix(int *ok);
    int clearChannelPrefix(void);
    int toggleLevelSystem(bool levels_on);
    bool levelSystemStatus(int *ok);
    int enableCog(uint32_t Cog);
    int disableCog(uint32_t Cog);
    uint32_t checkCogs(int *ok);
    int setBanMsg(const char *msg);
    int clearBanMsg(void);
    const char *getBanMsg(int *ok);
    int setRoleColor(uint8_t R, uint8_t G, uint8_t B);
    int getRoleColor(uint8_t *R, uint8_t *G, uint8_t *B);
    int clearRoleColor();

    // watchlist functions
    int watchlist_insert(string id);
    bool in_watchlist(string id, int *ok);
    int watchlist_status(string id, int *poli_ban, int *monitored,
                         int *Override, int *exists);
    int update_watchlist_monitor(string id, int warnings, int ok);
    int update_monitor(string id, int monitor);
    int update_poli_ban(string id, int banned);
    int update_override(string id, int Override);

    // Class Message database functions
    int add_message(string channel_id, string msg_id);
    int user_delete_message(string channel_id, string msg_id);
    int get_messages(string channel_id, int *ok);
    int bulk_delete(string channel_id);
    long long getMessage(void);

    // guild flags
    int addGuild(long long guildID);
    int resetGuildFlags();
    int setFlags(int flags);
    int getFlags(int *ok);
    int setArchiveFlag(int flag);
    int getArchiveFlag(int *ok);
    int setPurgeRoleFlag(int flag);
    int getPurgeRoleFlag(int *ok);
    int setConfiguredFlag(int flag);
    int getConfiguredFlag(int *ok);

    // bot status flags
    int getStartedFlag(int *ok);
    int setStartedFlag(int running);
    int getInstances(int *ok);
    int setInstances(int instances);
    int getCleanExit(int *ok);
    int setCleanExit(int clean);

    // quiz data
    int insertQuizUser(string id);
    int updateQuizData(string id, unsigned int quizData);
    quizData_t fetchIndividualQuizData(string id, int *ok);
    list<quizData_t> fetchQuizData(int *ok);

    // server data request functions
    list<string> requestData(Tables type, int *ok);
    const char *getDataHeader();

    // Easter Eggs container
    int createEasterEgg(Blob_data_t file_meta);
    int removeEasterEgg(string egg);
    list<Blob_data_t> fetchEasterEggs(int *ok);

  private:
    // raw database object
    sqlite3 *db;

    // Objects that contains data
    struct userData data;
    list<userData> allUsers;
    list<string> channels;
    string msgID;
    list<string> roll_IDs;
    list<string> emoji_IDs;
    list<msgData> msgList;
    list<string> msgIDs;
    list<warningMsgData> warningData;
    string guild;
    string channel_id;
    bool exists;
    WatchlistData wdata;
    guildFlags Flags;
    quizData_t qData;
    list<quizData_t> aQdata;
    list<string> serverData;
    string header;
    bool headerSet;
    list<Blob_data_t> blobData;

    // callback functions for sqlite lib
    static int defaultCallback(void *data, int argc, char **argv,
                               char **azColName);

    static int callbackUser(void *data, int argc, char **argv,
                            char **azColName);
    int callbackUser(int argc, char **argv, char **azColName);

    static int callbackUser2(void *data, int argc, char **argv,
                             char **azColName);
    int callbackUser2(int argc, char **argv, char **azColName);

    static int callbackChannels(void *data, int argc, char **argv,
                                char **azColName);
    int callbackChannels(int argc, char **argv, char **azColName);

    static int callbackPoll(void *data, int argc, char **argv,
                            char **azColName);
    int callbackPoll(int argc, char **argv, char **azColName);

    static int callbackCheckID(void *data, int argc, char **argv,
                               char **azColName);

    static int callbackSettings(void *data, int argc, char **argv,
                                char **azColName);
    int callbackSettings(int argc, char **argv, char **azColName);

    static int callbackWatchList(void *data, int argc, char **argv,
                                 char **azColName);
    int callbackWatchList(int argc, char **argv, char **azColName);

    static int callbackPoliMsg(void *data, int argc, char **argv,
                               char **azColName);
    int callbackPoliMsg(int argc, char **argv, char **azColName);

    static int callbackMsgData(void *data, int argc, char **argv,
                               char **azColName);
    int callbackMsgData(int argc, char **argv, char **azColName);

    static int callbackGuildFlags(void *data, int argc, char **argv,
                                  char **azColName);
    int callbackGuildFlags(int argc, char **argv, char **azColName);

    static int callbackWarnings(void *data, int argc, char **argv,
                                char **azColName);
    int callbackWarnings(int argc, char **argv, char **azColName);

    static int callbackQuizData(void *data, int argc, char **argv,
                                char **azColName);
    int callbackQuizData(int argc, char **argv, char **azColName);

    static int callbackQuizData2(void *data, int argc, char **argv,
                                 char **azColName);
    int callbackQuizData2(int argc, char **argv, char **azColName);

    static int callbackServerData(void *data, int argc, char **argv,
                                  char **azColName);
    int callbackServerData(int argc, char **argv, char **azColName);

    static int callbackEasterEggs(void *data, int argc, char **argv,
                                  char **azColName);
    int callbackEasterEggs(int argc, char **argv, char **azColName);

    // Utility functions
    void setExistFlag(void);
    static bool sortUserData(const userData &first, const userData &second);
    int constructSQLstr(const char *sql, const char *types, ...);
    int safeSelect(const char *sql, void *data,
                   int (*callback)(void *, int, char **, char **),
                   const char *types, ...);
    int safeSelect1(const char *sql, void *data,
                    int (*callback)(void *, int, char **, char **),
                    const char *types, vector<string> values);
};

#endif
