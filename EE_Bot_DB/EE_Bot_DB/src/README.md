The SQLITE3 source code is not included, but is automatically downloaded if setup script or batch file is ran.

The source code for SQLITE3 can be found here: https://sqlite.org/download.html

Note: Both the header file and the source file are needed.
