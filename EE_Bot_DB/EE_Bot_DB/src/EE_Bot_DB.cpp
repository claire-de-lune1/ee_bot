/******************************************************************************
 * BSD 3 - Clause License
 *
 * Copyright(c) 2021, Tom Schmitz
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include "EE_Bot_DB.h"

// Construct database handler object
EE_Bot_DB::EE_Bot_DB(const char *database, int &ok, const char *guild,
                     int createTables)
{
    // Open database
    int s_ok = sqlite3_open(database, &db);

    this->guild = guild;

    headerSet = false;

    // Check if database opened without error
    if (s_ok != SQLITE_OK)
    {
        // Send error message
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // Set error code
        ok = 1;
    }
    else
        initTables(createTables); // initialize tables if opened correctly
    this->data.id = "";
    data.rank = 0;
    data.posts = 0;
    data.filter = true;
    data.notif = true;
    data.warnings = 0;
    channel_id = "";

    qData.userID = 0;
    qData.veryEasyCorrect = 0;
    qData.veryEasyQuestions = 0;
    qData.easyCorrect = 0;
    qData.easyQuestions = 0;
    qData.normalCorrect = 0;
    qData.normalQuestions = 0;
    qData.hardCorrect = 0;
    qData.hardQuestions = 0;
    qData.veryHardCorrect = 0;
    qData.veryHardQuestions = 0;
}

// Destroy database handler object
EE_Bot_DB::~EE_Bot_DB()
{
    // Close database
    int ok = sqlite3_close(db);

    // Send error message if database was not closed properly
    if (ok != SQLITE_OK)
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
}

// Function that initializes sqlite tables, if they exist
void EE_Bot_DB::initTables(int createTables)
{
    string sql;
    char *err;
    int ok;

    if (createTables)
    {
        // sql string for creating the USERS table
        sql = "CREATE TABLE IF NOT EXISTS USERS_";
        sql += guild;
        sql += "("
               "ID INTEGER PRIMARY KEY AUTOINCREMENT,"
               "USERID TEXT UNIQUE,"
               "RANK INT NOT NULL,"
               "FILTER INT NOT NULL,"
               "TOTALPOSTS INT NOT NULL,"
               "NOTIF INT NOT NULL,"
               "WARNINGS INT NOT NULL,"
               "XP INT NOT NULL);";

        char *err;

        // Execute sql statement
        int ok = sqlite3_exec(db, sql.c_str(), defaultCallback, NULL, &err);

        // Check if database is ok
        if (ok != SQLITE_OK)
        {
            // Send error message
            cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
            // Free error
            sqlite3_free(err);
        }

        // Clear sql string
        sql.clear();
        // sql string for creating channels table, if it exists
        sql = "CREATE TABLE IF NOT EXISTS CHANNELS_";
        sql += guild;
        sql += "("
               "ID INTEGER PRIMARY KEY AUTOINCREMENT,"
               "NAME TEXT NOT NULL);";

        // Execute sql statement
        ok = sqlite3_exec(db, sql.c_str(), defaultCallback, NULL, &err);

        // Check if database is ok
        if (ok != SQLITE_OK)
        {
            // Send error message
            cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
            // Free error
            sqlite3_free(err);
        }

        // Clear sql string
        sql.clear();
        // sql string for creating the poll table, if it exists
        sql = "CREATE TABLE IF NOT EXISTS POLL_";
        sql += guild;
        sql += "("
               "ID INTEGER PRIMARY KEY AUTOINCREMENT,"
               "MSG_ID TEXT NOT NULL,"
               "ROLE TEXT NOT NULL,"
               "EMOJI TEXT NOT NULL);";

        // Execute sql statement
        ok = sqlite3_exec(db, sql.c_str(), defaultCallback, NULL, &err);

        // Check if database is ok
        if (ok != SQLITE_OK)
        {
            // Send error message
            cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
            // Free error
            sqlite3_free(err);
        }

        // clear sql string
        sql.clear();
        // sql string for creating guild settings channel if it exists
        sql = "CREATE TABLE IF NOT EXISTS GUILD_SETTINGS("
              "ID INTEGER PRIMARY KEY AUTOINCREMENT,"
              "GUILD TEXT UNIQUE,"
              "LOG TEXT NOT NULL,"
              "ROLE TEXT NOT NULL,"
              "LEVEL TEXT NOT NULL,"
              "SHAME TEXT NOT NULL,"
              "WATCH TEXT NOT NULL,"
              "POLI_MSG TEXT NULL,"
              "POLI_RULE_VIO TEXT NULL,"
              "POLI_RULE_CH TEXT NULL,"
              "CH_PREFIX TEXT NULL,"
              "LEVEL_SYS INT NOT NULL,"
              "COGS INT NOT NULL,"
              "BAN_MSG TEXT NULL,"
              "ROLE_COLOR INT NULL);";

        // execute sql string
        ok = sqlite3_exec(db, sql.c_str(), defaultCallback, NULL, &err);

        // Check if database is ok
        if (ok != SQLITE_OK)
        {
            // send error message
            cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
            // free error
            sqlite3_free(err);
        }

        // clear sql string
        sql.clear();
        // sql string for creating watchlist tables
        sql = "CREATE TABLE IF NOT EXISTS WATCHLIST_";
        sql += guild;
        sql += "(ID INTEGER PRIMARY KEY AUTOINCREMENT,"
               "USERID TEXT UNIQUE,"
               "POLI_BAN INT NOT NULL,"
               "MONITOR INT NOT NULL,"
               "OVERRIDE INT NOT NULL);";

        // execute sql string
        ok = sqlite3_exec(db, sql.c_str(), defaultCallback, NULL, &err);

        // check for errors
        if (ok != SQLITE_OK)
        {
            // send error message
            cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
            // free error
            sqlite3_free(err);
        }

        // clear sql string
        sql.clear();
        // sql string for creating message tables
        sql = "CREATE TABLE IF NOT EXISTS MESSAGES_";
        sql += guild;
        sql += "(ID INTEGER PRIMARY KEY AUTOINCREMENT,"
               "CHANNEL_ID TEXT NOT NULL,"
               "MSG_ID TEXT NOT NULL,"
               "DELETED INT NOT NULL);";

        // execute sql string
        ok = sqlite3_exec(db, sql.c_str(), defaultCallback, NULL, &err);

        // check for errors
        if (ok != SQLITE_OK)
        {
            // send error message
            cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
            // free error
            sqlite3_free(err);
        }
    }

    // clear sql string
    sql.clear();
    // sql string for creating message tables
    sql = "CREATE TABLE IF NOT EXISTS GUILD_FLAGS";
    sql += "(ID INTEGER PRIMARY KEY AUTOINCREMENT,"
           "GUILD TEXT UNIQUE,"
           "ARCHIVE INT NOT NULL,"
           "PURGE_ROLE INT NOT NULL,"
           "CONFIGURED INT NOT NULL);";

    // execute sql string
    ok = sqlite3_exec(db, sql.c_str(), defaultCallback, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // send error message
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    // clear sql string
    sql.clear();
    // construct sql string
    sql += "CREATE TABLE BOT_FLAGS";
    sql += "(ID INTEGER PRIMARY KEY AUTOINCREMENT,"
           "RUNNING INT NOT NULL,"
           "INSTANCES INT NOT NULL,"
           "CLEAN INT NOT NULL);";

    // execute sql string
    ok = sqlite3_exec(db, sql.c_str(), defaultCallback, NULL, &err);

    // check for errors. If no error is detected, insert one entry
    if (ok != SQLITE_OK)
    {
        if (strcmp("table BOT_FLAGS already exists", sqlite3_errmsg(db)) != 0)
        {
            // send error message
            cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        }
        // free error
        sqlite3_free(err);
    }
    else
    {
        // construct sql string
        sql = "INSERT INTO BOT_FLAGS(RUNNING,INSTANCES,CLEAN)"
              "VALUES(0,0,1);";

        // run sql string
        ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

        // check for errors
        if (ok != SQLITE_OK)
        {
            // send error message
            cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
            // free error
            sqlite3_free(err);
        }
    }

    // clear string
    sql.clear();

    // construct sql string
    sql = "CREATE TABLE IF NOT EXISTS WARNINGS";
    sql += "(ID INTEGER PRIMARY KEY AUTOINCREMENT,"
           "GUILD_ID TEXT NOT NULL,"
           "CHANNEL TEXT NOT NULL,"
           "USER_ID TEXT NOT NULL,"
           "USER_NAME TEXT NOT NULL,"
           "ADMIN_ID TEXT NOT NULL,"
           "ADMIN_NAME TEXT NOT NULL,"
           "RULE INT NOT NULL,"
           "REASON TEXT NOT NULL,"
           "DATE CHAR(11) NOT NULL);";

    // execute sql string
    ok = sqlite3_exec(db, sql.c_str(), defaultCallback, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    // clear sql string
    sql.clear();

    // construct sql string
    sql = "CREATE TABLE IF NOT EXISTS QUIZ_DATA";
    sql += "(ID INTEGER PRIMARY KEY AUTOINCREMENT,"
           "USERID TEXT UNIQUE,"
           "VEASY INT DEFAULT 0,"
           "VEASYQ INT DEFAULT 0,"
           "EASY INT DEFAULT 0,"
           "EASYQ INT DEFAULT 0,"
           "NORMAL INT DEFAULT 0,"
           "NORMALQ INT DEFAULT 0,"
           "HARD INT DEFAULT 0,"
           "HARDQ INT DEFAULT 0,"
           "VHARD INT DEFAULT 0,"
           "VHARDQ INT DEFAULT 0);";

    // execute sql string
    ok = sqlite3_exec(db, sql.c_str(), defaultCallback, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    // clear sql string
    sql.clear();

    sql = "CREATE TABLE IF NOT EXISTS EASTER_EGGS";
    sql += "(ID INTEGER PRIMARY KEY AUTOINCREMENT,"
           "GUILD TEXT NOT NULL,"
           "PHRASE TEXT NOT NULL,"
           "FILENAME TEXT NOT NULL,"
           "FILESIZE INT NOT NULL,"
           "LIMITED INT NOT NULL,"
           "FILE BLOB NOT NULL);";

    // execute sql string
    ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }
}

// function that sets up flags for guilds upon start up
void EE_Bot_DB::setupFlags(list<long long> guildIDs)
{
    // string template used for insertion
    string str = "INSERT INTO GUILD_FLAGS(GUILD,ARCHIVE,PURGE_ROLE,CONFIGURED)"
                 "VALUES('";

    string sql;
    char *err;
    int ok;

    // loop through list until empty
    while (!guildIDs.empty())
    {
        // clear sql string
        sql.clear();
        // set sql string to template
        sql = str;
        // convert item into string and add it to sql string
        sql += std::to_string(guildIDs.front());
        // finish constructing sql string
        sql += "', 0, 0, 0);";

        // execute sql string
        ok = sqlite3_exec(db, sql.c_str(), defaultCallback, NULL, &err);

        // check for errors
        if (ok != SQLITE_OK)
        {
            // print error message
            cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
            // free error
            sqlite3_free(err);
        }
        // remove item from list
        guildIDs.pop_front();
    }
}

// function that inserts new users into database
void EE_Bot_DB::insertUser(string id, int suppressError, int &ok)
{
    // Create sql string
    string sql = "INSERT ";
    if (suppressError)
        sql += "OR IGNORE ";
    sql += "INTO USERS_";
    sql += guild;
    sql += "(USERID,RANK,FILTER,TOTALPOSTS,NOTIF,WARNINGS, XP)"
           "VALUES('";
    // Add user ID to string
    sql += id;
    // Init other values
    sql += "',0,1,0,1,0,0);";
    char *err;

    // Execute sql statement
    int sok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // Check for error
    if (sok != SQLITE_OK)
    {
        // Send error to error log
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free sql error
        sqlite3_free(err);
        // set status flag for python
        ok = 1;
    }
}

// Function that deletes a user from the database
void EE_Bot_DB::deleteUser(string id, int &ok)
{
    // create sql string
    string sql = "DELETE FROM USERS_";
    sql += guild;
    sql += " WHERE USERID = '";
    // add user ID to string
    sql += id;
    sql += "';";

    char *err;

    // execute sql string
    int sok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for an error
    if (sok != SQLITE_OK)
    {
        // send error message
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        // set status flag
        ok = 1;
    }
}

// function that updates user's posts
bool EE_Bot_DB::updateUserPosts(string id, int &rank, int &ok)
{
    // create sql string to retrieve user data from database
    string sql = "SELECT * FROM USERS_";
    sql += guild;
    sql += " WHERE USERID = '";
    // add user ID to sql string
    sql += id;
    sql += "';";

    char *err;

    // Execute sql statement
    int sok = sqlite3_exec(db, sql.c_str(), callbackUser,
                           static_cast<void *>(this), &err);

    // check for database error
    if ((ok != SQLITE_OK) || (data.id == ""))
    {
        // Output error message
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error from database
        sqlite3_free(err);
        // set error flag for python
        ok = 1;
        // return result
        return false;
    }

    // Init new variables
    bool rank_updated = false;
    int newRank = 0;

    // increment posts data
    ++data.posts;
    ++data.xp;

    // avoid errors
    if (data.xp > 0)
    {
        // generate rank based off of user data
        newRank = (int)floor(cbrt((1.25) * (float)data.xp));
        // set reference to rank generated
        rank = newRank;
        // check if rank retrieved from database is less than the new rank
        if (data.rank < newRank)
            rank_updated = true; // set rank updated flag
    }

    // Change sql statement
    sql = "UPDATE USERS_";
    sql += guild;
    sql += " SET TOTALPOSTS = ";
    // update posts
    sql += to_string(data.posts);
    sql += ", XP = ";
    // update xp
    sql += to_string(data.xp);
    sql += " WHERE USERID = '";
    // add id
    sql += id;
    sql += "';";

    // execute sql string
    sok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (sok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error from database
        sqlite3_free(err);
        // set error flag
        ok = 1;
        // return result
        return false;
    }

    // check if the rank was updated
    if (rank_updated)
    {
        // generate new sql string
        sql = "UPDATE USERS_";
        sql += guild;
        sql += " SET RANK = ";
        // add new rank to update string
        sql += to_string(newRank);
        sql += " WHERE USERID = '";
        // add user ID to sql string
        sql += id;
        sql += "';";

        // execute sql string
        sok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

        // check for errors
        if (sok != SQLITE_OK)
        {
            // print error
            cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
            // free error from database
            sqlite3_free(err);
            // set error flag
            ok = 1;
            // return result
            return false;
        }
    }

    // If the rank was updated and notifications are on, then return true,
    // otherwise false
    return (rank_updated && data.notif) ? true : false;
}

// Function that updates the filter flag in database
void EE_Bot_DB::updateUserFilter(string id, bool filter, int &ok)
{
    // create sql string
    string sql = "UPDATE USERS_";
    sql += guild;
    sql += " SET FILTER = ";
    // place new filter value
    sql += to_string((int)filter);
    sql += " WHERE USERID = '";
    // add user ID
    sql += id;
    sql += "';";

    char *err;

    // execute sql string
    int sok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (sok != SQLITE_OK)
    {
        // print error message
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error from database
        sqlite3_free(err);
        // set error flag for python
        ok = 1;
    }
}

// Function that retrieves filter flag from database
bool EE_Bot_DB::checkUserFilter(string id, int &ok)
{
    // create sql string
    string sql = "SELECT * FROM USERS_";
    sql += guild;
    sql += " WHERE USERID = '";
    // add user id to sql string
    sql += id;
    sql += "';";

    char *err;

    // execute sql string
    int sok = sqlite3_exec(db, sql.c_str(), callbackUser,
                           static_cast<void *>(this), &err);

    // check for errors
    if ((ok != SQLITE_OK) || (data.id == ""))
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error from database
        sqlite3_free(err);
        // set error flag for python
        ok = 1;
        // return false by default
        return true;
    }

    // return filter flag retrieved from database
    return data.filter;
}

// Function that retrieves the user rank from database
int EE_Bot_DB::checkUserRank(string id, float &progress, int &ok)
{
    // generate sql string
    string sql = "SELECT * FROM USERS_";
    sql += guild;
    sql += " WHERE USERID = '";
    // add user ID to sql string
    sql += id;
    sql += "'";

    char *err;

    // execute sql string
    int sok = sqlite3_exec(db, sql.c_str(), callbackUser,
                           static_cast<void *>(this), &err);

    // process data, if any
    if (data.posts > 0)
    {
        // calculate progress towards next level
        progress = (float)((cbrt((1.25) * data.xp) - (float)data.rank) * 100);
        // set to 0 if negative
        if (progress < 0.0)
            progress = 0.0;
    }

    // check for errors
    if ((ok != SQLITE_OK) || (data.id == ""))
    {
        // print error message
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error from database
        sqlite3_free(err);
        // set error flag for python
        ok = 1;
        // return 0 by default
        return 0;
    }

    // return retrieved rank
    return data.rank;
}

// Function that updates the notification flag in database
void EE_Bot_DB::Level_Notifications(string id, bool notify, int &ok)
{
    // construct sql string
    string sql = "UPDATE USERS_";
    sql += guild;
    sql += " SET NOTIF = ";
    // add notification flag update to string
    sql += to_string((int)notify);
    sql += " WHERE USERID = '";
    // add user ID to sql string
    sql += id;
    sql += "'";

    char *err;

    // execute sql string
    int sok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for error
    if (sok != SQLITE_OK)
    {
        // send error message
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error from db
        sqlite3_free(err);
        // set error flag for python
        ok = 1;
    }
}

// Function that calculates new level for each user
void EE_Bot_DB::adjust_level(string id, int &rank, int &ok)
{
    // construct sql string
    string sql = "SELECT * FROM USERS_";
    sql += guild;
    sql += " WHERE USERID = '";
    // add user ID
    sql += id;
    sql += "'";

    char *err;

    // execute sql string
    int sok = sqlite3_exec(db, sql.c_str(), callbackUser,
                           static_cast<void *>(this), &err);

    // check for error
    if (sok != SQLITE_OK)
    {
        // send error message
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error from database
        sqlite3_free(err);
        // set error flag for python
        ok = 1;
        // early return
        return;
    }

    // calculate new rank
    int newRank = (int)floor(cbrt((1.25) * (float)data.posts));

    // construct new sql string
    sql = "UPDATE USERS_";
    sql += guild;
    sql += " SET RANK = ";
    // add new rank to update string
    sql += to_string(newRank);
    sql += " WHERE USERID = '";
    // add user id to sql string
    sql += id;
    sql += "'";

    // execute sql string
    sok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for error
    if (sok != SQLITE_OK)
    {
        // display error message
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error from database
        sqlite3_free(err);
        // set error flag for python
        ok = 1;
        // early return
        return;
    }

    // set reference to rank to the new rank
    rank = newRank;
}

// Function that gets all the users from the database and returns a sorted list
// by posts
list<userData> EE_Bot_DB::getAllUsers(int *ok)
{
    // construct sql string
    string sql = "SELECT * FROM USERS_";
    sql += guild;
    sql += ";";

    char *err;

    allUsers.clear();

    // execute sql string
    *ok = sqlite3_exec(db, sql.c_str(), callbackUser2,
                       static_cast<void *>(this), &err);

    // check for sql errors
    if (*ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        // return empty list
        return list<userData>();
    }

    allUsers.sort(&EE_Bot_DB::sortUserData);

    return allUsers;
}

// Function that demotes a user
void EE_Bot_DB::demoteUser(string id, unsigned int numLevels, int *ok)
{
    // construct sql string
    string sql = "SELECT * FROM USERS_";
    sql += guild;
    sql += " WHERE USERID = '";
    sql += id;
    sql += "';";

    char *err;

    // execute sql string
    *ok = sqlite3_exec(db, sql.c_str(), callbackUser, static_cast<void *>(this),
                       &err);

    // check for sql error
    if (*ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        return;
    }

    // reset to level 0
    if (numLevels >= data.rank)
    {
        data.xp = 0;
        data.rank = 0;
    }
    // subtract rank and calculate xp
    else
    {
        data.rank -= numLevels;
        data.xp = (unsigned int)ceil(pow(data.rank, 3) * 0.8);
    }

    // clear sql string
    sql.clear();

    // construct new sql string
    sql = "UPDATE USERS_";
    sql += guild;
    sql += " SET RANK = ";
    sql += to_string(data.rank);
    sql += ", XP = ";
    sql += to_string(data.xp);
    sql += " WHERE USERID = '";
    sql += id;
    sql += "';";

    // execute sql string
    *ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (*ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }
}

// Function that checks if new channel can be created
void EE_Bot_DB::createChannel(string id, string channel, bool admin, int &error)
{
    // construct sql string
    string sql = "SELECT * FROM USERS_";
    sql += guild;
    sql += " WHERE USERID = '";
    // insert user ID into sql string
    sql += id;
    sql += "';";

    char *err;

    // execute sql string
    int ok = sqlite3_exec(db, sql.c_str(), callbackUser,
                          static_cast<void *>(this), &err);

    // check for errors
    if ((ok != SQLITE_OK) || (data.id == ""))
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error from database
        sqlite3_free(err);
        // set error flag for python
        error = 1;
        // early return
        return;
    }

    // check the user's rank
    if (data.rank >= 10 || admin)
    {
        // construct new sql string to get existing channels
        sql = "SELECT * FROM CHANNELS_";
        sql += guild;
        sql += ";";

        // execute sql string
        ok = sqlite3_exec(db, sql.c_str(), callbackChannels,
                          static_cast<void *>(this), &err);

        // check for error
        if (ok != SQLITE_OK)
        {
            // print error
            cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
            // free error from database
            sqlite3_free(err);
            // set error flag for python
            error = 2;
            // early return
            return;
        }

        // loop through channel titles
        while (!channels.empty())
        {
            // check if channel is already in database
            if (channels.front() == channel)
            {
                // set error flag for python
                error = 3;
                // print error
                cerr << "Channel " << channel << " already exists!" << endl;
                // clear channel list
                channels.clear();
                // early return
                return;
            }
            // pop the first item from list
            channels.pop_front();
        }

        sql.clear();
        // construct new sql string to insert new channel
        sql = "INSERT INTO CHANNELS_";
        sql += guild;
        sql += "(NAME) VALUES(?);";

        // construct sql string further
        ok = constructSQLstr(sql.c_str(), "s", channel.c_str());

        // ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

        // check for error
        if (ok != SQLITE_OK)
        {
            // print error message
            cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
            // set error flag for python
            error = 4;
        }
    }
    else
    {
        // print error message
        cerr << "Rank " << data.rank
             << " user cannot create channel! User ID: " << id << endl;
        // set error flag for python
        error = 5;
        return;
    }
}

// Function that deletes a channel from the database
void EE_Bot_DB::deleteChannel(string channel, int &ok)
{
    // construct sql string
    string sql = "DELETE FROM CHANNELS_";
    sql += guild;
    sql += " WHERE NAME = ?";

    // prepare sql string
    int sok = constructSQLstr(sql.c_str(), "s", channel.c_str());

    // check for errors
    if (sok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // set error flag for python
        ok = 1;
    }
}

// Function that creates new polls
int EE_Bot_DB::createNewPoll(string msgID, string roleID, string emoji)
{
    // construct sql string
    string sql = "INSERT INTO POLL_";
    sql += guild;
    sql += "(MSG_ID, ROLE, EMOJI)"
           "VALUES(?, ?, ?);";

    // execute sql string
    int ok = constructSQLstr(sql.c_str(), "sss", msgID.c_str(), roleID.c_str(),
                             emoji.c_str());

    // check for errors
    if (ok != SQLITE_OK)
    {
        // display error message
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // return error code for pyhton
        return 1;
    }

    // return error code for python
    return 0;
}

// function that retrieves poll from database
int EE_Bot_DB::retrievePoll(string msgID, int *ok)
{
    // clear role list, emoji list, and messageID
    roll_IDs.clear();
    emoji_IDs.clear();
    this->msgID.clear();
    // construct sql string
    string sql = "SELECT * FROM POLL_";
    sql += guild;
    sql += " WHERE MSG_ID = '";
    // insert message ID
    sql += msgID;
    sql += "';";

    char *errMsg;

    // execute sql string
    int sok = sqlite3_exec(db, sql.c_str(), callbackPoll,
                           static_cast<void *>(this), &errMsg);

    // check for errors
    if (sok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error from database
        sqlite3_free(errMsg);
        // set error flag for python
        *ok = 1;
        // return a size of 0
        return 0;
    }

    // check if the list is empty
    if (roll_IDs.empty())
        return 0; // return size 0 if list is empty

    // return size of list
    return (int)roll_IDs.size();
}

// Function that returns message ID
const char *EE_Bot_DB::getMsgID(void) { return msgID.c_str(); }

// Function that returns the front element of role IDs list
const char *EE_Bot_DB::getRoll_id(void) { return roll_IDs.front().c_str(); }

// Function that returns the front element of emoji list
const char *EE_Bot_DB::getEmoji_id(void) { return emoji_IDs.front().c_str(); }

// Function that clears front element from lists
void EE_Bot_DB::clearEntry(void)
{
    // check for empty lists
    if (!emoji_IDs.empty() && !roll_IDs.empty())
    {
        // pop front from lists
        emoji_IDs.pop_front();
        roll_IDs.pop_front();
    }
}

// Function that deletes a poll given a message ID
void EE_Bot_DB::deletePoll(string msgID, int *ok)
{
    // construct SQL string
    string sql = "DELETE FROM POLL_";
    sql += guild;
    sql += " WHERE MSG_ID = '";
    // insert message ID
    sql += msgID;
    sql += "';";

    char *errMsg;

    // execute sql string
    int sok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &errMsg);

    // check for error
    if (sok != SQLITE_OK)
    {
        // print error message
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error from database
        sqlite3_free(errMsg);
        // set error flag for python
        *ok = 1;
    }
    // clear poll related stuff
    roll_IDs.clear();
    emoji_IDs.clear();
    this->msgID.clear();
}

// Function to check if the msgID matches a saved poll
bool EE_Bot_DB::checkPoll_id(string msgID, int *ok)
{
    // construct sql string
    string sql = "SELECT * FROM POLL_";
    sql += guild;
    sql += " WHERE MSG_ID = '";
    sql += msgID;
    sql += "';";

    // parameters to be passed into execution function
    char *err;
    this->exists = false;

    // execute sql string
    int sok = sqlite3_exec(db, sql.c_str(), callbackCheckID,
                           static_cast<void *>(this), &err);

    // Check for errors
    if (sok != SQLITE_OK)
    {
        // print error message
        cerr << "There was a problem with the database" << endl;
        // print error message
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        // set error flag for pyhton
        *ok = 7;
        // return result
        return false;
    }

    // return result
    return exists;
}

// Function that deletes a poll entry
int EE_Bot_DB::deletePoll_Entry(string msgID, string role, string emoji)
{
    // Construct sql string
    string sql = "DELETE FROM POLL_";
    sql += guild;
    sql += " WHERE MSG_ID = ? AND ROLE= ? AND EMOJI= ?;";

    // execute sql statement
    int ok = constructSQLstr(sql.c_str(), "sss", msgID.c_str(), role.c_str(),
                             emoji.c_str());

    // check if execution went smoothly
    if (ok != SQLITE_OK)
    {
        // send error messages
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // express error in return value
        return 8;
    }

    // return 0 to express no error
    return 0;
}

// Function that adds a warning to a given user
void EE_Bot_DB::addWarning(string id, int *ok)
{
    // construct sql string
    string sql = "SELECT * FROM USERS_";
    sql += guild;
    sql += " WHERE USERID = '";
    sql += id;
    sql += "';";

    char *err;

    // execute sql statement
    int sok = sqlite3_exec(db, sql.c_str(), callbackUser,
                           static_cast<void *>(this), &err);

    // check if execution went smoothly
    if (sok != SQLITE_OK)
    {
        // print error message
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        *ok = 1;
    }

    int warnings = data.warnings + 1;

    sql.clear();
    // construct new sql string
    sql = "UPDATE USERS_";
    sql += guild;
    sql += " SET WARNINGS = ";
    sql += to_string(warnings);
    sql += " WHERE USERID='";
    sql += id;
    sql += "';";

    // execute sql statement
    sok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    if (sok != SQLITE_OK)
    {
        // print error message
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        *ok = 1;
    }

    *ok = update_watchlist_monitor(id, warnings, *ok);
}

// Function that retrieves the warnings from the database
int EE_Bot_DB::retrieveWarnings(string id, int *ok)
{
    // construct sql string
    string sql = "SELECT * FROM USERS_";
    sql += guild;
    sql += " WHERE USERID = '";
    sql += id;
    sql += "';";

    char *err;

    // execute sql string
    int sok = sqlite3_exec(db, sql.c_str(), callbackUser,
                           static_cast<void *>(this), &err);

    // check for errors
    if (sok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        // set python error flag
        *ok = 1;
        return 0;
    }

    // return number of warnings
    return (int)data.warnings;
}

// Function that removes a warning from the database given a user id
void EE_Bot_DB::removeWarning(string id, int *ok)
{
    // construct sql string
    string sql = "SELECT * FROM USERS_";
    sql += guild;
    sql += " WHERE USERID = '";
    sql += id;
    sql += "';";

    char *err;

    // execute sql statement
    int sok = sqlite3_exec(db, sql.c_str(), callbackChannels,
                           static_cast<void *>(this), &err);

    // check if execution went smoothly
    if (sok != SQLITE_OK)
    {
        // print error message
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        *ok = 1;
    }

    if (data.warnings != 0)
        data.warnings -= 1;

    // construct new sql string
    sql = "UPDATE USERS_";
    sql += guild;
    sql += " SET WARNINGS = ";
    sql += to_string(data.warnings);
    sql += " WHERE USERID='";
    sql += id;
    sql += "';";

    // execute sql statement
    sok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    if (sok != SQLITE_OK)
    {
        // print error message
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        *ok = 1;
    }

    // update monitor flag
    *ok = update_watchlist_monitor(id, data.warnings, *ok);
}

// function that resets a user's warnings to 0
void EE_Bot_DB::clearWarnings(string id, int *ok)
{
    // construct sql string
    string sql = "UPDATE USERS_";
    sql += guild;
    sql += " SET WARNINGS = 0 WHERE USERID = '";
    sql += id;
    sql += "';";

    char *err;

    // execute sql string
    int sok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (sok != SQLITE_OK)
    {
        // print error message
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        // set error code for python
        *ok = 1;
    }

    // update monitor flag
    *ok = update_watchlist_monitor(id, 0, *ok);
}

// function that logs a warning into the database
int EE_Bot_DB::logWarning(warningMsgData data)
{
    // construct sql string
    string sql = "INSERT INTO "
                 "WARNINGS(GUILD_ID,CHANNEL,USER_ID,USER_NAME,ADMIN_ID,ADMIN_"
                 "NAME,RULE,REASON,DATE)"
                 "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?);";

    // execute sql command
    int ok = constructSQLstr(
        sql.c_str(), "ssssssiss", std::to_string(data.guildID).c_str(),
        data.channel.c_str(), std::to_string(data.userID).c_str(),
        data.userName.c_str(), std::to_string(data.adminID).c_str(),
        data.adminName.c_str(), (int)data.ruleViolated, data.reason.c_str(),
        data.date.c_str());

    // check for error
    if (ok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
    }

    // return error code
    return ok;
}

// function that retrieves warnings from database based on callback function
// passed in
list<warningMsgData> EE_Bot_DB::getWarnings(int *ok, int searchParam,
                                            vector<string> values)
{
    string parameters = "";
    string sql = "SELECT * FROM WARNINGS";
    if (!warningData.empty())
        warningData.clear();
    if (searchParam & ALL)
    {
        sql += ";";
        *ok = safeSelect(sql.c_str(), static_cast<void *>(this),
                         callbackWarnings, "");
    }
    else
    {
        sql += " WHERE";
        if (searchParam & GUILD)
        {
            parameters += "s";
            sql += " GUILD_ID = ?";
        }
        if (searchParam & CHANNEL)
        {
            if (parameters.length() >= 1)
                sql += " and";
            parameters += "s";
            sql += " CHANNEL = ?";
        }
        if (searchParam & USER_ID)
        {
            if (parameters.length() >= 1)
                sql += " and";
            parameters += "s";
            sql += " USER_ID = ?";
        }
        if (searchParam & USER_NAME)
        {
            if (parameters.length() >= 1)
                sql += " and";
            parameters += "s";
            sql += " USER_NAME = ?";
        }
        if (searchParam & ADMIN_ID)
        {
            if (parameters.length() >= 1)
                sql += " and";
            parameters += "s";
            sql += " ADMIN_ID = ?";
        }
        if (searchParam & ADMIN_NAME)
        {
            if (parameters.length() >= 1)
                sql += " and";
            parameters += "s";
            sql += " ADMIN_NAME = ?";
        }
        if (searchParam & RULE)
        {
            if (parameters.length() >= 1)
                sql += " and";
            parameters += "i";
            sql += " RULE = ?";
        }
        if (searchParam & REASON)
        {
            if (parameters.length() >= 1)
                sql += " and";
            parameters += "s";
            sql += " REASON = ?";
        }
        if (searchParam & DATE)
        {
            if (parameters.length() >= 1)
                sql += " and";
            parameters += "s";
            sql += " DATE = ?";
        }
        if (parameters.length() == 0)
        {
            *ok = 1;
            return warningData;
        }

        // call another version of the safe select function
        *ok = safeSelect1(sql.c_str(), static_cast<void *>(this),
                          callbackWarnings, parameters.c_str(), values);
    }

    return warningData;
}

// function that sets the logging channels for a guild
int EE_Bot_DB::configure_guild(string logID, string roleOptIn, string levelID,
                               string shame, string watch)
{
    // construct sql string
    string sql =
        "INSERT INTO "
        "GUILD_SETTINGS(GUILD,LOG,ROLE,LEVEL,SHAME,WATCH,LEVEL_SYS,COGS)"
        "VALUES('";
    sql += guild;
    sql += "','";
    sql += logID;
    sql += "','";
    sql += roleOptIn;
    sql += "','";
    sql += levelID;
    sql += "','";
    sql += shame;
    sql += "','";
    sql += watch;
    sql += "',1,4294967291)";

    char *err;

    // execute sql command
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for error
    if (ok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }
    else
        ok = this->setConfiguredFlag(1);

    // return error code
    return ok;
}

// function that updates the guild settings
int EE_Bot_DB::reconfigure_guild(string logID, string roleOptIn, string levelID,
                                 string shame, string watch)
{
    // construct sql string
    string sql = "UPDATE GUILD_SETTINGS SET LOG='";
    sql += logID;
    sql += "', ROLE='";
    sql += roleOptIn;
    sql += "', LEVEL='";
    sql += levelID;
    sql += "', SHAME = '";
    sql += shame;
    sql += "', WATCH = '";
    sql += watch;
    sql += "' WHERE GUILD='";
    sql += guild;
    sql += "';";

    char *err;

    // execute sql command
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        sqlite3_free(err);
    }

    return ok;
}

// function that retrieves guild utility channels
/*
 *  0: Log channel
 *  1: Role opt in channel
 *  2: level channel
 *  3: shame channel
 *  4: watch channel
 */
const char *EE_Bot_DB::get_channel_id(int channel, int *ok)
{
    channel &= 0x7;
    string sql = "SELECT ";

    if (channel == 0)
        sql += "LOG ";
    else if (channel == 1)
        sql += "ROLE ";
    else if (channel == 2)
        sql += "LEVEL ";
    else if (channel == 3)
        sql += "SHAME ";
    else if (channel == 4)
        sql += "WATCH ";

    sql += "FROM GUILD_SETTINGS WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    *ok = sqlite3_exec(db, sql.c_str(), callbackSettings,
                       static_cast<void *>(this), &err);

    if (*ok != SQLITE_OK)
    {
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        sqlite3_free(err);
    }

    return channel_id.c_str();
}

// Function that sets the poli msg value in table
int EE_Bot_DB::set_poli_msg(string msgID)
{
    // construct sql string
    string sql = "UPDATE GUILD_SETTINGS SET POLI_MSG = '";
    sql += msgID;
    sql += "' WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    // execute sql command
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return ok;
}

// function that retrieves the poli msg id
const char *EE_Bot_DB::get_poli_msg(int *ok)
{
    // construct sql string
    string sql = "SELECT POLI_MSG FROM GUILD_SETTINGS WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    channel_id.clear();

    // execute sql command
    int sok = sqlite3_exec(db, sql.c_str(), callbackPoliMsg,
                           static_cast<void *>(this), &err);

    if (sok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        // set error flag
        *ok = 1;
    }

    // return message id
    return channel_id.c_str();
}

// function that clears the poli message id
int EE_Bot_DB::clear_poli_msg(void)
{
    // construct sql string
    string sql = "UPDATE GUILD_SETTINGS SET POLI_MSG = NULL WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    // execute sql string
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // display error message
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return ok;
}

// function that sets poli rules for server
int EE_Bot_DB::setPoliRules(string rules)
{
    // construct sql string
    string sql = "UPDATE GUILD_SETTINGS SET POLI_RULE_VIO = '";
    sql += rules;
    sql += "' WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    // execute sql string
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for error
    if (ok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    // return error flag
    return ok;
}

// function that retrieves poli rules from database
const char *EE_Bot_DB::getPoliRules(int *ok)
{
    // construct sql string
    string sql = "SELECT POLI_RULE_VIO FROM GUILD_SETTINGS WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    channel_id.clear();

    // execute sql string
    int sok = sqlite3_exec(db, sql.c_str(), callbackPoliMsg,
                           static_cast<void *>(this), &err);

    // check for errors
    if (sok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        // set error flag
        *ok = 1;
    }

    // return poli rules
    return channel_id.c_str();
}

// function that clears the poli rules of server
int EE_Bot_DB::clearPoliRules(void)
{
    // construct sql string
    string sql =
        "UPDATE GUILD_SETTINGS SET POLI_RULE_VIO = NULL WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    // exexcute sql string
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return ok;
}

// function that sets poli rules channel
int EE_Bot_DB::setPoliChannel(string channelID)
{
    // construct sql string
    string sql = "UPDATE GUILD_SETTINGS SET POLI_RULE_CH = '";
    sql += channelID;
    sql += "' WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    // execute sql string
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return ok;
}

// function that gets poli rules channel
const char *EE_Bot_DB::getPoliChannel(int *ok)
{
    // construct sql string
    string sql = "SELECT POLI_RULE_CH FROM GUILD_SETTINGS WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    // clear string
    channel_id.clear();

    // execute sql string
    int sok = sqlite3_exec(db, sql.c_str(), callbackPoliMsg,
                           static_cast<void *>(this), &err);

    // check for error
    if (sok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        // set error flag
        *ok = 1;
    }

    return channel_id.c_str();
}

// function that clears poli rules channel from database
int EE_Bot_DB::clearPoliChannel(void)
{
    // construct sql string
    string sql =
        "UPDATE GUILD_SETTINGS SET POLI_RULE_CH = NULL WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    // execute sql string
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for error
    if (ok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return ok;
}

// function that sets the channel prefix for a given guild
int EE_Bot_DB::setChannelPrefix(string prefix)
{
    // construct sql string
    string sql = "UPDATE GUILD_SETTINGS SET CH_PREFIX = ? WHERE GUILD = ?;";

    // execute sql string
    int ok = constructSQLstr(sql.c_str(), "ss", prefix.c_str(), guild.c_str());

    // check for error
    if (ok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
    }

    return ok;
}

// function that gets the prefix from the database
const char *EE_Bot_DB::getChannelPrefix(int *ok)
{
    // construct sql string
    string sql = "SELECT CH_PREFIX FROM GUILD_SETTINGS WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    channel_id.clear();

    // execute sql string
    int sok = sqlite3_exec(db, sql.c_str(), callbackPoliMsg,
                           static_cast<void *>(this), &err);

    // check for error
    if (sok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        // set error flag
        *ok = 1;
    }

    return channel_id.c_str();
}

// function that clears the prefix from the database
int EE_Bot_DB::clearChannelPrefix(void)
{
    // construct sql string
    string sql = "UPDATE GUILD_SETTINGS SET CH_PREFIX = NULL WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    // execute sql string
    int sok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for error
    if (sok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return sok;
}

// function that turns on or off the level system
int EE_Bot_DB::toggleLevelSystem(bool levels_on)
{
    // construct sql string
    string sql = "UPDATE GUILD_SETTINGS SET LEVEL_SYS = ";
    sql += std::to_string((unsigned int)levels_on);
    sql += " WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    // execute sql string
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return ok;
}

// function that gets the level system status
bool EE_Bot_DB::levelSystemStatus(int *ok)
{
    // construct sql string
    string sql = "SELECT LEVEL_SYS FROM GUILD_SETTINGS WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    // clear string
    channel_id.clear();

    // execute sql string
    *ok = sqlite3_exec(db, sql.c_str(), callbackSettings,
                       static_cast<void *>(this), &err);

    // check for error
    if (*ok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        return false;
    }

    return (bool)std::atoi(channel_id.c_str());
}

// function that enables a cog for a server
int EE_Bot_DB::enableCog(uint32_t Cog)
{
    // construct sql string
    string sql = "SELECT COGS FROM GUILD_SETTINGS WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    // get previous settings
    int ok = sqlite3_exec(db, sql.c_str(), callbackSettings,
                          static_cast<void *>(this), &err);

    // check for error
    if (ok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        return ok;
    }

    // update bit
    channel_id = std::to_string(std::atoi(channel_id.c_str()) | Cog);

    // construct new sql string
    sql = "UPDATE GUILD_SETTINGS SET COGS = ";
    sql += channel_id;
    sql += " WHERE GUILD = '";
    sql += guild;
    sql += "';";

    // execute sql string
    ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return ok;
}

// function that disables a cog for a server
int EE_Bot_DB::disableCog(uint32_t Cog)
{
    // construct sql string
    string sql = "SELECT COGS FROM GUILD_SETTINGS WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    // get previous settings
    int ok = sqlite3_exec(db, sql.c_str(), callbackSettings,
                          static_cast<void *>(this), &err);

    // check for error
    if (ok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        return ok;
    }

    // update bit
    channel_id = std::to_string(std::atoi(channel_id.c_str()) & (~Cog));

    // construct sql string
    sql = "UPDATE GUILD_SETTINGS SET COGS = ";
    sql += channel_id;
    sql += " WHERE GUILD = '";
    sql += guild;
    sql += "';";

    // execute sql string
    ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return ok;
}

// function that checks cog status
uint32_t EE_Bot_DB::checkCogs(int *ok)
{
    // construct sql string
    string sql = "SELECT COGS FROM GUILD_SETTINGS WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    // clear string
    channel_id.clear();

    // execute sql string
    *ok = sqlite3_exec(db, sql.c_str(), callbackSettings,
                       static_cast<void *>(this), &err);

    // check for errors
    if (*ok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        // set to default value
        channel_id = "4294967291";
    }

    return (uint32_t)std::atoi(channel_id.c_str());
}

// function that sets the ban message for a server
int EE_Bot_DB::setBanMsg(const char *msg)
{
    // construct sql string
    string sql = "UPDATE GUILD_SETTINGS SET BAN_MSG = ? WHERE GUILD = ?;";

    // execute sql string
    int ok = constructSQLstr(sql.c_str(), "ss", msg, guild.c_str());

    // check for errors
    if (ok != SQLITE_OK)
    {
        // print error message
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
    }

    return ok;
}

// function that clear the ban message for a server
int EE_Bot_DB::clearBanMsg(void)
{
    // construct sql string
    string sql = "UPDATE GUILD_SETTINGS SET BAN_MSG = NULL WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    // execute sql string
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // print error message
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return ok;
}

// function that gets the ban message for a server
const char *EE_Bot_DB::getBanMsg(int *ok)
{
    // construct sql string
    string sql = "SELECT BAN_MSG FROM GUILD_SETTINGS WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    // clear string before use
    channel_id.clear();

    // execute sql string
    *ok = sqlite3_exec(db, sql.c_str(), callbackPoliMsg,
                       static_cast<void *>(this), &err);

    // check for errors
    if (*ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        // return empty string
        return "";
    }

    return channel_id.c_str();
}

int EE_Bot_DB::setRoleColor(uint8_t R, uint8_t G, uint8_t B)
{
    // create sql string
    string sql = "UPDATE GUILD_SETTINGS SET ROLE_COLOR = ? WHERE GUILD = ?;";
    // construct rgb number
    uint32_t rgbValue = (R << 16) | (G << 8) | B;

    // execute sql string
    int ok = constructSQLstr(sql.c_str(), "is", (int)rgbValue, guild.c_str());

    // check for errors
    if (ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
    }

    // return result
    return ok;
}

int EE_Bot_DB::getRoleColor(uint8_t *R, uint8_t *G, uint8_t *B)
{
    // construct sql command
    string sql = "SELECT ROLE_COLOR FROM GUILD_SETTINGS WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    // clear string before use
    channel_id.clear();

    // execute sql string
    int ok = sqlite3_exec(db, sql.c_str(), callbackPoliMsg,
                          static_cast<void *>(this), &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        // early return
        return ok;
    }

    // Return if not configure
    if (channel_id.empty())
        return 1;

    // convert rgb value to unsigned int
    uint32_t rgb = (uint32_t)std::stoi(channel_id);

    // deconstruct unsigned int to get rgb values
    *B = rgb & 0xFF;
    *G = (rgb >> 8) & 0xFF;
    *R = (rgb >> 16) & 0xFF;

    return ok;
}

int EE_Bot_DB::clearRoleColor()
{
    // construct sql string
    string sql = "UPDATE GUILD_SETTINGS SET ROLE_COLOR = ? WHERE GUILD = ?";

    // execute sql string
    int ok = constructSQLstr(sql.c_str(), "ns", guild.c_str());

    // check for sql error
    if (ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
    }

    return ok;
}

// inserts new user into watchlist
int EE_Bot_DB::watchlist_insert(string id)
{
    // construct sql string
    string sql = "INSERT INTO WATCHLIST_";
    sql += guild;
    sql += "(USERID,POLI_BAN,MONITOR,OVERRIDE)"
           "VALUES('";
    sql += id;
    sql += "',0,0,0);";

    char *err;

    // execute sql string
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // display errors
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return ok;
}

// function that checks if user is in watchlist
bool EE_Bot_DB::in_watchlist(string id, int *ok)
{
    // construct sql string
    string sql = "SELECT * FROM WATCHLIST_";
    sql += guild;
    sql += " WHERE USERID = '";
    sql += id;
    sql += "';";

    char *err;

    // clear wdata
    wdata.id = "";
    wdata.monitor = 0;
    wdata.Override = 0;
    wdata.poli_ban = 0;

    // execute sql string
    int sok = sqlite3_exec(db, sql.c_str(), callbackWatchList,
                           static_cast<void *>(this), &err);

    // check for errors
    if (sok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        *ok = 1;
        return false;
    }

    // check if data was retrieved
    if (wdata.id == id)
        return true;
    return false;
}

// function that retrieves data from the watchlist
int EE_Bot_DB::watchlist_status(string id, int *poli_ban, int *monitored,
                                int *Override, int *exists)
{
    int ok = 0;

    // call function that retrieves all watchlist data
    bool watched = in_watchlist(id, &ok);

    // Update data passed in
    if (watched && ok == 0)
    {
        *poli_ban = wdata.poli_ban;
        *monitored = wdata.monitor;
        *Override = wdata.Override;
        *exists = 1;
    }
    else if (!watched) // Entry does not exist
        *exists = 0;

    // return errors
    return ok;
}

// function that updates monitored flag given warnings and database success
// SIDE NOTE: if override is set to true, then this function will not update the
// monitor flag
int EE_Bot_DB::update_watchlist_monitor(string id, int warnings, int ok)
{
    // Check if previous queries were successful
    if (ok == SQLITE_OK)
    {
        int poli_ban = 0, monitored = 0, Override = 0, exists = 0;
        // retrieve status flags
        ok = watchlist_status(id, &poli_ban, &monitored, &Override, &exists);
        // check status flags
        if (ok == 0 && Override == 0 && exists == 1)
        {
            // construct sql string
            string sql = "UPDATE WATCHLIST_";
            sql += guild;
            sql += " set MONITOR = ";

            if (warnings >= 3)
                sql += "1 WHERE USERID = '";
            else
                sql += "0 WHERE USERID = '";
            sql += id;
            sql += "';";

            char *err;

            // execute sql string
            ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

            // check for errors
            if (ok != SQLITE_OK)
            {
                // display errors
                cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db)
                     << endl;
                // free error
                sqlite3_free(err);
            }
        }
    }
    return ok;
}

// function that updates the monitor flag manually
// SIDE NOTE: This function ignores the override flag
int EE_Bot_DB::update_monitor(string id, int monitor)
{
    int ok = 0;
    // Check if entry exists
    bool exists = in_watchlist(id, &ok);

    if (exists)
    {
        // construct sql string
        string sql = "UPDATE WATCHLIST_";
        sql += guild;
        sql += " SET MONITOR = ";
        sql += to_string(monitor);
        sql += " WHERE USERID = '";
        sql += id;
        sql += "';";

        char *err;

        // execute sql string
        ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

        // check for errors
        if (ok != SQLITE_OK)
        {
            // display error code
            cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
            // free error
            sqlite3_free(err);
        }
    }
    else
        ok = 1;

    return ok;
}

// function that updates the poli ban flag manually
// SIDE NOTE: The poli ban flag is not affected by the override flag
int EE_Bot_DB::update_poli_ban(string id, int banned)
{
    int ok = 0;
    // Check if entry exists
    bool exists = in_watchlist(id, &ok);

    if (exists)
    {
        // construct sql string
        string sql = "UPDATE WATCHLIST_";
        sql += guild;
        sql += " SET POLI_BAN = ";
        sql += to_string(banned);
        sql += " WHERE USERID = '";
        sql += id;
        sql += "';";

        char *err;

        // execute sql string
        ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

        // check for errors
        if (ok != SQLITE_OK)
        {
            // display error code
            cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
            // free error
            sqlite3_free(err);
        }
    }
    else
        ok = 1;

    return ok;
}

// function that updates the override flag
// SIDE NOTE: The override flag affects the monitor flag automatic updates only
int EE_Bot_DB::update_override(string id, int Override)
{
    int ok = 0;
    // Check if entry exists
    bool exists = in_watchlist(id, &ok);

    if (exists)
    {
        // construct sql string
        string sql = "UPDATE WATCHLIST_";
        sql += guild;
        sql += " SET OVERRIDE = ";
        sql += to_string(Override);
        sql += " WHERE USERID = '";
        sql += id;
        sql += "';";

        char *err;

        // execute sql string
        ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

        // check for errors
        if (ok != SQLITE_OK)
        {
            // display error code
            cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
            // free error
            sqlite3_free(err);
        }
    }
    else
        ok = 1;

    return ok;
}

// function that adds a message to the database
int EE_Bot_DB::add_message(string channel_id, string msg_id)
{
    // create sql string
    string sql = "INSERT INTO MESSAGES_";
    sql += guild;
    sql += "(CHANNEL_ID,MSG_ID,DELETED)"
           "VALUES('";
    sql += channel_id;
    sql += "','";
    sql += msg_id;
    sql += "',0);";

    char *err;

    // execute sql string
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // display error message
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return ok;
}

// function that marks a message as deleted
int EE_Bot_DB::user_delete_message(string channel_id, string msg_id)
{
    // construct sql string
    string sql = "UPDATE MESSAGES_";
    sql += guild;
    sql += " SET DELETED = 1 WHERE CHANNEL_ID = '";
    sql += channel_id;
    sql += "' and MSG_ID = '";
    sql += msg_id;
    sql += "';";

    char *err;

    // execute sql string
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // display error message
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return ok;
}

// function that gets the messages from database
int EE_Bot_DB::get_messages(string channel_id, int *ok)
{
    // construct sql string
    string sql = "SELECT * FROM MESSAGES_";
    sql += guild;
    sql += ";";

    char *err;

    *ok = sqlite3_exec(db, sql.c_str(), callbackMsgData,
                       static_cast<void *>(this), &err);

    // Check for error
    if (*ok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }
    else
    {
        msgData data;
        // Populate list
        for (list<msgData>::iterator i = msgList.begin(); i != msgList.end();
             ++i)
        {
            data = *i;
            if ((data.channel_id == channel_id) && (data.deleted == 0))
                msgIDs.push_back(data.message_id);
        }
        msgList.clear();
    }

    return msgIDs.size();
}

// function that bulk deletes messages from database given channel id
int EE_Bot_DB::bulk_delete(string channel_id)
{
    // create sql string
    string sql = "DELETE FROM MESSAGES_";
    sql += guild;
    sql += " WHERE CHANNEL_ID = '";
    sql += channel_id;
    sql += "';";

    char *err;

    // execute sql command
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return ok;
}

long long EE_Bot_DB::getMessage(void)
{
    if (msgIDs.size() == 0)
        return 0;
    long long item = std::stoll(msgIDs.front());
    msgIDs.pop_front();
    return item;
}

// function that adds a guild to the database
int EE_Bot_DB::addGuild(long long guildID)
{
    // construct sql string
    string sql = "INSERT INTO GUILD_FLAGS(GUILD,ARCHIVE,PURGE_ROLE,CONFIGURED)"
                 "VALUES('";
    sql += std::to_string(guildID);
    sql += "',0,0,0);";

    char *err;

    // execute sql string
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    // return execution status
    return ok;
}

// function that resets flags for guild.
// Should be called whenever bot is shutdown.
int EE_Bot_DB::resetGuildFlags()
{
    // construct sql string
    string sql = "UPDATE GUILD_FLAGS SET ARCHIVE = 0, PURGE_ROLE = 0;";

    char *err;

    // execute sql string
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for sql error
    if (ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    // return result
    return ok;
}

// function that sets the flags for guilds
/*
        0: archive
        1: purge role
        2: configured
        3-30: undefined
        31: error
*/
int EE_Bot_DB::setFlags(int flags)
{
    // construct sql string
    string sql = "UPDATE GUILD_FLAGS SET ARCHIVE = ";
    sql += std::to_string(flags & 1);
    sql += ", PURGE_ROLE = ";
    sql += std::to_string((flags & 2) >> 1);
    sql += " WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    // execute sql string
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // print errors
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    // return status
    return ok;
}

// function that gets the flags for guilds
/*
        0: archive
        1: purge role
        2: configured
        3-30: undefined
        31: error
*/
int EE_Bot_DB::getFlags(int *ok)
{
    // construct sql string
    string sql = "SELECT * FROM GUILD_FLAGS WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    // execute sql string
    *ok = sqlite3_exec(db, sql.c_str(), callbackGuildFlags,
                       static_cast<void *>(this), &err);

    // check for sql errors
    if (*ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        // return flags with error flag set
        return -1;
    }

    // encode flags
    int statFlags =
        (Flags.configured << 2) | (Flags.purge_roles << 1) | Flags.archive;

    // return flags
    return statFlags;
}

// function that sets archive flag
int EE_Bot_DB::setArchiveFlag(int flag)
{
    // construct sql string
    string sql = "UPDATE GUILD_FLAGS SET ARCHIVE = ";
    sql += std::to_string(flag);
    sql += " WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    // execute sql string
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for sql errors
    if (ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return ok;
}

// function that gets the archive flag
int EE_Bot_DB::getArchiveFlag(int *ok)
{
    // call function to get both flags
    int flags = getFlags(ok);

    // check for error
    if (flags & 0x80000000)
        return flags; // return all flags if error flag is high
    return flags & 1; // return archive flag
}

// function that sets purge roles flag
int EE_Bot_DB::setPurgeRoleFlag(int flag)
{
    // construct sql
    string sql = "UPDATE GUILD_FLAGS SET PURGE_ROLE = ";
    sql += std::to_string(flag);
    sql += " WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    // execute sql string
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // print errors
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return ok;
}

// function that gets purge roles flag
int EE_Bot_DB::getPurgeRoleFlag(int *ok)
{
    // call function to get both flags
    int flags = getFlags(ok);

    // check for error
    if (flags & 0x80000000)
        return flags;        // return all flags if error flag is high
    return (flags >> 1) & 1; // return role purge flag
}

// Function that sets if a guild has been configured or not
int EE_Bot_DB::setConfiguredFlag(int flag)
{
    // construct sql
    string sql = "UPDATE GUILD_FLAGS SET CONFIGURED = ";
    sql += std::to_string(flag);
    sql += " WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    // execute sql string
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // print errors
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return ok;
}

// function that gets configured flag
int EE_Bot_DB::getConfiguredFlag(int *ok)
{
    // call function to get both flags
    int flags = getFlags(ok);

    // check for error
    if (flags & 0x80000000)
        return flags;        // return all flags if error flag is high
    return (flags >> 2) & 1; // return role purge flag
}

// Function that checks if the bot was started
int EE_Bot_DB::getStartedFlag(int *ok)
{
    // construct sql string
    string sql = "SELECT RUNNING FROM BOT_FLAGS WHERE ID = 1";
    char *err;

    // execute sql string
    *ok = sqlite3_exec(db, sql.c_str(), callbackSettings,
                       static_cast<void *>(this), &err);

    // check for errors
    if (*ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    // convert flag to int
    int flag = std::stoi(channel_id);

    return flag & 1;
}

// Function that sets the running flag
int EE_Bot_DB::setStartedFlag(int running)
{
    // construct sql string
    string sql = "UPDATE BOT_FLAGS SET RUNNING = ";
    sql += std::to_string(running & 1);
    sql += " WHERE ID = 1;";

    char *err;

    // execute sql string
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return ok;
}

// Function that gets the number of instances
int EE_Bot_DB::getInstances(int *ok)
{
    // construct sql string
    string sql = "SELECT INSTANCES FROM BOT_FLAGS WHERE ID = 1;";

    char *err;

    // execute sql string
    *ok = sqlite3_exec(db, sql.c_str(), callbackSettings,
                       static_cast<void *>(this), &err);

    // check for errors
    if (*ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return std::stoi(channel_id);
}

// Function that sets the number of instances
int EE_Bot_DB::setInstances(int instances)
{
    // construct sql string
    string sql = "UPDATE BOT_FLAGS SET INSTANCES = ";
    sql += std::to_string(instances);
    sql += " WHERE ID = 1;";

    char *err;

    // execute sql string
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return ok;
}

// function that sets a flag upon clean exit
int EE_Bot_DB::getCleanExit(int *ok)
{
    // construct sql string
    string sql = "SELECT CLEAN FROM BOT_FLAGS WHERE ID = 1";

    char *err;

    // execute sql string
    *ok = sqlite3_exec(db, sql.c_str(), callbackSettings,
                       static_cast<void *>(this), &err);

    // check for errors
    if (*ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return std::stoi(channel_id) & 1;
}

int EE_Bot_DB::setCleanExit(int clean)
{
    // construct sql string
    string sql = "UPDATE BOT_FLAGS SET CLEAN = ";
    sql += std::to_string(clean);
    sql += " WHERE ID = 1;";

    char *err;

    // execute sql string
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return ok;
}

// function that inserts a user into the quiz data table
int EE_Bot_DB::insertQuizUser(string id)
{
    // construct sql string, no security threat since user cannot enter data
    string sql = "INSERT OR IGNORE INTO QUIZ_DATA(USERID)"
                 "VALUES('";
    sql += id;
    sql += "');";

    char *err;

    // execute sql string
    int ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for error
    if (ok != SQLITE_OK)
    {
        // display error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return ok;
}

// function that updates the quiz data for a user
int EE_Bot_DB::updateQuizData(string id, unsigned int quizData)
{
    // construct sql string to fetch the old data
    string sql = "SELECT * FROM QUIZ_DATA WHERE USERID = '";
    // This is safe because the id is a numeric value
    sql += id;
    sql += "';";

    char *err;

    // execute sql string
    int ok = sqlite3_exec(db, sql.c_str(), callbackQuizData,
                          static_cast<void *>(this), &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
        // early return
        return ok;
    }

    // clear previous sql string
    sql.clear();

    // create new sql string
    sql = "UPDATE QUIZ_DATA SET ";

    // get difficulty
    unsigned int difficulty = quizData & 7;

    // update the user's quiz data
    switch (difficulty)
    {
    case 0:
        qData.veryEasyQuestions++;
        qData.veryEasyCorrect += ((quizData & 8) >> 3);
        sql += " VEASY = ";
        sql += std::to_string(qData.veryEasyCorrect);
        sql += ", VEASYQ = ";
        sql += std::to_string(qData.veryEasyQuestions);
        break;
    case 1:
        qData.easyQuestions++;
        qData.easyCorrect += ((quizData & 8) >> 3);
        sql += " EASY = ";
        sql += std::to_string(qData.easyCorrect);
        sql += ", EASYQ = ";
        sql += std::to_string(qData.easyQuestions);
        break;
    case 2:
        qData.normalQuestions++;
        qData.normalCorrect += ((quizData & 8) >> 3);
        sql += " NORMAL = ";
        sql += std::to_string(qData.normalCorrect);
        sql += ", NORMALQ = ";
        sql += std::to_string(qData.normalQuestions);
        break;
    case 3:
        qData.hardQuestions++;
        qData.hardCorrect += ((quizData & 8) >> 3);
        sql += " HARD = ";
        sql += std::to_string(qData.hardCorrect);
        sql += ", HARDQ = ";
        sql += std::to_string(qData.hardQuestions);
        break;
    case 4:
        qData.veryHardQuestions++;
        qData.veryHardCorrect += ((quizData & 8) >> 3);
        sql += " VHARD = ";
        sql += std::to_string(qData.hardCorrect);
        sql += ", VHARDQ = ";
        sql += std::to_string(qData.hardQuestions);
        break;
    default:
        return 1;
        break;
    }

    sql += " WHERE USERID = '";
    sql += id;
    sql += "';";

    // run sql string
    ok = sqlite3_exec(db, sql.c_str(), NULL, NULL, &err);

    // check for errors
    if (ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return ok;
}

// function that gets the quiz data for an individual user
quizData_t EE_Bot_DB::fetchIndividualQuizData(string id, int *ok)
{
    // construct sql string
    string sql = "SELECT * FROM QUIZ_DATA WHERE USERID = '";
    sql += id;
    sql += "';";

    char *err;

    // execute sql string
    *ok = sqlite3_exec(db, sql.c_str(), callbackQuizData,
                       static_cast<void *>(this), &err);

    // check for errors
    if (*ok != SQLITE_OK)
    {
        // print errors
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return qData;
}

// function to fetch all quiz data
list<quizData_t> EE_Bot_DB::fetchQuizData(int *ok)
{
    // construct sql string
    string sql = "SELECT * FROM QUIZ_DATA;";

    char *err;

    // clear list from previous calls
    aQdata.clear();

    // execute sql string
    *ok = sqlite3_exec(db, sql.c_str(), callbackQuizData2,
                       static_cast<void *>(this), &err);

    // check for errors
    if (*ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return aQdata;
}

// function that requests server data
list<string> EE_Bot_DB::requestData(Tables type, int *ok)
{
    char *err;
    // construct sql string
    string sql = "SELECT * FROM ";
    switch (type)
    {
    case USERS:
        sql += "USERS_";
        sql += guild;
        break;
    case CHANNELS:
        sql += "CHANNELS_";
        sql += guild;
        break;
    case POLL:
        sql += "POLL_";
        sql += guild;
        break;
    case WATCHLIST:
        sql += "WATCHLIST_";
        sql += guild;
        break;
    case MESSAGES:
        sql += "MESSAGES_";
        sql += guild;
        break;
    default:
        *ok = 1;
        return list<string>();
        break;
    }
    sql += ";";

    // clear previous outputs
    serverData.clear();
    header.clear();
    headerSet = false;

    // execute sql string
    *ok = sqlite3_exec(db, sql.c_str(), callbackServerData,
                       static_cast<void *>(this), &err);

    // check for errors
    if (*ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return serverData;
}

// function that gets the data header
const char *EE_Bot_DB::getDataHeader() { return header.c_str(); }

// function that inserts new easter egg into the database
int EE_Bot_DB::createEasterEgg(Blob_data_t file_meta)
{
    // construct sql string
    string sql = "INSERT INTO EASTER_EGGS(GUILD, PHRASE, FILENAME, FILESIZE, "
                 "LIMITED, FILE)"
                 "VALUES (?, ?, ?, ?, ?, ?);";

    // insert data into database
    int ok = constructSQLstr(
        sql.c_str(), "sssiib", guild.c_str(), file_meta.phrase.c_str(),
        file_meta.filename.c_str(), file_meta.filesize, file_meta.filter,
        file_meta.fileContents, file_meta.filesize);

    // check for error
    if (ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
    }

    return ok;
}

// function that removes easter eggs from the database
int EE_Bot_DB::removeEasterEgg(string egg)
{
    // construct sql command
    string sql = "DELETE FROM EASTER_EGGS WHERE GUILD = ? AND PHRASE = ?;";

    // compile and execute sql command
    int ok = constructSQLstr(sql.c_str(), "ss", guild.c_str(), egg.c_str());

    // check for error
    if (ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
    }

    return ok;
}

// function that fetches easter eggs from the database
list<Blob_data_t> EE_Bot_DB::fetchEasterEggs(int *ok)
{
    // construct sql string
    string sql = "SELECT * FROM EASTER_EGGS WHERE GUILD = '";
    sql += guild;
    sql += "';";

    char *err;

    blobData.clear();

    // execute sql command
    *ok = sqlite3_exec(db, sql.c_str(), callbackEasterEggs,
                       static_cast<void *>(this), &err);

    // check for error
    if (*ok != SQLITE_OK)
    {
        // print error
        cerr << sqlite3_errcode(db) << ": " << sqlite3_errmsg(db) << endl;
        // free error
        sqlite3_free(err);
    }

    return blobData;
}

// default callback function which prints table
int EE_Bot_DB::defaultCallback(void *data, int argc, char **argv,
                               char **azColName)
{
    // loop through columns
    for (int i = 0; i < argc; ++i)
    {
        // print result
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    // print newline
    printf("\n");
    // return 0
    return 0;
}

// Callback function for user data
int EE_Bot_DB::callbackUser(void *data, int argc, char **argv, char **azColName)
{
    // check if data parameter is not null
    if (data != NULL)
    {
        // call non-static callback function
        return static_cast<EE_Bot_DB *>(data)->callbackUser(argc, argv,
                                                            azColName);
    }
    return 1;
}

// non-static callback function for user data
int EE_Bot_DB::callbackUser(int argc, char **argv, char **azColName)
{
    // retrieve ID
    data.id = argv[1];
    // retrieve rank
    data.rank = std::atoi(argv[2]);
    // retrieve filter flag
    data.filter = (bool)std::atoi(argv[3]);
    // retrieve posts
    data.posts = std::atoi(argv[4]);
    // retrieve notification flag
    data.notif = (bool)std::atoi(argv[5]);
    // retrieve warnings
    data.warnings = std::atoi(argv[6]);
    data.xp = std::atoi(argv[7]);
    return 0;
}

// Alternate callback function for user data
int EE_Bot_DB::callbackUser2(void *data, int argc, char **argv,
                             char **azColName)
{
    // check if data parameter is not null
    if (data != NULL)
    {
        // call non-static callback function
        return static_cast<EE_Bot_DB *>(data)->callbackUser2(argc, argv,
                                                             azColName);
    }
    return 1;
}

// non-static alternate callback function for user data
int EE_Bot_DB::callbackUser2(int argc, char **argv, char **azColName)
{
    callbackUser(argc, argv, azColName);
    allUsers.push_back(data);
    return 0;
}

// callback function for channels table
int EE_Bot_DB::callbackChannels(void *data, int argc, char **argv,
                                char **azColName)
{
    // check if data parameter is not null
    if (data != NULL)
    {
        // call non-static callback function
        return static_cast<EE_Bot_DB *>(data)->callbackChannels(argc, argv,
                                                                azColName);
    }
    return 1;
}

// non-static callback function for channels
int EE_Bot_DB::callbackChannels(int argc, char **argv, char **azColName)
{
    // push channel name to list
    channels.push_back(argv[1]);
    return 0;
}

// callback function for polls table
int EE_Bot_DB::callbackPoll(void *data, int argc, char **argv, char **azColName)
{
    // check if data parameter is not null
    if (data != NULL)
    {
        // call non-static callback function
        return static_cast<EE_Bot_DB *>(data)->callbackPoll(argc, argv,
                                                            azColName);
    }
    return 1;
}

// non-static callback function for polls
int EE_Bot_DB::callbackPoll(int argc, char **argv, char **azColName)
{
    // set message ID data
    msgID = argv[1];
    // add role ID to list
    roll_IDs.push_back(argv[2]);
    // add emoji to list
    emoji_IDs.push_back(argv[3]);
    return 0;
}

// callback function for checking message ID
int EE_Bot_DB::callbackCheckID(void *data, int argc, char **argv,
                               char **azColName)
{
    // Check if data was passed in
    if (data != NULL)
    {
        // set exists flag
        static_cast<EE_Bot_DB *>(data)->setExistFlag();
    }

    return 0;
}

// callback function for retrieving settings
int EE_Bot_DB::callbackSettings(void *data, int argc, char **argv,
                                char **azColName)
{
    if (data != NULL)
        return static_cast<EE_Bot_DB *>(data)->callbackSettings(argc, argv,
                                                                azColName);
    return 1;
}

// callback helper for retrieving settings
int EE_Bot_DB::callbackSettings(int argc, char **argv, char **azColName)
{
    this->channel_id = argv[0];
    return 0;
}

// callback function for retrieving watchlist data
int EE_Bot_DB::callbackWatchList(void *data, int argc, char **argv,
                                 char **azColName)
{
    if (data != NULL)
        return static_cast<EE_Bot_DB *>(data)->callbackWatchList(argc, argv,
                                                                 azColName);
    return 1;
}

// callback helper for retrieving watchlist data
int EE_Bot_DB::callbackWatchList(int argc, char **argv, char **azColName)
{
    wdata.id = argv[1];
    wdata.poli_ban = atoi(argv[2]);
    wdata.monitor = atoi(argv[3]);
    wdata.Override = atoi(argv[4]);
    return 0;
}

// callback function for retrieving poli msg id or poli rules
int EE_Bot_DB::callbackPoliMsg(void *data, int argc, char **argv,
                               char **azColName)
{
    if (data != NULL)
        return static_cast<EE_Bot_DB *>(data)->callbackPoliMsg(argc, argv,
                                                               azColName);
    return 1;
}

// callback helper for retrieving poli msg id or poli rules
int EE_Bot_DB::callbackPoliMsg(int argc, char **argv, char **azColName)
{
    if (argv[0] != NULL)
        channel_id = argv[0];
    return 0;
}

// callback function for getting message data
int EE_Bot_DB::callbackMsgData(void *data, int argc, char **argv,
                               char **azColName)
{
    if (data != NULL)
    {
        return static_cast<EE_Bot_DB *>(data)->callbackMsgData(argc, argv,
                                                               azColName);
    }
    return 1;
}

int EE_Bot_DB::callbackMsgData(int argc, char **argv, char **azColName)
{
    msgList.push_back({atoi(argv[0]), argv[1], argv[2], atoi(argv[3])});
    return 0;
}

// callback function for getting guild flags
int EE_Bot_DB::callbackGuildFlags(void *data, int argc, char **argv,
                                  char **azColName)
{
    if (data != NULL)
    {
        return static_cast<EE_Bot_DB *>(data)->callbackGuildFlags(argc, argv,
                                                                  azColName);
    }
    return 1;
}

// callback function helper for getting guild flags
int EE_Bot_DB::callbackGuildFlags(int argc, char **argv, char **azColName)
{
    Flags.archive = atoi(argv[2]);
    Flags.purge_roles = atoi(argv[3]);
    Flags.configured = atoi(argv[4]);
    return 0;
}

// callback function for warnings
int EE_Bot_DB::callbackWarnings(void *data, int argc, char **argv,
                                char **azColName)
{
    // check if data is not null
    if (data != NULL)
    {
        return static_cast<EE_Bot_DB *>(data)->callbackWarnings(argc, argv,
                                                                azColName);
    }
    return 1;
}

// callback function helper for getting warning data
int EE_Bot_DB::callbackWarnings(int argc, char **argv, char **azColName)
{
    warningMsgData data;
    data.guildID = std::stoll(argv[1]);
    data.channel = argv[2];
    data.userID = std::stoll(argv[3]);
    data.userName = argv[4];
    data.adminID = std::stoll(argv[5]);
    data.adminName = argv[6];
    data.ruleViolated = std::stoul(argv[7]);
    data.reason = argv[8];
    data.date = argv[9];
    warningData.push_back(data);
    return 0;
}

// callback function for quiz data
int EE_Bot_DB::callbackQuizData(void *data, int argc, char **argv,
                                char **azColName)
{
    // check if data is not null
    if (data != NULL)
    {
        return static_cast<EE_Bot_DB *>(data)->callbackQuizData(argc, argv,
                                                                azColName);
    }
    return 1;
}

// callback helper for quiz data
int EE_Bot_DB::callbackQuizData(int argc, char **argv, char **azColName)
{
    qData.userID = std::stoll(argv[1]);
    qData.veryEasyCorrect = (unsigned int)std::atoi(argv[2]);
    qData.veryEasyQuestions = (unsigned int)std::atoi(argv[3]);
    qData.easyCorrect = (unsigned int)std::atoi(argv[4]);
    qData.easyQuestions = (unsigned int)std::atoi(argv[5]);
    qData.normalCorrect = (unsigned int)std::atoi(argv[6]);
    qData.normalQuestions = (unsigned int)std::atoi(argv[7]);
    qData.hardCorrect = (unsigned int)std::atoi(argv[8]);
    qData.hardQuestions = (unsigned int)std::atoi(argv[9]);
    qData.veryHardCorrect = (unsigned int)std::atoi(argv[10]);
    qData.veryHardQuestions = (unsigned int)std::atoi(argv[11]);
    return 0;
}

// callback function for getting all the quiz data
int EE_Bot_DB::callbackQuizData2(void *data, int argc, char **argv,
                                 char **azColName)
{
    // check if data is not null
    if (data != NULL)
    {
        return static_cast<EE_Bot_DB *>(data)->callbackQuizData2(argc, argv,
                                                                 azColName);
    }
    return 1;
}

// callback helper for getting all the quiz data
int EE_Bot_DB::callbackQuizData2(int argc, char **argv, char **azColName)
{
    // use other callback for getting data for one row
    callbackQuizData(argc, argv, azColName);
    // push data to list
    aQdata.push_back(qData);
    return 0;
}

// callback function for getting server data
int EE_Bot_DB::callbackServerData(void *data, int argc, char **argv,
                                  char **azColName)
{
    // check if data was passed in
    if (data != NULL)
    {
        return static_cast<EE_Bot_DB *>(data)->callbackServerData(argc, argv,
                                                                  azColName);
    }
    return 1;
}

// callback helper for getting server data
int EE_Bot_DB::callbackServerData(int argc, char **argv, char **azColName)
{
    string temp;
    // check if the header was set
    if (!headerSet)
    {
        // set the header
        for (int i = 0; i < argc; ++i)
        {
            header += azColName[i];
            header += ",";
        }
        // get rid of extra comma
        header.pop_back();
        // set the flag to true
        headerSet = true;
    }

    // format data
    for (int i = 0; i < argc; ++i)
    {
        // check if the data is null
        if (argv[i] != NULL)
        {
            // check for commas
            if (string(argv[i]).find(',') != string::npos)
            {
                temp += "\"";
                temp += argv[i];
                temp += "\"";
            }
            else
                temp += argv[i];
        }
        temp += ",";
    }
    // get rid of extra comma
    temp.pop_back();

    // push entry to the back
    serverData.push_back(temp);

    return 0;
}

int EE_Bot_DB::callbackEasterEggs(void *data, int argc, char **argv,
                                  char **azColName)
{
    // check if data was passed in
    if (data)
    {
        return static_cast<EE_Bot_DB *>(data)->callbackEasterEggs(argc, argv,
                                                                  azColName);
    }
    return 1;
}

int EE_Bot_DB::callbackEasterEggs(int argc, char **argv, char **azColName)
{
    Blob_data_t data;
    data.phrase = argv[2];
    data.filename = argv[3];
    data.filesize = (size_t)atoi(argv[4]);
    data.filter = (bool)atoi(argv[5]);
    data.safeFileContents.reserve(data.filesize);
    std::copy(argv[6], argv[6] + data.filesize,
              std::back_inserter(data.safeFileContents));
    blobData.push_back(data);
    return 0;
}

// function that sets exists flag
void EE_Bot_DB::setExistFlag(void)
{
    // set exists flag
    exists = true;
}

// function that sorts user data
bool EE_Bot_DB::sortUserData(const userData &first, const userData &second)
{
    return (first.posts > second.posts);
}

// generic function that prepares, binds, step, and finalize sql strings
int EE_Bot_DB::constructSQLstr(const char *sql, const char *types, ...)
{
    // create variables
    sqlite3_stmt *st;
    va_list v1;
    double ddata;
    int idata;
    sqlite3_int64 i64data;
    sqlite3_value *vdata;
    const char *tdata;
    void *bdata;
    size_t bsize;
    int n = strlen(types);
    // prepare the string
    int ok = sqlite3_prepare_v2(db, sql, -1, &st, NULL);

    // get the start of variable arguments
    va_start(v1, types);

    for (int i = 1; i <= n; ++i)
    {
        // exit early if error detected
        if (ok != SQLITE_OK)
            break;
        switch (types[i - 1])
        {
        case 'd': // double argument
            ddata = va_arg(v1, double);
            ok = sqlite3_bind_double(st, i, ddata);
            break;
        case 'i': // integer argument
            idata = va_arg(v1, int);
            ok = sqlite3_bind_int(st, i, idata);
            break;
        case 'I': // 64-bit integer argument
            i64data = va_arg(v1, sqlite3_int64);
            ok = sqlite3_bind_int64(st, i, i64data);
            break;
        case 'n': // null argument
            ok = sqlite3_bind_null(st, i);
            break;
        case 's': // text argument
            tdata = va_arg(v1, const char *);
            ok = sqlite3_bind_text(st, i, tdata, string(tdata).size(),
                                   SQLITE_TRANSIENT);
            break;
        case 'v': // generic variable argument
            vdata = va_arg(v1, sqlite3_value *);
            ok = sqlite3_bind_value(st, i, vdata);
            break;
        case 'b': // blob, requires data and size
            bdata = va_arg(v1, void *);
            bsize = va_arg(v1, size_t);
            ok = sqlite3_bind_blob(st, i, bdata, bsize, SQLITE_TRANSIENT);
            break;
        default: // abort prepared statement here
            sqlite3_clear_bindings(st);
            ok = 1;
            break;
        }
    }

    // end the variable arguments
    va_end(v1);

    // step
    if (ok == SQLITE_OK)
        ok = (sqlite3_step(st) != SQLITE_DONE);

    // finalize changes
    sqlite3_finalize(st);

    return ok;
}

// "Safe" function for getting data from the database
int EE_Bot_DB::safeSelect(const char *sql, void *data,
                          int (*callback)(void *, int, char **, char **),
                          const char *types, ...)
{
    // create variables for retrieving data
    sqlite3_stmt *st;
    va_list v1;
    double ddata;
    int idata;
    sqlite3_int64 i64data;
    sqlite3_value *vdata;
    const char *tdata;

    // get length of variable list
    int n = strlen(types);
    // get the start of the variable list
    va_start(v1, types);
    // prepare the string
    int ok = sqlite3_prepare_v2(db, sql, -1, &st, NULL);

    for (int i = 1; i <= n; ++i)
    {
        // early exit if error detected
        if (ok != SQLITE_OK)
            break;
        switch (types[i - 1])
        {
        case 'd': // double argument
            ddata = va_arg(v1, double);
            ok = sqlite3_bind_double(st, i, ddata);
            break;
        case 'i': // integer argument
            idata = va_arg(v1, int);
            ok = sqlite3_bind_int(st, i, idata);
            break;
        case 'I': // 64-bit integer argument
            i64data = va_arg(v1, sqlite3_int64);
            ok = sqlite3_bind_int64(st, i, i64data);
            break;
        case 'v': // sqlite3 variable argument
            vdata = va_arg(v1, sqlite3_value *);
            ok = sqlite3_bind_value(st, i, vdata);
            break;
        case 's': // text argument
            tdata = va_arg(v1, const char *);
            ok = sqlite3_bind_text(st, i, tdata, string(tdata).capacity(),
                                   SQLITE_TRANSIENT);
            break;
        case 'n': // null argument
            ok = sqlite3_bind_null(st, i);
            break;
        default:
            ok = SQLITE_ERROR; // invalid argument
            break;
        }
    }

    // end the variable list
    va_end(v1);

    if (ok == SQLITE_OK)
    {
        // use the callback function
        int columns;
        // char ** are used instead of list or vector for legacy purposes
        char **results = NULL;
        char **names = NULL;
        // loop through all the rows
        while (sqlite3_step(st) == SQLITE_ROW)
        {
            // get column count
            columns = sqlite3_column_count(st);
            // allocate space for returned data
            results = (char **)malloc(sizeof(char *) * columns);
            // allocate space for returned column names
            names = (char **)malloc(sizeof(char *) * columns);

            // retrieve the data from the rows
            for (int i = 0; i < columns; ++i)
            {
                // get row data from column i. Make deep copies of the strings
                results[i] = strdup((char *)sqlite3_column_text(st, i));
                // get name of column i. Make deep copies of the strings
                names[i] = strdup((const char *)sqlite3_column_name(st, i));
            }

            // call the callback function to process the data
            callback(data, columns, results, names);

            // free up the deep copies of the strings
            for (int i = 0; i < columns; ++i)
            {
                free(results[i]);
                free(names[i]);
            }
            // free up the array of strings
            free(results);
            free(names);
        }
        // finalize when finished
        ok = sqlite3_finalize(st);
        return ok;
    }

    // free up any memory allocated
    sqlite3_finalize(st);

    // return error code
    return ok;
}

int EE_Bot_DB::safeSelect1(const char *sql, void *data,
                           int (*callback)(void *, int, char **, char **),
                           const char *types, vector<string> values)
{
    // create variables for retrieving data
    sqlite3_stmt *st;
    double ddata;
    int idata;
    sqlite3_int64 i64data;
    sqlite3_value *vdata;
    const char *tdata;

    // get length of variable list
    int n = strlen(types);
    // prepare the string
    int ok = sqlite3_prepare_v2(db, sql, -1, &st, NULL);

    for (int i = 1; i <= n; ++i)
    {
        // early exit if error detected
        if (ok != SQLITE_OK)
            break;
        switch (types[i - 1])
        {
        case 'd': // double argument
            ok = sqlite3_bind_double(st, i, std::stod(values[i - 1]));
            break;
        case 'i': // integer argument
            ok = sqlite3_bind_int(st, i, std::stoi(values[i - 1]));
            break;
        case 'I': // 64-bit integer argument
            ok = sqlite3_bind_int64(st, i,
                                    (sqlite3_int64)std::stoll(values[i - 1]));
            break;
        case 's': // text argument
            ok = sqlite3_bind_text(st, i, values[i - 1].c_str(), -1,
                                   SQLITE_STATIC);
            break;
        default:
            ok = SQLITE_ERROR; // invalid argument
            break;
        }
    }

    if (ok == SQLITE_OK)
    {
        // use the callback function
        int columns;
        // char ** are used instead of list or vector for legacy purposes
        char **results = NULL;
        char **names = NULL;
        // loop through all the rows
        while (sqlite3_step(st) == SQLITE_ROW)
        {
            // get column count
            columns = sqlite3_column_count(st);
            // allocate space for returned data
            results = (char **)malloc(sizeof(char *) * columns);
            // allocate space for returned column names
            names = (char **)malloc(sizeof(char *) * columns);

            // retrieve the data from the rows
            for (int i = 0; i < columns; ++i)
            {
                // get row data from column i. Make deep copies of the strings
                results[i] = strdup((char *)sqlite3_column_text(st, i));
                // get name of column i. Make deep copies of the strings
                names[i] = strdup((const char *)sqlite3_column_name(st, i));
            }

            // call the callback function to process the data
            callback(data, columns, results, names);

            // free up the deep copies of the strings
            for (int i = 0; i < columns; ++i)
            {
                free(results[i]);
                free(names[i]);
            }
            // free up the array of strings
            free(results);
            free(names);
        }
        // finalize when finished
        ok = sqlite3_finalize(st);
        return ok;
    }

    // free up any memory allocated
    sqlite3_finalize(st);

    // return error code
    return ok;
}
