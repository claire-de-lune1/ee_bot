/******************************************************************************
 * BSD 3 - Clause License
 *
 * Copyright(c) 2021, Tom Schmitz
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#define PY_SSIZE_T_CLEAN

#include "EE_Bot_DB.h"
#include "utility.h"
#include <Python.h>

// Function that gets the version of the code
PyObject *version(PyObject *self, PyObject *args)
{
    return Py_BuildValue("s", "v3.6.0");
}

// Function that constructs new database manager object
PyObject *construct(PyObject *self, PyObject *args)
{
    char *filename;
    char *guildID;
    int createTables;
    int ok = 0;
    // retrieve filename
    PyArg_ParseTuple(args, "ssi", &filename, &guildID, &createTables);

    // create new data database manager object
    EE_Bot_DB *db = new EE_Bot_DB(filename, ok, guildID, createTables);
    // create new python pointer to the manager object
    PyObject *dbCapsule =
        PyCapsule_New(static_cast<void *>(db), "EE_Bot_DB", NULL);
    // set the python pointer
    PyCapsule_SetPointer(dbCapsule, static_cast<void *>(db));

    // return new python object and status flag
    return Py_BuildValue("Oi", dbCapsule, ok);
}

// Function that adds inserts new guild into flags table upon startup
PyObject *setupGuildFlags(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    PyObject *List;

    // get arguments from python
    PyArg_ParseTuple(args, "OO", &dbCapsule, &List);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));

    // Check for python list
    if (PyList_Check(List))
    {
        PyObject *item;
        long long id;
        list<long long> guildIDs;
        // convert python list to cpp list
        for (Py_ssize_t i = 0; i < PyList_Size(List); ++i)
        {
            // get item from list
            item = PyList_GetItem(List, i);
            // convert item
            id = PyLong_AsLongLong(item);
            // push item to list
            guildIDs.push_back(id);
        }
        // call cpp function
        db->setupFlags(guildIDs);
    }

    return Py_BuildValue("");
}

// Function that inserts new user into database
PyObject *insertUser(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *id;
    int ok = 0;
    int suppressError;

    // retrieve python object and user ID
    PyArg_ParseTuple(args, "Osi", &dbCapsule, &id, &suppressError);

    // retrieve database pointer from pyhton object
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function to insert new user
    db->insertUser(id, suppressError, ok);

    // return status flag
    return Py_BuildValue("i", ok);
}

// Function that deletes user from database
PyObject *deleteUser(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *id;
    int ok = 0;

    // retrieve python object and user ID
    PyArg_ParseTuple(args, "Os", &dbCapsule, &id);

    // retrieve database pointer from pyhton object
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function to delete user
    db->deleteUser(id, ok);

    // return status flag
    return Py_BuildValue("i", ok);
}

// Function that updates user posts
PyObject *updateUserPosts(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *id;
    int ok = 0;
    int rank = 0;

    // retrieve python object and user ID
    PyArg_ParseTuple(args, "Os", &dbCapsule, &id);

    // retrieve database pointer from pyhton object
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function to update user posts
    bool rankUp = db->updateUserPosts(id, rank, ok);

    // return rank flag, calculated rank, and status flag
    return Py_BuildValue("iii", (int)rankUp, rank, ok);
}

// Function that updates the filter flag in database
PyObject *updateUserFilter(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *id;
    int ok = 0;
    int filter = 0;

    // retrieve python object, user ID, and new filter flag
    PyArg_ParseTuple(args, "Osi", &dbCapsule, &id, &filter);

    // retrieve database pointer from pyhton object
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function to update filter flag
    db->updateUserFilter(id, (bool)filter, ok);

    // return status flag
    return Py_BuildValue("i", ok);
}

// Function that retrieves filter flag from database
PyObject *checkUserFilter(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *id;
    int ok = 0;

    // retrieve python object and user ID
    PyArg_ParseTuple(args, "Os", &dbCapsule, &id);

    // retrieve database pointer from pyhton object
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function to check flag in database
    bool filter = db->checkUserFilter(id, ok);

    // return filter flag and status flag
    return Py_BuildValue("ii", (int)filter, ok);
}

// Python function to retrieve user rank from database
PyObject *retreiveUserRank(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *id;
    int ok = 0;
    float progress = 0;

    // retrieve python object and user ID
    PyArg_ParseTuple(args, "Os", &dbCapsule, &id);
    // retrieve database pointer from pyhton object
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function to retrieve user's rank
    int rank = db->checkUserRank(id, progress, ok);

    // return rank, progress, and status flag
    return Py_BuildValue("ifi", rank, progress, ok);
}

// Function that updates notification flag
PyObject *updateNotifications(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int ok = 0;
    int notif;
    char *id;

    // retrieve python object, user ID, and new notification flag
    PyArg_ParseTuple(args, "Osi", &dbCapsule, &id, &notif);
    // retrieve database pointer from pyhton object
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function to update notification flag
    db->Level_Notifications(id, (bool)notif, ok);

    // return status flag
    return Py_BuildValue("i", ok);
}

// Function that adjusts levels in database
PyObject *adjustRank(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int ok = 0;
    int newRank = 0;
    char *id;

    // retrieve python object and user ID
    PyArg_ParseTuple(args, "Os", &dbCapsule, &id);
    // retrieve database pointer from pyhton object
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function that adjust the rank in database
    db->adjust_level(id, newRank, ok);

    // return new rank and status flag
    return Py_BuildValue("ii", newRank, ok);
}

// function that inserts new channel into database, if it doesn't exist already
PyObject *createChannel(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *id;
    char *channel;
    int error = 0;
    int admin;

    // retrieve python object, user ID, and channel name
    PyArg_ParseTuple(args, "Ossi", &dbCapsule, &id, &channel, &admin);

    // retrieve database pointer from pyhton object
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function to check if channel exists and insert it into table if
    // it doesn't exist
    db->createChannel(id, channel, (bool)admin, error);

    // return status flag
    return Py_BuildValue("i", error);
}

// function that deletes a channel from the database
PyObject *deleteChannel(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *channel;
    int ok = 0;

    // retrieve data from python
    PyArg_ParseTuple(args, "Os", &dbCapsule, &channel);

    // retrieve database pointer from python object
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function to delete channel from database
    db->deleteChannel(channel, ok);

    return Py_BuildValue("i", ok);
}

// Function that inserts new poll into table
PyObject *createNewPoll(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *msgID;
    char *roleID;
    char *emoji;

    // retrieve python object, message ID, role ID, and emoji from python
    PyArg_ParseTuple(args, "Osss", &dbCapsule, &msgID, &roleID, &emoji);
    // retrieve database pointer from pyhton object
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call function to insert new poll into database
    int ok = db->createNewPoll(msgID, roleID, emoji);

    // return status flag
    return Py_BuildValue("i", ok);
}

// Function that retrieve poll from database
PyObject *retrievePoll(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *msgID;

    // retrieve python object and message ID from python
    PyArg_ParseTuple(args, "Os", &dbCapsule, &msgID);

    int ok = 0;

    // retrieve database pointer from pyhton object
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function that loads lists and returns the length
    int length = db->retrievePoll(msgID, &ok);

    if (ok != 0)
        length = 0;

    // Create list
    PyObject *List = PyList_New((Py_ssize_t)length);

    // Check if there were no errors
    if (length > 0)
    {
        PyObject *data;
        for (int i = 0; i < length; ++i)
        {
            // Create data object
            data = Py_BuildValue("[Ls]", std::stoll(db->getRoll_id()),
                                 db->getEmoji_id());
            // Append item to list
            PyList_SetItem(List, i, data);
            // clear item
            db->clearEntry();
        }
    }

    // return length and status flag
    return Py_BuildValue("Oi", List, ok);
}

// function that retrieves the message ID from database manager object
PyObject *getMsgID(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;

    // retrieve python object from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // retrieve database pointer from pyhton object
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // retrieve message id from database manager
    const char *msgID = db->getMsgID();

    // return messageID as a string to python
    return Py_BuildValue("s", msgID);
}

// function that retrieves the first role ID from list
PyObject *getRoleID(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;

    // retrieve python object from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // retrieve database pointer from pyhton object
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // retrieve role ID from database manager
    const char *roleID = db->getRoll_id();

    // return role ID as a string to python
    return Py_BuildValue("s", roleID);
}

// function that retrieves the first emoji from list
PyObject *getEmoji_id(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;

    // retrieve python object from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // retrieve database pointer from pyhton object
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // retrieve emoji from database manager
    const char *emoji_id = db->getEmoji_id();

    // return emoji to python
    return Py_BuildValue("s", emoji_id);
}

// function that clears the first entry from list
PyObject *clearEntry(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;

    // retrieve python object from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // retrieve database pointer from pyhton object
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function that deletes first entry from list
    db->clearEntry();

    // return NULL
    return Py_BuildValue("");
}

// function that deletes polls from database
PyObject *deletePoll(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *msgID;
    int ok = 0;

    // retrieve python object and message id from python
    PyArg_ParseTuple(args, "Os", &dbCapsule, &msgID);

    // retrieve database pointer from pyhton object
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function that deletes poll from database
    db->deletePoll(msgID, &ok);

    // return status flag
    return Py_BuildValue("i", ok);
}

// function that checks if poll ID is in database
PyObject *checkPoll_id(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *msgID;
    int ok = 0;

    // retrieve python object and message id from python
    PyArg_ParseTuple(args, "Os", &dbCapsule, &msgID);

    // retrieve database pointer from pyhton object
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function that checks if message ID is in database
    int exists = (int)db->checkPoll_id(msgID, &ok);

    // return exists flag and status flag
    return Py_BuildValue("ii", exists, ok);
}

// function that deletes a single entry from the database
PyObject *deletePoll_Entry(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *msgID;
    char *role;
    char *emoji;

    // retrieve python object, message ID, role ID, and emoji from python
    PyArg_ParseTuple(args, "Osss", &dbCapsule, &msgID, &role, &emoji);

    // retrieve database pointer from pyhton object
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function that deletes entry from database
    int ok = db->deletePoll_Entry(msgID, role, emoji);

    // return status flag
    return Py_BuildValue("i", ok);
}

// function that adds a warning to the database
PyObject *addWarning(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *ID;
    int ok = 0;

    // retrieve python object and user ID from python
    PyArg_ParseTuple(args, "Os", &dbCapsule, &ID);

    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function that adds a warning to database
    db->addWarning(ID, &ok);

    // return status flag
    return Py_BuildValue("i", ok);
}

// function that retrieves warnings from database
PyObject *retrieveWarnings(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *id;
    int ok = 0;

    // retrieve python object and user id from python
    PyArg_ParseTuple(args, "Os", &dbCapsule, &id);

    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function to retrieve warnings
    int warnings = db->retrieveWarnings(id, &ok);

    // return number of warnings and status flag
    return Py_BuildValue("ii", warnings, ok);
}

// function that removes a warning from database
PyObject *removeWarning(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *ID;
    int ok = 0;

    // retrieve python object and user ID from python
    PyArg_ParseTuple(args, "Os", &dbCapsule, &ID);

    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function that removes a warning to database
    db->removeWarning(ID, &ok);

    // return status flag
    return Py_BuildValue("i", ok);
}

// function that resets the warnings in the database
PyObject *clearWarnings(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *id;
    int ok = 0;

    // retrieve python object and user ID from python
    PyArg_ParseTuple(args, "Os", &dbCapsule, &id);

    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function that clears warnings from database
    db->clearWarnings(id, &ok);

    // return status flag
    return Py_BuildValue("i", ok);
}

// function that configures the guild settings for the bot
PyObject *configureGuild(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *logs;
    char *role;
    char *level;
    char *shame;
    char *watch;

    // retrieve python object, log ID, role ID, level ID
    PyArg_ParseTuple(args, "Osssss", &dbCapsule, &logs, &role, &level, &shame,
                     &watch);

    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function that configures the guild
    int ok = db->configure_guild(logs, role, level, shame, watch);

    // return status flag
    return Py_BuildValue("i", ok);
}

// function that reconfigures the guild settings for the bot
PyObject *reconfigureGuild(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *logs;
    char *role;
    char *levels;
    char *shame;
    char *watch;

    // retrieve python object, log ID, role ID, level ID
    PyArg_ParseTuple(args, "Osssss", &dbCapsule, &logs, &role, &levels, &shame,
                     &watch);

    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function that configures the guild
    int ok = db->reconfigure_guild(logs, role, levels, shame, watch);

    return Py_BuildValue("i", ok);
}

// function that retrieves channel IDs from database
PyObject *getLogsID(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int logIndex;
    int ok;

    // retrieve python object and channel wanted
    PyArg_ParseTuple(args, "Oi", &dbCapsule, &logIndex);

    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    const char *channel_id = db->get_channel_id(logIndex, &ok);

    return Py_BuildValue("si", channel_id, ok);
}

// function that sets the poli message
PyObject *setPoliMsg(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *msgID;

    // retreive arguments from python
    PyArg_ParseTuple(args, "Os", &dbCapsule, &msgID);

    // get object pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // execute function
    int ok = db->set_poli_msg(msgID);

    return Py_BuildValue("i", ok);
}

// function that gets the poli message
PyObject *getPoliMsg(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int ok = 0;

    // retreive args from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get object pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // execute function
    const char *str = db->get_poli_msg(&ok);

    return Py_BuildValue("si", str, ok);
}

// function that clears the poli message
PyObject *clearPoliMsg(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;

    // retreive arguments from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get object pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // execute cpp function
    int ok = db->clear_poli_msg();

    return Py_BuildValue("i", ok);
}

// function that sets poli rules
PyObject *setPoliRules(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *rules;

    // retreive arguments from python
    PyArg_ParseTuple(args, "Os", &dbCapsule, &rules);

    // get object pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // execute cpp function
    int ok = db->setPoliRules(rules);

    return Py_BuildValue("i", ok);
}

// function that gets poli rules
PyObject *getPoliRules(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int ok = 0;

    // retreive arguments from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get object pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // execute cpp function
    const char *rules = db->getPoliRules(&ok);

    return Py_BuildValue("si", rules, ok);
}

// function that clears poli rules
PyObject *clearPoliRules(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;

    // retreive arguments from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get object pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // execute cpp function
    int ok = db->clearPoliRules();

    return Py_BuildValue("i", ok);
}

// function that sets poli rules channel
PyObject *setPoliRulesChannel(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *id;

    // retreive arguments from python
    PyArg_ParseTuple(args, "Os", &dbCapsule, &id);

    // get object pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // execute cpp function
    int ok = db->setPoliChannel(id);

    return Py_BuildValue("i", ok);
}

// function that gets poli rules channel
PyObject *getPoliRulesChannel(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int ok = 0;

    // retreive arguments from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get object pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // execute cpp function
    const char *id = db->getPoliChannel(&ok);

    return Py_BuildValue("si", id, ok);
}

// function that clears poli rules channel
PyObject *clearPoliRulesChannel(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;

    // retreive arguments from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get object pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // execute cpp function
    int ok = db->clearPoliChannel();

    return Py_BuildValue("i", ok);
}

// function that sets the channel prefix for a guild
PyObject *setChannelPrefix(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *prefix;

    // retrieve arguments from python
    PyArg_ParseTuple(args, "Os", &dbCapsule, &prefix);

    // get object pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // execute cpp function
    int ok = db->setChannelPrefix(prefix);

    return Py_BuildValue("i", ok);
}

// function that gets the channel prefix from the database
PyObject *getChannelPrefix(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int ok = 0;

    // retrieve argument from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get object pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // execute cpp function
    const char *prefix = db->getChannelPrefix(&ok);

    return Py_BuildValue("si", prefix, ok);
}

// function that clears the channel prefix from the database
PyObject *clearChannelPrefix(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;

    // retrieve argument from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get object pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // execute cpp function
    int ok = db->clearChannelPrefix();

    return Py_BuildValue("i", ok);
}

// function that inserts user into watchlist
PyObject *insertInWatchlist(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *id;

    // retrieve python object and user id
    PyArg_ParseTuple(args, "Os", &dbCapsule, &id);

    // get object pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok = db->watchlist_insert(id);

    // return value
    return Py_BuildValue("i", ok);
}

// function that checks if user is in watchlist
PyObject *inWatchlist(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *id;
    int ok = 0;

    // retrieve python object and user id
    PyArg_ParseTuple(args, "Os", &dbCapsule, &id);

    // get object pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int watched = (int)db->in_watchlist(id, &ok);

    // return values
    return Py_BuildValue("ii", watched, ok);
}

// Function that checks the status flags in the watchlist
PyObject *watchlistStatus(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *id;
    int poli_ban, monitored, Override, exists;

    // retrieve python object and user id
    PyArg_ParseTuple(args, "Os", &dbCapsule, &id);

    // Get object pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok =
        db->watchlist_status(id, &poli_ban, &monitored, &Override, &exists);

    // return flags
    return Py_BuildValue("iiiii", poli_ban, monitored, Override, exists, ok);
}

// Function that updates the monitor flag
PyObject *updateMonitor(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *id;
    int monitor;

    // get arguments from python
    PyArg_ParseTuple(args, "Osi", &dbCapsule, &id, &monitor);

    // get object
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok = db->update_monitor(id, monitor);

    // return error flag
    return Py_BuildValue("i", ok);
}

// Function that updates the poliban flag
PyObject *updatePoliBan(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *id;
    int poliBan;

    // get arguments from python
    PyArg_ParseTuple(args, "Osi", &dbCapsule, &id, &poliBan);

    // Get object pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok = db->update_poli_ban(id, poliBan);

    // return error flag to python
    return Py_BuildValue("i", ok);
}

// function that updates the override flag
PyObject *updateOverride(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *id;
    int Override;

    // get arguments from python
    PyArg_ParseTuple(args, "Osi", &dbCapsule, &id, &Override);

    // Get object pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok = db->update_override(id, Override);

    // return error flag to python
    return Py_BuildValue("i", ok);
}

// function that inserts new message into database
PyObject *insertMsg(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *channel;
    char *msg;

    // get arguments from python
    PyArg_ParseTuple(args, "Oss", &dbCapsule, &channel, &msg);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok = db->add_message(channel, msg);

    return Py_BuildValue("i", ok);
}

// function that marks a message as deleted in the database
PyObject *markMsgDeleted(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *channel;
    char *msg;

    // get arguments from python
    PyArg_ParseTuple(args, "Oss", &dbCapsule, &channel, &msg);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok = db->user_delete_message(channel, msg);

    return Py_BuildValue("i", ok);
}

// function that returns a list of messages
PyObject *getMessages(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *channel;

    // get python arguments
    PyArg_ParseTuple(args, "Os", &dbCapsule, &channel);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok = 0;
    int length = db->get_messages(channel, &ok);

    PyObject *List = PyList_New(0);

    if (ok == 0 && length != 0)
    {
        // Create python list
        for (int i = 0; i < length; ++i)
        {
            long long msg = db->getMessage();
            PyList_Append(List, PyLong_FromLongLong(msg));
        }
    }

    return Py_BuildValue("Oi", List, ok);
}

// function that deletes messages from database given channel id
PyObject *msgBulkDelete(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *channel;

    // get python args
    PyArg_ParseTuple(args, "Os", &dbCapsule, &channel);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok = db->bulk_delete(channel);

    return Py_BuildValue("i", ok);
}

// function that adds a guild to guild flags table
PyObject *addGuild(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    long long guildID;

    // get args from python
    PyArg_ParseTuple(args, "OL", &dbCapsule, &guildID);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok = db->addGuild(guildID);

    return Py_BuildValue("i", ok);
}

// function that resets flags for guilds
PyObject *resetGuildFlags(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;

    // get args from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok = db->resetGuildFlags();

    return Py_BuildValue("i", ok);
}

// function that sets flags for a given guild
PyObject *setFlags(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int flags;

    // get args from python
    PyArg_ParseTuple(args, "Oi", &dbCapsule, &flags);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok = db->setFlags(flags);

    return Py_BuildValue("i", ok);
}

// function that gets flags for a given guild
PyObject *getFlags(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int ok = 0;

    // get args from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int flags = db->getFlags(&ok);

    return Py_BuildValue("ii", flags, ok);
}

// function that sets archive flag
PyObject *setArchiveFlag(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int flag;

    // get args from python
    PyArg_ParseTuple(args, "Oi", &dbCapsule, &flag);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok = db->setArchiveFlag(flag & 1);

    return Py_BuildValue("i", ok);
}

// function that gets archive flag
PyObject *getArchiveFlag(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int ok = 0;

    // get args from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int flags = db->getArchiveFlag(&ok);

    return Py_BuildValue("ii", flags, ok);
}

// function that sets purge role flag
PyObject *setPurgeRolesFlag(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int flag;

    // get args from python
    PyArg_ParseTuple(args, "Oi", &dbCapsule, &flag);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok = db->setPurgeRoleFlag(flag & 1);

    return Py_BuildValue("i", ok);
}

// function that get purge roles flag
PyObject *getPurgeRolesFlag(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int ok = 0;

    // get args from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int flags = db->getPurgeRoleFlag(&ok);

    return Py_BuildValue("ii", flags, ok);
}

// function that gets the started flag
PyObject *getStartedFlag(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int ok = 0;

    // get args from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // run cpp function
    int running = db->getStartedFlag(&ok);

    return Py_BuildValue("ii", running, ok);
}

// function that sets the started flag
PyObject *setStartedFlag(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int started;

    // get args from python
    PyArg_ParseTuple(args, "Oi", &dbCapsule, &started);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // run cpp function
    int ok = db->setStartedFlag(started);

    return Py_BuildValue("i", ok);
}

// function that gets the number of instances
PyObject *getInstances(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int ok = 0;

    // get args from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // run cpp function
    int instances = db->getInstances(&ok);

    return Py_BuildValue("ii", instances, ok);
}

// function that sets the number of instances
PyObject *setInstances(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int instances;

    // get args from python
    PyArg_ParseTuple(args, "Oi", &dbCapsule, &instances);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // run cpp function
    int ok = db->setInstances(instances);

    return Py_BuildValue("i", ok);
}

// function that gets the clean flag from bot flags
PyObject *getCleanExit(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int ok = 0;

    // get args from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // run cpp function
    int flag = db->getCleanExit(&ok);

    return Py_BuildValue("ii", flag, ok);
}

// function that sets the clean flag in bot flags
PyObject *setCleanExit(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int flag;

    // get args from python
    PyArg_ParseTuple(args, "Oi", &dbCapsule, &flag);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // run cpp function
    int ok = db->setCleanExit(flag);

    return Py_BuildValue("i", ok);
}

// function that gets the top 15 ranked users and the requester's rank
PyObject *getRankLeaders(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *userID;
    int ok = 0;

    // Get args from python
    PyArg_ParseTuple(args, "Os", &dbCapsule, &userID);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // run cpp function
    list<userData> data = db->getAllUsers(&ok);

    // create python list
    PyObject *List = PyList_New(0);
    // error
    if (ok != 0)
        return Py_BuildValue("Oii", List, 0, ok);

    int index;
    // check if there are enough entries for 15 people
    int size = (data.size() > 15) ? 15 : data.size();
    list<userData>::iterator it = data.begin();

    // Insert userIDs into list along with their rank
    for (index = 1; index <= size; ++index)
    {
        PyList_Append(
            List, Py_BuildValue("[Lii]", std::stoll(it->id), it->rank, index));
        it++;
    }

    // get requester's position in the leaderboard
    index = 1;
    for (it = data.begin(); it != data.end(); ++it)
    {
        if (it->id == userID)
            break;
        index++;
    }

    return Py_BuildValue("Oii", List, index, ok);
}

// function that logs warnings
PyObject *logWarnings(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    warningMsgData data;
    char *channel;
    char *userName;
    char *adminName;
    char *reason;
    char *date;

    // get args from python
    PyArg_ParseTuple(args, "OLsLsLsiss", &dbCapsule, &data.guildID, &channel,
                     &data.userID, &userName, &data.adminID, &adminName,
                     &data.ruleViolated, &reason, &date);

    // set remaining fields of struct
    data.channel = channel;
    data.userName = userName;
    data.adminName = adminName;
    data.reason = reason;
    data.date = date;

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok = db->logWarning(data);

    return Py_BuildValue("i", ok);
}

// function that retrieves the warnings from the database based on type
PyObject *getWarningData(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    PyObject *Values;
    int parameters;
    int ok = 0;

    // get args from python
    PyArg_ParseTuple(args, "OiO", &dbCapsule, &parameters, &Values);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // Init cpp list

    PyObject *Value;
    PyObject *List = PyList_New(0);

    vector<string> pyData;
    for (int i = 0; i < PyTuple_Size(Values); ++i)
    {
        Value = PyTuple_GetItem(Values, i);
        Value = PyUnicode_AsASCIIString(Value);
        if (Value != NULL)
        {
            pyData.push_back(PyBytes_AsString(Value));
            Py_XDECREF(Value);
        }
    }

    list<warningMsgData> data;
    // call cpp function
    data = db->getWarnings(&ok, parameters, pyData);

    // convert cpp list to python list
    for (list<warningMsgData>::iterator it = data.begin(); it != data.end();
         ++it)
    {
        PyList_Append(
            List, Py_BuildValue("[LsLsLsiss]", it->guildID, it->channel.c_str(),
                                it->userID, it->userName.c_str(), it->adminID,
                                it->adminName.c_str(), it->ruleViolated,
                                it->reason.c_str(), it->date.c_str()));
    }

    return Py_BuildValue("Oi", List, ok);
}

// function that turns on and off the level system
PyObject *toggleLevelSystem(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int status;

    // get args from python
    PyArg_ParseTuple(args, "Oi", &dbCapsule, &status);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok = db->toggleLevelSystem((bool)status);

    return Py_BuildValue("i", ok);
}

// function that checks if the level system status
PyObject *levelSystemStatus(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int ok = 0;

    // get args from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    bool status = db->levelSystemStatus(&ok);

    return Py_BuildValue("ii", (int)status, ok);
}

// function that enables a cog
PyObject *enableCog(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    uint32_t Cog;

    // get args from python
    PyArg_ParseTuple(args, "OI", &dbCapsule, &Cog);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok = db->enableCog((uint8_t)Cog);

    return Py_BuildValue("i", ok);
}

// function that disables a cog
PyObject *disableCog(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    uint32_t Cog;

    // get args from python
    PyArg_ParseTuple(args, "OI", &dbCapsule, &Cog);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok = db->disableCog((uint8_t)Cog);

    return Py_BuildValue("i", ok);
}

// function that gets the cog flags
PyObject *checkCogs(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int ok = 0;

    // get args from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    unsigned int Cogs = (int)db->checkCogs(&ok);

    return Py_BuildValue("Ii", Cogs, ok);
}

// function that set the server configured flag
PyObject *setServerConfiguredFlag(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int flag;

    // get args from python
    PyArg_ParseTuple(args, "Oi", &dbCapsule, &flag);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok = db->setConfiguredFlag(flag);

    return Py_BuildValue("i", ok);
}

// function that get the server configured flag
PyObject *getServerConfiguredFlag(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int ok = 0;

    // get args from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int flag = db->getConfiguredFlag(&ok);

    return Py_BuildValue("ii", flag, ok);
}

// function that demotes a member
PyObject *demoteMember(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *id;
    unsigned int numLevels;
    int ok = 0;

    // get args from python
    PyArg_ParseTuple(args, "Osi", &dbCapsule, &id, &numLevels);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    db->demoteUser(id, numLevels, &ok);

    return Py_BuildValue("i", ok);
}

// function that sets the ban message for a server
PyObject *setBanMsg(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *msg;

    // get args from python
    PyArg_ParseTuple(args, "Os", &dbCapsule, &msg);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok = db->setBanMsg(msg);

    return Py_BuildValue("i", ok);
}

// function that clears the ban message for a server
PyObject *clearBanMsg(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;

    // get args from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok = db->clearBanMsg();

    return Py_BuildValue("i", ok);
}

// function that gets the ban message for a server
PyObject *getBanMsg(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int ok = 0;

    // get args from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    const char *msg = db->getBanMsg(&ok);

    return Py_BuildValue("si", msg, ok);
}

// function that inserts a user into the quiz data table
PyObject *insertQuizUser(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *userID;

    // get args from python
    PyArg_ParseTuple(args, "Os", &dbCapsule, &userID);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok = db->insertQuizUser(userID);

    return Py_BuildValue("i", ok);
}

// function that updates the user quiz data
PyObject *updateQuizData(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *userID;
    unsigned int newData;

    // get args from python
    PyArg_ParseTuple(args, "OsI", &dbCapsule, &userID, &newData);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    int ok = db->updateQuizData(userID, newData);

    return Py_BuildValue("i", ok);
}

// function that gets individual stats for quiz data
PyObject *fetchQuizEntry(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    char *userID;
    int ok = 0;

    // get args from python
    PyArg_ParseTuple(args, "Os", &dbCapsule, &userID);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function}
    quizData_t qData = db->fetchIndividualQuizData(userID, &ok);

    PyObject *quizData = Py_BuildValue(
        "[LIIIIIIIIII]", qData.userID, qData.veryEasyCorrect,
        qData.veryEasyQuestions, qData.easyCorrect, qData.easyQuestions,
        qData.normalCorrect, qData.normalQuestions, qData.hardCorrect,
        qData.hardQuestions, qData.veryHardCorrect, qData.veryHardQuestions);

    return Py_BuildValue("Oi", quizData, ok);
}

// function that gets all stats for quiz data
PyObject *fetchQuizEntries(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int ok = 0;

    // get args from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    list<quizData_t> qData = db->fetchQuizData(&ok);

    // instantiate empty list
    PyObject *quizData = PyList_New(0);

    if (ok == 0)
    {
        // convert cpp list into python list
        for (list<quizData_t>::iterator it = qData.begin(); it != qData.end();
             ++it)
        {
            PyList_Append(quizData,
                          Py_BuildValue("[LIIIIIIIIII]", it->userID,
                                        it->veryEasyCorrect,
                                        it->veryEasyQuestions, it->easyCorrect,
                                        it->easyQuestions, it->normalCorrect,
                                        it->normalQuestions, it->hardCorrect,
                                        it->hardQuestions, it->veryHardCorrect,
                                        it->veryHardQuestions));
        }
    }

    return Py_BuildValue("Oi", quizData, ok);
}

// function that retrieves the server data for a specific table
PyObject *requestServerData(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;
    int table;
    int ok = 0;

    // get args from python
    PyArg_ParseTuple(args, "Oi", &dbCapsule, &table);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    list<string> data = db->requestData(static_cast<Tables>(table), &ok);

    // create empty list
    PyObject *List = PyList_New(0);

    if (!ok)
    {
        // get data header
        const char *header = db->getDataHeader();
        // add the data header to the list
        PyList_Append(List, Py_BuildValue("s", header));

        // loop through the data to convert it from C++ list to python list
        for (list<string>::iterator it = data.begin(); it != data.end(); ++it)
        {
            PyList_Append(List, Py_BuildValue("s", it->c_str()));
        }
    }

    return Py_BuildValue("Oi", List, ok);
}

// function wrapper for itoa
PyObject *base_repr(PyObject *self, PyObject *args)
{
    int64_t num;
    int32_t radix;
    char buffer[65];

    // get integer from python
    PyArg_ParseTuple(args, "Li", &num, &radix);

    char *err = itoa(num, buffer, radix);

    if (!err)
    {
        PyErr_SetString(PyExc_ValueError, "base must be >= 2 and <= 36, or 0");
        return (PyObject *)NULL;
    }

    return Py_BuildValue("s", buffer);
}
// function that creates a new easter egg
PyObject *createEasterEgg(PyObject *, PyObject *args)
{
    PyObject *dbCapsule;
    char *phrase;
    char *filename;
    int filter, ok;
    char *file_buffer;
    Py_ssize_t filesize;
    Blob_data_t blob_data;

    // Parse arguments from python
    PyArg_ParseTuple(args, "Ossis#", &dbCapsule, &phrase, &filename, &filter,
                     &file_buffer, &filesize);

    // set blob data
    blob_data.phrase = phrase;
    blob_data.filename = filename;
    blob_data.filter = (bool)filter;
    blob_data.fileContents = (void *)file_buffer;
    blob_data.filesize = (size_t)filesize;

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    ok = db->createEasterEgg(blob_data);

    return Py_BuildValue("i", ok);
}

// function to set the role color
PyObject *setRoleColor(PyObject *, PyObject *args)
{
    PyObject *dbCapsule;
    unsigned int R, G, B;
    int ok;

    // get args from python
    PyArg_ParseTuple(args, "OIII", &dbCapsule, &R, &G, &B);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    ok = db->setRoleColor(R, G, B);

    return Py_BuildValue("i", ok);
}

// function that removes an easter egg from the database
PyObject *removeEasterEgg(PyObject *, PyObject *args)
{
    PyObject *dbCapsule;
    char *phrase;
    int ok;

    // parse arguments from python
    PyArg_ParseTuple(args, "Os", &dbCapsule, &phrase);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    ok = db->removeEasterEgg(phrase);

    return Py_BuildValue("i", ok);
}

// function that gets all the blob data for a server
PyObject *fetchEasterEggs(PyObject *, PyObject *args)
{
    PyObject *dbCapsule;
    int ok = 0;

    // parse arguments from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    list<Blob_data_t> blobs = db->fetchEasterEggs(&ok);

    // create python list
    PyObject *blob_meta = PyList_New(0);

    if (!ok)
    {
        PyObject *bytesBuffer;
        PyObject *newDict;
        // copy list to python list
        for (list<Blob_data_t>::iterator it = blobs.begin(); it != blobs.end();
             ++it)
        {
            // create bytes object
            bytesBuffer = PyBytes_FromStringAndSize(
                reinterpret_cast<char *>(it->safeFileContents.data()),
                (Py_ssize_t)it->filesize);
            // Build new dictionary
            newDict =
                Py_BuildValue("{s:s,s:s,s:i,s:O}", "phrase", it->phrase.c_str(),
                              "filename", it->filename.c_str(), "Limited",
                              (int)it->filter, "file", bytesBuffer);
            // append blob data to list
            PyList_Append(blob_meta, newDict);
        }
    }

    return Py_BuildValue("Oi", blob_meta, ok);
}
// function to get the role color
PyObject *getRoleColor(PyObject *, PyObject *args)
{
    PyObject *dbCapsule;
    uint8_t R = 0, G = 0, B = 0;
    int ok;

    // get args from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));

    // call cpp function
    ok = db->getRoleColor(&R, &G, &B);

    return Py_BuildValue("IIIi", R, G, B, ok);
}

PyObject *clearRoleColor(PyObject *, PyObject *args)
{
    PyObject *dbCapsule;
    int ok;

    // get args from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // get pointer
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // call cpp function
    ok = db->clearRoleColor();

    return Py_BuildValue("i", ok);
}

// function that deletes database manager object
PyObject *destroy(PyObject *self, PyObject *args)
{
    PyObject *dbCapsule;

    // retrieve python object from python
    PyArg_ParseTuple(args, "O", &dbCapsule);

    // retrieve database pointer from pyhton object
    EE_Bot_DB *db =
        static_cast<EE_Bot_DB *>(PyCapsule_GetPointer(dbCapsule, "EE_Bot_DB"));
    // free memory allocated to database manager
    delete db;

    // return nothing
    return Py_BuildValue("");
}

// List of python wrapper functions
PyMethodDef EE_Bot_DB_Func[] = {
    {"construct", construct, METH_VARARGS, NULL},

    {"__version__", version, METH_NOARGS, NULL},

    {"setupGuildFlags", setupGuildFlags, METH_VARARGS, NULL},

    {"insertUser", insertUser, METH_VARARGS, NULL},

    {"deleteUser", deleteUser, METH_VARARGS, NULL},

    {"updateUserPosts", updateUserPosts, METH_VARARGS, NULL},

    {"updateUserFilter", updateUserFilter, METH_VARARGS, NULL},

    {"checkUserFilter", checkUserFilter, METH_VARARGS, NULL},

    {"updateNotifications", updateNotifications, METH_VARARGS, NULL},

    {"createChannel", createChannel, METH_VARARGS, NULL},

    {"deleteChannel", deleteChannel, METH_VARARGS, NULL},

    {"adjustLevel", adjustRank, METH_VARARGS, NULL},

    {"retreiveUserRank", retreiveUserRank, METH_VARARGS, NULL},

    {"createNewPoll", createNewPoll, METH_VARARGS, NULL},

    {"retrievePoll", retrievePoll, METH_VARARGS, NULL},

    {"getMsgID", getMsgID, METH_VARARGS, NULL},

    {"getRoleID", getRoleID, METH_VARARGS, NULL},

    {"getEmoji_id", getEmoji_id, METH_VARARGS, NULL},

    {"clearEntry", clearEntry, METH_VARARGS, NULL},

    {"deletePoll", deletePoll, METH_VARARGS, NULL},

    {"checkPoll_id", checkPoll_id, METH_VARARGS, NULL},

    {"deletePoll_Entry", deletePoll_Entry, METH_VARARGS, NULL},

    {"addWarning", addWarning, METH_VARARGS, NULL},

    {"retrieveWarnings", retrieveWarnings, METH_VARARGS, NULL},

    {"removeWarning", removeWarning, METH_VARARGS, NULL},

    {"clearWarnings", clearWarnings, METH_VARARGS, NULL},

    {"configureGuild", configureGuild, METH_VARARGS, NULL},

    {"reconfigureGuild", reconfigureGuild, METH_VARARGS, NULL},

    {"getLogsID", getLogsID, METH_VARARGS, NULL},

    {"setPoliMsg", setPoliMsg, METH_VARARGS, NULL},

    {"getPoliMsg", getPoliMsg, METH_VARARGS, NULL},

    {"clearPoliMsg", clearPoliMsg, METH_VARARGS, NULL},

    {"setPoliRules", setPoliRules, METH_VARARGS, NULL},

    {"getPoliRules", getPoliRules, METH_VARARGS, NULL},

    {"clearPoliRules", clearPoliRules, METH_VARARGS, NULL},

    {"setPoliRulesChannel", setPoliRulesChannel, METH_VARARGS, NULL},

    {"setChannelPrefix", setChannelPrefix, METH_VARARGS, NULL},

    {"getChannelPrefix", getChannelPrefix, METH_VARARGS, NULL},

    {"clearChannelPrefix", clearChannelPrefix, METH_VARARGS, NULL},

    {"getPoliRulesChannel", getPoliRulesChannel, METH_VARARGS, NULL},

    {"clearPoliRulesChannel", clearPoliRulesChannel, METH_VARARGS, NULL},

    {"insertIntoWatchlist", insertInWatchlist, METH_VARARGS, NULL},

    {"inWatchlist", inWatchlist, METH_VARARGS, NULL},

    {"watchlistStatus", watchlistStatus, METH_VARARGS, NULL},

    {"updateMonitor", updateMonitor, METH_VARARGS, NULL},

    {"updatePoliBan", updatePoliBan, METH_VARARGS, NULL},

    {"updateOverride", updateOverride, METH_VARARGS, NULL},

    {"destroyDB", destroy, METH_VARARGS, NULL},

    {"insertMsg", insertMsg, METH_VARARGS, NULL},

    {"markMsgDeleted", markMsgDeleted, METH_VARARGS, NULL},

    {"getMessages", getMessages, METH_VARARGS, NULL},

    {"msgBulkDelete", msgBulkDelete, METH_VARARGS, NULL},

    {"addGuild", addGuild, METH_VARARGS, NULL},

    {"resetGuildFlags", resetGuildFlags, METH_VARARGS, NULL},

    {"setFlags", setFlags, METH_VARARGS, NULL},

    {"getFlags", getFlags, METH_VARARGS, NULL},

    {"setArchiveFlag", setArchiveFlag, METH_VARARGS, NULL},

    {"getArchiveFlag", getArchiveFlag, METH_VARARGS, NULL},

    {"setPurgeRolesFlag", setPurgeRolesFlag, METH_VARARGS, NULL},

    {"getPurgeRolesFlag", getPurgeRolesFlag, METH_VARARGS, NULL},

    {"getStartedFlag", getStartedFlag, METH_VARARGS, NULL},

    {"setStartedFlag", setStartedFlag, METH_VARARGS, NULL},

    {"getInstances", getInstances, METH_VARARGS, NULL},

    {"setInstances", setInstances, METH_VARARGS, NULL},

    {"getCleanExit", getCleanExit, METH_VARARGS, NULL},

    {"setCleanExit", setCleanExit, METH_VARARGS, NULL},

    {"getRankLeaders", getRankLeaders, METH_VARARGS, NULL},

    {"logWarnings", logWarnings, METH_VARARGS, NULL},

    {"getWarningData", getWarningData, METH_VARARGS, NULL},

    {"toggleLevelSystem", toggleLevelSystem, METH_VARARGS, NULL},

    {"levelSystemStatus", levelSystemStatus, METH_VARARGS, NULL},

    {"enableCog", enableCog, METH_VARARGS, NULL},

    {"disableCog", disableCog, METH_VARARGS, NULL},

    {"checkCogs", checkCogs, METH_VARARGS, NULL},

    {"setServerConfiguredFlag", setServerConfiguredFlag, METH_VARARGS, NULL},

    {"getServerConfiguredFlag", getServerConfiguredFlag, METH_VARARGS, NULL},

    {"demoteMember", demoteMember, METH_VARARGS, NULL},

    {"setBanMsg", setBanMsg, METH_VARARGS, NULL},

    {"clearBanMsg", clearBanMsg, METH_VARARGS, NULL},

    {"getBanMsg", getBanMsg, METH_VARARGS, NULL},

    {"insertQuizUser", insertQuizUser, METH_VARARGS, NULL},

    {"updateQuizData", updateQuizData, METH_VARARGS, NULL},

    {"fetchQuizEntry", fetchQuizEntry, METH_VARARGS, NULL},

    {"fetchQuizEntries", fetchQuizEntries, METH_VARARGS, NULL},

    {"requestServerData", requestServerData, METH_VARARGS, NULL},

    {"base_repr", base_repr, METH_VARARGS, NULL},

    {"createEasterEgg", createEasterEgg, METH_VARARGS, NULL},

    {"setRoleColor", setRoleColor, METH_VARARGS, NULL},

    {"removeEasterEgg", removeEasterEgg, METH_VARARGS, NULL},

    {"getRoleColor", getRoleColor, METH_VARARGS, NULL},

    {"fetchEasterEggs", fetchEasterEggs, METH_VARARGS, NULL},

    {"clearRoleColor", clearRoleColor, METH_VARARGS, NULL},

    {NULL, NULL, 0, NULL}};

// Python module structure
struct PyModuleDef EE_Bot_DB_mod = {PyModuleDef_HEAD_INIT, "_EE_Bot_DB", NULL,
                                    -1, EE_Bot_DB_Func};

// function to initialize python module
PyMODINIT_FUNC PyInit__EE_Bot_DB(void)
{
    return PyModule_Create(&EE_Bot_DB_mod);
}