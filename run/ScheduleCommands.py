# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import discord
from discord.ext import tasks
import asyncio
from discord.commands import slash_command
from discord.commands import Option
from enum import IntEnum
from datetime import datetime
from datetime import timedelta
import pytz
from EE_Bot_DB import EE_DB
from EE_Bot_DB import channelType
from EE_Bot_DB import cogEnum
import pickle as pk
from globals import embedColor


# Autocomplete choices for date of execution
monthChoices = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
dayChoices = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
              13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
              23, 24, 25, 26, 27, 28, 29, 30, 31]


# Autocomplete choices for times of execution and timezones
minuteChoices = list(range(60))
hourChoices = list(range(24))
timezoneChoices = pytz.common_timezones


class taskEnum(IntEnum):
    """Enumerated type for tasks"""
    NONE = 0
    ARCHIVE = 1
    CLEAR_ROLES = 2


class taskData:
    """Class that contains the task data"""
    def __init__(self, taskType: taskEnum, channel: discord.TextChannel, role: discord.Role):
        self.__type = taskType
        # If archive task, check for channel
        if self.__type == taskType.ARCHIVE:
            if channel is None:
                raise RuntimeError('Channel must be provided for archive tasks!')
        if channel is not None:
            self.__channel = channel.id
        else:
            self.__channel = None
        if role is not None:
            self.__role = role.id
        else:
            self.__role = None
        self._complete = False

    # ==
    def __eq__(self, item):
        # Check if the types are equivalent
        if type(item) != type(self):
            return False
        # Check if the data is the same
        return (item.__type == self.__type and 
                item.__role == self.__role and
                item.__channel == self.__channel)

    def __bool__(self):
        return self._complete

    @property
    def channel(self):
        return self.__channel

    @property
    def taskType(self):
        return self.__type

    @property
    def role(self):
        return self.__role

    def markComplete(self):
        self._complete = True


class taskQueue:
    """A custom queue to contain the tasks"""
    def __init__(self, guild: int):
        self._guild = guild
        self._tasks = []
        self._locked = False
        self._dateExecution = None

    # int()
    def __int__(self):
        return self._guild

    # len()
    def __len__(self):
        return len(self._tasks)

    # iterator
    def __iter__(self):
        for elem in self._tasks:
            yield elem

    # taskQueue[index]
    def __getitem__(self, index):
        return self._tasks[index]

    # del taskQueue[index]
    def __delitem__(self, index):
        del self._tasks[index]

    # x in taskQueue
    def __contains__(self, item):
        return item in self._tasks

    # bool()
    def __bool__(self):
        return self._locked

    # ==
    def __eq__(self, guild):
        return guild == self._guild

    # Clear the task queue
    def clear(self):
        if not self._locked:
            self._tasks.clear()

    # Create a new archive task
    def newArchive(self, channel: discord.TextChannel):
        data = taskData(taskEnum.ARCHIVE, channel, None)
        # check for duplicates
        if data not in self._tasks:
            # Add item
            self._tasks.append(data)
            # Return response message
            return 'Created a new Archive task!'
        return 'Cannot duplicate tasks!'

    # Create new clear role task
    def newClearRoles(self, role: discord.Role = None):
        data = taskData(taskEnum.CLEAR_ROLES, None, role)
        # Check for duplicates
        if data not in self._tasks:
            # Add item
            self._tasks.append(data)
            # Return response message
            return 'Created a new remove role task!'
        return 'Cannot duplicate tasks!'

    @property
    def locked(self):
        return self._locked

    @property
    def date(self):
        return self._dateExecution

    # Unlock the Queue
    def unlock(self):
        self._locked = False
        self._dateExecution = None

    # Function that checks if the tasks can be scheduled
    def scheduleTasks(self, month: int, day: int):
        # Check if locked
        if self._locked:
            return 'Tasks are already scheduled!'
        # Get the current date
        now = datetime.now()
        try:
            # Determine if date given is valid
            if now.month == 12 and now.day > 11 and month < 12:
                dt = datetime(now.year + 1, month, day)
            else:
                dt = datetime(now.year, month, day)
        except Exception:
            return 'Invalid date given!'

        # If same day, schedule for next day
        if (dt - now).total_seconds() > -86400:
            if (dt - now).total_seconds() < 0:
                dt += timedelta(days=1)

        # Check if the date has passed
        if (dt - now).total_seconds() < 0:
            return 'Date has already passed!'

        # Check if day given is 20 or more days away
        if (dt - now).total_seconds() > 1728000:
            return 'Cannot schedule a task that is more than 20 days away!'

        # Check if there's contents in the task queue
        if not self._tasks:
            return 'No tasks given!'

        # Set locked flag and date of execution
        self._locked = True
        self._dateExecution = dt
        return 'Scheduling tasks...'

    # Marks a task as complete
    def markTaskComplete(self, index):
        self._task[index].markComplete()

    # Removes completed tasks from queue
    def clearCompletedTasks(self):
        completed_tasks = [x for x in self._tasks if bool(x)]
        self._tasks = [x for x in self._tasks if x not in completed_tasks]


class Announcement:
    """Custom container class to hold announcement data"""
    def __init__(self, guild: int, time: str, timezone: str, msg: str, channel_id: int = -1):
        self._msg = msg
        self._guild = guild
        self._channel_id = channel_id
        local = pytz.timezone(timezone)
        now = datetime.now()
        native = datetime.strptime(f'{time} {now: %m-%d-%Y}', '%H:%M %m-%d-%Y')
        local_dt = local.localize(native, is_dst=None)
        self._utc_time = local_dt.astimezone(pytz.utc)
        # Recalculate if time has already passed today (aka move to next day)
        if (self._utc_time - datetime.now(pytz.utc)).total_seconds() < 0:
            self._utc_time += timedelta(days=1)

    # int()
    def __int__(self):
        return self._guild

    # str()
    def __str__(self):
        return self._msg

    # ==
    def __eq__(self, guild):
        return self._guild == guild

    @property
    def time(self):
        return self._utc_time

    @property
    def channel(self):
        return self._channel_id


class Schedule(discord.Cog):
    """Category for scheduling commands"""
    def __init__(self, client, database, launchFlag=False):
        self.client = client
        self.database = database
        self._taskQueues = []
        self._maxLength = 35
        self._runningTasks = []
        self._changed = False
        self._cancelMsg = 'Tasks cancelled'
        self._DEBUG = not launchFlag
        self._announcements = []
        self._announcement_tasks = []
        self._Announcements_changed = False
        self._announcement_cancel_msg = 'Announcement cancelled'

    # Command that creates tasks and adds it onto the queue
    @slash_command(description='Create a task')
    async def create_task(
        self, 
        ctx,
        task: Option(str, 'Task type', choices = ['archive', 'clear roles']),
        channel: Option(discord.TextChannel, 'Channel that gets archived', default = None, required = False),
        role: Option(discord.Role, 'Role that gets cleared. This is optional.', default = None, required = False)
    ):
        # Check if command was ran inside a guild
        if ctx.guild is None:
            await ctx.respond('Cannot run this command in DMs')
            return
        # Check if the cog is enabled
        db = EE_DB(self.database, ctx.guild.id)
        if not db.checkCog(cogEnum.Schedule):
            await ctx.respond('This command has been disabled by the server owner!')
            return
        # check if the user has admin permissions
        if not ctx.author.guild_permissions.administrator:
            await ctx.respond('You do not have permission to use this command')
            return

        # Get the index of the queue
        try:
            index = self._taskQueues.index(ctx.guild.id)
        except ValueError:
            # If queue does not exist, create one
            self._taskQueues.append(taskQueue(ctx.guild.id))
            index = len(self._taskQueues) - 1

        if len(self._taskQueues[index]) >= self._maxLength:
            await ctx.respond('You cannot insert anymore tasks into the queue!')
            return

        # Check the task type
        if task == 'archive':
            # Check if channel was given
            if channel is None:
                await ctx.respond('Channel is needed for archive task!')
                return

            # Check if queue is locked and add new archive task
            if not bool(self._taskQueues[index]):
                await ctx.respond(self._taskQueues[index].newArchive(channel))
                self._changed = True
                return
            await ctx.respond('Unable to add archive task! The queue is locked. To unlock the queue, run cancel_tasks command.')
            return
        elif task == 'clear roles':
            # Check if queue is locked and add new clear roles task
            if not bool(self._taskQueues[index]):
                await ctx.respond(self._taskQueues[index].newClearRoles(role))
                self._changed = True
                return
            await ctx.respond('Unable to add clear roles task! The queue is locked. To unlock the queue, run cancel_tasks command.')
            return
        return

    # Command that allows user to view task queue
    @slash_command(description='View the task queue')
    async def view_tasks(self, ctx):
        # Check if ran in DM
        if ctx.guild is None:
            await ctx.respond('Cannot run this command in DMs')
            return
        # Check if cog has been enabled
        db = EE_DB(self.database, ctx.guild.id)
        if not db.checkCog(cogEnum.Schedule):
            await ctx.respond('This command has been disabled by the server owner!')
            return
        # Check if user is an admin
        if not ctx.author.guild_permissions.administrator:
            await ctx.respond('You do not have permission to use this command')
            return
        # get task queue
        try:
            index = self._taskQueues.index(ctx.guild.id)
        except ValueError:
            # No task queue found
            embed=discord.Embed(title="Task Queue", description="There are no tasks in the queue.", color=embedColor)
            await ctx.respond(embed=embed)
            return
        embed=discord.Embed(title="Task Queue", color=embedColor)
        # Display task queue in embed
        i = 1
        for x in self._taskQueues[index]:
            if x.taskType == taskEnum.ARCHIVE:
                param = f'Archive <#{x.channel}>'
            elif x.taskType == taskEnum.CLEAR_ROLES:
                if x.role is None:
                    param = 'Clear roles'
                else:

                    param = f'Clear roles <@&{x.role}>'
            else:
                param = 'None'
            name = f'{i}'
            embed.add_field(name=name, value=param, inline=False)
            i += 1
        # Set the thumbnail and respond
        embed.set_thumbnail(url=self.client.user.display_avatar.url)
        await ctx.respond(embed=embed)
        return

    # Command that removes a task from the task queue
    @slash_command(description='Removes a task from the task queue given a task id')
    async def remove_task(
        self,
        ctx,
        task_id: Option(int, 'The task id given by view_tasks')
    ):
        # Check if in guild
        if ctx.guild is None:
            await ctx.respond('Cannot run this command in DMs')
            return
        # Check if cog is enabled
        db = EE_DB(self.database, ctx.guild.id)
        if not db.checkCog(cogEnum.Schedule):
            await ctx.respond('This command has been disabled by the server owner!')
            return
        # Check if user is an admin
        if not ctx.author.guild_permissions.administrator:
            await ctx.respond('You do not have permission to use this command')
            return
        # Get index of the queue
        try:
            index = self._taskQueues.index(ctx.guild.id)
        except ValueError:
            await ctx.respond('No tasks in the queue!')
            return
        # Check if task id is valid
        if task_id < 1 or task_id > len(self._taskQueues[index]):
            await ctx.respond('Task id not found!')
            return
        # Check if the queue is locked
        if bool(self._taskQueues[index]):
            await ctx.respond('Unable to remove task. The queue is locked. To unlock the queue, run cancel_tasks command.')
            return
        # Delete task from queue or the entire queue depending on queue size
        if len(self._taskQueues[index]) != 1:
            del self._taskQueues[index]
        else:
            del self._taskQueues[index][task_id - 1]
        await ctx.respond('Task removed from queue')
        self._changed = True
        return

    # Command that clears the task queue
    @slash_command(description='Clears the task queue')
    async def clear_tasks(self, ctx):
        # Check if ran in server
        if ctx.guild is None:
            await ctx.respond('Cannot run this command in DMs')
            return
        # Check if cog is enabled
        db = EE_DB(self.database, ctx.guild.id)
        if not db.checkCog(cogEnum.Schedule):
            await ctx.respond('This command has been disabled by the server owner!')
            return
        # Check if user is an admin
        if not ctx.author.guild_permissions.administrator:
            await ctx.respond('You do not have permission to use this command')
            return
        # Find the task queue
        try:
            index = self._taskQueues.index(ctx.guild.id)
        except ValueError:
            await ctx.respond('No tasks in the queue!')
            return
        # check if queue is locked
        if bool(self._taskQueues[index]):
            await ctx.respond('Unable to clear tasks. The queue is locked. To unlock the queue, run cancel_tasks command.')
            return
        # Delete task queue
        del self._taskQueues[index]
        await ctx.respond('Cleared task queue')
        self._changed = True
        return

    # Command that schedules the tasks for execution
    @slash_command(description='Schedules a time for the task queue to execute.')
    async def schedule_tasks(
        self,
        ctx,
        month: Option(int, 'Month of task execution', choices = monthChoices),
        day: Option(int, 'Day of task execution', autocomplete = discord.utils.basic_autocomplete(dayChoices)),
        announce: Option(str, 'Announce to the server?', choices = ['yes', 'no']),
        announcement_ch: Option(discord.TextChannel, 'Specify the channel for the bot to post to?', required = False, default = None)
    ):
        # check if in guild
        if ctx.guild is None:
            await ctx.respond('Cannot run this command in DMs')
            return
        # Check if cog is enabled
        db = EE_DB(self.database, ctx.guild.id)
        if not db.checkCog(cogEnum.Schedule):
            await ctx.respond('This command has been disabled by the server owner!')
            return
        # Check if admin
        if not ctx.author.guild_permissions.administrator:
            await ctx.respond('You do not have permission to use this command')
            return
        # Get index of queue
        try:
            index = self._taskQueues.index(ctx.guild.id)
        except ValueError:
            await ctx.respond('Task queue is empty!')
            return

        # Check if tasks are already scheduled
        if bool(self._taskQueues[index]):
            await ctx.respond('Tasks are already scheduled!')
            return

        # Run queue checks
        Response = self._taskQueues[index].scheduleTasks(month, day)

        await ctx.respond(Response)

        # Check if queue got locked
        if bool(self._taskQueues[index]):
            # Create task
            task = asyncio.create_task(self.runTasks(ctx, self._taskQueues[index].date))
            # Append task and guild id to running tasks list
            self._runningTasks.append([ctx.guild.id, task])
            self._changed = True

            # Check if user wants to announce that the tasks have been scheduled
            if announce == 'yes':
                suffix = {1: "st", 2: "nd", 3: "rd"}
                dt = self._taskQueues[index].date
                # Create an embed
                embed = discord.Embed(title='Tasks scheduled for Execution', description=f"@everyone, the following tasks are scheduled for exection on {dt.strftime('%B')} {dt.day}{suffix.get(dt.day % 10, 'th')}.\n\u200b", color=embedColor)
                # Add tasks to embed
                for x in self._taskQueues[index]:
                    if x.taskType == taskEnum.ARCHIVE:
                        taskType = 'Archive'
                        param = f'<#{x.channel}>'
                    elif x.taskType == taskEnum.CLEAR_ROLES:
                        taskType = 'Clear roles'
                        if x.role is None:
                            param = '\u200b'
                        else:
                            param = f'<@&{x.role}>'
                    embed.add_field(name=taskType, value=param, inline=False)

                # If announcement channel was not given, find one
                if announcement_ch is None:
                    # Find announcement channel
                    for ch in ctx.guild.text_channels:
                        if ch.type == discord.ChannelType.news:
                            announcement_ch = x
                            break
                    # Otherwise just use first channel
                    if announcement_ch is None:
                        announcement_ch = ctx.guild.text_channels[0]
                embed.set_thumbnail(url=self.client.user.display_avatar.url)
                await announcement_ch.send(embed=embed)

    # Command that cancels the tasks
    @slash_command(description='Cancel the scheduled tasks')
    async def cancel_tasks(self, ctx):
        # Check if in guild
        if ctx.guild is None:
            await ctx.respond('Cannot run this command in DMs')
            return
        # Check if the cog is enabled
        db = EE_DB(self.database, ctx.guild.id)
        if not db.checkCog(cogEnum.Schedule):
            await ctx.respond('This command has been disabled by the server owner!')
            return
        # Check if user is an admin
        if not ctx.author.guild_permissions.administrator:
            await ctx.respond('You do not have permission to use this command')
            return
        # Get queue index
        try:
            index = self._taskQueues.index(ctx.guild.id)
        except ValueError:
            await ctx.respond('Task Queue is empty!')
            return

        # check if queue is scheduled for execution
        if not bool(self._taskQueues[index]):
            await ctx.respond('Tasks were not scheduled!')
            return

        # Find running task associated with guild
        for x in self._runningTasks:
            if ctx.guild.id in x:
                # Cancel task, unlock the Queue, and remove task from running tasks list
                x[1].cancel()
                self._taskQueues[index].unlock()
                self._runningTasks.remove(x)
                break
        await ctx.respond('Tasks cancelled!')
        self._changed = True

    #Command that creates an announcement
    @slash_command(description='Schedules an announcement for the server.')
    async def schedule_announcement(
        self,
        ctx,
        msg: Option(str, 'The announcement'),
        hour: Option(int, 'The hour of the day (0-23) to make the announcement', autocomplete = discord.utils.basic_autocomplete(hourChoices)),
        minute: Option(int, 'The minute of the hour (0-59) to make the announcement', autocomplete = discord.utils.basic_autocomplete(minuteChoices)),
        timezone: Option(str, "The timezone you're referencing.", autocomplete = discord.utils.basic_autocomplete(timezoneChoices), default = 'UTC', required = False),
        channel: Option(discord.TextChannel, 'The channel to make the announcement in.', default = None, required = False)
    ):
        # Check if ran in server
        if ctx.guild is None:
            await ctx.respond('Must be in a server to run this command!')
            return
        db = EE_DB(self.database, ctx.guild.id)
        # Check if command is enabled
        if not db.checkCog(cogEnum.Schedule):
            await ctx.respond('This command has been disabled by the server owner!')
            return
        # Check user permissions
        if not ctx.author.guild_permissions.administrator:
            await ctx.respond('You do not have permission to do this!')
            return
        # Check if announcement is already scheduled
        if ctx.guild.id in self._announcements:
            await ctx.respond('This server already has an announcement scheduled.')
            return
        # Check if times are in range
        if hour not in hourChoices or minute not in minuteChoices:
            await ctx.respond('Invalid input for time')
            return
        # Check if timezone is valid
        if timezone not in timezoneChoices:
            await ctx.respond('Invalid timezone')
            return
        ch = -1
        # Check if channel was given
        if channel is not None:
            ch = channel.id
        LOG = db.getLoggingID(channelType.log)
        # Check if logging channel has been set up
        if self.client.get_channel(LOG) is None:
            await ctx.respond('Please run /configure first!')
            return
        # Create announcement task
        anncm = Announcement(ctx.guild.id, f'{hour}:{minute}', timezone, msg, ch)
        self._announcements.append(anncm)
        self._Announcements_changed = True
        task = asyncio.create_task(self.runAnnouncement(anncm))
        self._announcement_tasks.append([ctx.guild.id, task])
        await ctx.respond(f'Announcement scheduled for {hour}:{minute}')
        return

    #Command that cancels an announcement
    @slash_command(description='Cancels a scheduled announcement for the server.')
    async def cancel_announcement(self, ctx):
        # Check if in guild
        if ctx.guild is None:
            await ctx.respond('Cannot run this command in DMs')
            return
        # Check if the cog is enabled
        db = EE_DB(self.database, ctx.guild.id)
        if not db.checkCog(cogEnum.Schedule):
            await ctx.respond('This command has been disabled by the server owner!')
            return
        # Check if user is an admin
        if not ctx.author.guild_permissions.administrator:
            await ctx.respond('You do not have permission to use this command')
            return
        # Check for scheduled announcement
        if ctx.guild.id not in self._announcements:
            await ctx.respond('There are no scheduled announcements!')
            return
        # Search for announcement task
        for x in self._announcement_tasks:
            if ctx.guild.id in x:
                x[1].cancel()
                self._announcement_tasks.remove(x)
                self._announcements.remove(ctx.guild.id)
                break
        await ctx.respond('Announcement cancelled')
        self._Announcements_changed = True
        return

    # Helper function that starts the scheduled task
    async def runTasks(self, ctx, dt):
        # Get task Queue
        taskQ = None
        for x in self._taskQueues:
            if x == ctx.guild.id:
                taskQ = x
                break
        # Get logging channel
        db = EE_DB(self.database, ctx.guild.id)
        LOG = db.getLoggingID(channelType.log)
        LOG = self.client.get_channel(LOG)

        if LOG is None:
            await ctx.channel.send('Please run /configure first')
            taskQ.unlock()
            # Remove task from running tasks
            for i in self._runningTasks:
                if ctx.guild.id in i:
                    self._runningTasks.remove(i)
                    break
            return

        # Calculate how many seconds are needed to sleep
        if self._DEBUG:
            sec = 3
        else:
            sec = (dt - datetime.now()).total_seconds()
        try:
            await asyncio.sleep(sec)

            await LOG.send('Starting Tasks...')

            i = 0

            # Run tasks specified by task queue
            for x in taskQ:
                if x.taskType == taskEnum.ARCHIVE:
                    channel = self.client.get_channel(x.channel)
                    await self.runArchive(ctx.guild, channel)
                elif x.taskType == taskEnum.CLEAR_ROLES:
                    role = None
                    if x.role is not None:
                        role = ctx.guild.get_role(x.role)
                    await self.runClearRoles(ctx.guild, role)
                taskQ.markTaskComplete(i)
                i += 1
                self._changed = True

            await LOG.send('Finished running tasks')
            # Remove task queue
            self._taskQueues.remove(ctx.guild.id)
            # Remove task from running tasks
            for i in self._runningTasks:
                if ctx.guild.id in i:
                    self._runningTasks.remove(i)
                    break
            self._changed = True
        except asyncio.CancelledError:
            taskQ.clearCompletedTasks()
            self._changed = True
            await LOG.send(self._cancelMsg)
            raise

    # Archive helper
    async def runArchive(self, guild, channel: discord.TextChannel):
        db = EE_DB(self.database, guild.id)
        # Poll archive flag
        while db.getArchiveFlag():
            await asyncio.sleep(3)
        # Claim archive rights
        db.setArchiveFlag(True)
        LOG = db.getLoggingID(channelType.log)
        LOG = self.client.get_channel(LOG)

        # Get messages
        msg = db.getMessages(channel.id)

        # If empty, report error, release archive rights, and return
        if not msg:
            await LOG.send(f'No messages found in {channel.name}')
            db.setArchiveFlag(False)
            return

        await LOG.send(f'Archiving {channel.name}')

        # Clone new channel first
        newChannel = await channel.clone()
        await newChannel.edit(position=channel.position)
        # Get name for archive channel
        archiveChannelName = f'{channel.name}-archive'
        # Check if there is an archive category. Ff not, create one...
        category = discord.utils.find(lambda c: c.name == 'Archive', guild.categories)

        if category is None:
            category = await guild.create_category('Archive')
            await category.set_permissions(guild.default_role, add_reactions=False,
                                           attach_files=False, manage_channels=False,
                                           manage_messages=False, manage_permissions=False,
                                           read_message_history=True, read_messages=False,
                                           send_messages=False, send_tts_messages=False,
                                           use_slash_commands=False)
            # Create archive channel by cloning old channel
            archiveChannel = await channel.clone()
            # Edit the name
            await archiveChannel.edit(name=archiveChannelName, category=category)
        else:
            # Check if archive channel already exist
            channels = category.text_channels
            archiveChannel = discord.utils.find(lambda ch: ch.name == archiveChannelName, channels)
            # Create a new archive channel
            if archiveChannel is None:
                archiveChannel = await channel.clone()
                # Edit the name
                await archiveChannel.edit(name=archiveChannelName, category=category)
                new = int(''.join(filter(str.isdigit, channel.name)))
                pos = 0
                end = True
                # Find position of archive channel
                for x in channels:
                    ch = int(''.join(filter(str.isdigit, x)))
                    if ch > new:
                        pos = x.position
                        end = False
                        break
                if not end:
                    await archiveChannel.edit(position=pos)

        # Get roles of channel being archived and update their permissions
        roles = channel.changed_roles
        overwrite = discord.PermissionOverwrite()
        overwrite.send_messages = False
        overwrite.read_messages = False
        overwrite.read_message_history = False
        for x in roles:
            await channel.set_permissions(x, overwrite=overwrite)

        # Get roles from archive channel, save their permissions for later, and update them
        roles = archiveChannel.changed_roles 
        permissions = []
        for x in roles:
            permissions.append([x, archiveChannel.overwrites_for(x)])
            await archiveChannel.set_permissions(x, overwrite=overwrite)

        # Create archive header
        date = datetime.today()
        month = date.month
        year = date.year
        term = ''
        if month > 8:
            term = 'Fall %d' % year
        elif (month < 9) and (month > 5):
            term = 'Summer %d' % year
        elif month < 6:
            term = 'Spring %d' % year

        await archiveChannel.send(f'----------\n__**Start of the {term} semester**__\n----------')

        msgRef = []
        MsgRef = None
        files = []

        # Loop through messages
        for i in msg:
            MSG = await channel.fetch_message(i)
            date = MSG.created_at
            await archiveChannel.send('----------\nMessage create by %s at %d/%d/%d %d:%d' %
                                  (MSG.author.name, date.month, date.day, date.year, date.hour, date.minute))
            # Find reference
            if MSG.reference:
                if MSG.reference.guild_id == guild.id and MSG.reference.channel_id == channel.id:
                    # Go backwards to save time
                    for x in range(len(msgRef)-1, -1, -1):
                        if msgRef[x][0] == MSG.reference.message_id:
                            MsgRef = msgRef[x][1]
                            break
                else:
                    MsgRef = MSG.reference

            # Get attachments
            if MSG.attachments:
                files = MSG.attachments
                for i in range(0, len(files)):
                    files[i] = await files[i].to_file(spoiler=files[i].is_spoiler())

            #Post message
            newMsg = await archiveChannel.send(content=MSG.content, tts=MSG.tts,
                                               files=files, reference=MsgRef)
            #Add message to reference list
            item = [i, newMsg]
            msgRef.append(item)
            MsgRef = None
            files = []
            #Sleep for 3 seconds to allow for cooldown
            await asyncio.sleep(3)

        await archiveChannel.send(f'----------\n__**End of the {term} semester**__\n----------')

        # Reset permissions from earlier
        for x in permissions:
            await archiveChannel.set_permissions(x[0], overwrite=x[1])

        # Clear the messages from the database and delete the old channel
        db.bulkMsgDelete(channel.id)
        await channel.delete()

        await LOG.send(f'Finished archiving {channel.name}')

        # Release the archive flag
        db.setArchiveFlag(False)
        return

    # Helper function for clear roles
    async def runClearRoles(self, guild, role: discord.Role):
        db = EE_DB(self.database, guild.id)
        # Poll for the purge roles flag
        while db.getPurgeRolesFlag():
            await asyncio.sleep(3)
        # Claim the purge roles flag
        db.setPurgeRoleFlag(True)

        LOG = db.getLoggingID(channelType.log)
        LOG = self.client.get_channel(LOG)

        await LOG.send('Starting Role Purge...')

        # Check if role was given
        if role is not None:
            members = role.members
            # Remove given role from all members
            for member in members:
                await member.remove_roles(role)
        else:
            members = guild.members
            # Purge all roles whose names are numeric from all members
            for member in members:
                roles = member.roles
                roles = tuple([x for x in roles if str(x).isdigit()])
                if roles:
                    await member.remove_roles(*roles, atomic=True)
        await LOG.send('Finished purging roles...')
        # Release purge roles flag
        db.setPurgeRoleFlag(False)
        return

    # Helper function meant for restarting tasks when booting up
    async def restartTask(self, guild, dt):
        # Get task Queue
        taskQ = None
        for x in self._taskQueues:
            if x == guild:
                taskQ = x
                break
        guild = self.client.get_guild(guild)
        db = EE_DB(self.database, guild.id)
        LOG = db.getLoggingID(channelType.log)
        LOG = self.client.get_channel(LOG)

        # Calculate time between now and scheduled execution date
        sec = (dt - datetime.now()).total_seconds()
        try:
            await asyncio.sleep(sec)

            await LOG.send('Starting Tasks...')

            i = 0
            # Run tasks specified by task queue
            for x in taskQ:
                if x.taskType == taskEnum.ARCHIVE:
                    channel = self.client.get_channel(x.channel)
                    await self.runArchive(guild, channel)
                elif x.taskType == taskEnum.CLEAR_ROLES:
                    role = None
                    if x.role is not None:
                        role = guild.get_role(x.role)
                    await self.runClearRoles(guild, role)
                taskQ.markTaskComplete(i)
                i += 1
                self._changed = True

            await LOG.send('Finished running tasks')
            # Delete task queue
            self._taskQueues.remove(guild.id)
            # Remove task from running tasks list
            for i in self._runningTasks:
                if guild.id in i:
                    self._runningTasks.remove(i)
                    break
            self._changed = True
        except asyncio.CancelledError:
            taskQ.clearCompletedTasks()
            self._changed = True
            await LOG.send(self._cancelMsg)
            raise

    # Helper function meant for running the announcement task
    async def runAnnouncement(self, anncm: Announcement):
        # get logging channel
        db = EE_DB(self.database, int(anncm))
        LOG = db.getLoggingID(channelType.log)
        LOG = self.client.get_channel(LOG)

        guild = await self.client.fetch_guild(int(anncm))

        seconds = (anncm.time - datetime.now(pytz.utc)).total_seconds()
        try:
            await asyncio.sleep(seconds)

            # Try to fetch the channel
            try:
                channel = await guild.fetch_channel(anncm.channel)
            except Exception:
                channel = None
            # If no channel was fetched, use the first text channel or news channel
            if channel is None:
                channels = await guild.fetch_channels()
                for x in channels:
                    if x.type == discord.ChannelType.news:
                        channel = x
                        break
                    if channel is None and x.type == discord.ChannelType.text:
                        channel = x
            # Send message and clean up
            await channel.send(str(anncm))
            self._announcements.remove(anncm)
            self._Announcements_changed = True
            for x in self._announcement_tasks:
                if int(anncm) in x:
                    self._announcement_tasks.remove(x)
                    break
        except asyncio.CancelledError:
            await LOG.send(self._announcement_cancel_msg)
            return

    # Task that checks for Queue updates and backs them up
    @tasks.loop(minutes=5)
    async def saveQueues(self, launched: bool):
        if self._changed and launched:
            # Dump queues to a file
            with open('/mnt/disk/taskQueues.pickle', 'wb') as f:
                pk.dump(self._taskQueues, f)
            # Reset changed flag
            self._changed = False
        if self._Announcements_changed and launched:
            # Dump announcements to a file
            with open('/mnt/disk/announcements.pickle', 'wb') as f:
                pk.dump(self._announcements, f)
            # Reset announcement changes flag
            self._Announcements_changed = False

    # Function that cancels all the running tasks
    def killQueues(self, launched):
        # Update cancle message
        self._cancelMsg = 'Tasks cancelled due to bot maintenance. The tasks will resume when the bot is back online.'
        self._announcement_cancel_msg = 'Announcement cancelled due to bot maintenance. The announcement will be rescheduled when the bot is back online.'

        # Cancel all the running tasks
        if self._runningTasks:
            for x in self._runningTasks:
                x[1].cancel()
        if self._announcement_tasks:
            for x in self._announcement_tasks:
                x[1].cancel()
        if launched:
            # Dump the queue data
            with open('/mnt/disk/taskQueues.pickle', 'wb') as f:
                pk.dump(self._taskQueues, f)
            with open('/mnt/disk/announcements.pickle', 'wb') as f:
                pk.dump(self._announcements, f)

    def loadQueues(self, launched: bool):
        if launched:
            # load tasks queue
            with open('/mnt/disk/taskQueues.pickle', 'rb') as f:
                self._taskQueues = pk.load(f)
            # Restart running tasks
            if self._taskQueues:
                index = 0
                for Q in self._taskQueues:
                    if bool(Q):
                        task = asyncio.create_task(self.restartTask(int(Q), Q.date, index))
                        self._runningTasks.append([int(Q), task])
                    index += 1
            with open('/mnt/disk/announcements.pickle', 'rb') as f:
                self._announcements = pk.load(f)
            if self._announcements:
                for x in self._announcements:
                    task = asyncio.create_task(self.runAnnouncement(x))
                    self._announcement_tasks.append([int(x), task])
