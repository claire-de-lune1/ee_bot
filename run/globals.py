# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import platform
import distro

# Global flags/constants
launchFlag = None
originGuild = None
embedColor = 0x0e456e
sEmbedColor = '#0e456e'
selectCompiler = None
architectures = ['x86_64', 'mips', 'mips64', 'avr', 'aarch64', 'msp430', 'arm', 'riscv32', 'riscv64']
compilers = ['gcc', 'g++', 'clang', 'clang++']


#Function that inits the flags/constants
def InitLaunch():
    global launchFlag
    global originGuild

    #Check if running on test machine or launch machine
    if __debug__:
        launchFlag = False
        originGuild = 759172018737905664
    else:
        launchFlag = True
        originGuild = 758600193070858251


async def selectCompiler_x86_64(ctx, compiler, architecture, endianness):
    response = ''
    Valid = False
    if architecture == 'x86_64':
        response = 'x86_64 architecture targeted.\n'
        if endianness == 'big':
            response += 'x86_64 does not support big endian! Switching to little endian.'
        Compiler = compiler
        Valid = True
    elif architecture == 'mips':
        response = 'mips 32-bit architecture targeted.\n'
        if endianness == 'big':
            response += 'Big endian selected.'
            if 'clang' in compiler:
                response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
            else:
                Compiler = f'toolchains/x86_64/mips/bin/mips-unknown-elf-{compiler}'
                Valid = True
        else:
            response += 'Little endian selected.'
            if 'clang' in compiler:
                response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
            else:
                Compiler = f'toolchains/x86_64/mipsel/bin/mipsel-unknown-elf-{compiler}'
                Valid = True
    elif architecture == 'mips64':
        response = 'mips 64-bit architecture targeted.\n'
        if endianness == 'big':
            response += 'Big endian selected.'
            if 'clang' in compiler:
                response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
            else:
                Compiler = f'toolchains/x86_64/mips64/bin/mips64-unknown-linux-gnu-{compiler}'
                Valid = True
        else:
            response += 'Little endian selected.'
            if 'clang' in compiler:
                response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
            else:
                Compiler = f'toolchains/x86_64/mips64el/bin/mips64el-unknown-linux-gnu-{compiler}'
                Valid = True
    elif architecture == 'avr':
        response = 'avr architecture targeted.\n'
        if endianness == 'big':
            response += 'avr does not support big endian! Switching to little endian.'
        if 'clang' in compiler:
            response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
        else:
            Compiler = f'toolchains/x86_64/avr/bin/avr-{compiler}'
            Valid = True
    elif architecture == 'aarch64':
        response = '64-bit ARM architecture targeted.\n'
        if endianness == 'big':
            response += 'Big endian selected.'
            if 'clang' in compiler:
                response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
            else:
                Compiler = f'toolchains/x86_64/aarch64_be/bin/aarch64_be-unknown-linux-uclubc-{compiler}'
                Valid = True
        else:
            response += 'Little endian selected.'
            if 'clang' in compiler:
                response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
            else:
                Compiler = f'toolchains/x86_64/aarch64/bin/aarch64-unknown-linux-uclubc-{compiler}'
                Valid = True
    elif architecture == 'arm':
        response = '32-bit ARM architecture targeted.\n'
        if endianness == 'big':
            response += 'Big endian selected.'
            if 'clang' in compiler:
                response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
            else:
                Compiler = f'toolchains/x86_64/armeb/bin/armeb-cortexa5-linux-uclibcgnueabihf-{compiler}'
                Valid = True
        else:
            response += 'Little endian selected.'
            if 'clang' in compiler:
                response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
            else:
                Compiler = f'toolchains/x86_64/arm/bin/arm-cortexa5-linux-uclibcgnueabihf-{compiler}'
                Valid = True
    elif architecture == 'msp430':
        response = 'msp430 architecture targeted.\n'
        if endianness == 'big':
            response += 'msp430 does not support big endian! Switching to little endian.'
        if 'clang' in compiler:
            response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
        else:
            Compiler = f'toolchains/x86_64/msp430/bin/msp430-unknown-elf-{compiler}'
            Valid = True
    elif architecture == 'riscv32':
        response = '32-bit RISCV architecture targeted.\n'
        if endianness == 'big':
            response += 'RISCV does not support big endian! Switching to little endian.'
        if 'clang' in compiler:
            response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
        else:
            Compiler = f'toolchains/x86_64/riscv32/bin/riscv32-unknown-elf-{compiler}'
            Valid = True
    elif architecture == 'riscv64':
        response = '64-bit RISCV architecture targeted.\n'
        if endianness == 'big':
            response += 'RISCV does not support big endian! Switching to little endian.'
        if 'clang' in compiler:
            response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
        else:
            Compiler = f'toolchains/x86_64/riscv64/bin/riscv64-unknown-elf-{compiler}'
            Valid = True
    elif architecture == 'tic6x':
        response = 'tic6x architecture targeted.\n'
        if endianness == 'big':
            response += 'Big endian selected.'
            if 'clang' in compiler:
                response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
            else:
                Compiler = f'toolchains/x86_64/tic6x-be/bin/tic6x-uclinux-{compiler}'
                Valid = True
        else:
            response += 'Little endian selected.'
            if 'clang' in compiler:
                response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
            else:
                Compiler = f'toolchains/x86_64/tic6x/bin/tic6x-uclinux-{compiler}'
                Valid = True
    else:
        if endianness:
            response = 'No architecture given. Defaulting to system.\n'
        response += 'No architecture given. Targeting x86_64 architecture.'
        Compiler = compiler
        Valid = True
    await ctx.respond(response)
    return Valid, Compiler


async def selectCompiler_aarch64(ctx, compiler, architecture, endianness):
    response = ''
    Valid = False
    if architecture == 'x86_64':
        response = 'x86_64 architecture targeted.\n'
        if endianness == 'big':
            response += 'x86_64 does not support big endian! Switching to little endian.'
        if 'clang' in compiler:
            response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
        else:
            Compiler = f'toolchains/RPi/x86_64-unknown-linux-gnu/bin/x86_64-unknown-linux-gnu-{compiler}'
            Valid = True
    elif architecture == 'mips':
        response = 'mips 32-bit architecture targeted.\n'
        if endianness == 'big':
            response += 'Big endian selected.'
            if 'clang' in compiler:
                response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
            else:
                Compiler = f'toolchains/RPi/mips-big-linux-gnu/bin/mips-ar2315-linux-gnu-{compiler}'
                Valid = True
        else:
            response += 'Little endian selected.'
            if 'clang' in compiler:
                response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
            else:
                Compiler = f'toolchains/RPi/mips-little-linux-gnu/bin/mipsel-ar2315-linux-gnu-{compiler}'
                Valid = True
    elif architecture == 'mips64':
        response = 'mips 64-bit architecture targeted.\n'
        if endianness == 'big':
            response += 'Big endian selected.'
            if 'clang' in compiler:
                response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
            else:
                Compiler = f'toolchains/RPi/mips64-big-linux-gnu/bin/mips64-unknown-linux-gnu-{compiler}'
                Valid = True
        else:
            response += 'Little endian selected.'
            if 'clang' in compiler:
                response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
            else:
                Compiler = f'toolchains/RPi/mips64-little-linux-gnu/bin/mips64el-unknown-linux-gnu-{compiler}'
                Valid = True
    elif architecture == 'avr':
        response = 'avr architecture targeted.\n'
        if endianness == 'big':
            response += 'avr does not support big endian! Switching to little endian.'
        if 'clang' in compiler:
            response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
        else:
            Compiler = f'toolchains/RPi/avr/bin/avr-{compiler}'
            Valid = True
    elif architecture == 'aarch64':
        response = '64-bit ARM architecture targeted.\n'
        if endianness == 'big':
            response += 'Big endian selected.'
            if 'clang' in compiler:
                response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
            else:
                Compiler = f'toolchains/RPi/aarch64_be-unknown-linux-gnu/bin/aarch64_be-unknown-linux-gnu-{compiler}'
                Valid = True
        else:
            response += 'Little endian selected.'
            Compiler = compiler
            Valid = True
    elif architecture == 'arm':
        response = '32-bit ARM architecture targeted.\n'
        if endianness == 'big':
            response += 'Big endian selected.'
            if 'clang' in compiler:
                response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
            else:
                Compiler = f'toolchains/RPi/armeb-cortexa5-linux-uclibcgnueabihf/bin/armeb-cortexa5-linux-uclibcgnueabihf-{compiler}'
                Valid = True
        else:
            response += 'Little endian selected.'
            if 'clang' in compiler:
                response += '\nClang and clang++ are not supported! Please try again with gcc or g++.'
            else:
                Compiler = f'toolchains/RPi/arm-cortexa5-linux-uclibcgnueabihf/bin/arm-cortexa5-linux-uclibcgnueabihf-{compiler}'
                Valid = True
    elif architecture == 'msp430':
        response = 'msp430 architecture targeted.\n'
        if endianness == 'big':
            response += 'msp430 does not support big endian! Switching to little endian.'
        if 'clang' in compiler or compiler == 'g++':
            response += '\nClang, clang++, and g++ are not supported! Please try again with gcc.'
        else:
            Compiler = f'toolchains/RPi/msp430-unknown-elf/bin/msp430-unkown-elf-gcc'
            Valid = True
    elif architecture == 'riscv32':
        response = '32-bit RISCV architecture targeted.\n'
        if endianness == 'big':
            response += 'RISCV does not support big endian! Switching to little endian.'
        if 'clang' in compiler or compiler == 'g++':
            response += '\nClang, clang++, and g++ are not supported! Please try again with gcc.'
        else:
            Compiler = f'toolchains/RPi/riscv32-unknown-elf/bin/riscv32-unkown-elf-gcc'
            Valid = True
    elif architecture == 'riscv64':
        response = '64-bit RISCV architecture targeted.\n'
        if endianness == 'big':
            response += 'RISCV does not support big endian! Switching to little endian.'
        if 'clang' in compiler or compiler == 'g++':
            response += '\nClang, clang++, and g++ are not supported! Please try again with gcc.'
        else:
            Compiler = f'toolchains/RPi/riscv64-unknown-elf/bin/riscv64-unkown-elf-gcc'
            Valid = True
    else:
        if endianness:
            response = 'No architecture given. Defaulting to system.\n'
        response += 'No architecture given. Targeting aarch64 architecture.'
        Compiler = compiler
        Valid = True
    await ctx.respond(response)
    return Valid, Compiler


if platform.architecture() == 'x86_64' and distro.linux_distribution() == ('Ubuntu', '20.04', 'focal'):
    selectCompiler = selectCompiler_x86_64
    architectures.append('tic6x')
    compilers.append('gcc-11.2.0')
elif platform.architecture() == 'aarch64' and platform == 'linux':
    selectCompiler = selectCompiler_aarch64
