# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import discord
from discord.commands import slash_command
from discord.commands import Option
from discord.ext import commands
import emoji as Emoji
from EE_Bot_DB import EE_DB
from EE_Bot_DB import cogEnum
from EE_Bot_DB import channelType
from YosysCommands import YS
import re
import io
import csv
from PIL import Image
embedColor = 0x0e456e


class serverConfig(commands.Cog):
    """This category is for server configurations"""
    def __init__(self, client, database, MASTER):
        self.MASTER = MASTER
        self.client = client
        self.database = database
        return

    async def pre_checks(self, ctx, configured=True):
        #Check if in guild
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this command.')
            return False
        # Check if the owner is using this command
        if ctx.author.id != ctx.guild.owner_id:
            with open('./images/EasterEggs/adminPermssions.png', 'rb') as f:
                await ctx.respond(file=discord.File(f, 'adminPermissions.png'))
            return False
        db = EE_DB(self.database, ctx.guild.id)
        # Get log channel id
        log = db.getLoggingID(channelType.log)
        log = self.client.get_channel(log)
        # Check if log channel exists
        if log is None and configured:
            await ctx.respond('Error: Please set your logging channels first! See `/help configure`.')
            return False
        return True

    #Command that configures a server
    @slash_command(description='Configure server channels.')
    async def configure(
        self, 
        ctx, 
        log: Option(discord.TextChannel, 'The bot logging channel.'), 
        role: Option(discord.TextChannel, 'The role opt in channel.'),
        level: Option(discord.TextChannel, 'The level notification channel.'),
        shaming: Option(discord.TextChannel, 'The server shaming channel.'),
        monitor: Option(discord.TextChannel, 'The user monitoring channel.')
    ):
        if not await self.pre_checks(ctx, False):
            return

        log = log.id
        roleOptIn = role.id
        levelNotif = level.id
        shaming = shaming.id
        monitor = monitor.id

        #Save channel IDs inside database
        db = EE_DB(self.database, ctx.guild.id)
        db.configureGuild(log, roleOptIn, levelNotif, shaming, monitor)

        #Respond
        if db.getError() != 0:
            db.reconfigureGuild(log, roleOptIn, levelNotif, shaming, monitor)
            if db.getError() != 0:
                await ctx.respond('An Error occured during the configuration')
            else:
                channel = self.client.get_channel(log)
                await channel.send('Configuration successful')
                await ctx.respond('Done')
        else:
            channel = self.client.get_channel(log)
            await channel.send('Configuration successful')
            await ctx.respond('Done')
        return

    #Command that tests the server's configurations
    @slash_command(description='Test server configuration.')
    async def config_test(self, ctx):
        if not await self.pre_checks(ctx):
            return
        await ctx.respond('Testing server...')
        db = EE_DB(self.database, ctx.guild.id)
        try:
            #Test each channel
            cID = db.getLoggingID(channelType.log)
            channel = self.client.get_channel(cID)
            await channel.send('Checking log channel')
            cID = db.getLoggingID(channelType.role)
            channel = self.client.get_channel(cID)
            await channel.send('Checking role opt in channel')
            cID = db.getLoggingID(channelType.level)
            channel = self.client.get_channel(cID)
            await channel.send('Checking level channel')
            cID = db.getLoggingID(channelType.shame)
            channel = self.client.get_channel(cID)
            await channel.send('Checking shame channel')
            cID = db.getLoggingID(channelType.watch)
            channel = self.client.get_channel(cID)
            await channel.send('Checking monitor channel')
        except (Exception):
            await ctx.channel.send('Test failed! Please run `/configure`')
        return

    #Command that sets the poli rules message
    @slash_command(description='Set the political rules message')
    async def set_poli_msg(
        self, 
        ctx, 
        msg_url: Option(str, 'The url of the message.'),
        emoji: Option(str, 'The reaction emoji')
    ):
        if not await self.pre_checks(ctx):
            return
        #Check for an emoji
        if emoji not in Emoji.UNICODE_EMOJI and emoji not in Emoji.UNICODE_EMOJI_ENGLISH:
            if re.sub(r'<:\w+:(\d+)>', '', emoji) != '':
                await ctx.respond('Please use an emoji')
                return
        #Get channel and msg id from url
        msgInfo = msg_url.split('/')[-2:]

        db = EE_DB(self.database, ctx.guild.id)
        logCh = db.getLoggingID(channelType.log)
        logCh = self.client.get_channel(logCh)
        #Set the poli rules channel and check for database errors
        db.setPoliRulesChannel(int(msgInfo[0]))
        if db.getError() != 0:
            await logCh.send('An error occured with the database')
            await ctx.respond('Done')
            return
        else:
            await logCh.send('Poli Rules channel set')
        #Set the poli rules msg and check for database errors
        db.setPoliMsg(int(msgInfo[1]))
        if db.getError() != 0:
            await logCh.send('An error occured with the database')
            await ctx.respond('Done')
            return
        else:
            await logCh.send('Poli message set')
        #Fetch the message and add a reaction
        msg = await ctx.guild.get_channel(int(msgInfo[0])).fetch_message(int(msgInfo[1]))
        await msg.add_reaction(emoji)
        await ctx.respond('Done')
        return

    #Command that clears the poli message
    @slash_command(description='Clears the poli message from the database')
    async def clear_poli_msg(self, ctx):
        if not await self.pre_checks(ctx):
            return
        db = EE_DB(self.database, ctx.guild.id)
        channel = db.getLoggingID(channelType.log)
        channel = self.client.get_channel(channel)
        #Clear the poli msg and check for errors
        db.clearPoliMsg()
        if db.getError() != 0:
            await channel.send('An error occured with the database')
            return
        else:
            await channel.send('Poli message set')
        #Clear the poli channel and check for errors
        db.clearPoliRulesChannel()
        if db.getError() != 0:
            await channel.send('An error occured with the database')
            return
        else:
            await channel.send('Poli message cleared')
        await ctx.respond('Done')
        return

    #Command that sets the poli rules
    @slash_command(description='Command that sets the poli rules.')
    async def set_poli_rules(
        self, 
        ctx, 
        rules: Option(str, 'The rules that get a user banned from the poli channels.')
    ):
        if not await self.pre_checks(ctx):
            return
        #Read in the rules as a list
        rules = next(csv.reader(io.StringIO(rules), delimiter=' '))
        #Try converting the rules into integers
        for i in range(0, len(rules)):
            try:
                rules[i] = int(rules[i])
            except (Exception):
                await ctx.respond('The rules must be integers')
                return
        db = EE_DB(self.database, ctx.guild.id)
        channel = db.getLoggingID(channelType.log)
        channel = self.client.get_channel(channel)
        #Set the poli rules and check for database errors
        db.setPoliRules(rules)
        if db.getError() != 0:
            await channel.send('An error occured with the database')
        else:
            await channel.send('Poli rules set')
        await ctx.respond('Done')

    #Command that clears the poli rules
    @slash_command(description='Clear the political channel rules.')
    async def clear_poli_rules(self, ctx):
        if not await self.pre_checks(ctx):
            return
        #Check logging
        db = EE_DB(self.database, ctx.guild.id)
        channel = db.getLoggingID(channelType.log)
        channel = self.client.get_channel(channel)
        #Clear the rules and check for errors
        db.clearPoliRules()
        if db.getError() != 0:
            await channel.send('An error occured with the database')
        else:
            await channel.send('Poli rules cleared')
        await ctx.respond('Done')
        return

    #Command that enables/disables the level system
    @slash_command(description='Turn on and off the level system')
    async def level_system(
        self, 
        ctx, 
        status: Option(str, 'Enable or disable the level system', choices = ['on', 'off'])
    ):
        if not await self.pre_checks(ctx, False):
            return
        db = EE_DB(self.database, ctx.guild.id, False)
        #Save setting into database
        if status == 'on':
            db.toggleLevelSystem(True)
        else:
            db.toggleLevelSystem(False)
        await ctx.respond('Server Level System is now turned %s.' % status)
        return

    #Command that checks the status of the level system
    @slash_command(description='Check if the level system is on or off.')
    async def check_level_system(self, ctx):
        if not await self.pre_checks(ctx, False):
            return
        db = EE_DB(self.database, ctx.guild.id)
        #Get the level system status
        status = db.levelSystemStatus()
        if status:
            status = 'on'
        else:
            status = 'off'
        #Respond
        await ctx.respond('The level system is %s.' % status)
        return

    #Command that enables command categories
    @slash_command(description='Enable groups of commands.')
    async def enable_cog(
        self, 
        ctx, 
        category: Option(str, description = 'The group of commands that get enabled.', choices = [x.name for x in cogEnum][1:-2])
    ):
        if not await self.pre_checks(ctx):
            return
        cat = cogEnum.NONE
        #Find cog enum
        for x in cogEnum:
            if x == cogEnum.NONE:
                continue
            if category == x.name:
                cat = x
                break
        db = EE_DB(self.database, ctx.guild.id)
        #Enable cog
        db.enableCog(cat)
        if db.getError():
            log = db.getLoggingID(channelType.log)
            log = self.client.get_channel(log)
            await log.send('An error occured.')
            await ctx.respond('Check log')
            return
        #Respond
        await ctx.respond('The %s category is now enabled.' % category.lower().capitalize())
        return

    #Command that disables command categories
    @slash_command(description='Disable groups of commands.')
    async def disable_cog(
        self, 
        ctx, 
        category: Option(str, 'The group of commands that get disabled.', choices = [x.name for x in cogEnum][1:-2])
    ):
        if not await self.pre_checks(ctx):
            return
        cat = cogEnum.NONE
        #Find cog enum
        for x in cogEnum:
            if x == cogEnum.NONE:
                continue
            if category.lower() == x.name.lower():
                cat = x
                break
        db = EE_DB(self.database, ctx.guild.id)
        #Disable cog
        db.disableCog(cat)
        if db.getError():
            log = db.getLoggingID(channelType.log)
            log = self.client.get_channel(log)
            await log.send('An error occured.')
            await ctx.respond('Check log.')
            return
        #Respond
        await ctx.respond('The %s category is now disabled.' % category)
        return

    #Command that shows which cogs are enabled and disabled
    @slash_command(description='Check the cogs that are enabled and disabled.')
    async def check_cog_status(self, ctx):
        if not await self.pre_checks(ctx, False):
            return
        db = EE_DB(self.database, ctx.guild.id)
        #Start embed construction
        embed = discord.Embed(title='Command Category Status', description='', color=embedColor)
        enabled = False
        embed.set_thumbnail(url=self.client.user.display_avatar.url)
        #Add fields that tell whether the cogs are enabled or not
        for x in cogEnum:
            if x == cogEnum.NONE or x == cogEnum.ALL or x == cogEnum.DEFAULT:
                continue
            enabled = db.checkCog(x)
            if not YS and x == cogEnum.Yosys:
                enabled = False
            if enabled:
                embed.add_field(name='{}'.format(x.name), value='Status: **ENABLED**', inline=False)
            else:
                embed.add_field(name='{}'.format(x.name), value='Status: **DISABLED**', inline=False)
        #Respond
        await ctx.respond(embed=embed)
        return

    #Command that sets the ban message
    @slash_command(description='Set a custom ban message. It is recommended you include a link to an appeal form.')
    async def set_ban_msg(
        self, 
        ctx, 
        msg: Option(str, 'The custom ban message.')
    ):
        if not await self.pre_checks(ctx):
            return
        # Check if the string can fit in the database
        if len(msg) > 1000:
            await ctx.respond('Error: Ban message cannot excede 1000 characters! Current length: %d', len(msg))
            return
        db = EE_DB(self.database, ctx.guild.id)
        # Get log channel id
        log = db.getLoggingID(channelType.log)
        # Get actual log channel
        log = self.client.get_channel(log)
        # Set the message in the database
        db.setBanMsg(msg)
        # Check for errors
        if db.getError() != 0:
            # Send error message
            await log.send('An error occured with the database')
            return
        # Acknowledge the new setting
        await log.send('Ban message configured successfully')
        await ctx.respond('Done')
        return

    #Command that clears the ban message
    @slash_command(description='Clear the ban message for the server.')
    async def clear_ban_msg(self, ctx):
        if not await self.pre_checks(ctx):
            return
        db = EE_DB(self.database, ctx.guild.id)
        # Get log channel id
        log = db.getLoggingID(channelType.log)
        # Get actual log channel
        log = self.client.get_channel(log)
        # Clear the message from the database
        db.clearBanMsg()
        # Check for errors
        if db.getError() != 0:
            # Send error message
            await log.send('An error occured with the database')
            return
        # Acknowledge the setting being cleared
        await log.send('Ban message cleared successfully!')
        await ctx.respond('Done')
        return

    @slash_command()
    async def create_easter_egg(
        self,
        ctx,
        phrase: Option(str, 'Activation phrase'),
        file: Option(discord.Attachment, 'The image that gets displayed'),
        limited: Option(str, 'Limit the Easter egg', choices = ['yes', 'no'])
    ):
        if not await self.pre_checks(ctx):
            return
        db = EE_DB(self.database, ctx.guild.id)
        # Get log channel id
        log = db.getLoggingID(channelType.log)
        log = ctx.guild.get_channel(log)
        await ctx.respond('Creating Easter Egg. Check log for information.')
        # Read the bytes from the file
        fp = await file.read()
        # Check the size of the file. If more than 4 MB, throw an error
        if len(fp) > 4194304:
            await log.send('Error: File must be less than 4 MB!')
            return
        # Limit servers to 20 Easter Eggs
        if len(db.fetchEasterEggs()) == 20:
            await log.send('Error: The server cannot have more than 20 easter eggs!')
            return
        # Insert new Easter Egg into database. It will not insert if the phrase already
        # exists for the server, and it will return false if the easter egg already exists
        exists = db.createEasterEgg(file.filename, phrase.lower(), (limited == 'yes'), fp)
        if not exists:
            await log.send('Error: Easter Egg already exists!')
            return
        # Check for a database error
        if db.getError():
            await log.send('An error occured with the database!')
            return

    @slash_command()
    async def remove_easter_egg(
        self,
        ctx,
        phrase: Option(str, 'The easter egg activation phrase')
    ):
        if not await self.pre_checks(ctx):
            return
        db = EE_DB(self.database, ctx.guild.id)
        # Get log channel id
        log = db.getLoggingID(channelType.log)
        log = ctx.guild.get_channel(log)
        await ctx.respond('Removing Easter Egg...')
        db = EE_DB(self.database, ctx.guild.id)
        # Remove the Easter Egg from the database. If the phrase does not exist
        # the the function will return false
        exists = db.removeEasterEgg(phrase)
        if not exists:
            await log.send('Error: Easter Egg does not exist!')
            return
        # Check for a database error
        if db.getError():
            await log.send('An error occured in the database')
        return

    @slash_command()
    async def view_easter_eggs(self, ctx):
        if not await self.pre_checks(ctx):
            return
        db = EE_DB(self.database, ctx.guild.id)
        # Get log channel id
        log = db.getLoggingID(channelType.log)
        log = ctx.guild.get_channel(log)
        await ctx.respond('Check log for easter eggs')
        db = EE_DB(self.database, ctx.guild.id)
        # fetch the Easter Eggs from the database
        eggs = db.fetchEasterEggs()
        # No easter eggs found in the database
        if not eggs:
            await log.send('No Easter Eggs found!')
            return
        # Check for database error
        if db.getError():
            await log.send('An error occured in the database')
            return
        # Loop through eggs
        for egg in eggs:
            # Format the message string
            msg = f"{egg['phrase']}\nLimited: {bool(egg['Limited'])}"
            # Create bytesIO object and use it in the discord file object
            with io.BytesIO(egg['file']) as fp:
                fp.seek(0)
                await log.send(msg, file=discord.File(fp, egg['filename']))

    @slash_command(description="Set the default role color for creating class roles.")
    async def set_color(
        self,
        ctx,
        red: Option(int, 'The red component of the color'),
        green: Option(int, 'The green component of the color'),
        blue: Option(int, 'The blue component of the color')
    ):
        if not await self.pre_checks(ctx):
            return
        db = EE_DB(self.database, ctx.guild.id)
        # Get log channel id
        log = db.getLoggingID(channelType.log)
        # Get actual log channel
        log = self.client.get_channel(log)
        # set channel
        db.setRoleColor(red, green, blue)
        if db.getError():
            # Send error message
            await log.send('An error occured with the database')
            return
        # Acknowledge the setting being set
        await log.send('Role color set successfully!')
        await ctx.respond('Done')
        return

    @slash_command(description="See role color")
    async def role_color(self, ctx):
        if not await self.pre_checks(ctx):
            return
        db = EE_DB(self.database, ctx.guild.id)
        # Get role color
        RGB = db.getRoleColor()
        # If role color field is NULL, then use default role color
        if db.getError():
            RGB = discord.colour.Color.dark_teal().to_rgb()
        # Generate image and send it
        with io.BytesIO() as f:
            img = Image.new('RGB', (128, 128), RGB)
            img.save(f, format='JPEG')
            f.seek(0)
            await ctx.respond(file=discord.File(f, 'roleColor.jpg'))
        return

    @slash_command(description='Clear the role color')
    async def clear_color(self, ctx):
        if not await self.pre_checks(ctx):
            return
        db = EE_DB(self.database, ctx.guild.id)
        #Clear the role color
        db.clearRoleColor()
        await ctx.respond('Role color cleared')
        return
