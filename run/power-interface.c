/*****************************************************************************//**
* BSD 3 - Clause License
*
* Copyright(c) 2021, Tom Schmitz
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
* list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its
* contributors may be used to endorse or promote products derived from
* this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*********************************************************************************/


#include <stdio.h>
#include <stdlib.h>

#define PATH_SIZE 8192

void myStrCat(char * dest, const char * src, char delim, unsigned int destSize)
{
	int i = -1;
	int j = -1;
	while (dest[++i] != '\0')
		if (i == destSize)
			return;
	if (delim != '\0')
		dest[i++] = delim;
	while (src[++j] != '\0' && i != (destSize - 1))
		dest[i++] = src[j];
	dest[i] = '\0';
}

int strlen(const char * src)
{
	int len = 0;
	while (src[len++]);
	return len - 1;
}

void strcpy(char * dest, const char * src)
{
	int len = 0;
	while (src[len])
	{
		dest[len] = src[len];
		++len;
	}
	dest[len] = src[len];
}

int main(int argc, char ** argv)
{
	char path[PATH_SIZE];
	int len = 19;
	char * str = NULL;
	FILE * fp;
	int status;

	for (int i = 1; i < argc; ++i)
	{
		len += strlen(argv[i]);
		len += 1;
	}

	str = (char *)malloc(len);
	strcpy(str, "cd power;./power.out ");

	for (int i = 1; i < argc; ++i)
		myStrCat(str, argv[i], ' ', len);

	fp = popen(str, "r");
	if (fp == NULL)
	{
		printf("popen function failed\n");
		free(str);
		exit(EXIT_FAILURE);
	}

	while (fgets(path, PATH_SIZE, fp) != NULL)
		printf("%s", path);

	status = pclose(fp);
	if (status == -1)
	{
		printf("Error closing terminal\n");
	}

	free(str);

	return 0;
}