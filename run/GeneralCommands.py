# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import discord
from discord import File
from discord.commands import slash_command
from discord.commands import Option
from discord.ext import commands
from discord.ext import tasks
from EE_Bot_DB import EE_DB
from EE_Bot_DB import channelType
import EE_Bot_DB as DB
from datetime import datetime
from subprocess import PIPE
from subprocess import Popen
import EE_Bot_help as help
import threading
import asyncio
import re
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload
from oauth2client.service_account import ServiceAccountCredentials
from globals import embedColor
from globals import selectCompiler
from globals import architectures
from globals import compilers


class General(commands.Cog):
    """General Commands"""

    #Initialize Cog
    def __init__(self, MASTER, client, database, key_file_location: str, version):
        self.MASTER = MASTER
        self.client = client
        self.database = database
        self.key_file_location = key_file_location
        self._version = version

    #Status command
    @slash_command(description='Displays status of the bot')
    async def status(self, ctx):
        await ctx.respond("<@%d> Status: Ok" % ctx.author.id)
        # potential idea: also output the latency of the bot

    @slash_command(description='Checks the version of the bot.')
    async def version(self, ctx):
        if ctx.author.bot:
            return
        await ctx.respond('Bot version: %s\nDatabase version: %s' % (self._version, DB.__version__))

    #Filter command (Easter Egg)
    @slash_command(default_permissions=False)
    async def filter(
        self, 
        ctx, 
        filter: Option(str, 'Toggle status of filter.', choices = ['on', 'off'])
    ):
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this command!')
            return
        #Open database
        db = EE_DB(self.database, ctx.guild.id)
        #check rank
        rank, progress = db.checkUserRank(ctx.author.id)
        #Early exit if rank < 5 or if there was a database error
        if (rank < 5) or (db.getError() != 0):
            await ctx.respond('An error occurred')  #Update this later
            return
        #Check for correct keywords
        if filter == 'on':
            #Update flag in database
            db.updateUserFilter(True, ctx.author.id)
            if db.getError() != 0:
                channel_id = db.getLoggingID(channelType.log)
                if channel_id != 0:
                    await self.client.get_channel(channel_id).send("Unable to find " + ctx.author.name + " in database")
                    await ctx.respond('An error occurred')
        elif filter == 'off':
            #Update flag in database
            db.updateUserFilter(False, ctx.author.id)
            if db.getError() != 0:
                channel_id = db.getLoggingID(channelType.log)
                if channel_id != 0:
                    await self.client.get_channel(channel_id).send("Unable to find " + ctx.author.name + " in database")
                    await ctx.respond('An error occurred')
        await ctx.respond('Filter updated')
        del db 

    #Command to check rank and progress towards next rank
    @slash_command(description='Displays level and progress')
    async def rank(self, ctx):
        if ctx.guild is None:
                await ctx.respond('You must be in a server to run this command!')
                return
        #Open database
        db = EE_DB(self.database, ctx.guild.id)
        #Check rank and progress
        Rank, Progress = db.checkUserRank(ctx.author.id)
        #check for a database error
        if db.getError() != 0:
            channel_id = db.getLoggingID(channelType.log)
            if channel_id != 0:
                await self.client.get_channel(channel_id).send("Unable to find " + ctx.author.name + " in database")
            await ctx.respond('An error occurred')
        else:
            #Display rank and progress
            await ctx.respond("<@%d> Rank: %d\nProgress: %.2f %%" % (ctx.author.id, Rank, Progress))
        del db
        return 

    #Command to turn on/off rank up notifications
    @slash_command(description='Toggles level up notifications')
    async def notif(self, ctx, notifications: Option(str, 'Turn on or off level notifications?', choices = ['on', 'off'])):
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this command!')
            return
        #Open database
        db = EE_DB(self.database, ctx.guild.id)
        #Check for correct arguments
        if notifications == 'on':
            #Update flag in database
            db.updateNotifications(ctx.author.id, 1)
            if db.getError() != 0:
                channel_id = db.getLoggingID(channelType.log)
                if channel_id != 0:
                    await self.client.get_channel(channel_id).send("Unable to find " + ctx.author.name + " in database")
                    await ctx.respond('An error occurred')
            else:
                #Send feedback message
                await ctx.respond("<@%d> Level up notifications are now turned on for you. To see current level and progress, type: '!rank' or '!level'" % (ctx.author.id))
        elif notifications == 'off':
            #Update flag in database
            db.updateNotifications(ctx.author.id, 0)
            if db.getError() != 0:
                channel_id = db.getLoggingID(channelType.log)
                if channel_id != 0:
                    await self.client.get_channel(channel_id).send("Unable to find " + ctx.author.name + " in database")
                    await ctx.respond('An error occurred')
            else:
                #Send feedback message
                await ctx.respond("<@%d> Level up notifications are now turned off for you. To see current level and progress, type: '!rank' or '!level'" % (ctx.author.id))
            del db
        return

    def runCompilation(self, args, mutex, outputDict):
        # Create process
        process = Popen(args, stdout=PIPE, stderr=PIPE, universal_newlines=True)
        try:
            # start process with 5 minute timeout
            output, error = process.communicate(timeout=300)
        except TimeoutError:
            # Process timeout error
            output = ''
            error = '\n Timeout error: process took too long.'
        finally:
            # kill process
            process.kill()
        #Acquire mutex, set outputs, and release mutex
        mutex.acquire()
        outputDict['output'] = output
        outputDict['error'] = error
        outputDict['done'] = True
        mutex.release()

    #Compile command. This is commented out until the discord API allows files in slash commands
    @slash_command(description='See /help for information')
    async def compile(
        self, 
        ctx, 
        source_file: Option(discord.Attachment, 'Your C or C++ file'),
        compiler: Option(str, 'Compiler used', choices = compilers), 
        architecture: Option(str, 'The target architecture', choices = architectures, required = False, default = ''),
        endianness: Option(str, 'The endianness of the system', choices = ['little', 'big'], required = False, default = ''),
        flags: Option(str, 'Compiler flags', required = False, default = '')
    ):
        preprocess = False

        # Check if compile command is supported
        if selectCompiler is None:
            await ctx.respond('Not Supported')
            return

        # Parse the filename and check if it is a .c or .cpp file
        parsedFileName = source_file.filename.split('.')
        if parsedFileName[-1] != 'c' and parsedFileName[-1] != 'cpp' and parsedFileName[-1] != 'cc' and parsedFileName[-1] != 'cxx':
            await ctx.respond('Please attach a .c, .cpp, .cc, or .cxx file.')
            return

        # Run the function to select a compiler
        Valid, compiler = await selectCompiler(ctx, compiler, architecture, endianness)

        # Check if a valid compiler was selected
        if not Valid:
            return

        if flags:
            if '-E' in flags.split(' '):
                # Flag that does not allow compiling. Will output message given instead
                preprocess = True
            elif '-S' not in flags.split(' '):
                # Add compiler flag to output assembly
                flags += ' -S'
        else:
            flags = '-S'

        # Save the file and generate the assembly filename
        filename = source_file.filename
        await source_file.save(fp="./Compile/{}".format(filename))
        parsedFileName.pop()
        parsedFileName.append('s')
        assembly = '.'.join(parsedFileName)
        # Arguments for command line
        L = ['./compile.out', f'"{compiler}"', f'"{filename}"', f'"{flags}"']

        # Create thread and run
        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'done': False}
        thread = threading.Thread(target=self.runCompilation, args=(L, mutex, outputDict))
        thread.run()

        # Wait until thread has finished
        while not outputDict['done']:
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()

        output = outputDict['output']
        _error = outputDict['error']

        #Try sending output
        n = 1500
        try:
            if output != '':
                out = [output[i:i+n] for i in range(0, len(output), n)]
                for i in range(0, len(out)):
                    await ctx.channel.send("```%s```" % (out[i]))
                if not preprocess:
                    try:
                        with open('Compile/'+assembly, 'rb') as f:
                            await ctx.channel.send(file=File(f, 'Compile/%s' % assembly))
                    except Exception:
                        pass
            if _error != '':
                await ctx.channel.send('Errors:')
                out = [_error[i:i+n] for i in range(0, len(_error), n)]
                for i in range(0, len(out)):
                    await ctx.channel.send("```%s```" % (out[i]))
        except Exception:
            pass
        # Cleanup
        L = ["bash", "Compilecleanup.sh"]
        process = Popen(L)
        out, err = process.communicate()
        process.kill()

    # Dumb command that echos a user's message
    @slash_command(description='*sigh')
    async def echo(self, ctx, msg: Option(str, 'Why do you want to run this?')):
        if ctx.author.bot:
            return
        await ctx.respond(msg)

    # Function that keeps kicost command from blocking bot
    def execKiCost(self, args, mutex, outputRef):
        process = Popen(args, stdout=PIPE, stderr=PIPE, universal_newlines=True)
        output, _error = process.communicate()
        process.kill()
        mutex.acquire()
        outputRef['output'] = output
        outputRef['error'] = _error
        outputRef['done'] = True
        mutex.release()

    #Compile command. This is commented out until the discord API allows files in slash commands
    @slash_command(description='Run kicost on a BOM')
    async def kicost(
        self, 
        ctx, 
        bom: Option(discord.Attachment, 'The BOM generated by KiCAD'),
        flags: Option(str, 'The flags for kicost')
    ):
        if __debug__:
            file = await bom.to_file()
            await ctx.respond(file=file)
            return
        parsedFileName = bom.filename.split('.')
        if parsedFileName[-1] == 'xml' or parsedFileName[-1] == 'csv':
            if '-i' in flags or '--input' in flags:
                await ctx.respond('Error: Unrecognized flag. The bot automatically uses the input flag!')
                return
            if '-o' in flags or '--output' in flags:
                await ctx.respond('Error: Unrecognized flag. The bot automatically uses the output flag!')
                return
            if '-h' in flags or '--help' in flags:
                await help.help(ctx, self.client.user.avatar_url, 'kicost')
                return
            if '-v' in flags or '--version' in flags:
                await ctx.respond('Error: Version flag is not supported!')
                return
            if '--info' in flags:
                await ctx.respond('Error: Info flag is not supported!')
                return
            if '--gui' in flags:
                await ctx.respond('Error: GUI flag is not supported!')
                return
            if '--user' in flags:
                await ctx.respond('Error: User flag is not supported!')
                return
            if '--setup' in flags:
                await ctx.respond('Error: Setup flag is not supported!')
                return
            if '--unsetup' in flags:
                await ctx.respond('Error: Unsetup flag is not supported!')
                return
            filename = bom.filename
            await bom.save(fp='./kicost/{}'.format(filename))
            parsedFileName.pop()
            parsedFileName.append('xlsx')
            outFile = '.'.join(parsedFileName)
            L = ['kicost', '-i', 'kicost/%s' % filename, '-o', 'kicost/%s' % outFile]
            if len(flags) != 0:
                L += list(flags)
            cmdOutputDict = {'output': '', 'error': '', 'done': False}
            mutex = threading.Lock()
            kicostCmd = threading.Thread(target=self.execKiCost, args=(L, mutex, cmdOutputDict))
            kicostCmd.start()
            await ctx.respond('Processing request. This may take a while.')

            mutex.acquire()
            while not cmdOutputDict['done']:
                mutex.release()
                await asyncio.sleep(1)
                mutex.acquire()
            mutex.release()

            output = cmdOutputDict['output']
            error = cmdOutputDict['error']
            #Try sending output
            n = 1500
            try:
                if output != '':
                    out = [output[i:i+n] for i in range(0, len(output), n)]
                    for i in range(0, len(out)):
                        await ctx.channel.send("```%s```" % (out[i]))
                if error != '':
                    out = [error[i:i+n] for i in range(0, len(error), n)]
                    for i in range(0, len(out)):
                        await ctx.channel.send("```%s```" % (out[i]))
                with open('kicost/%s' % outFile, 'rb') as f:
                    await ctx.channel.send("KiCost is powered by the Kitspace PartInfo API. Partinfo hooks into paid-for 3rd party services. If you find KiCost useful please donate to the Kitspace Open Collective. If Kitspace doesn't receive enough donations then this free service will have to be shut down. https://opencollective.com/kitspace", file=File(f, outFile))
                L = ['rm', 'kicost/%s' % filename, 'kicost/%s' % outFile]
                process = Popen(L)
                process.communicate()
                process.kill()
            except (Exception):
                pass

    #Command that displays level leaderboard
    @slash_command(description='Shows xp leaderboard for server')
    async def leaderboard(self, ctx):
        #Throw error if user tries running inside DM
        if isinstance(ctx.channel, discord.channel.DMChannel):
            await ctx.respond('Please run this command in a server!')
            return
        db = EE_DB(self.database, ctx.guild.id)
        #Get leaders and users current position in leaderboard
        Leaders, pos = db.getLeaderBoard(ctx.author.id)
        #Respond if no errors occured
        if db.getError() == 0:
            #Construct embed
            embed=discord.Embed(title="Leaderboard", description="These are the top members on the server.", color=embedColor)
            for item in Leaders:
                user = discord.utils.find(lambda m: m.id == item[0], ctx.guild.members)
                embed.add_field(name="#%d" % item[2], value="%s\nLevel %d" % (user.name, item[1]), inline=False)
            embed.set_footer(text='You are #%d in the leaderboard.' % pos)
            embed.set_thumbnail(url=self.client.user.display_avatar.url)
            #Respond to user
            await ctx.respond(embed=embed)

    @slash_command(description='Change radix of a given number')
    async def radix(
        self, 
        ctx, 
        convert_from: Option(int, 'Current Radix'), 
        convert_to: Option(int, 'Desired Radix'), 
        number: Option(str, 'This is self-explanatory')
    ):
        try:
            dec_number = int(number, base=convert_from)
            new_number = DB.base_repr(dec_number, convert_to)
        except ValueError as err:
            await ctx.respond(f'Convert_from: {str(err)}')
            return
        #construct embed and send it
        embed=discord.Embed(description=f'Base {convert_from} {number} = Base {convert_to} {new_number}', color=embedColor)
        embed.set_thumbnail(url=self.client.user.display_avatar.url)
        await ctx.respond(embed=embed)
        return

    @slash_command(description='Description converts between mils and mm.')
    async def mils_mm(
        self,
        ctx,
        convert_to: Option(str, 'Convert to this unit.', choices = ['mils', 'mm']),
        value: Option(float, 'The value that gets converted.')
    ):
        # Select the proper case
        if convert_to == 'mils':
            # 1 mm == 39.3701 mils
            newValue = value * 39.3701
            newValue = f'{value} mm == {newValue} mils'
        else:
            # 1 mil == 0.0254 mm
            newValue = value * 0.0254
            newValue = f'{value} mils == {newValue} mm'
        await ctx.respond(newValue)
        return

    @tasks.loop(hours=1)  #Run this task every hour in the background
    async def backupDB(self, launched: bool):
        time = datetime.now()
        if ((time.hour == 0) or (time.hour == 12)) and launched:
            scopes = ['https://www.googleapis.com/auth/drive']
            #key_file_location = 'client_secrets.json'

            # Load credentials
            try:
                credentials = ServiceAccountCredentials.from_json_keyfile_name(self.key_file_location, scopes=scopes)
            except Exception as err:
                print(err)
                return

            #Log into account
            service = build('drive', 'v3', credentials=credentials)

            # Get files from google drive
            results = service.files().list(fields="nextPageToken, files(id, name)").execute()
            items = results.get('files', [])
            index = -1
            minIndex = 0
            minIndexID = ''
            firstEntry = True

            # Calculate indexes
            if not items:
                pass
            else:
                for item in items:
                    if 'EE_Bot' in item['name']:
                        result = re.sub('[^0-9]', '', item['name'])
                        result = int(result)
                        if result < minIndex or firstEntry:
                            minIndex = result
                            minIndexID = item['id']
                            firstEntry = False
                        if result > index:
                            index = result

            index += 1
            # Create file metadata
            file_meta = {'name': 'EE_Bot_%s.db' % (str(index)),
                         'parents': ['1J9tehWK-WtSOQyz3lxpHtBR7YQKQW3VE']}

            #Upload file
            media = MediaFileUpload(self.database, resumable=True)
            file = service.files().create(body=file_meta, media_body=media, fields='id').execute()
            # Print ID to log file
            print('File ID: %s' % file.get('id'))

            # Delete old backup
            index -= 20
            if minIndex < index:
                #Delete file by ID
                service.files().delete(fileId=minIndexID).execute()
                print('Delete File ID: %s' % minIndexID)
