# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from discord.commands import slash_command
from discord.commands import Option
from discord.ext import commands
from EE_Bot_DB import EE_DB
from EE_Bot_DB import cogEnum
from subprocess import PIPE
from subprocess import Popen
import threading
import asyncio
import os
#import io
powerExists = os.path.isdir('power')


class Power(commands.Cog):
    def __init__(self, client, database, MASTER):
        self.client = client
        self.database = database
        self.MASTER = MASTER
        return

    def runCommand(self, args, inputDict):
        #Create process to run command
        process = Popen(args, stdout=PIPE, stderr=PIPE, universal_newlines=True)
        #Run the command and capture output
        #Most commands have timeout implemented already
        inputDict['output'], inputDict['error'] = process.communicate()

        inputDict['done'] = True
        return

    @slash_command(description='Calculates power')
    async def power(
        self,
        ctx,
        calc_type: Option(str, 'Type of power calculation', choices = ['Real', 'Reactive', 'Apparent', 'Power']),
        voltage: Option(float, 'The voltage supplied to the circuit in Volts', default = None, required = False),
        current: Option(float, 'The current supplied to the circuit in Amps', default = None, required = False),
        resistance: Option(float, "The circuit's resistance in Ohms", default = None, required = False),
        reactance: Option(float, "The circuit's reactance in Ohms", default = None, required = False)
    ):
        # Perform prechecks
        if ctx.guild is None:
            db = EE_DB(self.database, -1, False)
        else:
            db = EE_DB(self.database, ctx.guild.id)

        if not db.checkCog(cogEnum.Power):
            await ctx.respond('This command has been disabled!')
            return

        # Create list for arguments in the command line
        L = ['power/power.out', calc_type.lower()]

        # Append the proper flags
        if voltage is not None:
            L.append('--voltage')
            L.append(str(voltage))
        if current is not None:
            L.append('--current')
            L.append(str(current))
        if resistance is not None:
            L.append('--resistance')
            L.append(str(resistance))
        if reactance is not None:
            L.append('--reactance')
            L.append(str(reactance))

        #Create mutex
        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'done': False}
        #Create thread
        PowerThread = threading.Thread(target=self.runCommand, args=(L, outputDict))
        #Run command
        PowerThread.start()
        #Respond so discord doesn't freak out
        await ctx.respond('Calculating...')
        #acquire mutex
        mutex.acquire()
        while not outputDict['done']:
            #Release mutex
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()
        output = outputDict['output']
        _error = outputDict['error']
        #check for outputs
        if len(output) != 0:
            await ctx.channel.send(f"```{output}```")
        if len(_error) != 0:
            await ctx.channel.send(f"```diff\n- {_error}```")
        return

    @slash_command(description='Calculates line to phase and vice versa')
    async def line_phase(
        self,
        ctx,
        configuration: Option(str, 'The circuit configuration', choices = ['delta', 'star', 'wye']),
        line_voltage: Option(float, 'The line voltage in Volts', default = None, required = False),
        phase_voltage: Option(float, 'The phase voltage in Volts', default = None, required = False),
        line_current: Option(float, 'The line current in Amps', default = None, required = False),
        phase_current: Option(float, 'The phase current in Amps', default = None, required = False)
    ):
        # Perform prechecks
        if ctx.guild is None:
            db = EE_DB(self.database, -1, False)
        else:
            db = EE_DB(self.database, ctx.guild.id)

        if not db.checkCog(cogEnum.Power):
            await ctx.respond('This command has been disabled!')
            return

        # Create list for arguments in the command line
        L = ['power/power.out', 'line-phase', f'--{configuration}']

        # Append the proper flags
        if line_voltage is not None:
            L.append('--line-voltage')
            L.append(str(line_voltage))
        if phase_voltage is not None:
            L.append('--phase-voltage')
            L.append(str(phase_voltage))
        if line_current is not None:
            L.append('--line-current')
            L.append(str(line_current))
        if phase_current is not None:
            L.append('--phase-current')
            L.append(str(phase_current))

        #Create mutex
        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'done': False}
        #Create thread
        PowerThread = threading.Thread(target=self.runCommand, args=(L, outputDict))
        #Run command
        PowerThread.start()
        #Respond so discord doesn't freak out
        await ctx.respond('Calculating...')
        #acquire mutex
        mutex.acquire()
        while not outputDict['done']:
            #Release mutex
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()
        output = outputDict['output']
        _error = outputDict['error']
        #check for outputs
        if len(output) != 0:
            await ctx.channel.send(f"```{output}```")
        if len(_error) != 0:
            await ctx.channel.send(f"```diff\n- {_error}```")
        return

    @slash_command(description='Calculates the reactance')
    async def reactance(
        self,
        ctx,
        frequency: Option(float, 'The frequency that the AC circuit is operating at. Uses Hz as the default'),
        frequency_units: Option(str, 'The units that the frequency is in', default = None, required = False, choices = ['Hz', 'Radians']),
        inductance: Option(float, 'The inductance of the circuit in Henries', default = None, required = False),
        capacitance: Option(float, 'The capacitance of the circuit in Farads', default = None, required = False)
    ):
        # Perform prechecks
        if ctx.guild is None:
            db = EE_DB(self.database, -1, False)
        else:
            db = EE_DB(self.database, ctx.guild.id)

        if not db.checkCog(cogEnum.Power):
            await ctx.respond('This command has been disabled!')
            return

        # Create list for arguments in the command line
        L = ['power/power.out', 'reactance', '--frequency', str(frequency)]

        # Append the proper flags
        if frequency_units is not None:
            if frequency_units == 'Radians':
                frequency_units = 'rads'
            L.append(frequency_units)
        if inductance is not None:
            L.append('--inductance')
            L.append(str(inductance))
        if capacitance is not None:
            L.append('--capacitance')
            L.append(str(capacitance))

        #Create mutex
        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'done': False}
        #Create thread
        PowerThread = threading.Thread(target=self.runCommand, args=(L, outputDict))
        #Run command
        PowerThread.start()
        #Respond so discord doesn't freak out
        await ctx.respond('Calculating...')
        #acquire mutex
        mutex.acquire()
        while not outputDict['done']:
            #Release mutex
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()
        output = outputDict['output']
        _error = outputDict['error']
        #check for outputs
        if len(output) != 0:
            await ctx.channel.send(f"```{output}```")
        if len(_error) != 0:
            await ctx.channel.send(f"```diff\n- {_error}```")
        return
