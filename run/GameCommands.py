# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from discord.commands import slash_command
from discord.commands import Option
from discord import File
from discord.ext import commands
from discord.ext import tasks
from EE_Bot_DB import EE_DB
from EE_Bot_DB import cogEnum
from quizLogic import QuizLogic
import pickle as pk
import io


class Games(commands.Cog):
    """Games Commands"""

    #Initialize Games Cog
    def __init__(self, MASTER, client, database):
        self.MASTER = MASTER
        self.client = client
        self.games = []
        self.__changed = False
        self.database = database

    #Command to create a quiz
    @slash_command(description='Start a quiz')
    async def quiz(
        self, 
        ctx, 
        difficulty: Option(str, 'Choose the difficulty', choices = ['very easy', 'easy', 'normal', 'hard', 'very hard'])
    ):
        #Check if in server or DM
        if ctx.guild is None:
            db = EE_DB(self.database, -1, False)
        else:
            db = EE_DB(self.database, ctx.guild.id)
        #Check if the command has been disabled
        if not db.checkCog(cogEnum.Games):
            await ctx.respond('This command has been disabled.')
            return

        #check if user has game ongoing...
        for i in range(0, len(self.games)):
            if self.games[i].userID == ctx.author.id:
                #Send error message
                await ctx.respond("<@%d> Cannot create new game! Please finish or quit current game." % (ctx.author.id))
                return
        #Check difficulty and create new QuizLogic object and append it to list
        if difficulty == 'very easy':
            newQuiz = QuizLogic('very easy', ctx.author.id)
            self.games.append(newQuiz)
            await ctx.channel.send("<@%d> The difficulty selected is currently in the beta version. If you encounter any errors, such as an answer not counted correctly, or the bot has the wrong result, please report the question, bot answer, and the error here: https://gitlab.com/tomschmitz98/ee_bot/-/issues/1" % (ctx.author.id))
        elif difficulty == 'easy':
            newQuiz = QuizLogic('easy', ctx.author.id)
            self.games.append(newQuiz)
            await ctx.channel.send("<@%d> The difficulty selected is currently in the beta version. If you encounter any errors, such as an answer not counted correctly, or the bot has the wrong result, please report the question, bot answer, and the error here: https://gitlab.com/tomschmitz98/ee_bot/-/issues/2" % (ctx.author.id))
        elif difficulty == 'normal':
            #newQuiz = QuizLogic('normal', ctx.author.id)
            #self.games.append(newQuiz)
            await ctx.channel.send("Oops, it looks like this difficulty is still under development.")
        elif difficulty == 'hard':
            await ctx.channel.send("Oops, it looks like this difficulty is still under development.")
            #newQuiz = QuizLogic('hard', ctx.author.id)
            #self.games.append(newQuiz)
        elif difficulty == 'very hard':
            await ctx.channel.send("Oops, it looks like this difficulty is still under development.")
            #newQuiz = QuizLogic('very hard', ctx.author.id)
            #self.games.append(newQuiz)
        else:  #Send error message if difficulty entered is invalid
            await ctx.channel.send("<@%d> Non-valid difficulty entered. Please type '!help quiz' for more help." % (ctx.author.id))
            return

        # Change modified flag
        self.__changed = True

        #Get the first question, file, and index
        question, file, index = self.games[len(self.games)-1].nextQuestion()

        #Send question
        if file is not None:
            #Get file extension
            extension = file.split('.')[-1]
            with open(file, 'rb') as f:
                await ctx.respond("<@%d> Question %d/5: %s" % (ctx.author.id, index, question), file=File(f, f'Question{index}.{extension}'))
        else:
            await ctx.respond("<@%d> Question %d/5: %s" % (ctx.author.id, index, question))

    #Answer command
    @slash_command(description='Your answer to the game.')
    async def answer(
        self, 
        ctx, 
        ans: Option(str, "Your answer to the question. Don't forget units!")
    ):
        #Check if in server or DM
        if ctx.guild is None:
            db = EE_DB(self.database, -1, False)
        else:
            db = EE_DB(self.database, ctx.guild.id)
        #Check if the command has been disabled
        if not db.checkCog(cogEnum.Games):
            await ctx.respond('This command has been disabled.')
            return
        ongoing_game = False

        #Check for ongoing games
        index = 0
        for i in range(0, len(self.games)):
            if self.games[i].userID == ctx.author.id:
                ongoing_game = True
                index = i
                break

        if ongoing_game:
            #Register answer
            accepted, response = self.games[index].ans(ans)
            if not accepted:  #Check if answer was accepted, send error if not
                await ctx.respond("<@%d> %s" % (ctx.author.id, response))
            else:  #Get next question
                # Change modified flag
                self.__changed = True
                question, file, i = self.games[index].nextQuestion()
                #Send next question, if possible
                if file is not None:
                    #Get file extension
                    extension = file.split('.')[-1]
                    with open(file, 'rb') as f:
                        await ctx.respond("<@%d> Question %d/5: %s" % (ctx.author.id, i, question), file=File(f, f'Question{i}.{extension}'))
                elif question is not None:
                    await ctx.respond("<@%d> Question %d/5: %s" % (ctx.author.id, i, question))

                #Check results
                if self.games[index].showResults():
                    #Get score and answers
                    score, fig = self.games[index].render_table()

                    #end of construction of table
                    await ctx.respond("<@%d> Your score was %d/5" % (ctx.author.id, score))
                    with io.BytesIO() as buf:
                        fig.savefig(buf, format='png')
                        buf.seek(0)
                        await ctx.channel.send(file=File(buf, 'results.png'))
        else:  #Send error message
            await ctx.respond("<@%d> Oops, it looks like you do not have any ongoing games." % (ctx.author.id))
        return

    #Quit command
    @slash_command(description='Quit your current game.')
    async def quit(self, ctx):
        #Check if in server or DM
        if ctx.guild is None:
            db = EE_DB(self.database, -1, False)
        else:
            db = EE_DB(self.database, ctx.guild.id)
        #Check if the command has been disabled
        if not db.checkCog(cogEnum.Games):
            await ctx.respond('This command has been disabled.')
            return
        #pass #Check if player has ongoing game...
        ongoing_game = False
        index = 0
        for i in range(0, len(self.games)):
            if self.games[i].userID == ctx.author.id:
                ongoing_game = True
                index = i
                break
        if ongoing_game:
            # Set modified flag
            self.__changed = True
            # delete game with coresponding ID
            self.games.remove(self.games[index])
            await ctx.respond("<@%d> you have quit your ongoing game." % (ctx.author.id))
        else:  #Send error message
            await ctx.respond("<@%d> you do not have an ongoing game." % (ctx.author.id))

    #Command that displays ongoing game
    @slash_command(description='Repeats prompt of ongoing game.')
    async def game(self, ctx):
        #Check if in server or DM
        if ctx.guild is None:
            db = EE_DB(self.database, -1, False)
        else:
            db = EE_DB(self.database, ctx.guild.id)
        #Check if the command has been disabled
        if not db.checkCog(cogEnum.Games):
            await ctx.respond('This command has been disabled')
            return
        #pass #Check if player has ongoing game...
        ongoing_game = False
        index = 0
        for i in range(0, len(self.games)):
            if self.games[i].userID == ctx.author.id:
                ongoing_game = True
                index = i
                break
        if ongoing_game:
            # Get current status of game
            question, file, i = self.games[index].nextQuestion()
            #display status of game
            if file is not None:
                with open(file, 'rb') as f:
                    await ctx.respond("<@%d> Question %d/5: %s" % (ctx.author.id, i, question), file=File(f, 'Question%d.jpg' % (i)))
            else:
                await ctx.respond("<@%d> Question %d/5: %s" % (ctx.author.id, index, question))
        else:  #Send error message
            await ctx.respond("<@%d> Oops, it looks like you don't have an ongoing game." % (ctx.author.id))

    # Function that can be called anytime to save the games.
    # This should only be called at the end of execution
    def saveGames(self):
        if self.__changed:
            #Save games
            with open('/mnt/disk/ongoingGames.pickle', 'wb') as f:
                pk.dump(self.games, f)

    @tasks.loop(minutes=5)  #Run this task in the background
    async def backupGames(self):
        # Check if the games have been modified
        if self.__changed:
            # Save Games
            with open('/mnt/disk/ongoingGames.pickle', 'wb') as f:
                pk.dump(self.games, f)
            # Reset flag
            self.__changed = False

    def loadGames(self):
        #Load games
        with open('/mnt/disk/ongoingGames.pickle', 'rb') as f:
            self.games = pk.load(f)
