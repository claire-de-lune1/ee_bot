# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import discord
from discord.commands import slash_command
from discord.commands import Option
from discord.commands import permissions
from discord.ext import commands
from EE_Bot_DB import EE_DB
from EE_Bot_DB import cogEnum
from EE_Bot_DB import channelType
from subprocess import PIPE
from subprocess import Popen
import threading
import asyncio
import globals as GL

# Initialize and get globals
GL.InitLaunch()
originGuild = GL.originGuild
launch = GL.launchFlag


class Hardware(commands.Cog):
    """Hardware Commands"""

    # Initialize Cog
    def __init__(self, MASTER, Client, database):
        self.MASTER = MASTER
        self.client = Client
        self.database = database

    # Command to check temperature of CPU
    @slash_command(guild_ids=[originGuild], description='Checks the temperature of CPU', default_permission=False)
    @permissions.default_permissions(
        administrator=True
    )
    async def temp(self, ctx):
        db = EE_DB(self.database, ctx.guild.id)
        # noinspection SpellCheckingInspection
        process = Popen(['vcgencmd', 'measure_temp'], stdout=PIPE)  # Run terminal command to get temperature
        output, _error = process.communicate()  # Capture output
        process.kill()
        output = float(output[output.index(bytes('=', 'ascii')) + 1:output.rindex(
            bytes("'", 'ascii'))])  # Convert output to floating point
        channel_id = db.getLoggingID(channelType.log)
        if channel_id == 0:
            await ctx.respond('Please configure guild.')
            return
        LOG = self.client.get_channel(channel_id)
        await LOG.send('CPU Temp: %.1f °C' % output)  # Send temperature to Log channel
        await ctx.respond('Check log channel')

    @slash_command(guild_ids=[originGuild], description='Turn on fan to cool CPU', default_permission=False)
    @permissions.default_permissions(
        administrator=True
    )
    async def cool(
            self,
            ctx,
            time: Option(float, 'Amount of time that the fan is on.', required = False, default = 10.0)
    ):
        await ctx.respond('This command is not functional.')

    # function for thread to run a command
    # noinspection PyMethodMayBeStatic
    def executeCmd(self, args, mutex, outputRef):
        # Run terminal command
        print("Started command")
        process = Popen(args, stdout=PIPE, universal_newlines=True)
        output, _error = process.communicate()  # Capture output
        process.kill()
        print("Concluded command")
        # print(output)
        # print("\n\nLength of Output: ", len(output))
        mutex.acquire()
        outputRef['output'] = output
        outputRef['error'] = _error
        outputRef['done'] = True
        mutex.release()
        return

    @slash_command(description='Runs terminal commands on hardware')
    @permissions.default_permissions(
        administrator=True
    )
    async def cmd(
            self,
            ctx,
            command: Option(str, 'The command ran in the command line.')
    ):
        if ctx.guild is None:
            db = EE_DB(self.database, -1, False)
        else:
            db = EE_DB(self.database, ctx.guild.id)
        if not db.checkCog(cogEnum.Hardware):
            await ctx.respond('This command has been disabled.')
            return
        developer = False
        guild = discord.utils.find(lambda g: g.id == 758600193070858251, ctx.author.mutual_guilds)
        if guild is not None:
            mem = discord.utils.find(lambda m: m.id == ctx.author.id, guild.members)
            role = discord.utils.find(lambda r: r.name == 'Bot Developer', mem.roles)
            if role is not None:
                developer = True

        # Make sure rm command is not ran
        if command[:2] == 'rm':
            developer = False
            ctx.author.id = 0
        # Execute command on certain conditions
        if (ctx.author.id == self.MASTER) or developer:
            await ctx.respond('Running command')
            # Convert args into list
            L = command.split(' ')

            cmdOutputDict = {'output': '', 'error': '', 'done': False}
            mutex = threading.Lock()

            # Create and start thread
            cmdThread = threading.Thread(target=self.executeCmd, args=(L, mutex, cmdOutputDict))
            cmdThread.start()

            # Wait for thread to stop running without blocking other commands
            mutex.acquire()
            while not cmdOutputDict['done']:
                mutex.release()
                await asyncio.sleep(1)
                mutex.acquire()

            output = cmdOutputDict['output']
            error = cmdOutputDict['error']
            # Try sending output
            n = 1500
            try:
                if output != '':
                    out = [output[i:i + n] for i in range(0, len(output), n)]
                    for i in range(0, len(out)):
                        await ctx.channel.send("```%s```" % (out[i]))
                if error != '':
                    out = [error[i:i + n] for i in range(0, len(error), n)]
                    for i in range(0, len(out)):
                        await ctx.channel.send("```%s```" % (out[i]))
            except Exception:
                pass

            mutex.release()
        else:  # Send error message
            await ctx.respond('Nice try')
