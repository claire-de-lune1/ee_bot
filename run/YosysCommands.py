# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import discord
from discord.commands import slash_command
from discord.commands import Option
from discord import File
from discord.ext import commands
from EE_Bot_DB import EE_DB
from EE_Bot_DB import cogEnum
from subprocess import PIPE
from subprocess import Popen
from subprocess import TimeoutExpired
import threading
import asyncio
YS = False  #Update this later
try:
    import yosys.yosys as ys
    YS = True
except (Exception):
    YS = False
embedColor = 0x0e456e


class Yosys(commands.Cog):
    """Commands that run Yosys commands (http://www.clifford.at/yosys/). For VHDL, the verific library extension is required, but it requires a license"""
    def __init__(self, MASTER, client, database):
        self.MASTER = MASTER
        self.client = client
        self.database = database

    def exec_yosys_cmd(self, cmd, filename, mutex, outputRef):
        #Generate command script
        modules, Errors, top = ys.run_command(cmd,
                                              "yosys/%s" % filename)
        _error = ''
        output = ''
        L = ["yosys", "-s", "yosys/yosys.sh", "yosys/%s" % filename]
        # Create new terminal process
        process = Popen(L, stdout=PIPE, universal_newlines=True, stderr=PIPE)
        #Run terminal process
        try:
            # This process is only allowed 1 minute to save resources
            output, _error = process.communicate(timeout=60)  #Capture output
        #Catch timeout exception
        except TimeoutExpired:
            #Set the error and output
            output = ''
            _error += '\n Timeout error: process took too long.'
        finally:
            #Kill the process
            process.kill()

        #Set outputs
        mutex.acquire()
        outputRef['output'] = output
        outputRef['error'] = _error
        outputRef['modules'] = modules
        outputRef['top'] = top
        outputRef['done'] = True
        mutex.release()

    @slash_command(description='Command that goes through your design and generates a picture of what it sees.')
    async def pre_synth(
        self, 
        ctx, 
        attachment: Option(discord.Attachment, 'Verilog file that gets analyzed.'),
        flags: Option(str, 'flags', required = False, default = '')
    ):
        if ctx.guild is None:
            db = EE_DB(self.database, -1, False)
        else:
            db = EE_DB(self.database, ctx.guild.id)
        if not db.checkCog(cogEnum.Yosys):
            await ctx.respond('This command has been disabled.')
            return
        #Get filename
        filename = attachment.filename
        #Check file extension
        if not filename.endswith('.v'):
            await ctx.respond('Please send 1 Verilog file')
            return
        #Save file into local directory
        await attachment.save(fp="yosys/{}".format(filename))
        #Create mutex
        mutex = threading.Lock()
        #Output of threaded function
        outputDict = {'output': '', 'error': '', 'modules': 0,
                        'done': False, 'top': ''}
        #Create thread to prevent blocking
        yosys_thread = threading.Thread(target=self.exec_yosys_cmd,
                                        args=('pre-synth', filename,
                                                mutex, outputDict))
        yosys_thread.start()
        await ctx.respond('Running command. This may take a while.')

        #acquire mutex
        mutex.acquire()
        #Loop while thread is not finished
        while not outputDict['done']:
            #Release mutex
            mutex.release()
            #Wait 1 second
            await asyncio.sleep(1)
            #acquire mutex
            mutex.acquire()

        #Get outputs from thread
        output = outputDict['output']
        _error = outputDict['error']
        modules = outputDict['modules']

        #Try sending output
        n = 1500
        try:
            #Check if output is populated
            if output != '':
                #Check for print flag
                if '-print' in flags: 
                    #Print output message
                    out = [output[i:i+n] for i in range(0, len(output), n)]
                    for i in range(0, len(out)):
                        await ctx.channel.send("```%s```" % (out[i]))
                #Try sending file(s)
                try:
                    if len(modules) == 1:
                        with open('yosys/%s.jpg' % (filename), 'rb') as f:
                            await ctx.channel.send(file=File(f, '%s.jpg' % (filename)))
                    elif len(modules) > 1:
                        for i in range(0, len(modules)):
                            with open('yosys/%s_%s.jpg' % (filename, modules[i]), 'rb') as f:
                                await ctx.channel.send(file=File(f, '%s_%s.jpg' % (filename, modules[i])))
                    else:
                        pass
                #Check for discord error
                except discord.HTTPException as Err:
                    #File size error
                    if Err.code == 40005:
                        #Send error message
                        await ctx.channel.send('Output file too large. Trying to optimize.')
                        file = None
                        #Optimize jpeg file(s)
                        if len(modules) == 1:
                            file = 'yosys/%s.jpg' % filename
                            process = Popen(['jpegoptim', file], stdout=PIPE, universal_newlines=True)
                            out, err = process.communicate()
                            process.kill()
                        else:
                            for i in range(0, len(modules)):
                                file = 'yosys/%s_%s.jpg' % (filename, modules[i])
                                process = Popen(['jpegoptim', file], stdout=PIPE, universal_newlines=True)
                                out, err = process.communicate()
                                process.kill()
                        #Try sending files again
                        try:
                            if len(modules) == 1:
                                with open('yosys/%s.jpg' % (filename), 'rb') as f:
                                    await ctx.channel.send(file=File(f, '%s.jpg' % (filename)))
                            elif len(modules) > 1:
                                for i in range(0, len(modules)):
                                    with open('yosys/%s_%s.jpg' % (filename, modules[i]), 'rb') as f:
                                        await ctx.channel.send(file=File(f, '%s_%s.jpg' % (filename, modules[i])))
                        #Catch discord error
                        except discord.HTTPException as Err:
                            #Attachment too large
                            if Err.code == 40005:
                                await ctx.channel.send('Synthesis successful, but output files are too large.')
                            #Other errors
                            else:
                                print(Err)
                                await ctx.channel.send('An error occured with discord.')
                        #Generic error handler
                        except Exception:
                            print(Exception)
                            await ctx.channel.send('An error occured and was reported.')
                    #Other discord error
                    else:
                        print(Err)
                        await ctx.channel.send('An error occured with discord.')
                #Generic error handler
                except (Exception):
                    await ctx.channel.send('An error occured. Please try again with the -print flag to see the error')
            #Print error message
            if _error != '':
                out = [_error[i:i+n] for i in range(0, len(_error), n)]
                for i in range(0, len(out)):
                    await ctx.channel.send("```%s```" % (out[i]))
        except (Exception):
            pass
        #Clean Yosys directory
        L = ["make", "cleanYosys"]
        process = Popen(L, stdout=PIPE, universal_newlines=True)
        output, _error = process.communicate()  #Capture output
        process.kill()

    #@commands.command(pass_context=True, enabled=YS, brief='Accepts 1 Verilog file at a time.', description='Command that goes through your design and generates a picture of what it sees.\nFlags:\nPrint the output of yosys: -print')
    @slash_command(description='Generates an optimized block diagram of your verilog design.')
    async def synth(
        self, 
        ctx, 
        attachment: Option(discord.Attachment, 'Your Verilog file.'),
        synth_proccess: Option(str, 'Select type of synthesis to be carried out. Leave blank if general.', required = False, choices = ['achronix', 'anlogic', 'coolrunner2', 'easic', 'ecp5', 'gowin', 'greenpak4', 'ice40', 'intel', 'sf2', 'xilinx'], default = 'synth'),
        flags: Option(str, 'flags', required = False, default = '')
    ):
        if ctx.guild is None:
            db = EE_DB(self.database, -1, False)
        else:
            db = EE_DB(self.database, ctx.guild.id)
        if not db.checkCog(cogEnum.Yosys):
            await ctx.respond('This command has been disabled.')
            return
        #Get filename
        filename = attachment.filename
        #check file extension
        if not filename.endswith('.v'):
            await ctx.respond('Please send 1 Verilog file.')
            return
        #Save file
        await attachment.save(fp="yosys/{}".format(filename))
        #Create mutex
        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'modules': 0,
                        'done': False, 'top': ''}
        #Create thread to prevent blocking
        yosys_thread = threading.Thread(target=self.exec_yosys_cmd,
                                        args=(synth_proccess, filename,
                                                mutex, outputDict))
        yosys_thread.start()
        await ctx.respond('Running command. this may take a while.')

        #Acquire mutex
        mutex.acquire()
        #Check if thread is done
        while not outputDict['done']:
            #Release mutex
            mutex.release()
            #Wait 1 second
            await asyncio.sleep(1)
            #Acquire mutex
            mutex.acquire()

        #Get output, errors, and modules 
        output = outputDict['output']
        _error = outputDict['error']
        modules = outputDict['modules']

        #Try sending output
        n = 1500
        try:
            #Check for output
            if output != '':
                #Check for print flag
                if '-print' in flags: 
                    out = [output[i:i+n] for i in range(0, len(output), n)]
                    for i in range(0, len(out)):
                        await ctx.channel.send("```%s```" % (out[i]))
                #Try sending file(s)
                try:
                    if len(modules) == 1:
                        with open('yosys/%s.jpg' % (filename), 'rb') as f:
                            await ctx.channel.send(file=File(f, '%s.jpg' % (filename)))
                    elif len(modules) > 1:
                        for i in range(0, len(modules)):
                            with open('yosys/%s_%s.jpg' % (filename, modules[i]), 'rb') as f:
                                await ctx.channel.send(file=File(f, '%s_%s.jpg' % (filename, modules[i])))
                #Catch discord error
                except discord.HTTPException as Err:
                    #Check if file(s) is too large
                    if Err.code == 40005:
                        #Send error message
                        await ctx.channel.send('Output file too large. Trying to optimize.')
                        file = None
                        #Optimize jpeg file(s)
                        if len(modules) == 1:
                            file = 'yosys/%s.jpg' % filename
                            process = Popen(['jpegoptim', file], stdout=PIPE, universal_newlines=True)
                            out, err = process.communicate()
                            process.kill()
                        else:
                            for i in range(0, len(modules)):
                                file = 'yosys/%s_%s.jpg' % (filename, modules[i])
                                process = Popen(['jpegoptim', file], stdout=PIPE, universal_newlines=True)
                                out, err = process.communicate()
                                process.kill()
                        #Try sending files again
                        try:
                            if len(modules) == 1:
                                with open('yosys/%s.jpg' % (filename), 'rb') as f:
                                    await ctx.channel.send(file=File(f, '%s.jpg' % (filename)))
                            elif len(modules) > 1:
                                for i in range(0, len(modules)):
                                    with open('yosys/%s_%s.jpg' % (filename, modules[i]), 'rb') as f:
                                        await ctx.channel.send(file=File(f, '%s_%s.jpg' % (filename, modules[i])))
                        #Catch discord error
                        except discord.HTTPException as Err:
                            #Check for file size error
                            if Err.code == 40005:
                                await ctx.channel.send('Synthesis successful, but output files are too large.')
                            #Catch other errors
                            else:
                                print(Err)
                                await ctx.channel.send('An error occured with discord.')
                        #Generic error handler
                        except Exception:
                            print('An error occured in pre_synth')
                            await ctx.channel.send('An error occured and was reported.')
                    else:
                        print(Err)
                        await ctx.channel.send('An error occured with discord.')
                #Generic error handler
                except Exception:
                    await ctx.channel.send('An error occured. Please try again with the -print flag to see the error')
            #Print errors
            if _error != '':
                out = [_error[i:i+n] for i in range(0, len(_error), n)]
                for i in range(0, len(out)):
                    await ctx.channel.send("```%s```" % (out[i]))
        except Exception:
            pass
        #Cleanup
        L = ["make", "cleanYosys"]
        process = Popen(L, stdout=PIPE, universal_newlines=True)
        output, _error = process.communicate()  #Capture output
        process.kill()

    @slash_command(description='Analyzes the given Verilog file and generates a spice file.')
    async def gen_spice(
        self, 
        ctx, 
        attachment: Option(discord.Attachment, 'Your Verilog file'),
        flags: Option(str, 'flags', required = False, default = '')
    ):
        if ctx.guild is None:
            db = EE_DB(self.database, -1, False)
        else:
            db = EE_DB(self.database, ctx.guild.id)
        if not db.checkCog(cogEnum.Yosys):
            await ctx.respond('This command has been disabled.')
            return
        #Get filename
        filename = attachment.filename
        #Check file extension
        if not filename.endswith('.v'):
            await ctx.respond('Please attach 1 Verilog file.')
            return
        #Save file to local directory
        await attachment.save(fp="yosys/{}".format(filename))
        #Create mutex
        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'modules': 0,
                        'done': False, 'top': ''}
        #Create thread to prevent blocking
        yosys_thread = threading.Thread(target=self.exec_yosys_cmd, args=('spice', filename, mutex, outputDict))

        yosys_thread.start()
        await ctx.respond('Running command. This may take a while.')

        #Acquire mutex
        mutex.acquire()
        #Check if thread is finished
        while not outputDict['done']:
            #Release mutex
            mutex.release()
            #Sleep 1 second
            await asyncio.sleep(1)
            #Acquire mutex
            mutex.acquire()

        #Get outputs from threaded function
        output = outputDict['output']
        _error = outputDict['error']
        #modules = outputDict['modules']

        #Try sending output
        n = 1500
        try:
            #Check the output
            if output != '':
                #Check for the print flag
                if '-print' in flags: 
                    #Print output
                    out = [output[i:i+n] for i in range(0, len(output), n)]
                    for i in range(0, len(out)):
                        await ctx.channel.send("```%s```" % (out[i]))
                #Try sending file
                try:
                    with open('yosys/%s.sp' % filename, 'rb') as f:
                        await ctx.channel.send(file=File(f, '%s.sp' % filename))
                #Check for discord errors
                except discord.HTTPException as Err:
                    #File too large error
                    if Err.code == 40005:
                        await ctx.channel.send('Output file too large.')
                    else:
                        #Print other errors
                        print(Err)
                        await ctx.channel.send('A discord error occured.')
                #Catch other errors
                except Exception:
                    await ctx.channel.send('An error occured. Please try again with the -print flag to see the error')
            #Print error
            if _error != '':
                out = [_error[i:i+n] for i in range(0, len(_error), n)]
                for i in range(0, len(out)):
                    await ctx.channel.send("```%s```" % (_error[i]))
        except Exception:
            pass
        #Cleanup
        L = ["make", "cleanYosysSpice"]
        process = Popen(L, stdout=PIPE, universal_newlines=True)
        output, _error = process.communicate()  #Capture output
        process.kill()

    @slash_command(description='Analyzes the given Verilog file and generates a json file.')
    async def gen_json(
        self, 
        ctx, 
        attachment: Option(discord.Attachment, 'Your Verilog file.'),
        flags: Option(str, 'flags', required = False, default = '')
    ):
        if ctx.guild is None:
            db = EE_DB(self.database, -1, False)
        else:
            db = EE_DB(self.database, ctx.guild.id)
        if not db.checkCog(cogEnum.Yosys):
            await ctx.respond('This command has been disabled')
            return
        #Get filename
        filename = attachment.filename
        #Check file extension
        if not filename.endswith('.v'):
            await ctx.respond('Please attach 1 Verilog file.')
            return
        #Save file to local directory
        await attachment.save(fp="yosys/{}".format(filename))
        #Create mutex
        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'modules': 0,
                        'done': False, 'top': ''}
        #Create thread to prevent blocking
        yosys_thread = threading.Thread(target=self.exec_yosys_cmd, args=('json', filename, mutex, outputDict))

        yosys_thread.start()
        await ctx.respond('Command running. This may take a while.')

        #Acquire mutex
        mutex.acquire()
        #Check if threaded function is done
        while not outputDict['done']:
            #Release mutex
            mutex.release()
            #Wait 1 second
            await asyncio.sleep(1)
            #Acquire mutex
            mutex.acquire()

        #Get outputs
        output = outputDict['output']
        _error = outputDict['error']
        #modules = outputDict['modules']

        #Try sending output
        n = 1500
        try:
            #Check output
            if output != '':
                #Check for print flag
                if '-print' in flags: 
                    out = [output[i:i+n] for i in range(0, len(output), n)]
                    for i in range(0, len(out)):
                        await ctx.channel.send("```%s```" % (out[i]))
                #Try sending file
                try:
                    with open('yosys/%s.json' % filename, 'rb') as f:
                        await ctx.channel.send(file=File(f,
                                                '%s.json' % filename))
                #Check for discord errors
                except discord.HTTPException as Err:
                    #File too large
                    if Err.code == 40005:
                        await ctx.channel.send('''Output file too large.''')
                    #Other errors
                    else:
                        print(Err)
                        await ctx.channel.send('''A discord error occured.''')
                #Catch other errors
                except Exception:
                    await ctx.channel.send('''An error occured. Please try again with the -print flag to see the error''')
            #Check for error messages
            if _error != '':
                out = [_error[i:i+n] for i in range(0, len(_error), n)]
                for i in range(0, len(out)):
                    await ctx.channel.send("```%s```" % (_error[i]))
        except Exception:
            pass
        #Cleanup
        L = ["make", "cleanYosysJSON"]
        process = Popen(L, stdout=PIPE, universal_newlines=True)
        output, _error = process.communicate()  #Capture output
        process.kill()
