# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import discord
from discord import File
from discord.commands import Option
from discord.commands import permissions
from GeneralCommands import General
from ModeratorCommands import Moderator
from GameCommands import Games
from HardwareCommands import Hardware
from serverConfigCommands import serverConfig
from ScheduleCommands import Schedule
from YosysCommands import YS
from YosysCommands import Yosys
from RFCommands import RF
from RFCommands import rfExists
from PowerCommands import Power
from PowerCommands import powerExists
import urllib.request
import time
import platform
import emoji
import rsa
import re
import io
from EE_Bot_DB import EE_DB
from EE_Bot_DB import channelType
from EE_Bot_help import help
from EE_Bot_help import get_commands
import globals as GL

# Current version of the bot
# The bot version is different from
# the database version.
__version__ = 'v2.1.1'

# Initialize global constants and import them
GL.InitLaunch()
launchFlag = GL.launchFlag
originGuild = GL.originGuild

TokenFile = None
# Two possible database locations
database = '/mnt/disk/EE_Bot.db'
testingDatabase = './EE_Bot.db'
key_file_location = 'client_secrets.json'

if platform.system() == 'Linux':
    testingDatabase = './EE_Bot.db'

privKey = None
pubKey = None

retries = 0

# Generate encryption keys for safe rebooting
if launchFlag:
    try:
        with open('images/EasterEggs/gold.jpg', 'rb') as f:
            keyData = f.read()
        # noinspection SpellCheckingInspection
        privKey = rsa.PrivateKey.load_pkcs1(keyData)
        # noinspection SpellCheckingInspection
        with open('EasterEggs/towelie.jpg', 'rb') as f:
            keyData = f.read()
        pubKey = rsa.PublicKey.load_pkcs1(keyData)
    except Exception:
        # noinspection SpellCheckingInspection
        pubKey, privKey = rsa.newkeys(4096)
        with open('images/EasterEggs/gold.jpg', 'wb') as f:
            f.write(privKey.save_pkcs1())
        # noinspection SpellCheckingInspection
        with open('EasterEggs/towelie.jpg', 'wb') as f:
            f.write(pubKey.save_pkcs1())

cog_db = None

disableDB = False

# Select proper database location
if launchFlag is False:
    cog_db = testingDatabase
else:
    cog_db = database

# Locate bot Token
if launchFlag:
    TokenFile = './token.txt'
else:
    TokenFile = 'testBot.txt'

# Read token from file
with open(TokenFile, 'r') as f:
    TOKEN = f.readline()
MASTER = 751265080963891230

# Give bot all intents
intents = discord.Intents.all()
# Bot's prefix
prefix = '!'

# Initialize bot
client = discord.Bot(command_prefix=prefix, intents=intents, help_command=None)

client.add_cog(General(MASTER, client, cog_db, key_file_location, __version__))
client.add_cog(Moderator(MASTER, client, cog_db))
client.add_cog(Games(MASTER, client, cog_db))
client.add_cog(serverConfig(client, cog_db, MASTER))
client.add_cog(Hardware(MASTER, client, cog_db))
client.add_cog(Schedule(client, cog_db, launchFlag))
if YS:
    client.add_cog(Yosys(MASTER, client, cog_db))
if rfExists:
    client.add_cog(RF(client, cog_db, MASTER))
if powerExists:
    client.add_cog(Power(client, cog_db, MASTER))


def UpdateDB(guild_id: int, Users: list) -> None:
    global cog_db
    db = EE_DB(cog_db, guild_id)
    for x in Users:
        if x.bot:
            continue
        db.insertUser(x.id, True)
        db.adjustRank(x.id)
    del db
    return


@client.event
async def on_ready():
    await client.change_presence(activity=discord.Activity(
        type=discord.ActivityType.playing, name='Type /help for usage'))
    # Load and start background tasks here
    try:
        client.get_cog('General').backupDB.start(launchFlag)
        client.get_cog('Schedule').loadQueues(launchFlag)
        client.get_cog('Schedule').saveQueues.start(launchFlag)
        client.get_cog('Games').loadGames()
        client.get_cog('Games').backupGames.start()
    except Exception:
        print('Failed to load games')
    db = EE_DB(cog_db, 0, False)
    start = db.getRunFlag()
    if not start:
        db.setRunFlag(True)
        db.resetGuildFlags()
        guilds = client.guilds
        for i in range(0, len(guilds)):
            guilds[i] = guilds[i].id
        db.setupGuildFlags(guilds)
        for x in client.guilds:
            UpdateDB(x.id, x.members)
    print('Bot Connected')
    return


def isPhraseIn(phrase, msg):
    return re.search(r"\b{}\b".format(phrase), msg, re.IGNORECASE) is not None


# Message Event
@client.event
async def on_message(ctx):
    # Automatically exit if bot
    if ctx.author.bot:
        return
    global retries
    retries = 0

    # Check for prefix
    if not ctx.content.startswith('!'):
        db = None

        if ctx.guild is None:
            return

        # Open database
        db = EE_DB(cog_db, ctx.guild.id)
        rank = None
        rankUp = False

        # update user posts
        if not disableDB:
            db.insertUser(ctx.author.id, True)
            rankUp, rank = db.updateUserPosts(ctx.author.id)

        watched, poliban, monitor, override = db.retrieveWatchlistFlags(
            ctx.author.id)

        if ctx.channel.category is not None and ctx.channel.category.name == 'Classes' and ctx.type == discord.MessageType.default:
            db.addMessage(ctx.id, ctx.channel.id)

        # check if user is monitored
        if watched and monitor:
            mCh = db.getLoggingID(channelType.watch)
            if mCh != 0:
                channel = client.get_channel(mCh)
                await channel.send('%s: %s' % (ctx.author.name, ctx.jump_url))

        # remove punctuation
        msg = re.sub(r'[^\w\s]', '', ctx.content)

        # Process Easter Eggs
        if isPhraseIn('railgun', msg):
            await ctx.channel.send("Did someone say Railgun?")
            with open('images/EasterEggs/railgun1.jpg', 'rb') as f:
                await ctx.channel.send(file=File(f, 'railgun1.jpg'))
            with open('images/EasterEggs/railgun2.jpg', 'rb') as f:
                await ctx.channel.send(file=File(f, 'railgun2.jpg'))
            with open('images/EasterEggs/railgun3.jpg', 'rb') as f:
                await ctx.channel.send(file=File(f, 'railgun3.jpg'))
            with open('images/EasterEggs/railgun4.jpg', 'rb') as f:
                await ctx.channel.send(file=File(f, 'railgun4.jpg'))
            with open('images/EasterEggs/railgun1.mp4', 'rb') as f:
                await ctx.channel.send(file=File(f, 'railgun1.mp4'))
            with open('images/EasterEggs/railgun2.mp4', 'rb') as f:
                await ctx.channel.send(file=File(f, 'railgun2.mp4'))
            with open('images/EasterEggs/railgun3.mp4', 'rb') as f:
                await ctx.channel.send(file=File(f, 'railgun3.mp4'))

        # Fetch server easter eggs
        eggs = db.fetchEasterEggs()
        for egg in eggs:
            # Check if activation phrase is in the message
            if isPhraseIn(egg['phrase'], msg):
                # Check if the easter egg is limited to those with the filter turned off
                if egg['Limited'] and db.checkUserFilter(ctx.author.id):
                    continue
                # Prepare easter egg and send it
                with io.BytesIO(egg['file']) as fp:
                    fp.seek(0)
                    f = File(fp, egg['filename'], spoiler=bool(egg['Limited']))
                    await ctx.channel.send(file=f)

        if db.getError() == 0:
            if rankUp:
                channel_id = db.getLoggingID(channelType.level)
                if channel_id != 0:
                    # Send level up message if rankUp is true
                    if rank != 1:
                        await client.get_channel(channel_id).send('Congratulations <@%d>, you just advanced to level %d' % (ctx.author.id, rank))
                    else:
                        await client.get_channel(channel_id).send(
                            "Congratulations <@%d>, you just advanced to level %d! To turn off level notifications, "
                            "type '!notif off'. "
                            % (ctx.author.id, rank))
        # Send any database errors to LOG channel
        else:
            channel_id = db.getLoggingID(channelType.log)
            if channel_id != 0:
                LOG = client.get_channel(channel_id)
                await LOG.send('''Unable to find %s in database'''
                               % ctx.author.name)
        del db
    # Process commands if prefix detected
    else:
        await client.process_application_commands(ctx)


# Reaction add event
@client.event
async def on_raw_reaction_add(payload):
    # Check for bot reaction
    if payload.member.bot:
        return
    # Open database
    db = EE_DB(cog_db, payload.guild_id)
    # search for poll
    msgID, Pairs = db.retrievePoll(payload.message_id)
    # If returned ID matches message ID, then process reactions to assign role
    if payload.message_id == msgID:
        guildID = payload.guild_id
        # Get server
        guild = discord.utils.find(lambda g: g.id == guildID, client.guilds)
        roleID = None
        # Search pairs for matching emoji
        for i in range(0, len(Pairs)):
            if Pairs[i][1] == emoji.demojize(str(payload.emoji)):
                roleID = Pairs[i][0]
                break
        # Get role that matches ID
        Role = discord.utils.get(guild.roles, id=roleID)
        # Check if role was returned
        if Role is not None:
            # Get member
            mem = discord.utils.find(lambda m: m.id == payload.member.id,
                                     guild.members)
            if mem is not None:
                # Add role to member
                await mem.add_roles(Role)
            # Report any errors
            else:
                channel_id = db.getLoggingID(channelType.log)
                if channel_id != 0:
                    LOG = client.get_channel(channel_id)
                    await LOG.send("Unable to find " + payload.member.name)
    elif payload.message_id == db.getPoliMsg():
        guildID = payload.guild_id
        # Get server
        guild = discord.utils.find(lambda g: g.id == guildID, client.guilds)
        # Get role
        role = discord.utils.find(lambda r: r.name == 'politics', guild.roles)
        if role is not None:
            (exists, poliban,
             monitored, Override) = db.retrieveWatchlistFlags(payload.user_id)
            if exists and poliban and db.checkUserRank(payload.user_id) >= 3:
                return
            mem = discord.utils.find(lambda m: m.id == payload.member.id,
                                     guild.members)
            if mem is not None:
                await mem.add_roles(role)
            else:
                channel_id = db.getLoggingID(channelType.log)
                if channel_id != 0:
                    LOG = client.get_channel(channel_id)
                    await LOG.send('Unable to find ' + payload.member.name)


# Reaction removal event
@client.event
async def on_raw_reaction_remove(payload):
    guildID = payload.guild_id
    # Get guild
    guild = discord.utils.find(lambda g: g.id == guildID, client.guilds)
    # Get server member who reacted
    mem = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
    # Check if user is bot
    if mem.bot:
        return
    # Open database
    db = EE_DB(cog_db, payload.guild_id)
    # Retrieve Data poll data, if any
    msgID, Pairs = db.retrievePoll(payload.message_id)
    # Check if returned ID matches
    if payload.message_id == msgID:
        roleID = None
        # Search for matching role
        for i in range(0, len(Pairs)):
            if Pairs[i][1] == emoji.demojize(str(payload.emoji)):
                roleID = Pairs[i][0]
                break
        # Get role from guild
        Role = discord.utils.get(guild.roles, id=roleID)
        if Role is not None:
            if mem is not None:
                # Remove role if it exists
                await mem.remove_roles(Role)
            # Report any errors
            else:
                channel_id = db.getLoggingID(channelType.log)
                if channel_id != 0:
                    LOG = client.get_channel(channel_id)
                    await LOG.send("Unable to find " + mem.name)
    elif payload.message_id == db.getPoliMsg():
        role = discord.utils.find(lambda r: r.name == 'politics', mem.roles)
        if role is not None:
            await mem.remove_roles(role)


# Message delete event
@client.event
async def on_raw_message_delete(payload: discord.RawMessageDeleteEvent):
    guildID = payload.guild_id
    # Find guild
    guild = discord.utils.find(lambda g: g.id == guildID, client.guilds)
    channelID = payload.channel_id
    # Find channel
    channel = discord.utils.find(lambda c: c.id == channelID, guild.channels)
    if channel is None:
        return
    # Create database object
    db = EE_DB(cog_db, guildID)
    if channel.category.name == 'Classes':
        # Mark message as deleted
        db.markMsgDeleted(payload.message_id, channelID)
        # Report error
        if db.getError() != 0:
            channel_id = db.getLoggingID(channelType.log)
            if channel_id != 0:
                LOG = client.get_channel(channel_id)
                await LOG.send(
                    '''Unable to mark message %d as deleted in channel %s!''' % (payload.message_id, channel.name))
    elif db.getLoggingID(channelType.role) == channel.id:
        # Try removing poll from database
        db.deletePoll(payload.message_id)
    del db


# Member joining event
@client.event
async def on_member_join(member):
    # Check if bot user
    if member.bot:
        return
    # Open database
    db = EE_DB(cog_db, member.guild.id)
    channel_id = db.getLoggingID(channelType.log)
    # Add new member
    db.insertUser(member.id)
    if channel_id != 0:
        LOG = client.get_channel(channel_id)
        # Report errors
        if db.getError() != 0:
            await LOG.send("Unable to add " + member.name)
        else:  # Report success
            await LOG.send(member.name + " just joined the server")
    del db


# Member leaving event
@client.event
async def on_member_remove(member):
    # Exit if bot
    if member.bot:
        return
    # Open database
    db = EE_DB(cog_db, member.guild.id)
    channel_id = db.getLoggingID(channelType.log)
    # Delete user from database
    db.deleteUser(member.id)
    if channel_id != 0:
        # Get channel
        LOG = client.get_channel(channel_id)
        # Report any errors
        if db.getError() != 0:
            await LOG.send("Unable to remove " + member.name)
        else:
            await LOG.send(member.name + " was removed from the server.")
    del db


# Guild join event
@client.event
async def on_guild_join(guild):
    db = EE_DB(cog_db, 0, False)
    db.addGuild(guild.id)
    UpdateDB(guild.id, guild.members)
    await guild.channels[0].send('Please use `configure` command to set up the log, role opt in, level notification, '
                                 'shaming, and monitoring channels. By failing to do so, you will miss out on '
                                 'important messages.')


# Thread join event
@client.event
async def on_thread_join(thread):
    await thread.join()


# Disconnect event
@client.event
async def on_disconnect():
    # Tell python that global variable is being used
    global retries
    # Check if client was closed
    if client.is_closed():
        return
    # Check number of retries attempted
    if retries == 6:
        # Close client after 6 retries to connect
        await client.close()
        return
    # Iterate retries variable
    retries += 1


@client.slash_command(guild_ids=[originGuild], name='kill', description='Terminates execution of bot.')
async def _kill(ctx):
    if ctx.guild is None:
        return
    print(f'{ctx.author.id}: {ctx.author.name} invoked the _kill command.')
    # Check if the user is the owner and if the bot is launched
    if (ctx.author.id == MASTER) and launchFlag:
        # Report shutdown
        db = EE_DB(cog_db, ctx.guild.id)
        db.removeInstance()
        db.setRunFlag(False)
        channel_id = db.getLoggingID(channelType.log)
        if channel_id != 0:
            LOG = client.get_channel(channel_id)
            await LOG.send('Killing bot...')
        # Save task queues
        client.get_cog('Schedule').killQueues(True)
        await ctx.respond('ACK')
        # Save active games
        client.get_cog('Games').saveGames()
        # Print kill for the shell script
        print('kill')
        # Shutdown the bot
        await client.close()
    # Check if another user tried the command
    elif ctx.author.id != MASTER:
        # troll other users
        with open("images/EasterEggs/kill.jpg", 'rb') as f:
            await ctx.respond(file=File(f, 'kill.jpg'))
    # reboot if launchFlag is false
    else:
        await reboot(ctx)
    return


@client.slash_command(guild_ids=[originGuild], default_permission=False)
@permissions.default_permissions(
    administrator=True
)
async def reboot(ctx):
    if ctx.guild is None:
        return
    # Check user
    if ctx.author.id == MASTER:
        db = EE_DB(cog_db, ctx.guild.id)
        # check if bot was launched
        if launchFlag:
            # Send message to the log
            channel_id = db.getLoggingID(channelType.log)
            if channel_id != 0:
                LOG = client.get_channel(channel_id)
                await LOG.send('Rebooting Bot...')
            # save any active games
            client.get_cog('Games').saveGames()
        await ctx.respond('Shutting down')
        db.removeInstance()
        db.setRunFlag(False)
        # Save and close task queues
        client.get_cog('Schedule').killQueues(launchFlag)
        # print reboot for shell script
        print("reboot")
        # shutdown bot
        await client.close()
    return


# Command that wraps the help function, which is in a separate file.
@client.slash_command(name='help')
async def HelpWrapper(
        ctx,
        spec: Option(str, 'The command category or the actual command', required = False, default = '',
                     autocomplete = get_commands)
):
    # Call the function which will fetch the correct description for the spec passed in
    await help(ctx, client.user.display_avatar.url, spec, cog_db)
    return


# Check internet connection
internet = False
while not internet:
    try:
        urllib.request.urlopen('http://google.com')
        internet = True
    except (Exception):
        print('No internet!')
        time.sleep(15)

# Check if the bot was launched, but crashed for some reason
start = True
if launchFlag:
    db = EE_DB(cog_db, 0, False)
    # Try adding an instance. If there are already too many instances, then the db will reject the startup
    start = db.addInstance()
    # Check for a crashed state.
    if not db.getCleanExitFlag():
        start = True
        # Remove the crashed instance
        db.removeInstance()
        # Update the clean flag
        db.setCleanExitFlag(True)
    del db

if start:
    if launchFlag:
        # TODO: Load the password from a file so it is not included in source code
        password = 'pS8b?2bGbx2#MM7HoHL$aWIB3w&k@*Yun0TQNZoM8?|g-uW-alub7wmNKNz6yyP3uka$#Dcqd3%yV?RIE4cq_9kn?0$0Z2S73*2xJXkdD=v8n9lNDfAylOEFu1GLj3^FBoJO&gy2PoeU!|=Deh5n9R@WYKFrk^m#vllz-jh*cKF*?uWbI=|btAXQaZxSiK7QDnGR3z!f?D|yG#qyV#QVtm%xClu2&Ytgo7hFF_t|g&fY%-lWf|?OKX?DaKs!f-n?'.encode(
            'utf-8')
        # Encrypt password
        crypt = rsa.encrypt(password, pubKey)
        # Print encrypted password
        crypt = list(crypt)
        crypt = list(map(str, crypt))
        crypt = ','.join(crypt)
        crypt = '[%s]' % crypt
        print(crypt)
    # Start up bot
    client.run(TOKEN)
else:
    print('kill')
