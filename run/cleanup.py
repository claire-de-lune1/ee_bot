# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from EE_Bot_DB import EE_DB
import sys
import rsa

try:
    # Try finding the square brackets, which contains the encrypted password
    if sys.argv[1].find('[') == -1 or sys.argv[1].find(']') == -1:
        # Exit if brackets not found
        exit(0)
    #Get encrypted password and remove brackets
    password = sys.argv[1]
    password = password[1:-1]
    password = password.split(',')
    #Convert into bytes
    password = bytes(list(map(int, password)))
    key = None
    #Read in private key
    with open('EasterEggs/gold.jpg', 'rb') as f:
        key = f.read()
    privKey = rsa.PrivateKey.load_pkcs1(key)
    #Decrypt password
    password = rsa.decrypt(password, privKey).decode('utf-8')
    # Check if passwords match. TDOD: load password from file
    if password == 'pS8b?2bGbx2#MM7HoHL$aWIB3w&k@*Yun0TQNZoM8?|g-uW-alub7wmNKNz6yyP3uka$#Dcqd3%yV?RIE4cq_9kn?0$0Z2S73*2xJXkdD=v8n9lNDfAylOEFu1GLj3^FBoJO&gy2PoeU!|=Deh5n9R@WYKFrk^m#vllz-jh*cKF*?uWbI=|btAXQaZxSiK7QDnGR3z!f?D|yG#qyV#QVtm%xClu2&Ytgo7hFF_t|g&fY%-lWf|?OKX?DaKs!f-n?':
        db = EE_DB('/mnt/disk/EE_Bot.db', 0, False)
        #Update the clean exit flag
        db.setCleanExitFlag(False)
except Exception:
    pass
