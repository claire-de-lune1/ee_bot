# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import numpy as np
from random import randint
from skrf import tlineFunctions as tl

__Easy = []


def load_easy():
    #Open text file holding questions and filenames
    with open('EasyQuestions.txt', 'r', encoding='utf-8') as f:
        #Read file in with each line as an item 
        lines = f.read().splitlines()
    #iterate through list and format each line
    for i in range(0, len(lines)):
        #split string into 2 list items
        line = lines[i].split(';')
        #Check if 1st item is an empty string
        if line[1] == '':
            line[1] = None  #Set 1st item to None if empty
        #Append item into global list
        __Easy.append(line)


def freeEasy():
    #Clear global list
    __Easy.clear()


#########################################################
# Variants of question 1
def _Q1_v1(V1, V2, V3, R1, R2, R3, R4, R5, R6, R7, version):
    file = None
    if __Easy[-1 + version][1] is not None:
        file = "Easy/" + __Easy[-1 + version][1]
    question = __Easy[-1 + version][0] % (V1, V2, V3, R1, 
                                          R2, R3, R4, R5, R6, R7)
    X = np.array([[(1 / R1) + (1 / R3) + (1 / R6), -1 / R3, 0],
                  [-1 / R3, (1 / R2) + (1 / R3) + (1 / R7) + (1 / R4),
                   -1 / R4], [0, -1 / R4, (1 / R4) + (1 / R5)]])
    Y = np.array([(V1 / R1) + (V3 / R6), (V1 / R2) + (V2 / R4) + (V3 / R7),
                 -1 * (V2 / R4)])
    nodeVoltage = np.linalg.solve(X, Y)
    ans = 0
    if version == 1:
        ans = float(nodeVoltage[0])
    elif version == 2:
        ans = float(nodeVoltage[1])
    elif version == 3:
        ans = float(nodeVoltage[2])
    margin = 0.01
    return question, file, ans, margin


def _Q1_v2(V1, V2, R1, R2, R3, R4, R5, R6, R7, R8, R9, version):
    file = None
    if __Easy[2 + version][1] is not None:
        file = "Easy/" + __Easy[2 + version][1]
    question = __Easy[2 + version][0] % (V1, V2, R1, R2, R3, R4, R5,
                                         R6, R7, R8, R9)
    X = np.array([[(1 / R1) + (1 / R2) + (1 / R4), -1 / R4, -1 / R2, 0],
                  [-1 / R4, (1 / R9) + (1 / R7) + (1 / R4), -1 / R7, 0],
                  [-1 / R2, -1 / R7, (1 / R2) + (1 / R5) + (1 / R7), -1 / R5],
                  [0, 0, -1 / R5, (1 / R3) + (1 / R5) + (1 / R8) + (1 / R6)]])
    Y = np.array([V1 / R1, V2 / R9, 0, (V1 / R3) + (V2 / R8)])
    nodeVoltage = np.linalg.solve(X, Y)
    ans = 0
    if version == 1:
        ans = float(nodeVoltage[0])
    elif version == 2:
        ans = float(nodeVoltage[1])
    elif version == 3:
        ans = float(nodeVoltage[2])
    elif version == 4:
        ans = float(nodeVoltage[3])
    margin = 0.01
    return question, file, ans, margin


def _Q1_v3(V1, A, B, R1, R2, R3, R4, R5, R6, R7, R8, R9, R10, version):
    file = None
    if __Easy[6 + version][1] is not None:
        file = "Easy/" + __Easy[6 + version][1]
    question = __Easy[6 + version][0] % (V1, B, A, R1, R2, R3, R4, R5, R6,
                                         R7, R8, R9, R10)
    ans = 0
    if (version == 1) or (version == 5) or (version == 9) or (version == 13):
        X = np.array([[(1 / R2) + (1 / R3) + (1 / (R1 + R9)),
                       (A / (R1 + R9)) - (1 / R3), -1 / (R1 + R9),
                       (-1 * A) / (R1 + R9)], [-1 / R3,
                       (1 / R3) + (1 / R5) + (1 / R7), -1 / R7, -1 / R5],
                      [-1 / (R1 + R9),
                       ((-1 * A) / (R1 + R9)) + (-1 / R7) + ((-1 * B) / R10),
                       (1 / (R1 + R9)) + (1 / R7) + (1 / R10),
                       (A / (R1 + R9)) + (B / R10)], [0,
                       (-1 / R5) + ((-1 * B) / R8), 0,
                       (1 / R4) + (1 / R5) + (1 / R6) + (1 / R8) + (B / R8)]])
        Y = np.array([V1 / R2, 0, 0, V1 / R4])
        nodeVoltage = np.linalg.solve(X, Y)
        if version == 1:
            ans = float(nodeVoltage[0])
        elif version == 5:
            ans = float(nodeVoltage[1])
        elif version == 9:
            ans = float(nodeVoltage[2])
        else:
            ans = float(nodeVoltage[3])
    elif ((version == 2) or (version == 6) or
          (version == 10) or (version == 14)):
        X = np.array([[(1 / R2) + (1 / R3) + (1 / (R1 + R9)), -1 / R3,
                       -1 / (R1 + R9), A / (R6 * (R1 + R9))],
                      [-1 / R3, (1 / R3) + (1 / R5) + (1 / R7),
                       -1 / R7, -1 / R5], [-1 / (R1 + R9),
                      (-1 / R7) - (B / R10),
                      (1 / (R1 + R9)) + (1 / R7) + (1 / R10),
                      (B / R10) - (A / (R6 * (R1 + R9)))],
                      [0, (-1 / R5) - (B / R8), 0,
                      (1 / R4) + (1 / R5) + (1 / R6) + (1 / R8) + (B / R8)]])
        Y = np.array([V1 / R2, 0, 0, V1 / R4])
        nodeVoltage = np.linalg.solve(X, Y)
        if version == 2:
            ans = float(nodeVoltage[0])
        elif version == 6:
            ans = float(nodeVoltage[1])
        elif version == 10:
            ans = float(nodeVoltage[2])
        else:
            ans = float(nodeVoltage[3])
    elif ((version == 3) or (version == 7) or
          (version == 11) or (version == 15)):
        X = np.array([[(1 / R2) + (1 / R3) + (1 / (R1 + R9)),
         (A / (R1 + R9)) - (1 / R3), -1 / (R1 + R9),
         (-1 * A) / (R1 + R9)], [-1 / R3,
         (1 / R3) + (1 / R5) + (1 / R7), -1 / R7, -1 / R5],
         [-1 / (R1 + R9), ((-1 * A) / (R1 + R9)) - (1 / R7),
         (1 / (R1 + R9)) + (1 / R7) + (1 / R10),
         (A / (R1 + R9)) - (B / (R6 * R10))], [0, -1 / R5, 0,
         (1 / R4) + (1 / R5) + (1 / R6) + (1 / R8) + ((-1 * B) / (R6 * R8))]])
        Y = np.array([V1 / R2, 0, 0, V1 / R4])
        nodeVoltage = np.linalg.solve(X, Y)
        if version == 3:
            ans = float(nodeVoltage[0])
        elif version == 7:
            ans = float(nodeVoltage[1])
        elif version == 11:
            ans = float(nodeVoltage[2])
        else:
            ans = float(nodeVoltage[3])
    elif ((version == 4) or (version == 8) or 
          (version == 12) or (version == 16)):
        X = np.array([[(1 / R2) + (1 / R3) + (1 / (R1 + R9)), -1 / R3,
                       -1 / (R1 + R9), A / (R6 * (R1 + R9))], [-1 / R3,
                       (1 / R3) + (1 / R5) + (1 / R7), -1 / R7, -1 / R5],
                      [-1 / (R1 + R9), -1 / R7,
                       (1 / (R1 + R9)) + (1 / R7) + (1 / R10),
                       ((-1 * A) / (R6 * (R1 + R9))) - (B / (R6 * R10))],
                      [0, -1 / R5, 0, 
                       (1/R4)+(1/R5)+(1/R6)+(1/R8)-(B/(R6*R8))]])
        Y = np.array([V1 / R2, 0, 0, V1 / R4])
        nodeVoltage = np.linalg.solve(X, Y)
        if version == 4:
            ans = float(nodeVoltage[0])
        elif version == 8:
            ans = float(nodeVoltage[1])
        elif version == 12:
            ans = float(nodeVoltage[2])
        else:
            ans = float(nodeVoltage[3])
    margin = 0.01
    return question, file, ans, margin


def _Q1_v4(V1, I1, A, R1, R2, R3, R4, R5, R6, R7, R8, R9, R10, R11, version):
    file = None
    if __Easy[22 + version][1] is not None:
        file = "Easy/" + __Easy[22 + version][1]
    question = __Easy[22 + version][0] % (V1, I1, A, R1, R2, R3, R4, R5,
                                          R6, R7, R8, R9, R10, R11)
    I1 /= 1000
    ans = 0
    if ((version == 1) or (version == 5) or (version == 9) or
         (version == 13) or (version == 17)):
        X = np.array([[(1 / R2) + (1 / R5) + A, -1 / R5, (-1 / R2) - A, 0, 0],
                      [(-1 / R5) - A, (1 / R5) + (1 / R9), A, 0, -1 / R9],
                      [-1 / R2, 0, (1 / R2) + (1 / R4) + (1 / R3),
                       -1 / R4, 0], [0, 0, -1 / R3, 
                       (1 / R3) + (1 / R6) + (1 / R7), -1 / R7],
                      [0, -1 / R9, 0, -1 / R7,
                       (1 / R10) + (1 / R7) + (1 / R9) + (1 / R11)]])
        Y = np.array([0, 0, I1, 0, V1 / R11])
        nodeVoltage = np.linalg.solve(X, Y)
        if version == 1:
            ans = float(nodeVoltage[0])
        elif version == 5:
            ans = float(nodeVoltage[1])
        elif version == 9:
            ans = float(nodeVoltage[2])
        elif version == 13:
            ans = float(nodeVoltage[3])
        else:
            ans = float(nodeVoltage[4])
    elif ((version == 2) or (version == 6) or (version == 10) or 
          (version == 14) or (version == 18)):
        X = np.array([[(1 / R2) + (1 / R5), -1 / R5, -1 / R2, A / R7,
                       -1 * A / R7], [-1 / R5, (1 / R5) + (1 / R9),
                       0, -1 * A / R7, (A / R7) - (1 / R9)],
                      [-1 / R2, 0, (1 / R2) + (1 / R3) + (1 / R4), -1 / R4,
                       0], [0, 0, -1 / R4, (1 / R4) + (1 / R6) + (1 / R7),
                       -1 / R7], [0, -1 / R9, 0, -1 / R7,
                      (1 / R7) + (1 / R10) + (1 / R9) + (1 / R11)]])
        Y = np.array([0, 0, I1, 0, V1 / R11])
        nodeVoltage = np.linalg.solve(X, Y)
        if version == 2:
            ans = float(nodeVoltage[0])
        elif version == 6:
            ans = float(nodeVoltage[1])
        elif version == 10:
            ans = float(nodeVoltage[2])
        elif version == 14:
            ans = float(nodeVoltage[3])
        else:
            ans = float(nodeVoltage[4])
    elif ((version == 3) or (version == 7) or (version == 11) or
          (version == 15) or (version == 19)):
        X = np.array([[(1 / R2) + (1 / R5), (-1 / R5), -1 / R2, 0, A],
                      [-1 / R5, (1 / R5) + (1 / R9), 0, 0, (-1 / R9) - A],
                      [-1 / R2, 0, (1 / R2) + (1 / R4) + (1 / R3),
                       (-1 / R4), 0], [0, 0, -1 / R4,
                       (1 / R4) + (1 / R6) + (1 / R7), -1 / R7], 
                       [0, -1 / R9, 0, -1 / R7,
                        (1 / R7) + (1 / R9) + (1 / R10) + (1 / R11)]])
        Y = np.array([0, 0, I1, 0, V1 / R11])
        nodeVoltage = np.linalg.solve(X, Y)
        if version == 3:
            ans = float(nodeVoltage[0])
        elif version == 7:
            ans = float(nodeVoltage[1])
        elif version == 11:
            ans = float(nodeVoltage[2])
        elif version == 15:
            ans = float(nodeVoltage[3])
        else:
            ans = float(nodeVoltage[4])
    elif ((version == 4) or (version == 8) or (version == 12) or
          (version == 16) or (version == 20)):
        X = np.array([[(1 / R2) + (1 / R5), (-1 / R5), (-1 / R2), A / R6, 0],
                      [-1 / R5, (1 / R5) + (1 / R9), 0, (-1 * A) / R6,
                       -1 / R9], [-1 / R2, 0, (1 / R2) + (1 / R4) + (1 / R3),
                       -1 / R4, 0], [0, 0, -1 / R3,
                       (1 / R3) + (1 / R6) + (1 / R7), -1 / R7], 
                       [0, -1 / R9, 0, -1 / R7,
                        (1 / R10) + (1 / R7) + (1 / R9) + (1 / R11)]])
        Y = np.array([0, 0, I1, 0, V1 / R11])
        nodeVoltage = np.linalg.solve(X, Y)
        if version == 4:
            ans = float(nodeVoltage[0])
        elif version == 8:
            ans = float(nodeVoltage[1])
        elif version == 12:
            ans = float(nodeVoltage[2])
        elif version == 16:
            ans = float(nodeVoltage[3])
        else:
            ans = float(nodeVoltage[4])
    margin = 0.01
    return question, file, ans, margin


#########################################################
#Variants of question 2
def _Q2_v1(V1, V2, V3, R1, R2, R3, R4, R5, R6, R7, version):
    file = None
    if __Easy[42 + version][1] is not None:
        file = "Easy/" + __Easy[42 + version][1]
    question = __Easy[42 + version][0] % (V1, V2, V3, R1, R2, R3, R4,
                                          R5, R6, R7)
    X = np.array([[(1/R1) + (1/R3) + (1/R6), -1/R3, 0], 
                  [-1/R3, (1/R2) + (1/R3) + (1/R7) + (1/R4), -1/R4],
                  [0, -1/R4, (1/R4) + (1/R5)]])
    Y = np.array([(V1/R1) + (V3/R6), (V1/R2) + (V2/R4) + (V3/R7), -1*(V2/R4)])
    nodeVoltage = np.linalg.solve(X, Y)
    ans = 0
    if version == 1:
        ans = abs((V1 - float(nodeVoltage[0]))/R1)
    elif version == 2:
        ans = abs((V1 - float(nodeVoltage[1]))/R2)
    elif version == 3:
        ans = abs((float(nodeVoltage[0]-nodeVoltage[1]))/R3)
    elif version == 4:
        ans = abs((float(nodeVoltage[1])-V2-float(nodeVoltage[2]))/R4)
    elif version == 5:
        ans = abs(float(nodeVoltage[2])/R5)
    elif version == 6:
        ans = abs((V3-float(nodeVoltage[0]))/R6)
    elif version == 7:
        ans = abs((V3-nodeVoltage[1])/R7)
    margin = 0.01
    return question, file, ans, margin


def _Q2_v2(V1, V2, R1, R2, R3, R4, R5, R6, R7, R8, R9, version):
    file = None
    if __Easy[49+version][1] is not None:
        file = "Easy/" + __Easy[2+version][1]
    question = __Easy[49+version][0] % (V1, V2, R1, R2, R3, R4, R5, R6, R7,
                                        R8, R9)
    X = np.array([[(1/R1)+(1/R2)+(1/R4), -1/R4, -1/R2, 0],
                  [-1/R4, (1/R9) + (1/R7) + (1/R4), -1/R7, 0],
                  [-1/R2, -1/R7, (1/R2) + (1/R5) + (1/R7), -1/R5],
                  [0, 0, -1/R5, (1/R3) + (1/R5) + (1/R8) + (1/R6)]])
    Y = np.array([V1/R1, V2/R9, 0, (V1/R3) + (V2/R8)])
    nodeVoltage = np.linalg.solve(X, Y)
    if version == 1:
        ans = abs((V1-float(nodeVoltage[0]))/R1)
    elif version == 2:
        ans = abs((float(nodeVoltage[0])-float(nodeVoltage[2]))/R2)
    elif version == 3:
        ans = abs((V1-float(nodeVoltage[3]))/R3)
    elif version == 4:
        ans = abs((float(nodeVoltage[0]-nodeVoltage[1]))/R4)
    elif version == 5:
        ans = abs(float(nodeVoltage[2] - nodeVoltage[3])/R5)
    elif version == 6:
        ans = abs(float(nodeVoltage[3])/R6)
    elif version == 7:
        ans = abs(float(nodeVoltage[1]-nodeVoltage[2])/R7)
    elif version == 8:
        ans = abs((V2-float(nodeVoltage[3]))/R8)
    elif version == 9:
        ans = abs((V2-float(nodeVoltage[1]))/R9)
    margin = 0.01
    return question, file, ans, margin


def _Q2_v3(V1, A, B, R1, R2, R3, R4, R5, R6, R7, R8, R9, R10, version):
    file = None
    if __Easy[58+version][1] is not None:
        file = "Easy/" + __Easy[58+version][1]
    question = __Easy[58+version][0] % (V1, B, A, R1, R2, R3, R4, R5,
                                        R6, R7, R8, R9, R10)
    ans = 0
    if ((version == 1) or (version == 5) or (version == 9) or
        (version == 13) or (version == 17) or (version == 21) or
        (version == 25) or (version == 29) or (version == 33) or
        (version == 37)):
        X = np.array([[(1/R2) + (1/R3) + (1/(R1 + R9)), (A/(R1+R9))-(1/R3),
                       -1/(R1 + R9), (-1*A)/(R1+R9)],
                      [-1/R3, (1/R3) + (1/R5) + (1/R7), -1/R7, -1/R5],
                      [-1/(R1+R9), ((-1*A)/(R1+R9)) + (-1/R7) + ((-1*B)/R10),
                       (1/(R1+R9)) + (1/R7) + (1/R10), (A/(R1+R9)) + (B/R10)],
                       [0, (-1/R5) + ((-1*B)/R8), 0,
                        (1/R4) + (1/R5) + (1/R6) + (1/R8) + (B/R8)]])
        Y = np.array([V1/R2, 0, 0, V1/R4])
        nodeVoltage = np.linalg.solve(X, Y)
        if version == 1:
            ans = abs((float(nodeVoltage[0])+(A*(float(nodeVoltage[1]-
                                                       nodeVoltage[3])))-
                       float(nodeVoltage[2]))/(R1+R9))
        elif version == 5:
            ans = abs((V1-float(nodeVoltage[0]))/R2)
        elif version == 9:
            ans = abs(float(nodeVoltage[0]-nodeVoltage[1])/R3)
        elif version == 13:
            ans = abs((V1-float(nodeVoltage[3]))/R4)
        elif version == 17:
            ans = abs(float(nodeVoltage[1]-nodeVoltage[3])/R5)
        elif version == 21:
            ans = abs(float(nodeVoltage[3])/R6)
        elif version == 25:
            ans = abs(float(nodeVoltage[1]-nodeVoltage[2])/R7)
        elif version == 29:
            ans = abs((float(nodeVoltage[3])-(B*float(nodeVoltage[1]-
                                                      nodeVoltage[3])))/R8)
        elif version == 33:
            ans = abs((float(nodeVoltage[0])+(A*(float(nodeVoltage[1]-
                                                       nodeVoltage[3])))-
                       float(nodeVoltage[2]))/(R1+R9))
        elif version == 37:
            ans = abs((float(nodeVoltage[2])-(B*float(nodeVoltage[1]-
                                                      nodeVoltage[3])))/R10)
    elif ((version == 2) or (version == 6) or (version == 10) or
          (version == 14) or (version == 18) or (version == 22) or
          (version == 26) or (version == 30) or (version == 34) or
          (version == 38)):
        X = np.array([[(1/R2) + (1/R3) + (1/(R1+R9)), -1/R3, -1/(R1+R9),
                       A/(R6*(R1+R9))], [-1/R3, (1/R3)+(1/R5)+(1/R7),
                       -1/R7, -1/R5], [-1/(R1+R9), (-1/R7)-(B/R10),
                       (1/(R1+R9)) + (1/R7) + (1/R10),
                       (B/R10) - (A/(R6*(R1+R9)))], [0, (-1/R5) - (B/R8),
                       0, (1/R4) + (1/R5) + (1/R6) + (1/R8) + (B/R8)]])
        Y = np.array([V1/R2, 0, 0, V1/R4])
        nodeVoltage = np.linalg.solve(X, Y)
        if version == 2:
            ans = abs((float(nodeVoltage[0])+(A*(float(nodeVoltage[3])/R6))-
                       float(nodeVoltage[2]))/(R1+R9))
        elif version == 6:
            ans = abs((V1-float(nodeVoltage[0]))/R2)
        elif version == 10:
            ans = abs(float(nodeVoltage[0]-nodeVoltage[1])/R3)
        elif version == 14:
            ans = abs((V1-float(nodeVoltage[3]))/R4)
        elif version == 18:
            ans = abs(float(nodeVoltage[1]-nodeVoltage[3])/R5)
        elif version == 22:
            ans = abs(float(nodeVoltage[3])/R6)
        elif version == 26:
            ans = abs(float(nodeVoltage[1]-nodeVoltage[2])/R7)
        elif version == 30:
            ans = abs((float(nodeVoltage[3])-(B*float(nodeVoltage[1]-
                                                      nodeVoltage[3])))/R8)
        elif version == 34:
            ans = abs((float(nodeVoltage[0])+(A*(float(nodeVoltage[3])/R6))-
                       float(nodeVoltage[2]))/(R1+R9))
        elif version == 38:
            ans = abs((float(nodeVoltage[2])-(B*float(nodeVoltage[1]-
                                                      nodeVoltage[3])))/R10)
    elif ((version == 3) or (version == 7) or (version == 11) or
          (version == 15) or (version == 19) or (version == 23) or
          (version == 27) or (version == 31) or (version == 35) or
          (version == 39)):
        X = np.array([[(1/R2) + (1/R3) + (1/(R1 + R9)), (A/(R1+R9))-(1/R3),
                       -1/(R1 + R9), (-1*A)/(R1+R9)],
                      [-1/R3, (1/R3) + (1/R5) + (1/R7), -1/R7, -1/R5],
                      [-1/(R1+R9), ((-1*A)/(R1+R9))-(1/R7),
                       (1/(R1+R9))+(1/R7)+(1/R10), (A/(R1+R9))-(B/(R6*R10))],
                       [0, -1/R5, 0,
                        (1/R4)+(1/R5)+(1/R6)+(1/R8)+((-1*B)/(R6*R8))]])
        Y = np.array([V1/R2, 0, 0, V1/R4])
        nodeVoltage = np.linalg.solve(X, Y)
        if version == 3:
            ans = abs((float(nodeVoltage[0])+(A*(float(nodeVoltage[1]-
                                                       nodeVoltage[3])))-
                       float(nodeVoltage[2]))/(R1+R9))
        elif version == 7:
            ans = abs((V1-float(nodeVoltage[0]))/R2)
        elif version == 11:
            ans = abs(float(nodeVoltage[0]-nodeVoltage[1])/R3)
        elif version == 15:
            ans = abs((V1-float(nodeVoltage[3]))/R4)
        elif version == 19:
            ans = abs(float(nodeVoltage[1]-nodeVoltage[3])/R5)
        elif version == 23:
            ans = abs(float(nodeVoltage[3])/R6)
        elif version == 27:
            ans = abs(float(nodeVoltage[1]-nodeVoltage[2])/R7)
        elif version == 31:
            ans = abs((float(nodeVoltage[3])-
                       (B*(float(nodeVoltage[3])/R6)))/R8)
        elif version == 35:
            ans = abs((float(nodeVoltage[0])+(A*(float(nodeVoltage[1]-
                                                       nodeVoltage[3])))-
                       float(nodeVoltage[2]))/(R1+R9))
        elif version == 39:
            ans = abs((float(nodeVoltage[2])-
                       (B*(float(nodeVoltage[3])/R6)))/R10)
    elif ((version == 4) or (version == 8) or (version == 12) or
          (version == 16) or (version == 20) or (version == 24) or
          (version == 28) or (version == 32) or (version == 36) or
          (version == 40)):
        X = np.array([[(1/R2)+(1/R3)+(1/(R1+R9)), -1/R3,
                       -1/(R1+R9), A/(R6*(R1+R9))],
                      [-1/R3, (1/R3)+(1/R5)+(1/R7), -1/R7, -1/R5],
                      [-1/(R1+R9), -1/R7, (1/(R1+R9))+(1/R7)+(1/R10),
                       ((-1*A)/(R6*(R1+R9)))-(B/(R6*R10))],
                       [0, -1/R5, 0, 
                        (1/R4)+(1/R5)+(1/R6)+(1/R8)-(B/(R6*R8))]])
        Y = np.array([V1/R2, 0, 0, V1/R4])
        nodeVoltage = np.linalg.solve(X, Y)
        if version == 4:
            ans = abs((float(nodeVoltage[0])+(A*(float(nodeVoltage[3])/R6))-
                       float(nodeVoltage[2]))/(R1+R9))
        elif version == 8:
            ans = abs((V1-float(nodeVoltage[0]))/R2)
        elif version == 12:
            ans = abs(float(nodeVoltage[0]-nodeVoltage[1])/R3)
        elif version == 16:
            ans = abs((V1-float(nodeVoltage[3]))/R4)
        elif version == 20:
            ans = abs(float(nodeVoltage[1]-nodeVoltage[3])/R5)
        elif version == 24:
            ans = abs(float(nodeVoltage[3])/R6)
        elif version == 28:
            ans = abs(float(nodeVoltage[1]-nodeVoltage[2])/R7)
        elif version == 32:
            ans = abs((float(nodeVoltage[3])-
                       (B*(float(nodeVoltage[3])/R6)))/R8)
        elif version == 36:
            ans = abs((float(nodeVoltage[0])+(A*(float(nodeVoltage[3])/R6))-
                       float(nodeVoltage[2]))/(R1+R9))
        elif version == 40:
            ans = abs((float(nodeVoltage[2])-
                       (B*(float(nodeVoltage[3])/R6)))/R10)
    margin = 0.01
    return question, file, ans, margin


def _Q2_v4(V1, I1, A, R1, R2, R3, R4, R5, R6, R7, R8, R9, R10, R11, version):
    file = None
    if __Easy[98+version][1] is not None:
        file = "Easy/" + __Easy[98+version][1]
    question = __Easy[98+version][0] % (V1, I1, A, R1, R2, R3, R4, R5, R6, R7,
                                        R8, R9, R10, R11)
    I1 /= 1000
    ans = 0
    if ((version == 1) or (version == 5) or (version == 9) or
        (version == 13) or (version == 17) or (version == 21) or
        (version == 25) or (version == 29) or (version == 33) or
        (version == 37) or (version == 41)):
        X = np.array([[(1/R2)+(1/R5)+A, -1/R5, (-1/R2)-A, 0, 0],
                      [(-1/R5)-A, (1/R5)+(1/R9), A, 0, -1/R9],
                      [-1/R2, 0, (1/R2) + (1/R4) + (1/R3), -1/R4, 0],
                      [0, 0, -1/R3, (1/R3)+(1/R6)+(1/R7), -1/R7],
                      [0, -1/R9, 0, -1/R7, (1/R10)+(1/R7)+(1/R9)+(1/R11)]])
        Y = np.array([0, 0, I1, 0, V1/R11])
        nodeVoltage = np.linalg.solve(X, Y)
        I2 = abs(A*float(nodeVoltage[0]-nodeVoltage[2]))
        if version == 1:
            ans = I2
        elif version == 5:
            ans = abs(float(nodeVoltage[0]-nodeVoltage[2])/R2)
        elif version == 9:
            ans = abs(float(nodeVoltage[2])/R3)
        elif version == 13:
            ans = abs(float(nodeVoltage[2]-nodeVoltage[3])/R4)
        elif version == 17:
            ans = abs(float(nodeVoltage[0]-nodeVoltage[1])/R5)
        elif version == 21:
            ans = abs(float(nodeVoltage[3])/R6)
        elif version == 25:
            ans = abs(float(nodeVoltage[3]-nodeVoltage[4])/R7)
        elif version == 29:
            ans = I2
        elif version == 33:
            ans = abs(float(nodeVoltage[1]-nodeVoltage[4])/R9)
        elif version == 37:
            ans = abs(float(nodeVoltage[4])/R10)
        elif version == 41:
            ans = abs((V1-float(nodeVoltage[4]))/R11)
    elif ((version == 2) or (version == 6) or (version == 10) or
          (version == 14) or (version == 18) or (version == 22) or
          (version == 26) or (version == 30) or (version == 34) or
          (version == 38) or (version == 42)):
        X = np.array([[(1/R2)+(1/R5), -1/R5, -1/R2, A/R7, -1*A/R7],
                      [-1/R5, (1/R5)+(1/R9), 0, -1*A/R7, (A/R7)-(1/R9)],
                      [-1/R2, 0, (1/R2)+(1/R3)+(1/R4), -1/R4, 0],
                      [0, 0, -1/R4, (1/R4)+(1/R6)+(1/R7), -1/R7],
                      [0, -1/R9, 0, -1/R7, (1/R7)+(1/R10)+(1/R9)+(1/R11)]])
        Y = np.array([0, 0, I1, 0, V1/R11])
        nodeVoltage = np.linalg.solve(X, Y)
        I2 = abs(A*(float(nodeVoltage[3]-nodeVoltage[4])/R7))
        if version == 2:
            ans = I2
        elif version == 6:
            ans = abs(float(nodeVoltage[0]-nodeVoltage[2])/R2)
        elif version == 10:
            ans = abs(float(nodeVoltage[2])/R3)
        elif version == 14:
            ans = abs(float(nodeVoltage[2]-nodeVoltage[3])/R4)
        elif version == 18:
            ans = abs(float(nodeVoltage[0]-nodeVoltage[1])/R5)
        elif version == 22:
            ans = abs(float(nodeVoltage[3])/R6)
        elif version == 26:
            ans = abs(float(nodeVoltage[3]-nodeVoltage[4])/R7)
        elif version == 30:
            ans = I2
        elif version == 34:
            ans = abs(float(nodeVoltage[1]-nodeVoltage[4])/R9)
        elif version == 38:
            ans = abs(float(nodeVoltage[4])/R10)
        elif version == 42:
            ans = abs((V1-float(nodeVoltage[4]))/R11)
    elif ((version == 3) or (version == 7) or (version == 11) or
          (version == 15) or (version == 19) or (version == 23) or
          (version == 27) or (version == 31) or (version == 35) or
          (version == 39) or (version == 43)):
        X = np.array([[(1/R2)+(1/R5), (-1/R5), -1/R2, 0, A],
                      [-1/R5, (1/R5)+(1/R9), 0, 0, (-1/R9)-A],
                      [-1/R2, 0, (1/R2)+(1/R4)+(1/R3), (-1/R4), 0],
                      [0, 0, -1/R4, (1/R4)+(1/R6)+(1/R7), -1/R7],
                      [0, -1/R9, 0, -1/R7, (1/R7)+(1/R9)+(1/R10)+(1/R11)]])
        Y = np.array([0, 0, I1, 0, V1/R11])
        nodeVoltage = np.linalg.solve(X, Y)
        I2 = abs(A*float(nodeVoltage[4]))
        if version == 3:
            ans = I2
        elif version == 7:
            ans = abs(float(nodeVoltage[0]-nodeVoltage[2])/R2)
        elif version == 11:
            ans = abs(float(nodeVoltage[2])/R3)
        elif version == 15:
            ans = abs(float(nodeVoltage[2]-nodeVoltage[3])/R4)
        elif version == 19:
            ans = abs(float(nodeVoltage[0]-nodeVoltage[1])/R5)
        elif version == 23:
            ans = abs(float(nodeVoltage[3])/R6)
        elif version == 27:
            ans = abs(float(nodeVoltage[3]-nodeVoltage[4])/R7)
        elif version == 31:
            ans = I2
        elif version == 35:
            ans = abs(float(nodeVoltage[1]-nodeVoltage[4])/R9)
        elif version == 39:
            ans = abs(float(nodeVoltage[4])/R10)
        elif version == 43:
            ans = abs((V1-float(nodeVoltage[4]))/R11)
    elif ((version == 4) or (version == 8) or (version == 12) or
          (version == 16) or (version == 20) or
          (version == 24) or (version == 28) or
          (version == 32) or (version == 36) or
          (version == 40) or (version == 44)):
        X = np.array([[(1/R2)+(1/R5), (-1/R5), (-1/R2), A/R6, 0],
                      [-1/R5, (1/R5)+(1/R9), 0, (-1*A)/R6, -1/R9],
                      [-1/R2, 0, (1/R2) + (1/R4) + (1/R3), -1/R4, 0],
                      [0, 0, -1/R3, (1/R3)+(1/R6)+(1/R7), -1/R7],
                      [0, -1/R9, 0, -1/R7, (1/R10)+(1/R7)+(1/R9)+(1/R11)]])
        Y = np.array([0, 0, I1, 0, V1/R11])
        nodeVoltage = np.linalg.solve(X, Y)
        I2 = abs(A*float(nodeVoltage[3])/R6)
        if version == 4:
            ans = I2
        elif version == 8:
            ans = abs(float(nodeVoltage[0]-nodeVoltage[2])/R2)
        elif version == 12:
            ans = abs(float(nodeVoltage[2])/R3)
        elif version == 16:
            ans = abs(float(nodeVoltage[2]-nodeVoltage[3])/R4)
        elif version == 20:
            ans = abs(float(nodeVoltage[0]-nodeVoltage[1])/R5)
        elif version == 24:
            ans = abs(float(nodeVoltage[3])/R6)
        elif version == 28:
            ans = abs(float(nodeVoltage[3]-nodeVoltage[4])/R7)
        elif version == 32:
            ans = I2
        elif version == 36:
            ans = abs(float(nodeVoltage[1]-nodeVoltage[4])/R9)
        elif version == 40:
            ans = abs(float(nodeVoltage[4])/R10)
        elif version == 44:
            ans = abs((V1-float(nodeVoltage[4]))/R11)
    margin = 0.0001
    return question, file, ans, margin


#########################################################
#Variants of question 3
def _Q3_v1(V1, R1, R2, R3, R4, R5, R6):
    file = None
    if __Easy[143][1] is not None:
        file = "Easy/" + __Easy[143][1]
    question = __Easy[143][0] % (V1, R1, R2, R3, R4, R5, R6)
    X = np.array([[(1/R1)+(1/R2)+(1/R3)+(1/R4), -1/R3, -1/R4],
                  [-1/R3, (1/R3)+(1/R5)+(1/R6), -1/R6],
                  [-1/R4, -1/R6, (1/R4)+(1/R6)]])
    Y = np.array([V1/R1, 0, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    ans = float(nodeVoltage[2])
    margin = 0.01
    return question, file, ans, margin


def _Q3_v2(V1, R1, R2, R3, R4, R5, R6):
    file = None
    if __Easy[144][1] is not None:
        file = "Easy/" + __Easy[144][1]
    question = __Easy[144][0] % (V1, R1, R2, R3, R4, R5, R6)
    X = np.array([[(1/R1)+(1/R4)+(1/R6), -1/R4, -1/R6],
                  [-1/R4, (1/R2)+(1/R4)+(1/R5), -1/R5],
                  [-1/R6, -1/R5, (1/R3)+(1/R5)+(1/R6)]])
    Y = np.array([V1/R1, 0, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    ans = float(nodeVoltage[0]-nodeVoltage[2])
    margin = 0.01
    return question, file, ans, margin


def _Q3_v3(V1, A, B, R1, R2, R3, R4):
    file = None
    if __Easy[145][1] is not None:
        file = "Easy/" + __Easy[145][1]
    question = __Easy[145][0] % (V1, A, B, R1, R2, R3, R4)
    ans = (V1*A*R4)/(((R3+R4)*(R1+R2))+(A*B*R2*R4))
    margin = 0.01
    return question, file, ans, margin


#########################################################
#Variants of question 4
def _Q4_v1(V1, R1, R2, R3, R4, R5, R6):
    file = None
    if __Easy[146][1] is not None:
        file = "Easy/" + __Easy[146][1]
    question = __Easy[146][0] % (V1, R1, R2, R3, R4, R5, R6)
    X = np.array([[(1/R1)+(1/R2)+(1/R3)+(1/R4), -1/R3, -1/R4],
                  [-1/R3, (1/R3)+(1/R5)+(1/R6), -1/R6],
                  [-1/R4, -1/R6, (1/R4)+(1/R6)]])
    Y = np.array([V1/R1, 0, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    Vth = float(nodeVoltage[2])
    X = np.array([[(1/R1)+(1/R2)+(1/R3)+(1/R4), -1/R3],
                  [-1/R3, (1/R3)+(1/R5)+(1/R6)]])
    Y = np.array([V1/R1, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    I = (float(nodeVoltage[0])/R4)+(float(nodeVoltage[1])/R6)
    ans = Vth/I
    margin = 0.01
    return question, file, ans, margin


def _Q4_v2(V1, R1, R2, R3, R4, R5, R6):
    file = None
    if __Easy[147][1] is not None:
        file = "Easy/" + __Easy[147][1]
    question = __Easy[147][0] % (V1, R1, R2, R3, R4, R5, R6)
    X = np.array([[(1/R1)+(1/R4)+(1/R6), -1/R4, -1/R6],
                  [-1/R4, (1/R2)+(1/R4)+(1/R5), -1/R5],
                  [-1/R6, -1/R5, (1/R3)+(1/R5)+(1/R6)]])
    Y = np.array([V1/R1, 0, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    Vth = float(nodeVoltage[0]-nodeVoltage[2])
    X = np.array([[(1/R1)+(1/R3)+(1/R4)+(1/R5), (-1/R4)+(-1/R5)],
                  [(-1/R4)+(-1/R5), (1/R2)+(1/R4)+(1/R5)]])
    Y = np.array([V1/R1, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    I = (V1-float(nodeVoltage[0]))/R1
    I -= float(nodeVoltage[0]-nodeVoltage[1])/R4
    ans = Vth/I
    margin = 0.01
    return question, file, ans, margin


def _Q4_v3(V1, A, B, R1, R2, R3, R4):
    file = None
    if __Easy[148][1] is not None:
        file = "Easy/" + __Easy[148][1]
    question = __Easy[148][0] % (V1, A, B, R1, R2, R3, R4)
    Vth = (V1*A*R4)/(((R3+R4)*(R1+R2))+(A*B*R2*R4))
    I = (A*V1)/(R3*(R1+R2))
    ans = Vth/I
    margin = 0.01
    return question, file, ans, margin


#########################################################
#Variants of question 5
def _Q5_v1(V1, R1, R2, R3, R4, R5, R6):
    file = None
    if __Easy[149][1] is not None:
        file = "Easy/" + __Easy[149][1]
    question = __Easy[149][0] % (V1, R1, R2, R3, R4, R5, R6)
    X = np.array([[(1/R1)+(1/R2)+(1/R3)+(1/R4), -1/R3, -1/R4],
                  [-1/R3, (1/R3)+(1/R5)+(1/R6), -1/R6],
                  [-1/R4, -1/R6, (1/R4)+(1/R6)]])
    Y = np.array([V1/R1, 0, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    Vth = float(nodeVoltage[2])
    X = np.array([[(1/R1)+(1/R2)+(1/R3)+(1/R4), -1/R3],
                  [-1/R3, (1/R3)+(1/R5)+(1/R6)]])
    Y = np.array([V1/R1, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    I = (float(nodeVoltage[0])/R4)+(float(nodeVoltage[1])/R6)
    ans = Vth/I
    margin = 0.01
    return question, file, ans, margin


def _Q5_v2(V1, R1, R2, R3, R4, R5, R6):
    file = None
    if __Easy[150][1] is not None:
        file = "Easy/" + __Easy[150][1]
    question = __Easy[150][0] % (V1, R1, R2, R3, R4, R5, R6)
    X = np.array([[(1/R1)+(1/R4)+(1/R6), -1/R4, -1/R6],
                  [-1/R4, (1/R2)+(1/R4)+(1/R5), -1/R5],
                  [-1/R6, -1/R5, (1/R3)+(1/R5)+(1/R6)]])
    Y = np.array([V1/R1, 0, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    Vth = float(nodeVoltage[0]-nodeVoltage[2])
    X = np.array([[(1/R1)+(1/R3)+(1/R4)+(1/R5), (-1/R4)+(-1/R5)],
                  [(-1/R4)+(-1/R5), (1/R2)+(1/R4)+(1/R5)]])
    Y = np.array([V1/R1, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    I = (V1-float(nodeVoltage[0]))/R1
    I -= float(nodeVoltage[0]-nodeVoltage[1])/R4
    ans = Vth/I
    margin = 0.01
    return question, file, ans, margin


def _Q5_v3(V1, A, B, R1, R2, R3, R4):
    file = None
    if __Easy[151][1] is not None:
        file = "Easy/" + __Easy[151][1]
    question = __Easy[151][0] % (V1, A, B, R1, R2, R3, R4)
    Vth = (V1*A*R4)/(((R3+R4)*(R1+R2))+(A*B*R2*R4))
    I = (A*V1)/(R3*(R1+R2))
    ans = Vth/I
    margin = 0.01
    return question, file, ans, margin


#########################################################
#Variants of question 6
def _Q6_v1(V1, R1, R2, R3, R4, R5, R6):
    file = None
    if __Easy[152][1] is not None:
        file = "Easy/" + __Easy[152][1]
    question = __Easy[152][0] % (V1, R1, R2, R3, R4, R5, R6)
    X = np.array([[(1/R1)+(1/R2)+(1/R3)+(1/R4), -1/R3, -1/R4],
                  [-1/R3, (1/R3)+(1/R5)+(1/R6), -1/R6],
                  [-1/R4, -1/R6, (1/R4)+(1/R6)]])
    Y = np.array([V1/R1, 0, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    Vth = float(nodeVoltage[2])
    X = np.array([[(1/R1)+(1/R2)+(1/R3)+(1/R4), -1/R3],
                  [-1/R3, (1/R3)+(1/R5)+(1/R6)]])
    Y = np.array([V1/R1, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    I = (float(nodeVoltage[0])/R4)+(float(nodeVoltage[1])/R6)
    Rth = Vth/I
    ans = (Vth*Vth)/(4*Rth)
    margin = 0.01
    return question, file, ans, margin


def _Q6_v2(V1, R1, R2, R3, R4, R5, R6):
    file = None
    if __Easy[153][1] is not None:
        file = "Easy/" + __Easy[153][1]
    question = __Easy[153][0] % (V1, R1, R2, R3, R4, R5, R6)
    X = np.array([[(1/R1)+(1/R4)+(1/R6), -1/R4, -1/R6],
                  [-1/R4, (1/R2)+(1/R4)+(1/R5), -1/R5],
                  [-1/R6, -1/R5, (1/R3)+(1/R5)+(1/R6)]])
    Y = np.array([V1/R1, 0, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    Vth = float(nodeVoltage[0]-nodeVoltage[2])
    X = np.array([[(1/R1)+(1/R3)+(1/R4)+(1/R5), (-1/R4)+(-1/R5)],
                  [(-1/R4)+(-1/R5), (1/R2)+(1/R4)+(1/R5)]])
    Y = np.array([V1/R1, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    I = (V1-float(nodeVoltage[0]))/R1
    I -= float(nodeVoltage[0]-nodeVoltage[1])/R4
    Rth = Vth/I
    ans = (Vth*Vth)/(4*Rth)
    margin = 0.01
    return question, file, ans, margin


def _Q6_v3(V1, A, B, R1, R2, R3, R4):
    file = None
    if __Easy[154][1] is not None:
        file = "Easy/" + __Easy[154][1]
    question = __Easy[154][0] % (V1, A, B, R1, R2, R3, R4)
    Vth = (V1*A*R4)/(((R3+R4)*(R1+R2))+(A*B*R2*R4))
    I = (A*V1)/(R3*(R1+R2))
    Rth = Vth/I
    ans = (Vth*Vth)/(4*Rth)
    margin = 0.01
    return question, file, ans, margin


#########################################################
#Variants of question 7
def _Q7_v1(V1, C1, R1, R2, R3, R4, R5, R6):
    file = None
    if __Easy[155][1] is not None:
        file = "Easy/" + __Easy[155][1]
    question = __Easy[155][0] % (V1, C1, R1, R2, R3, R4, R5, R6)
    X = np.array([[(1/R2)+(1/R3)+(1/R5), -1/R5],
                  [-1/R5, (1/R4)+(1/R5)+(1/R6)]])
    Y = np.array([V1/R2, V1/R4])
    nodeVoltage = np.linalg.solve(X, Y)
    ans = float(nodeVoltage[1])
    margin = 0.01
    return question, file, ans, margin


def _Q7_v2(V1, C1, R1, R2, R3, R4, R5, R6, R7, R8):
    file = None
    if __Easy[156][1] is not None:
        file = "Easy/" + __Easy[156][1]
    question = __Easy[156][0] % (V1, C1, R1, R2, R3, R4, R5, R6, R7, R8)
    X = np.array([[(1/R3)+(1/R5), -1/R5, -1/R3, 0],
                  [-1/R5, (1/R5)+(1/R7), 0, -1/R7],
                  [-1/R3, 0, (1/R2)+(1/R3)+(1/R4)+(1/R6), -1/R6],
                  [0, -1/R7, -1/R6, (1/R6)+(1/R7)+(1/R8)]])
    Y = np.array([0, 0, V1/R2, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    ans = abs(float(nodeVoltage[0]-nodeVoltage[1]))
    margin = 0.01
    return question, file, ans, margin


def _Q7_v3(V1, V2, C1, R1, R2, R3, R4, R5):
    file = None
    if __Easy[157][1] is not None:
        file = "Easy/" + __Easy[157][1]
    question = __Easy[157][0] % (V1, V2, C1, R1, R2, R3, R4, R5)
    Va = (V1*R3*R4)/((R3*R4)+(R2*R4)+(R2*R3))
    ans = abs(V2-Va)
    margin = 0.01
    return question, file, ans, margin


#########################################################
#Variants of question 8
def _Q8_v1(V1, L1, R1, R2, R3, R4, R5):
    file = None
    if __Easy[158][1] is not None:
        file = "Easy/" + __Easy[158][1]
    question = __Easy[158][0] % (V1, L1, R1, R2, R3, R4, R5)
    Va = (V1*R4*R5)/((R4*R5)+(R1*R5)+(R1*R4))
    ans = (Va/R4)+(V1/R3)
    margin = 0.00001
    return question, file, ans, margin


def _Q8_v2(V1, I1, L1, R1, R2, R3):
    file = None
    if __Easy[159][1] is not None:
        file = "Easy/" + __Easy[159][1]
    question = __Easy[159][0] % (V1, I1, L1, R1, R2, R3)
    I1 /= 1000
    i1 = (-1*I1*R3)/(R1+R3)
    ans = abs(i1+I1)
    margin = 0.00001
    return question, file, ans, margin


def _Q8_v3(V1, I1, L1, R1, R2, R3, R4, R5, R6, R7, R8):
    file = None
    if __Easy[160][1] is not None:
        file = "Easy/" + __Easy[160][1]
    question = __Easy[160][0] % (V1, I1, L1, R1, R2, R3, R4, R5, R6, R7, R8)
    I1 /= 1000
    Va = (V1*R3)/(R1+R3)
    Vb = (V1*R5*R6)/((R5*R6)+(R4*R6)+(R4*R5))
    i1 = (R8*I1)/(R7+R8)
    i2 = Va/R3
    i3 = Vb/R5
    ans = i1+i2+i3
    margin = 0.00001
    return question, file, ans, margin


#########################################################
#Variants of question 9
def _Q9_v1(R1, R2, R3):
    file = None
    if __Easy[161][1] is not None:
        file = "Easy/" + __Easy[161][1]
    question = __Easy[161][0] % (R1, R2, R3)
    ans = -1*(R2/R1)
    margin = 0.01
    return question, file, ans, margin


def _Q9_v2(R1, R2, R3):
    file = None
    if __Easy[162][1] is not None:
        file = "Easy/" + __Easy[162][1]
    question = __Easy[162][0] % (R1, R2, R3)
    ans = 1+(R2/R1)
    margin = 0.01
    return question, file, ans, margin


#########################################################
#private function for question 10
def _Q10(T):
    file = None
    if __Easy[163][1] is not None:
        file = "Easy/" + __Easy[163][1]
    question = __Easy[163][0] % (T, T)
    ans = ['%dtri(t/%d)' % (T, T), '%dtri[t/%d]' % (T, T),
           '%dtrit/%d' % (T, T), '%dtri{t/%d}' % (T, T)]
    margin = 0
    return question, file, ans, margin


#########################################################
#Variants of question 11
def _Q11_v1(V1, L1, R1, R2, R3, R4):
    file = None
    if __Easy[164][1] is not None:
        file = "Easy/" + __Easy[164][1]
    question = __Easy[164][0] % (V1, L1, R1, R2, R3, R4)
    L1 /= 1000
    Req = (R2+R4)
    Req = (Req*R3)/(Req+R3)
    ans = L1/Req
    margin = 0.000001
    return question, file, ans, margin


def _Q11_v2(t, V1, L1, R1, R2, R3, R4):
    file = None
    if __Easy[165][1] is not None:
        file = "Easy/" + __Easy[165][1]
    question = __Easy[165][0] % (t, V1, L1, R1, R2, R3, R4)
    L1 /= 1000
    Req = (R2+R4)
    Req = (Req*R3)/(Req+R3)
    TC = L1/Req
    t /= -1000
    I0 = V1/R4
    ans = I0*float(np.exp(t/TC))
    margin = 0.000001
    return question, file, ans, margin


def _Q11_v3(V1, L1, R1, R2, R3, R4, R5, R6, R7):
    file = None
    if __Easy[166][1] is not None:
        file = "Easy/" + __Easy[166][1]
    question = __Easy[166][0] % (V1, L1, R1, R2, R3, R4, R5, R6, R7)
    L1 /= 1000
    Req = (R3*R4)/(R3+R4)
    Req += R6
    Req += (R5*R7)/(R5+R7)
    ans = L1/Req
    margin = 0.01
    return question, file, ans, margin


def _Q11_v4(t, V1, L1, R1, R2, R3, R4, R5, R6, R7):
    file = None
    if __Easy[167][1] is not None:
        file = "Easy/" + __Easy[167][1]
    question = __Easy[167][0] % (t, V1, L1, R1, R2, R3, R4, R5, R6, R7)
    L1 /= 1000
    Req = (R3*R4)/(R3+R4)
    Req += R6
    Req += (R5*R7)/(R5+R7)
    TC = L1/Req
    I0 = V1/R2
    t /= -1000
    ans = I0*float(np.exp(t/TC))
    margin = 0.000001
    return question, file, ans, margin


def _Q11_v5(I1, L1, R1, R2, R3, R4):
    file = None
    if __Easy[168][1] is not None:
        file = "Easy/" + __Easy[168][1]
    question = __Easy[168][0] % (I1, L1, R1, R2, R3, R4)
    L1 /= 1000
    Req = R2+R4
    Req = (Req*R3)/(Req+R3)
    ans = L1 / Req
    margin = 0.000001
    return question, file, ans, margin


def _Q11_v6(t, I1, L1, R1, R2, R3, R4):
    file = None
    if __Easy[169][1] is not None:
        file = "Easy/" + __Easy[169][1]
    question = __Easy[169][0] % (t, I1, L1, R1, R2, R3, R4)
    L1 /= 1000
    t /= -1000
    Req = R2+R4
    Req = (Req*R3)/(Req+R3)
    TC = L1/Req
    I0 = ((R1*I1)/(R1+R4)) / 1000
    ans = I0*float(np.exp(t/TC))
    margin = 0.000001
    return question, file, ans, margin


def _Q11_v7(V1, C1, R1, R2, R3, R4, R5, R6, R7, R8):
    file = None
    if __Easy[170][1] is not None:
        file = "Easy/" + __Easy[170][1]
    question = __Easy[170][0] % (V1, C1, R1, R2, R3, R4, R5, R6, R7, R8)
    C1 *= 1e-6
    Req = (R7*R8)/(R7+R8)
    Req += (R6*R5)/(R5+R6)
    Req = (R4*Req)/(R4+Req)
    Req += R3
    ans = C1*Req
    margin = 0.000001
    return question, file, ans, margin


def _Q11_v8(t, V1, C1, R1, R2, R3, R4, R5, R6, R7, R8):
    file = None
    if __Easy[171][1] is not None:
        file = "Easy/" + __Easy[171][1]
    question = __Easy[171][0] % (t, V1, C1, R1, R2, R3, R4, R5, R6, R7, R8)
    C1 /= 1000000
    Req = (R7*R8)/(R7+R8)
    Req += (R6*R5)/(R5+R6)
    Req = (R4*Req)/(R4+Req)
    Req += R3
    TC = C1*Req
    V0 = (R2*V1)/(R1+R2)
    t /= -1000
    ans = V0*float(np.exp(t/TC))
    margin = 0.000001
    return question, file, ans, margin


def _Q11_v9(I1, C1, R1, R2, R3, R4, R5):
    file = None
    if __Easy[172][1] is not None:
        file = "Easy/" + __Easy[172][1]
    question = __Easy[172][0] % (I1, C1, R1, R2, R3, R4, R5)
    C1 /= 1000000
    R01 = R2+R3
    R02 = R4+R5
    Req = (R01*R02)/(R01+R02)
    ans = C1*Req
    margin = 0.000001
    return question, file, ans, margin


def _Q11_v10(t, I1, C1, R1, R2, R3, R4, R5):
    file = None
    if __Easy[173][1] is not None:
        file = "Easy/" + __Easy[172][1]
    question = __Easy[173][0] % (t, I1, C1, R1, R2, R3, R4, R5)
    C1 /= 1000000
    R01 = R2+R3
    R02 = R4+R5
    Req = (R01*R02)/(R01+R02)
    TC = C1*Req
    Req = R3 + R4 + R5
    Req = (Req*R2)/(Req+R2)
    Req = (Req*R1)/(Req+R1)
    I1 /= 1000
    V = Req*I1
    V0 = (V*(R4+R5))/(R3+R4+R5)
    t /= -1000
    ans = V0*float(np.exp(t/TC))
    margin = 0.01
    return question, file, ans, margin


def _Q11_v11(V1, C1, R1, R2, R3, R4, R5, R6, R7, R8):
    file = None
    if __Easy[174][1] is not None:
        file = "Easy/" + __Easy[174][1]
    question = __Easy[174][0] % (V1, C1, R1, R2, R3, R4, R5, R6, R7, R8)
    Req = R3 + R5
    Req = (Req*R6)/(Req+R6)
    Req += R4
    Req = (Req*R7)/(Req+R7)
    Req += R8
    C1 /= 1000000
    ans = C1 * Req
    margin = 0.00001
    return question, file, ans, margin


def _Q11_v12(t, V1, C1, R1, R2, R3, R4, R5, R6, R7, R8):
    file = None
    if __Easy[175][1] is not None:
        file = "Easy/" + __Easy[175][1]
    question = __Easy[175][0] % (t, V1, C1, R1, R2, R3, R4, R5, R6, R7, R8)
    Req = R3 + R5
    Req = (Req*R6)/(Req+R6)
    Req += R4
    Req = (Req*R7)/(Req+R7)
    Req += R8
    C1 /= 1000000
    TC = C1 * Req
    X = np.array([[(1/R3)+(1/R4)+(1/R6), -1/R6],
                  [-1/R6, (1/R5)+(1/R6)+(1/R7)]])
    Y = np.array([V1/R3, V1/R5])
    nodeVoltage = np.linalg.solve(X, Y)
    V0 = float(nodeVoltage[1])
    t /= -1000
    ans = V0*float(np.exp(t/TC))
    margin = 0.01
    return question, file, ans, margin


#########################################################
#Variants of question 12
def _Q12_v1(V1, I1, L1, R1, R2, R3, R4, R5, R6):
    file = None
    if __Easy[176][1] is not None:
        file = "Easy/" + __Easy[176][1]
    question = __Easy[176][0] % (I1, V1, L1, R1, R2, R3, R4, R5, R6)
    Req = (R2*R3)/(R2+R3)
    Req = (Req*(R4+R6))/(Req+R4+R6)
    Req += R5
    L1 /= 1000
    ans = L1/Req
    margin = 0.00000001
    return question, file, ans, margin


def _Q12_v2(V1, I1, L1, R1, R2, R3, R4, R5, R6):
    file = None
    if __Easy[177][1] is not None:
        file = "Easy/" + __Easy[177][1]
    question = __Easy[177][0] % (I1, V1, L1, R1, R2, R3, R4, R5, R6)
    Req = (R1*R3)/(R1+R3)
    Req = (Req*(R4+R6))/(Req+R4+R6)
    Req += R5
    L1 /= 1000
    ans = L1/Req
    margin = 0.000001
    return question, file, ans, margin


def _Q12_v3(t, V1, I1, L1, R1, R2, R3, R4, R5, R6):
    file = None
    if __Easy[178][1] is not None:
        file = "Easy/" + __Easy[178][1]
    question = __Easy[178][0] % (t, I1, V1, L1, R1, R2, R3, R4, R5, R6)
    Req = (R1*R3)/(R1+R3)
    Req = (Req*(R4+R6))/(Req+R4+R6)
    Req += R5
    L1 /= 1000
    TC = L1/Req
    Vth = (V1*R3*(R4+R6)*R5)/((R3*(R4+R6)*R5)+(R2*(R4+R6)*R5)+(R2*R3*R5)+
                              (R2*R3*(R4+R6)))
    I0 = Vth / R5
    I1 /= 1000
    Req = ((R4+R6)*R5)/(R4+R6+R5)
    Req = (Req*R3)/(Req+R3)
    Req = (Req*R1)/(Req+R1)
    Vth = I1*Req
    If = Vth/R5
    t /= -1000
    ans = If+((I0-If)*float(np.exp(t/TC)))
    margin = 0.000001
    return question, file, ans, margin


def _Q12_v4(t, V1, I1, L1, R1, R2, R3, R4, R5, R6):
    file = None
    if __Easy[179][1] is not None:
        file = "Easy/" + __Easy[179][1]
    question = __Easy[179][0] % (t, I1, V1, L1, R1, R2, R3, R4, R5, R6)
    Req = (R2*R3)/(R2+R3)
    Req = (Req*(R4+R6))/(Req+R4+R6)
    Req += R5
    L1 /= 1000
    TC = L1/Req
    I1 /= 1000
    Req = ((R4+R6)*R5)/(R4+R6+R5)
    Req = (Req*R3)/(Req+R3)
    Req = (Req*R1)/(Req+R1)
    Vth = I1*Req
    I0 = Vth/R5
    Vth = (V1*R3*(R4+R6)*R5)/((R3*(R4+R6)*R5)+(R2*(R4+R6)*R5)+(R2*R3*R5)+
                              (R2*R3*(R4+R6)))
    If = Vth / R5
    t /= -1000
    ans = If+((I0-If)*float(np.exp(t/TC)))
    margin = 0.000001
    return question, file, ans, margin


def _Q12_v5(I1, I2, L1, R1, R2, R3, R4):
    file = None
    if __Easy[180][1] is not None:
        file = "Easy/" + __Easy[180][1]
    question = __Easy[180][0] % (I1, I2, L1, R1, R2, R3, R4)
    Req = R2
    L1 /= 1000
    ans = L1/Req
    margin = 0.000001
    return question, file, ans, margin


def _Q12_v6(I1, I2, L1, R1, R2, R3, R4):
    file = None
    if __Easy[181][1] is not None:
        file = "Easy/" + __Easy[181][1]
    question = __Easy[181][0] % (I1, I2, L1, R1, R2, R3, R4)
    Req = R3
    L1 /= 1000
    ans = L1/Req
    margin = 0.000001
    return question, file, ans, margin


def _Q12_v7(t, I1, I2, L1, R1, R2, R3, R4):
    file = None
    if __Easy[182][1] is not None:
        file = "Easy/" + __Easy[182][1]
    question = __Easy[182][0] % (t, I1, I2, L1, R1, R2, R3, R4)
    Req = R3
    L1 /= 1000
    TC = L1/Req
    t /= -1000
    I1 /= 1000
    I2 /= 1000
    ans = I2 + ((I1 - I2)*float(np.exp(t/TC)))
    margin = 0.000001
    return question, file, ans, margin


def _Q12_v8(t, I1, I2, L1, R1, R2, R3, R4):
    file = None
    if __Easy[183][1] is not None:
        file = "Easy/" + __Easy[183][1]
    question = __Easy[183][0] % (t, I1, I2, L1, R1, R2, R3, R4)
    Req = R2
    L1 /= 1000
    TC = L1/Req
    t /= -1000
    I1 /= 1000
    I2 /= 1000
    ans = I1 + ((I2 - I1)*float(np.exp(t/TC)))
    margin = 0.000001
    return question, file, ans, margin


def _Q12_v9(I1, I2, L1, R1, R2, R3, R4):
    file = None
    if __Easy[184][1] is not None:
        file = "Easy/" + __Easy[184][1]
    question = __Easy[184][0] % (I1, I2, L1, R1, R2, R3, R4)
    Req = (R1*R2)/(R1+R2)
    Req += R4
    Req = (Req*R3)/(Req+R3)
    L1 /= 1000
    ans = L1/Req
    margin = 0.000001
    return question, file, ans, margin


def _Q12_v10(I1, I2, L1, R1, R2, R3, R4):
    file = None
    if __Easy[185][1] is not None:
        file = "Easy/" + __Easy[185][1]
    question = __Easy[185][0] % (I1, I2, L1, R1, R2, R3, R4)
    Req = (R3*(R2+R4))/(R2+R3+R4)
    L1 /= 1000
    ans = L1/Req
    margin = 0.000001
    return question, file, ans, margin


def _Q12_v11(t, I1, I2, L1, R1, R2, R3, R4):
    file = None
    if __Easy[186][1] is not None:
        file = "Easy/" + __Easy[186][1]
    question = __Easy[186][0] % (t, I1, I2, L1, R1, R2, R3, R4)
    I1 /= 1000
    I2 /= 1000
    Req = (R1*R2)/(R1+R2)
    Req += R4
    Req = (Req*R3)/(Req+R3)
    L1 /= 1000
    TC = L1/Req
    I0 = (R2*I2)/(R2+R4)
    Req = (R1*R2)/(R1+R2)
    Req = (Req*R4)/(Req+R4)
    V = (I1+I2)*Req
    If = V/R4
    t /= -1000
    ans = If + ((I0-If)*float(np.exp(t/TC)))
    margin = 0.000001
    return question, file, ans, margin


def _Q12_v12(t, I1, I2, L1, R1, R2, R3, R4):
    file = None
    if __Easy[187][1] is not None:
        file = "Easy/" + __Easy[187][1]
    question = __Easy[187][0] % (t, I1, I2, L1, R1, R2, R3, R4)
    I1 /= 1000
    I2 /= 1000
    Req = (R3*(R2+R4))/(R2+R3+R4)
    L1 /= 1000
    TC = L1/Req
    If = (R2*I2)/(R2+R4)
    Req = (R1*R2)/(R1+R2)
    Req = (Req*R4)/(Req+R4)
    V = (I1+I2)*Req
    I0 = V/R4
    t /= -1000
    ans = If + ((I0-If)*float(np.exp(t/TC)))
    margin = 0.000001
    return question, file, ans, margin


def _Q12_v13(V1, I1, C1, R1, R2, R3):
    file = None
    if __Easy[188][1] is not None:
        file = "Easy/" + __Easy[188][1]
    question = __Easy[188][0] % (V1, I1, C1, R1, R2, R3)
    C1 /= 1000000
    Req = (R1*R2)/(R1+R2)
    ans = C1*Req
    margin = 0.000001
    return question, file, ans, margin


def _Q12_v14(V1, I1, C1, R1, R2, R3):
    file = None
    if __Easy[189][1] is not None:
        file = "Easy/" + __Easy[189][1]
    question = __Easy[189][0] % (V1, I1, C1, R1, R2, R3)
    C1 /= 1000000
    ans = C1*R3
    margin = 0.000001
    return question, file, ans, margin


def _Q12_v15(t, V1, I1, C1, R1, R2, R3):
    file = None
    if __Easy[190][1] is not None:
        file = "Easy/" + __Easy[190][1]
    question = __Easy[190][0] % (t, V1, I1, C1, R1, R2, R3)
    C1 /= 1000000
    TC = C1*R3
    V0 = (R2*V1)/(R1+R2)
    I1 /= 1000
    Vf = R3*I1
    t /= -1000
    ans = Vf + ((V0-Vf)*float(np.exp(t/TC)))
    margin = 0.001
    return question, file, ans, margin


def _Q12_v16(t, V1, I1, C1, R1, R2, R3):
    file = None
    if __Easy[191][1] is not None:
        file = "Easy/" + __Easy[191][1]
    question = __Easy[191][0] % (t, V1, I1, C1, R1, R2, R3)
    C1 /= 1000000
    Req = (R1*R2)/(R1+R2)
    TC = C1*Req
    Vf = (R2*V1)/(R1+R2)
    I1 /= 1000
    V0 = R3*I1
    t /= -1000
    ans = Vf + ((V0-Vf)*float(np.exp(t/TC)))
    margin = 0.001
    return question, file, ans, margin


def _Q12_v17(V1, I1, C1, R1, R2, R3, R4, R5):
    file = None
    if __Easy[192][1] is not None:
        file = "Easy/" + __Easy[192][1]
    question = __Easy[192][0] % (V1, I1, C1, R1, R2, R3, R4, R5)
    C1 /= 1000000
    Req = R3 + R4
    Req = (Req*R2)/(Req + R2)
    ans = C1*Req
    margin = 0.000001
    return question, file, ans, margin


def _Q12_v18(V1, I1, C1, R1, R2, R3, R4, R5):
    file = None
    if __Easy[193][1] is not None:
        file = "Easy/" + __Easy[193][1]
    question = __Easy[193][0] % (V1, I1, C1, R1, R2, R3, R4, R5)
    C1 /= 1000000
    Req = R3 + R4
    Req = (Req*R2)/(Req + R2)
    ans = C1*Req
    margin = 0.000001
    return question, file, ans, margin


def _Q12_v19(t, V1, I1, C1, R1, R2, R3, R4, R5):
    file = None
    if __Easy[194][1] is not None:
        file = "Easy/" + __Easy[194][1]
    question = __Easy[194][0] % (t, V1, I1, C1, R1, R2, R3, R4, R5)
    C1 /= 1000000
    Req = R3 + R4
    Req = (Req*R2)/(Req + R2)
    TC = C1*Req
    V0 = ((R3+R4)*V1)/(R2+R3+R4)
    I1 /= 1000
    X = np.array([[(1/R2)+(1/R3), -1/R3], [-1/R3, (1/R3)+(1/R4)]])
    Y = np.array([V1/R2, I1])
    nodeVoltage = np.linalg.solve(X, Y)
    Vf = float(nodeVoltage[0])
    t /= -1000
    ans = Vf + ((V0-Vf)*float(np.exp(t/TC)))
    margin = 0.001
    return question, file, ans, margin


def _Q12_v20(t, V1, I1, C1, R1, R2, R3, R4, R5):
    file = None
    if __Easy[195][1] is not None:
        file = "Easy/" + __Easy[195][1]
    question = __Easy[195][0] % (t, V1, I1, C1, R1, R2, R3, R4, R5)
    C1 /= 1000000
    Req = R3 + R4
    Req = (Req*R2)/(Req + R2)
    TC = C1*Req
    I1 /= 1000
    X = np.array([[(1/R2)+(1/R3), -1/R3], [-1/R3, (1/R3)+(1/R4)]])
    Y = np.array([V1/R2, I1])
    nodeVoltage = np.linalg.solve(X, Y)
    V0 = float(nodeVoltage[0])
    Vf = ((R3+R4)*V1)/(R2+R3+R4)
    t /= -1000
    ans = Vf + ((V0-Vf)*float(np.exp(t/TC)))
    margin = 0.001
    return question, file, ans, margin


def _Q12_v21(V1, I1, C1, R1, R2, R3):
    file = None
    if __Easy[196][1] is not None:
        file = "Easy/" + __Easy[196][1]
    question = __Easy[196][0] % (V1, I1, C1, R1, R2, R3)
    C1 /= 1000000
    Req = (R1*R3)/(R1+R3)
    ans = C1*Req
    margin = 0.000001
    return question, file, ans, margin


def _Q12_v22(V1, I1, C1, R1, R2, R3):
    file = None
    if __Easy[197][1] is not None:
        file = "Easy/" + __Easy[197][1]
    question = __Easy[197][0] % (V1, I1, C1, R1, R2, R3)
    C1 /= 1000000
    Req = (R2*R3)/(R2+R3)
    ans = C1*Req
    margin = 0.000001
    return question, file, ans, margin


def _Q12_v23(t, V1, I1, C1, R1, R2, R3):
    file = None
    if __Easy[198][1] is not None:
        file = "Easy/" + __Easy[198][1]
    question = __Easy[198][0] % (t, V1, I1, C1, R1, R2, R3)
    C1 /= 1000000
    Req = (R2*R3)/(R2+R3)
    TC = C1*Req
    I1 /= 1000
    V0 = (((R1*R3)/(R1+R3))*I1)-V1
    Vf = -1*V1
    t /= -1000
    ans = Vf+((V0-Vf)*float(np.exp(t/TC)))
    margin = 0.001
    return question, file, ans, margin


def _Q12_v24(t, V1, I1, C1, R1, R2, R3):
    file = None
    if __Easy[199][1] is not None:
        file = "Easy/" + __Easy[199][1]
    question = __Easy[199][0] % (t, V1, I1, C1, R1, R2, R3)
    C1 /= 1000000
    I1 /= 1000
    Req = (R1*R3)/(R1+R3)
    TC = C1*Req
    V0 = -1*V1
    Vf = (((R1*R3)/(R1+R3))*I1)-V1
    t /= -1000
    ans = Vf+((V0-Vf)*float(np.exp(t/TC)))
    margin = 0.001
    return question, file, ans, margin


#########################################################
#private function for question 13
def _Q13(version):
    file = None
    if __Easy[199+version][1] is not None:
        file = "Easy/" + __Easy[199+version][1]
    question = __Easy[199+version][0]
    ans = None
    if (version == 1) or (version == 2):
        ans = ['multiplexor', 'mux']
    elif (version == 3) or (version == 4):
        ans = ['decoder', 'demux', 'de-mux', 'demultiplexor']
    margin = 0
    return question, file, ans, margin


#########################################################
#Question 14 function
def Q14():
    file = None
    if __Easy[204][1] is not None:
        file = "Easy/" + __Easy[204][1]
    swr = 100
    while swr > 10:
        z0 = randint(1, 100)
        zl = complex(randint(1, 100), randint(-100, 100))
        swr = float(tl.zl_2_swr(z0, zl))
    theta = np.angle(complex(tl.zl_2_Gamma0(z0, zl)), True)
    if theta < 0:
        theta = 360 + theta
    Vmax = round((0.5 / 360) * theta, 3)
    question = __Easy[204][0] % (z0, swr, Vmax)
    ans = zl
    margin = 15+15j
    return question, file, ans, margin, 3


#########################################################
#private function for question 15
def _Q15(version):
    file = None
    if __Easy[204+version][1] is not None:
        file = "Easy/" + __Easy[204+version][1]
    question = __Easy[204+version][0]
    ans = None
    if version == 1:
        ans = "reduced instruction set computer"
    elif version == 2:
        ans = "complex instruction set computer"
    margin = 0
    return question, file, ans, margin


#########################################################
#Public function for question 1
def Q1():
    #randomly choose variant of question 1
    version = randint(1, 4)
    question = None
    file = None
    ans = None
    margin = None
    units = 2
    #Generate question
    if version == 1:
        version = randint(1, 3)
        V1 = randint(1, 25)
        V2 = randint(1, 10)
        V3 = randint(1, 25)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        question, file, ans, margin = _Q1_v1(V1, V2, V3, R1, R2, R3, R4, R5,
                                             R6, R7, version)
    elif version == 2:
        version = randint(1, 4)
        V1 = randint(1, 25)
        V2 = randint(1, 25)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        R8 = randint(1, 1000)
        R9 = randint(1, 1000)
        question, file, ans, margin = _Q1_v2(V1, V2, R1, R2, R3, R4, R5, R6,
                                             R7, R8, R9, version)
    elif version == 3:
        version = randint(1, 16)
        V1 = randint(1, 25)
        A = randint(1, 500)/100
        B = randint(1, 500)/100
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        R8 = randint(1, 1000)
        R9 = randint(1, 1000)
        R10 = randint(1, 1000)
        question, file, ans, margin = _Q1_v3(V1, A, B, R1, R2, R3, R4, R5,
                                             R6, R7, R8, R9, R10, version)
    elif version == 4:
        version = randint(1, 20)
        V1 = randint(1, 25)
        I1 = randint(1, 100)
        A = randint(1, 500)/100
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        R8 = randint(1, 1000)
        R9 = randint(1, 1000)
        R10 = randint(1, 1000)
        R11 = randint(1, 1000)
        question, file, ans, margin = _Q1_v4(V1, I1, A, R1, R2, R3, R4, R5,
                                             R6, R7, R8, R9, R10, R11,
                                             version)
    #Return question data
    return question, file, ans, margin, units


#Public function for question 2
def Q2():
    #randomly choose variant of question 2
    version = randint(1, 4)
    question = None
    file = None
    ans = None
    margin = None
    units = 1
    #Generate question
    if version == 1:
        version = randint(1, 7)
        V1 = randint(1, 25)
        V2 = randint(1, 10)
        V3 = randint(1, 25)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        question, file, ans, margin = _Q2_v1(V1, V2, V3, R1, R2, R3, R4, R5,
                                             R6, R7, version)
    elif version == 2:
        version = randint(1, 9)
        V1 = randint(1, 25)
        V2 = randint(1, 25)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        R8 = randint(1, 1000)
        R9 = randint(1, 1000)
        question, file, ans, margin = _Q2_v2(V1, V2, R1, R2, R3, R4, R5, R6,
                                             R7, R8, R9, version)
    elif version == 3:
        version = randint(1, 40)
        V1 = randint(1, 25)
        A = randint(1, 500)/100
        B = randint(1, 500)/100
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        R8 = randint(1, 1000)
        R9 = randint(1, 1000)
        R10 = randint(1, 1000)
        question, file, ans, margin = _Q2_v3(V1, A, B, R1, R2, R3, R4, R5, R6,
                                             R7, R8, R9, R10, version)
    elif version == 4:
        version = randint(1, 44)
        V1 = randint(1, 25)
        I1 = randint(1, 100)
        A = randint(1, 500)/100
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        R8 = randint(1, 1000)
        R9 = randint(1, 1000)
        R10 = randint(1, 1000)
        R11 = randint(1, 1000)
        question, file, ans, margin = _Q2_v4(V1, I1, A, R1, R2, R3, R4, R5,
                                             R6, R7, R8, R9, R10, R11,
                                             version)
    #Return question data
    return question, file, ans, margin, units


#Public function for question 3
def Q3():
    #randomly choose variant of question 3
    version = randint(1, 3)
    question = None
    file = None
    ans = None
    margin = None
    units = 2
    #Generate question
    if version == 1:
        V1 = randint(1, 25)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        question, file, ans, margin = _Q3_v1(V1, R1, R2, R3, R4, R5, R6)
    elif version == 2:
        V1 = randint(1, 25)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        question, file, ans, margin = _Q3_v2(V1, R1, R2, R3, R4, R5, R6)
    elif version == 3:
        V1 = randint(1, 25)
        A = randint(1, 1000)/10
        B = randint(1, 500)/1000
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        question, file, ans, margin = _Q3_v3(V1, A, B, R1, R2, R3, R4)
        #Return question data
    return question, file, ans, margin, units


#Public function for question 4
def Q4():
    #randomly choose variant of question 4
    version = randint(1, 3)
    question = None
    file = None
    ans = None
    margin = None
    units = 3
    #Generate question
    if version == 1:
        V1 = randint(1, 25)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        question, file, ans, margin = _Q4_v1(V1, R1, R2, R3, R4, R5, R6)
    elif version == 2:
        V1 = randint(1, 25)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        question, file, ans, margin = _Q4_v2(V1, R1, R2, R3, R4, R5, R6)
    elif version == 3:
        V1 = randint(1, 25)
        A = randint(1, 1000)/10
        B = randint(1, 500)/1000
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        question, file, ans, margin = _Q4_v3(V1, A, B, R1, R2, R3, R4)
    #Return question data
    return question, file, ans, margin, units


#Public function for question 5
def Q5():
    #randomly choose variant of question 5
    version = randint(1, 3)
    question = None
    file = None
    ans = None
    margin = None
    units = 3
    #Generate question
    if version == 1:
        V1 = randint(1, 25)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        question, file, ans, margin = _Q5_v1(V1, R1, R2, R3, R4, R5, R6)
    elif version == 2:
        V1 = randint(1, 25)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        question, file, ans, margin = _Q5_v2(V1, R1, R2, R3, R4, R5, R6)
    elif version == 3:
        V1 = randint(1, 25)
        A = randint(1, 1000)/10
        B = randint(1, 500)/1000
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        question, file, ans, margin = _Q5_v3(V1, A, B, R1, R2, R3, R4)
    #Return question data
    return question, file, ans, margin, units


#Public function for question 6
def Q6():
    #randomly choose variant of question 6
    version = randint(1, 3)
    question = None
    file = None
    ans = None
    margin = None
    units = 4
    #Generate question
    if version == 1:
        V1 = randint(1, 25)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        question, file, ans, margin = _Q6_v1(V1, R1, R2, R3, R4, R5, R6)
    elif version == 2:
        V1 = randint(1, 25)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        question, file, ans, margin = _Q6_v2(V1, R1, R2, R3, R4, R5, R6)
    elif version == 3:
        V1 = randint(1, 25)
        A = randint(1, 1000)/10
        B = randint(1, 500)/1000
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        question, file, ans, margin = _Q6_v3(V1, A, B, R1, R2, R3, R4)
    #Return question data
    return question, file, ans, margin, units


#Public function for question 7
def Q7():
    #randomly choose variant of question 7
    version = randint(1, 3)
    question = None
    file = None
    ans = None
    margin = None
    units = 2
    #Generate question
    if version == 1:
        V1 = randint(1, 25)
        C1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        question, file, ans, margin = _Q7_v1(V1, C1, R1, R2, R3, R4, R5, R6)
    elif version == 2:
        V1 = randint(1, 25)
        C1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        R8 = randint(1, 1000)
        question, file, ans, margin = _Q7_v2(V1, C1, R1, R2, R3, R4, R5, R6,
                                             R7, R8)
    elif version == 3:
        V1 = randint(1, 25)
        V2 = randint(1, 25)
        C1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        question, file, ans, margin = _Q7_v3(V1, V2, C1, R1, R2, R3, R4, R5)
    #Return question data
    return question, file, ans, margin, units


#Public function for question 8
def Q8():
    #randomly choose variant of question 8
    version = randint(1, 2)  # fix for later
    question = None
    file = None
    ans = None
    margin = None
    units = 1
    #Generate question
    if version == 1:
        V1 = randint(1, 25)
        L1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        question, file, ans, margin = _Q8_v1(V1, L1, R1, R2, R3, R4, R5)
    elif version == 2:
        V1 = randint(1, 25)
        I1 = randint(1, 500)
        L1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        question, file, ans, margin = _Q8_v2(V1, I1, L1, R1, R2, R3)
    elif version == 3:
        V1 = randint(1, 25)
        I1 = randint(1, 500)
        L1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        R8 = randint(1, 1000)
        question, file, ans, margin = _Q8_v3(V1, I1, L1, R1, R2, R3, R4, R5,
                                             R6, R7, R8)
    #Return question data
    return question, file, ans, margin, units


#Public function for question 9
def Q9():
    #randomly choose variant of question 9
    version = randint(1, 2)
    question = None
    file = None
    ans = None
    margin = None
    units = 8
    #Generate question
    R1 = randint(1, 1000)
    R2 = randint(1, 1000)
    R3 = randint(1, 1000)
    if version == 1:
        question, file, ans, margin = _Q9_v1(R1, R2, R3)
    elif version == 2:
        question, file, ans, margin = _Q9_v2(R1, R2, R3)
    #Return question data
    return question, file, ans, margin, units


#Public function for question 10
def Q10():
    #Generate question
    T = randint(1, 1000)
    question, file, ans, margin = _Q10(T)
    #Return question data
    return question, file, ans, margin, None


#Public function for question 11
def Q11():
    #randomly choose variant of question 11
    version = randint(1, 12)
    question = None
    file = None
    ans = None
    margin = None
    units = None
    #Generate question
    if version == 1:
        units = 9
        V1 = randint(1, 25)
        L1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        question, file, ans, margin = _Q11_v1(V1, L1, R1, R2, R3, R4)
    elif version == 2:
        units = 1
        V1 = randint(1, 25)
        L1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        t = randint(1, 2000)/1000
        question, file, ans, margin = _Q11_v2(t, V1, L1, R1, R2, R3, R4)
    elif version == 3:
        units = 9
        V1 = randint(1, 25)
        L1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        question, file, ans, margin = _Q11_v3(V1, L1, R1, R2, R3, R4, R5,
                                              R6, R7)
    elif version == 4:
        units = 1
        V1 = randint(1, 25)
        L1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        t = randint(1, 2000)/1000
        question, file, ans, margin = _Q11_v4(t, V1, L1, R1, R2, R3, R4, R5,
                                              R6, R7)
    elif version == 5:
        units = 9
        I1 = randint(1, 100)
        L1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        question, file, ans, margin = _Q11_v5(I1, L1, R1, R2, R3, R4)
    elif version == 6:
        units = 1
        I1 = randint(1, 100)
        L1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        t = randint(1, 2000)/1000
        question, file, ans, margin = _Q11_v6(t, I1, L1, R1, R2, R3, R4)
    elif version == 7:
        units = 9
        V1 = randint(1, 25)
        C1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        R8 = randint(1, 1000)
        question, file, ans, margin = _Q11_v7(V1, C1, R1, R2, R3, R4,
                                              R5, R6, R7, R8)
    elif version == 8:
        units = 2
        V1 = randint(1, 25)
        C1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        R8 = randint(1, 1000)
        t = randint(1, 2000)/1000
        question, file, ans, margin = _Q11_v8(t, V1, C1, R1, R2, R3, R4,
                                              R5, R6, R7, R8)
    elif version == 9:
        units = 9
        I1 = randint(1, 100)
        C1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        question, file, ans, margin = _Q11_v9(I1, C1, R1, R2, R3, R4, R5)
    elif version == 10:
        units = 2
        I1 = randint(1, 100)
        C1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        t = randint(1, 2000)/1000
        question, file, ans, margin = _Q11_v10(t, I1, C1, R1, R2, R3, R4, R5)
    elif version == 11:
        units = 9
        V1 = randint(1, 25)
        C1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        R8 = randint(1, 1000)
        question, file, ans, margin = _Q11_v11(V1, C1, R1, R2, R3, R4,
                                               R5, R6, R7, R8)
    elif version == 12:
        units = 2
        V1 = randint(1, 25)
        C1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        R8 = randint(1, 1000)
        t = randint(1, 2000)/1000
        question, file, ans, margin = _Q11_v12(t, V1, C1, R1, R2, R3, R4,
                                               R5, R6, R7, R8)
    #Return question data
    return question, file, ans, margin, units


#Public function for question 12
def Q12():
    #randomly choose variant of question 12
    version = randint(1, 24)
    question = None
    file = None
    ans = None
    margin = None
    units = None
    #Generate question
    if version == 1:
        units = 9
        V1 = randint(1, 25)
        I1 = randint(1, 100)
        L1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        question, file, ans, margin = _Q12_v1(V1, I1, L1, R1, R2, R3, R4,
                                              R5, R6)
    elif version == 2:
        units = 9
        V1 = randint(1, 25)
        I1 = randint(1, 100)
        L1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        question, file, ans, margin = _Q12_v2(V1, I1, L1, R1, R2, R3, R4,
                                              R5, R6)
    elif version == 3:
        units = 1
        V1 = randint(1, 25)
        I1 = randint(1, 100)
        L1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        t = randint(1, 2000)/1000
        question, file, ans, margin = _Q12_v3(t, V1, I1, L1, R1, R2, R3, R4,
                                              R5, R6)
    elif version == 4:
        units = 1
        V1 = randint(1, 25)
        I1 = randint(1, 100)
        L1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        t = randint(1, 2000)/1000
        question, file, ans, margin = _Q12_v4(t, V1, I1, L1, R1, R2, R3, R4,
                                              R5, R6)
    elif version == 5:
        units = 9
        I1 = randint(1, 100)
        I2 = randint(1, 100)
        L1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        question, file, ans, margin = _Q12_v5(I1, I2, L1, R1, R2, R3, R4)
    elif version == 6:
        units = 9
        I1 = randint(1, 100)
        I2 = randint(1, 100)
        L1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        question, file, ans, margin = _Q12_v6(I1, I2, L1, R1, R2, R3, R4)
    elif version == 7:
        units = 1
        I1 = randint(1, 100)
        I2 = randint(1, 100)
        L1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        t = randint(1, 2000)/1000
        question, file, ans, margin = _Q12_v7(t, I1, I2, L1, R1, R2, R3, R4)
    elif version == 8:
        units = 1
        I1 = randint(1, 100)
        I2 = randint(1, 100)
        L1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        t = randint(1, 2000)/1000
        question, file, ans, margin = _Q12_v8(t, I1, I2, L1, R1, R2, R3, R4)
    elif version == 9:
        units = 9
        I1 = randint(1, 100)
        I2 = randint(1, 100)
        L1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        question, file, ans, margin = _Q12_v9(I1, I2, L1, R1, R2, R3, R4)
    elif version == 10:
        units = 9
        I1 = randint(1, 100)
        I2 = randint(1, 100)
        L1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        question, file, ans, margin = _Q12_v10(I1, I2, L1, R1, R2, R3, R4)
    elif version == 11:
        units = 1
        I1 = randint(1, 100)
        I2 = randint(1, 100)
        L1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        t = randint(1, 2000)/1000
        question, file, ans, margin = _Q12_v11(t, I1, I2, L1, R1, R2, R3, R4)
    elif version == 12:
        units = 1
        I1 = randint(1, 100)
        I2 = randint(1, 100)
        L1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        t = randint(1, 2000)/1000
        question, file, ans, margin = _Q12_v12(t, I1, I2, L1, R1, R2, R3, R4)
    elif version == 13:
        units = 9
        V1 = randint(1, 25)
        I1 = randint(1, 100)
        C1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        question, file, ans, margin = _Q12_v13(V1, I1, C1, R1, R2)
    elif version == 14:
        units = 9
        V1 = randint(1, 25)
        I1 = randint(1, 100)
        C1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        question, file, ans, margin = _Q12_v14(V1, I1, C1, R1, R2)
    elif version == 15:
        units = 2
        V1 = randint(1, 25)
        I1 = randint(1, 100)
        C1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        t = randint(1, 2000)/1000
        question, file, ans, margin = _Q12_v15(t, V1, I1, C1, R1, R2)
    elif version == 16:
        units = 2
        V1 = randint(1, 25)
        I1 = randint(1, 100)
        C1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        t = randint(1, 2000)/1000
        question, file, ans, margin = _Q12_v16(t, V1, I1, C1, R1, R2)
    elif version == 17:
        units = 9
        V1 = randint(1, 25)
        I1 = randint(1, 100)
        C1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        question, file, ans, margin = _Q12_v17(V1, I1, C1, R1, R2, R3, R4, R5)
    elif version == 18:
        units = 9
        V1 = randint(1, 25)
        I1 = randint(1, 100)
        C1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        question, file, ans, margin = _Q12_v18(V1, I1, C1, R1, R2, R3, R4, R5)
    elif version == 19:
        units = 2
        V1 = randint(1, 25)
        I1 = randint(1, 100)
        C1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        t = randint(1, 2000)/1000
        question, file, ans, margin = _Q12_v19(t, V1, I1, C1, R1, R2,
                                               R3, R4, R5)
    elif version == 20:
        units = 2
        V1 = randint(1, 25)
        I1 = randint(1, 100)
        C1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        t = randint(1, 2000)/1000
        question, file, ans, margin = _Q12_v20(t, V1, I1, C1, R1, R2, R3,
                                               R4, R5)
    elif version == 21:
        units = 9
        V1 = randint(1, 25)
        I1 = randint(1, 100)
        C1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        question, file, ans, margin = _Q12_v21(V1, I1, C1, R1, R2, R3)
    elif version == 22:
        units = 9
        V1 = randint(1, 25)
        I1 = randint(1, 100)
        C1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        question, file, ans, margin = _Q12_v22(V1, I1, C1, R1, R2, R3)
    elif version == 23:
        units = 2
        V1 = randint(1, 25)
        I1 = randint(1, 100)
        C1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        t = randint(1, 2000)/1000
        question, file, ans, margin = _Q12_v23(t, V1, I1, C1, R1, R2, R3)
    elif version == 24:
        units = 2
        V1 = randint(1, 25)
        I1 = randint(1, 100)
        C1 = randint(1, 100)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        t = randint(1, 2000)/1000
        question, file, ans, margin = _Q12_v24(t, V1, I1, C1, R1, R2, R3)
    #Return question data
    return question, file, ans, margin, units


#Public function for question 13
def Q13():
    #Generate question
    version = randint(1, 4)
    question, file, ans, margin = _Q13(version)
    #Return question data
    return question, file, ans, margin, None


#Public function for question 15
def Q15():
    #Generate question
    version = randint(1, 2)
    question, file, ans, margin = _Q15(version)
    #Return question data
    return question, file, ans, margin, None
