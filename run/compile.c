/*****************************************************************************//**
* BSD 3 - Clause License
*
* Copyright(c) 2021, Tom Schmitz
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
* list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its
* contributors may be used to endorse or promote products derived from
* this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*********************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int myStrCpy(char * dest, const char * src)
{
	int i = -1;
	while (src[++i] != '\0')
		dest[i] = src[i];
	dest[i] = '\0';
	return i;
}

void myStrCat(char * dest, char * src, char delim)
{
	int i = -1;
	while (dest[++i] != '\0');
	if (delim != '\0')
		dest[i++] = delim;
	int j = -1;
	while (src[++j] != '\0')
		dest[i++] = src[j];
	dest[i] = '\0';
}

int main(int argc, char ** argv)
{
	char path[16384];
	if (argc == 4)
	{
		int len = strlen(argv[1]);
		len += strlen(argv[2]);
		len += strlen(argv[3]);
		len += 3;
		len += 11;
		char * str = (char *)malloc(len);
		for (int i = 0; i < len; ++i)
			str[i] = '\0';
		myStrCpy(str, "cd Compile;");
		myStrCat(str, argv[1], '\0');
		myStrCat(str, argv[2], ' ');
		myStrCat(str, argv[3], ' ');
		printf("%s\n", str);
		FILE * fp;
		int status;

		fp = popen(str, "r");
		if (fp == NULL)
		{
			printf("popen function failed\n");
			free(str);
			exit(EXIT_FAILURE);
		}
		
		while (fgets(path, 16384, fp) != NULL)
			printf("%s", path);

		status = pclose(fp);
		if (status == -1)
		{
			printf("Error closing terminal\n");
		}

		free(str);
	}

	return 0;
}
