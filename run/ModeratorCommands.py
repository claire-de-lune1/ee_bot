# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import discord
from discord import File
from discord.ext import commands
from discord.commands import slash_command
from discord.commands import Option
from discord.commands import permissions
from EE_Bot_DB import EE_DB
from EE_Bot_DB import searchWarnings
from EE_Bot_DB import cogEnum
from EE_Bot_DB import channelType
from datetime import datetime
from datetime import timedelta
from copy import deepcopy
import emoji
import asyncio
import csv
import io
import re
import globals as GL

# Initialize and get globals
GL.InitLaunch()
originGuild = GL.originGuild


class Moderator(commands.Cog):
    """Moderation Commands"""

    # Initialize Cog
    def __init__(self, MASTER, client, database):
        self.MASTER = MASTER
        self.client = client
        self.database = database
        self.csvHeader = ['guildID', 'channel', 'userID', 'userName', 'adminID', 'adminName', 'ruleViolated', 'reason',
                          'date', 'url']

    # Command to create a new channel
    # noinspection SpellCheckingInspection
    @slash_command(description='Create a new class channel')
    async def createchannel(
            self,
            ctx,
            channel: Option(int, 'The class number')
    ):
        # Check if command was ran inside server
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this command!')
            return
        # Open database
        db = EE_DB(self.database, ctx.guild.id)
        # Check if command is enabled
        if not db.checkCog(cogEnum.Moderator):
            await ctx.respond('Command has been disabled by your server owner!')
            return
        # Convert integer to string
        channel = str(channel)
        # Check the length of the input and checks if input contains only digits
        if len(channel) != 3:
            await ctx.respond("Invalid input. Expecting a three digit number for the input.")
            return

        # Checks if the author is a moderator
        moderator = ctx.author.guild_permissions.administrator

        # Calls database function to create new channel
        db.createChannel(channel, ctx.author.id, moderator)
        # Check error code of database
        err = db.getError()
        if err == 3:  # Channel already exists error
            await ctx.respond("Channel already exists! If you can't see it, please contact the server staff.")
            return
        elif err == 5:  # Not enough reputation
            await ctx.respond('Sorry, you are not a high enough level to create a new channel. Please contact the '
                              'server staff for channel creation.')
            return
        elif err != 0:  # Catch other errors
            channel_id = db.getLoggingID(channelType.log)
            if channel_id != 0:
                await self.client.get_channel(channel_id).send("Database fault. Unable to add " + channel)
                await ctx.respond("Database fault! Server owner notified.")
            return
        else:  # No errors
            # Respond so discord API doesn't freak out
            await ctx.respond('Creating new channel...')
            prefix = db.getChannelPrefix()
            if db.getError() != 0:
                channel_id = db.getLoggingID(channelType.log)
                if channel_id != 0:
                    await self.client.get_channel(channel_id).send("Database fault. Unable to add " + channel)
                    await ctx.respond("Database fault! Server owner notified.")
                return
            name = 'Classes'
            # Get category from guild
            category = discord.utils.get(ctx.guild.categories, name=name)
            textCategory = category

            logSet = False
            log = db.getLoggingID(channelType.log)
            if log != 0:
                log = self.client.get_channel(log)
                logSet = True

            # Calculate position of new channel
            position = 0
            end = True
            channels = category.text_channels
            for x in channels:
                try:
                    if int(x.name.lstrip(prefix)) > int(channel):
                        position = x.position
                        end = False
                        break
                except Exception:
                    if logSet:
                        await log.send('%s channel name does not follow the naming standards of this server!' % x.name)

            # Create new channel
            newChannel = await ctx.guild.create_text_channel((prefix + channel), category=category)
            newTextChannel = newChannel

            # Set position for new channel
            if not end:
                await newChannel.edit(position=position)

            # Get voice channel category
            name = 'Voice Channels'
            category = discord.utils.get(ctx.guild.categories, name=name)

            prefix = prefix.upper()
            # Calculate position
            channels = category.voice_channels
            if not end:
                for x in channels:
                    try:
                        if str(x)[:len(prefix)] != prefix:
                            continue
                        elif int(str(x).lstrip(prefix)) > int(channel):
                            position = x.position
                            break
                    except Exception:
                        pass  # Don't care about error, already processed it earlier in the code

            # Create new voice channel
            newChannel = await ctx.guild.create_voice_channel((prefix + channel), category=category)
            newVoiceChannel = newChannel

            # Set position of new channel
            if not end:
                await newChannel.edit(position=position)

            # Get role color
            RGB = db.getRoleColor()
            # If role color field is NULL, then use default role color
            if db.getError():
                color = discord.colour.Color.dark_teal()
            else:
                color = discord.colour.Color.from_rgb(RGB[0], RGB[1], RGB[2])

            # Create new role
            newRole = await ctx.guild.create_role(name=channel, colour=color, mentionable=True)

            for i in range(0, len(ctx.guild.roles)):
                if not ctx.guild.roles[i].permissions.administrator:
                    await newTextChannel.set_permissions(ctx.guild.roles[i], view_channel=False)
                    await newVoiceChannel.set_permissions(ctx.guild.roles[i], view_channel=False)

            await newTextChannel.set_permissions(newRole, view_channel=True)
            await newVoiceChannel.set_permissions(newRole, view_channel=True)
            await newVoiceChannel.set_permissions(ctx.guild.default_role, view_channel=False)
            await textCategory.set_permissions(newRole, view_channel=True)
        del db

        # Command to delete messages - Owner only

    @slash_command(description='Remove a given number of messages from a channel')
    async def clear(self, ctx, amount: Option(int, 'The number of messages to delete. Defaults to -1 for all '
                                                   'messages. Cannot be 0.', default = -1, required = False)
                    ):
        # Check if command was ran inside server
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this!')
            return
        if ctx.author.id == ctx.guild.owner_id:  # Check author ID
            if amount < 0:
                amount = 'all'
            elif amount == 0:
                await ctx.respond('Amount cannot be 0!')
                return
            if amount != 'all':  # Purge a certain number of messages
                await ctx.channel.purge(limit=amount)
                await ctx.respond(f'{amount} messages purged.')
            else:  # Purge all the messages allowed
                await ctx.respond('Purging as many messages as possible.')
                await ctx.channel.purge(limit=None)
        else:  # only purge the command given
            await ctx.respond('You cannot run this command!')

    # Command to clear the contents of an entire channel - Owner only
    # noinspection SpellCheckingInspection
    @slash_command(description="Clears the contents of a channel")
    async def clearchannel(self, ctx):
        # Check if command was ran inside server
        if ctx.guild is None:
            await ctx.respond("Must be in a server to run this!")
            return
        if ctx.author.id == ctx.guild.owner_id:  # Check for master ID
            old_channel = ctx.channel  # Get the old channel
            if old_channel is not None:
                await ctx.respond('Clearing channel!')
                await old_channel.clone()  # Clone the old channel (Does not copy messages over)
                await old_channel.delete()  # Deletes old channel
        else:
            await ctx.respond('You cannot run this command!')

    # Command that let's user assign themselves to roles
    # noinspection SpellCheckingInspection
    @slash_command(
        description='Assign yourself a class role. If the role is non-standard, please refer to the role poll.')
    async def assignrole(self, ctx, role: Option(int, 'The course number')):
        # Check if command was ran inside server
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this command!')
            return
        db = EE_DB(self.database, ctx.guild.id)
        # Check if command is enabled
        if not db.checkCog(cogEnum.Moderator):
            await ctx.respond('This command has been disabled!')
            return
        roles = ctx.guild.roles  # Gets roles from guild
        selectedRole = 0
        # Loop to find role by name
        for x in roles:
            if x.name == str(role):
                selectedRole = x
                break
        # Assign role if the role was found
        if selectedRole != 0:
            channel_id = db.getLoggingID(channelType.log)
            if channel_id != 0:
                await ctx.author.add_roles(selectedRole)
                await self.client.get_channel(channel_id).send(f'{ctx.author.name} added to role {role}')
            await ctx.respond(f'You have been assigned the {role} role')
        else:  # Send error message
            channel_id = db.getLoggingID(channelType.log)
            if channel_id != 0:
                await self.client.get_channel(channel_id).send(f'{ctx.author.name} tried assignning role {role}')
            await ctx.respond("Role does not exist!")

    # Command that let's user remove roles from themselves
    # noinspection SpellCheckingInspection
    @slash_command(
        description='Remove a class role from yourself. If the role is non-standard, please refer to the role poll.')
    async def unassignrole(self, ctx, role: Option(int, 'The course number')):
        # Check if command was ran inside server
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this command!')
            return
        db = EE_DB(self.database, ctx.guild.id)
        # Check if command is enabled
        if not db.checkCog(cogEnum.Moderator):
            await ctx.respond('This command has been disabled!')
            return
        roles = ctx.guild.roles  # Get guild roles
        selectedRole = 0
        for x in roles:  # Search guild roles by name
            if x.name == str(role):
                selectedRole = x
                break
        # Remove role if found
        if selectedRole != 0:
            if selectedRole in ctx.author.roles:
                channel_id = db.getLoggingID(channelType.log)
                if channel_id != 0:
                    await ctx.author.remove_roles(selectedRole)
                    await self.client.get_channel(channel_id).send(f"Removed {ctx.author.name} from {role}")
                await ctx.respond(f'You have been removed from the {role} role')
            else:  # Send error message
                channel_id = db.getLoggingID(channelType.log)
                if channel_id != 0:
                    await self.client.get_channel(channel_id).send("Removal not successful")
                await ctx.respond("Cannot remove a role that is not assigned to you!")
        else:  # Send error message
            channel_id = db.getLoggingID(channelType.log)
            if channel_id != 0:
                await self.client.get_channel(channel_id).send("Removal not successful")
            await ctx.channel.send("Role does not exist")

    # Function that runs the archive task asynchronously to prevent blocking
    async def runArchive(self, ctx):
        db = EE_DB(self.database, ctx.guild.id)
        # Get the message IDs from the database
        msg = db.getMessages(ctx.channel.id)
        logging = False
        cID = db.getLoggingID(channelType.log)
        if cID != 0:
            LOG = self.client.get_channel(cID)
            logging = True

        # Report empty channel error
        if len(msg) == 0:
            if logging:
                await LOG.send('No messages found in channel. Archive command aborted!')
            else:
                await ctx.channel.send('No messages found in channel. Archive command aborted! If you do not want the '
                                       'general public to see these messages, please consider setting up logging '
                                       'channels by running the `configure` command.')
            db.setArchiveFlag(False)
            return

        channel = ctx.channel
        guild = ctx.guild

        # Clone new channel first
        newChannel = await channel.clone()
        await newChannel.edit(position=channel.position)
        # Get name for archive channel
        archiveChannelName = f'{channel.name}-archive'
        # Check if there is an archive category. Ff not, create one...
        category = discord.utils.find(lambda c: c.name == 'Archive', guild.categories)

        if category is None:
            category = await guild.create_category('Archive')
            await category.set_permissions(guild.default_role, add_reactions=False,
                                           attach_files=False, manage_channels=False,
                                           manage_messages=False, manage_permissions=False,
                                           read_message_history=True, read_messages=False,
                                           send_messages=False, send_tts_messages=False,
                                           use_slash_commands=False)
            # Create archive channel by cloning old channel
            archiveChannel = await channel.clone()
            # Edit the name
            await archiveChannel.edit(name=archiveChannelName, category=category)
        else:
            # Check if archive channel already exist
            channels = category.text_channels
            archiveChannel = discord.utils.find(lambda ch: ch.name == archiveChannelName, channels)
            # Create a new archive channel
            if archiveChannel is None:
                archiveChannel = await channel.clone()
                # Edit the name
                await archiveChannel.edit(name=archiveChannelName, category=category)
                new = int(''.join(filter(str.isdigit, channel.name)))
                pos = 0
                end = True
                # Find position of archive channel
                for x in channels:
                    ch = int(''.join(filter(str.isdigit, x)))
                    if ch > new:
                        pos = x.position
                        end = False
                        break
                if not end:
                    await archiveChannel.edit(position=pos)

        # Get roles of channel being archived and update their permissions
        roles = channel.changed_roles
        overwrite = discord.PermissionOverwrite(
            send_messages=False,
            read_messages=False,
            read_message_history=False
        )
        for x in roles:
            await channel.set_permissions(x, overwrite=overwrite)

        # Get roles from archive channel, save their permissions for later, and update them
        roles = archiveChannel.changed_roles
        role_permissions = []
        for x in roles:
            role_permissions.append([x, archiveChannel.overwrites_for(x)])
            await archiveChannel.set_permissions(x, overwrite=overwrite)

        # Create archive header
        date = datetime.today()
        month = date.month
        year = date.year
        term = ''
        if month > 8:
            term = 'Fall %d' % year
        elif (month < 9) and (month > 5):
            term = 'Summer %d' % year
        elif month < 6:
            term = 'Spring %d' % year

        await archiveChannel.send(f'----------\n__**Start of the {term} semester**__\n----------')

        msgRef = []
        MsgRef = None
        files = []

        # Loop through messages
        for i in msg:
            MSG = await channel.fetch_message(i)
            date = MSG.created_at
            await archiveChannel.send('----------\nMessage create by %s at %d/%d/%d %d:%d' %
                                      (MSG.author.name, date.month, date.day, date.year, date.hour, date.minute))
            # Find reference
            if MSG.reference:
                if MSG.reference.guild_id == guild.id and MSG.reference.channel_id == channel.id:
                    # Go backwards to save time
                    for x in range(len(msgRef) - 1, -1, -1):
                        if msgRef[x][0] == MSG.reference.message_id:
                            MsgRef = msgRef[x][1]
                            break
                else:
                    MsgRef = MSG.reference

            # Get attachments
            if MSG.attachments:
                files = MSG.attachments
                for i in range(0, len(files)):
                    files[i] = await files[i].to_file(spoiler=files[i].is_spoiler())

            # Post message
            try:
                newMsg = await archiveChannel.send(content=MSG.content, tts=MSG.tts,
                                                   files=files, reference=MsgRef)
            except discord.HTTPException as err:
                newMsg = await archiveChannel.send(str(err))

            # Add message to reference list
            item = [i, newMsg]
            msgRef.append(item)
            MsgRef = None
            files = []
            # Sleep for 3 seconds to allow for cool down
            await asyncio.sleep(3)

        await archiveChannel.send(f'----------\n__**End of the {term} semester**__\n----------')

        # Reset permissions from earlier
        for x in role_permissions:
            await archiveChannel.set_permissions(x[0], overwrite=x[1])

        # Clear the messages from the database and delete the old channel
        db.bulkMsgDelete(channel.id)
        await channel.delete()

        await LOG.send(f'Finished archiving {channel.name}')

        # Release the archive flag
        db.setArchiveFlag(False)
        del db
        return

    # Command to archive channels
    @slash_command(description='Run this command inside a class channel to archive it.')
    async def archive(self, ctx):
        # Check if command was ran inside server
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this command!')
            return
        db = EE_DB(self.database, ctx.guild.id)
        # Check if command is enabled
        if not db.checkCog(cogEnum.Moderator):
            await ctx.respond('This command has been disabled!')
            return
        # Check if user is an admin
        if not ctx.author.guild_permissions.administrator:
            await ctx.respond('You need admin permissions to do this.')
            return
        # Check if channel is a class channel
        if ctx.channel.category.name != 'Classes':
            await ctx.respond('This action can only be performed in class channels.')
            return
        # Get archive flag
        Archive = db.getArchiveFlag()
        # Check for database errors
        if db.getError() != 0:
            await ctx.respond('An error occurred!')
            return
        # Run archive routine if false
        if not Archive:
            # Update archive flag
            db.setArchiveFlag(True)
            # Create asynchronous task
            asyncio.create_task(self.runArchive(ctx))
            await ctx.respond(f'{ctx.channel.name} is now getting archived.')
        else:
            await ctx.respond('Please wait until current archive task is done')

    # Command to ban a user
    @slash_command(description='Bans a user.')
    async def ban(self, ctx, member: Option(discord.Member, 'Member who gets banned'),
                  rule_violated: Option(int, 'The rule violated'), reason: Option(str, 'The reason for the ban')):
        # Check if command was ran inside server
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this command!')
            return
        db = EE_DB(self.database, ctx.guild.id)
        # Check if command is enabled
        if not db.checkCog(cogEnum.Moderator):
            await ctx.respond('The command has been disabled.')
            return
        if len(reason) < 5:  # Reason must be provided
            await ctx.respond('A reason must be provided for banning someone!')
            return
        if len(reason) > 512:  # Reason too long
            await ctx.respond('The provided reason is too long! Aborting ban...')
            return

        moderator = ctx.author.guild_permissions.administrator
        if moderator:  # Check moderator flag
            moderator = False  # Reset flag
            # Check user's roles
            moderator = member.guild_permissions.administrator
            if (member.id == self.MASTER) or member.bot:  # Troll person if they try banning the MASTER
                with open('images/EasterEggs/banVideo.mp4', 'rb') as f:
                    await ctx.respond(file=File(f, 'banVideo.mp4'))
            elif member.id == ctx.author.id:  # Failsafe for people banning themselves
                await ctx.respond('You cannot ban yourself!')
            elif moderator:  # Failsafe to keep moderators banning other moderators
                await ctx.respond('You cannot ban a moderator! Ask your server owner for a manual ban!')
            else:
                channel_id = db.getLoggingID(channelType.shame)
                if channel_id != 0:
                    # Broadcast to the entire server who got banned
                    shame = self.client.get_channel(channel_id)
                    await shame.send(
                        '@here <@%d> has been banned for %s. Let this serve as a reminder not to break the rules.' % (
                            member.id, reason))
                # Get custom message from database
                msg = db.getBanMsg()
                # Send a message to the user who got banned
                await member.send(
                    'You have been banned for breaking rule %d. Reason: %s. %s' % (rule_violated, reason, msg))
                await member.ban(reason=reason)  # Ban user
                await ctx.respond('The user has been banned')
        return

    @slash_command(description='Create a poll')
    async def create_poll(
            self,
            ctx,
            for_roles: Option(str, 'Is this poll for roles?', choices = ['yes', 'no']),
            emojis: Option(str, 'Emojis for the poll. There must be spaces between emojis.'),
            arguments: Option(str, 'The arguments for the poll.'),
            custom_msg: Option(str,
                               'The custom message for the poll. If it is a role poll, this parameter will be ignored.',
                               default = '', required = False)
    ):
        # Check if command was ran inside server
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this!')
            return
        # Open database
        db = EE_DB(self.database, ctx.guild.id)
        # Check if command is enabled
        if not db.checkCog(cogEnum.Moderator):
            await ctx.respond('This command has been disabled.')
            return
        # Parse emojis
        emojis = emoji.demojize(emojis)
        emojis = re.sub(r'([:>])([:<])', r'\1 \2', emojis)
        emojis = emojis.split(' ')
        # Remove any duplicate emojis
        emojis = [i for n, i in enumerate(emojis) if i not in emojis[:n]]
        # Check how many emojis there are
        if len(emojis) > 20:
            await ctx.respond('Cannot have more than 20 arguments!')
            return
        # Parse arguments
        Args = next(csv.reader(io.StringIO(arguments), delimiter=' '))
        # Check if there are the same amount of arguments as emojis
        if len(Args) != len(emojis):
            await ctx.respond(f'Error: There are {len(Args)} arguments and {len(emojis)} emojis.')
            return
        moderator = ctx.author.guild_permissions.administrator
        # Check if this is for a role poll
        if for_roles == 'yes' and moderator:
            Roles = []
            roles = ctx.guild.roles
            # Validate the roles
            for x in Args:
                role = discord.utils.get(roles, name=x)
                if role is None:
                    await ctx.respond(f'Error: The {x} role does not exist!')
                    return
                elif role in Roles:
                    await ctx.respond(f'Error: The {x} role is listed 2 or more times!')
                    return
                else:
                    Roles.append(role)
            Poll = []
            match = []
            # Construct poll message
            msg = "React with the following Emojis to assign yourself to roles:\n\n"
            for i in range(0, len(emojis)):
                msg += f'{emoji.emojize(emojis[i])} :   `{Args[i]}`\n\n'
                match.append(Roles[i].id)
                match.append(emojis[i])
                Poll.append(deepcopy(match))
                match.clear()
            channel_id = db.getLoggingID(channelType.role)
            # Send poll message
            if channel_id != 0:
                roleCh = self.client.get_channel(channel_id)
                await ctx.respond('Creating Poll...')
                msg = await roleCh.send(msg)
                for i in emojis:
                    await msg.add_reaction(emoji.emojize(i))
                # Save matches to database
                db.createPoll(msg.id, Poll)
            else:
                await ctx.respond('Error: Please configure your server by using `/configure`')
        elif for_roles == 'no':
            # Construct message
            msg = f'**{ctx.author.name}**\n\n'
            if custom_msg != '':
                msg += f'{custom_msg}\n\n'
            else:
                msg += 'React with the following: \n\n'
            for i in range(0, len(emojis)):
                msg += f'{emoji.emojize(emojis[i])} :   `{Args[i]}`\n\n'
            await ctx.respond('Sending poll...')
            # Post message
            msg = await ctx.channel.send(msg)
            # Add reactions
            for i in emojis:
                await msg.add_reaction(emoji.emojize(i))
        else:
            await ctx.respond('You do not have permission to create a poll for roles!')
        return

    # Command to delete a poll
    @slash_command(description='Delete a poll by providing a url')
    async def delete_poll(self, ctx, msg_url: Option(str, "The url of the poll that you're trying to delete.")):
        # Check if command was ran inside server
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this command!')
            return
        # Open database
        db = EE_DB(self.database, ctx.guild.id)
        # Check if command is enabled
        if not db.checkCog(cogEnum.Moderator):
            await ctx.respond("This command has been disabled.")
            return
        moderator = ctx.author.guild_permissions.administrator
        if not moderator:
            await ctx.respond('You do not have permission.')
            return
        channel_id = db.getLoggingID(channelType.role)
        if channel_id == 0:
            await ctx.respond(
                'ERROR: Unable to find poll channel! Please configure your server first by running `/configure`.')
            return
        # Check for database errors
        if db.getError() == 0:
            roleCh = ctx.guild.get_channel(channel_id)
            msg = await roleCh.fetch_message(int(msg_url.split('/')[-1]))
            if msg is not None:
                # Delete poll from channel
                await ctx.respond('Poll deleted')
                await msg.delete()
            else:
                await ctx.respond('Could not find message. Please ensure that it exists and it is for assigning roles.')

    # Command to fetch a database - MASTER Only
    @slash_command(guild_ids=[originGuild], default_permission=False)
    @permissions.default_permissions(
        administrator=True
    )
    async def fetch_db(self, ctx):
        if ctx.author.id != self.MASTER:
            await ctx.respond('Only the bot owner can run this command!')
        # Acknowledge command
        await ctx.respond('ACK')
        with open(self.database, 'rb') as f:  # Send database in DM
            await ctx.author.send(file=File(f, 'EE_Bot.db'))

    # Command to fetch error log - MASTER only
    @slash_command(guild_ids=[originGuild], default_permission=False)
    @permissions.default_permissions(
        administrator=True
    )
    async def fetch_logs(self, ctx):
        if ctx.author.id != self.MASTER:
            await ctx.respond('Only the bot owner can run this command!')
        # Acknowledge command
        await ctx.respond('ACK')
        with open('./ee_bot.log', 'rb') as f:  # Send log in DM
            await ctx.author.send(file=File(f, 'ee_bot.log'))
        with open('./errors.log', 'rb') as f:  # Send error log in DM
            await ctx.author.send(file=File(f, 'errors.log'))

    @slash_command(description='Edit a poll given the url')
    async def edit_poll(
            self,
            ctx,
            poll_url: Option(str, 'The url of the poll that gets edited'),
            mode: Option(str, 'Add or remove pair?', choices = ['add', 'remove']),
            role: Option(str, 'The role name.'),
            emoji_: Option(str, 'The emoji used for the reaction. Ignored for removal.', name = 'emoji',
                           required = False, default = '')
    ):
        # Check if command was ran inside server
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this command.')
            return
        db = EE_DB(self.database, ctx.guild.id)
        # Check if command is enabled
        if not db.checkCog(cogEnum.Moderator):
            await ctx.respond('This command has been disabled.')
            return
        moderator = ctx.author.guild_permissions.administrator
        # Check for user permission
        if not moderator:
            await ctx.respond('You do not have permission to do this.')
            return
        # Get logging channels
        loggingId = db.getLoggingID(channelType.log)
        roleCh = db.getLoggingID(channelType.role)
        # Check if role exists
        Role = discord.utils.get(ctx.guild.roles, name=role)
        if Role is None:
            await ctx.respond('Given role does not exist')
            return
        # Check if server has been configured
        if loggingId == 0 or roleCh == 0:
            await ctx.respond('Please run `/configure` before using this command.')
            return
        # Check if channel exists
        channel = self.client.get_channel(roleCh)
        if channel is None:
            await ctx.respond('Please run `/configure` before using this command')
            return
        # Check if message is valid
        if not db.checkPoll_id(int(poll_url.split('/')[-1])):
            await ctx.respond('Poll does not exist!')
            return
        # Get the poll pairs
        ID, Pairs = db.retrievePoll(int(poll_url.split('/')[-1]))
        # Check the mode
        if mode == 'add':
            # Check if emoji was passed in
            if not emoji_:
                await ctx.respond('You must add an emoji for the reaction.')
                return
            # Check if poll is at max length
            if len(Pairs) == 20:
                await ctx.respond('Cannot add new pair! There are already 20 pairs.')
                return
            # Double check ID
            if ID != int(poll_url.split('/')[-1]):
                await ctx.respond('An unexpected error occured!')
                return
            # Check for duplicates
            for x in Pairs:
                if x[0] == Role.id or emoji.demojize(emoji_) == x[1]:
                    await ctx.respond('Cannot duplicate role or emoji')
                    return
            # Add a new pair
            db.addNewPair(ID, Role.id, emoji.demojize(emoji_))
            if db.getError() != 0:
                await ctx.respond('There was an unexpected error.')
                return
            # Fetch the message
            msg = await channel.fetch_message(ID)
            # Edit message
            newContent = msg.content + f'\n\n{emoji_} :   `{role}`\n\n'
            await msg.edit(content=newContent)
            # Add new reaction
            await msg.add_reaction(emoji_)
            # Report that operation finished
            await ctx.respond('Done')
        else:
            # Check for minimum length
            if len(Pairs) == 1:
                await ctx.respond('Cannot remove reaction! Must have at least 1 reaction for a poll message.')
                return
            # Double check ID
            if ID != int(poll_url.split('/')[-1]):
                await ctx.respond('An unexpected error occured!')
                return
            error = True
            index = 0
            # Check if role exists in poll
            for i in range(0, len(Pairs)):
                if Role.id == Pairs[i][0]:
                    index = i
                    error = False
                    break
            # Report error if role was not found
            if error:
                await ctx.respond('Please check if the role entered is correct')
                return
            # Delete pair from database
            db.deletePair(ID, Pairs[index][0], Pairs[index][1])
            if db.getError() != 0:
                await ctx.respond('An unexpected error occured!')
                await self.client.get_channel(loggingId).send("Database error during role message edit.")
                return
            # Fetch message and edit it
            msg = await channel.fetch_message(ID)
            content = msg.content
            content = content.replace('\n\n%s :   `%s`' % (emoji.emojize(Pairs[index][1]), role), '')
            await msg.edit(content=content)
            # Remove all reactions to that entry
            await msg.clear_reaction(emoji.emojize(Pairs[index][1]))
            # Report that operation finished
            await ctx.respond('Done')

    # Command that warns a user
    @slash_command(description='Warn a user for inappropriate behavior.')
    async def warn(
            self,
            ctx,
            user: Option(discord.Member, 'The user who is getting warned.'),
            rule_violated: Option(int, 'The rule that was violated. Must be an integer.'),
            reason: Option(str, 'The reason to why they are getting warned.')
    ):
        # Check if valid user was given
        if user.id == 0 or user.bot:
            await ctx.respond('Must provide a valid user.')
            return
        # Check if command was ran inside server
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this command!')
            return
        # Open database
        db = EE_DB(self.database, ctx.guild.id)
        # Check if command is enabled
        if not db.checkCog(cogEnum.Moderator):
            await ctx.respond('This command has been disabled.')
            return

        moderator = ctx.author.guild_permissions.administrator
        if moderator:
            # Check if reason was provided
            if len(reason) < 5:
                await ctx.respond('A reason must be provided! (Must excede 5 characters)')
                return
            # Get number of warnings
            warnings = db.addWarning(user.id)
            if db.getError() == 0:
                # Log the warning in the database for records
                db.logWarning(ctx.guild.id, ctx.channel.name, user.id, user.name, ctx.author.id, ctx.author.name,
                              rule_violated, reason)
                # Notify user of warning or ban
                if warnings > 5:
                    await self.ban(self, ctx, user, rule_violated, reason)
                else:
                    # Send warning message to user
                    if warnings == 5:
                        await user.send(
                            'You have received warning #%d for %s. This is your last warning before a ban' % (
                                warnings, reason))
                    else:
                        await user.send('You have received warning #%d for %s. You have %d warnings left.' % (
                            warnings, reason, (5 - warnings)))
                    # Respond to command
                    await ctx.respond('<@%d> has been warned for %s' % (user.id, reason))
                    # Calculate time for timeout
                    timeout = (193.54 * (warnings ** 4)) - (1721.2 * (warnings ** 3)) + (5511.5 * (warnings ** 2)) - (
                            7383.7 * warnings) + 3405
                    timeout = timeout // 5 * 5
                    # Remove extra 5 minutes if final warning
                    if warnings == 5:
                        timeout -= 5
                    # Set timeout
                    await user.timeout_for(timedelta(minutes=timeout))
                    rules = db.getPoliRules()
                    # Check if the rule violated is part of the politics rules
                    if rule_violated in rules:
                        # Ban the user from the politics channels
                        db.updatePoliBan(user.id, True)
                        if db.getError() != 0:
                            cID = db.getLoggingID(channelType.log)
                            if cID != 0:
                                channel = self.client.get_channel(cID)
                                await channel.send('An error occurred with the database')
                            return
                        # Remove politics role from user
                        role = discord.utils.find(lambda r: r.name == 'politics', user.roles)
                        if role is not None:
                            await user.remove_roles(role)
            del db
        return

        # Command that removes a warning from a user

    @slash_command(description='Remove 1 warning from a user')
    async def remove_warning(
            self,
            ctx,
            user: Option(discord.User, 'User who gets a warning removed.')
    ):
        # Check if valid user
        if user.id == 0 or user.bot:
            await ctx.respond('Cannot remove warning from an invalid user.')
            return
        # Check if command was ran inside server
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this command!')
            return
        # Open database
        db = EE_DB(self.database, ctx.guild.id)
        # Check if command is enabled
        if not db.checkCog(cogEnum.Moderator):
            await ctx.respond('This command has been disabled')
            return
        # Check permission
        moderator = ctx.author.guild_permissions.administrator
        if not moderator:
            await ctx.respond('You do not have permission to do this!')
            return
        # Get current number of warnings and remove a warning if greater than 0
        warnings = db.retrieveWarnings(user.id)
        if warnings > 0 and db.getError() == 0:
            warnings = db.removeWarning(user.id)
        # Acknowledge warning removal
        if db.getError() == 0:
            await ctx.respond("%s now has %d warnings" % (user.name, warnings))
        del db

        # Command that clears warnings from user

    @slash_command(description='Remove all warnings from a user.')
    async def clear_warnings(
            self,
            ctx,
            user: Option(discord.User, 'The user who gets all their warnings removed')
    ):
        # Check if valid user
        if user.id == 0 or user.bot:
            await ctx.respond('Please enter a valid user.')
            return
        # Check if command was ran inside server
        if ctx.guild is None:
            await ctx.send('You must be in a server to run this command!')
            return
        # Open database
        db = EE_DB(self.database, ctx.guild.id)
        # Check if command is enabled
        if not db.checkCog(cogEnum.Moderator):
            await ctx.respond('This command has been disabled.')
            return
        # Checks if the author is a moderator
        moderator = ctx.author.guild_permissions.administrator
        if not moderator:
            await ctx.repsond('You do not have permission to do this!')
            return
        # Clear the warnings
        db.clearWarnings(user.id)
        if db.getError() == 0:
            await ctx.respond('Cleared warnings for %s' % user.name)
        else:
            await ctx.respond('A database error occurred')

    # Function that runs clear roles command as a task
    async def runClearRoles(self, ctx, role: discord.Role):
        # Check for permission
        moderator = ctx.author.guild_permissions.administrator
        if moderator:
            # Get members
            members = ctx.guild.members
            db = EE_DB(self.database, ctx.guild.id)
            logID = db.getLoggingID(channelType.log)
            log = self.client.get_channel(logID)
            # Check if logging has been configured
            if log is not None:
                await log.send('Starting class role purge.')
            else:
                await ctx.channel.send('Aborting role purge. Please configure your Server by running `/configure`.')
                db.setPurgeRoleFlag(False)
                return

            # Check if role was given
            if role is not None:
                members = role.members
                # Remove given role from all members
                for member in members:
                    await member.remove_roles(role)
            else:
                members = ctx.guild.members
                # Purge all roles whose names are numeric from all members
                for member in members:
                    roles = member.roles
                    roles = tuple([x for x in roles if str(x).isdigit()])
                    if roles:
                        await member.remove_roles(*roles, atomic=True)
            # Update purge roles flag
            db.setPurgeRoleFlag(False)
            await log.send('Finished class role purge.')

    # Command that clears all the class roles from everyone
    @slash_command(description='Clear class roles from everyone')
    async def clear_roles(
            self,
            ctx,
            role: Option(discord.Role, 'Specific role you want to clear', default = None, required = False)
    ):
        # Check if command was ran inside server
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this command!')
            return
        db = EE_DB(self.database, ctx.guild.id)
        # Check if command is enabled
        if not db.checkCog(cogEnum.Moderator):
            await ctx.respond('This command has been disabled.')
            return
        # Get purge roles flag
        purgeRoles = db.getPurgeRolesFlag()
        if db.getError() != 0:
            await ctx.respond('An error occurred!')
            return
        # Check flag
        if not purgeRoles:
            # Run clear roles
            db.setPurgeRoleFlag(True)
            asyncio.create_task(self.runClearRoles(ctx, role))
            await ctx.respond('Purge roles task created.')
        else:
            # Send error message if still purging
            await ctx.respond('Please wait until current role purge is finished.')

    # Command that deletes class channels and their roles
    @slash_command(description='Delete a class channel')
    async def delete_channel(
            self,
            ctx,
            channel: Option(discord.TextChannel, 'The channel that will get deleted')
    ):
        # Check if command was ran inside server
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this command!')
            return
        db = EE_DB(self.database, ctx.guild.id)
        prefix = db.getChannelPrefix()
        # Check length of string
        if (len(channel.name.replace(prefix, '')) != 3) or not channel.name.replace(prefix, '').isdigit():
            # Send error message and exit
            await ctx.respond('Invalid input. Expecting a three digit number for the input.')
            return
        # Check if command is enabled
        if not db.checkCog(cogEnum.Moderator):
            await ctx.respond('This command has been disabled.')
            return
        # Log any errors
        if db.getError() != 0:
            channel_id = db.getLoggingID(channelType.log)
            if channel_id != 0:
                await self.client.get_channel(channel_id).send("Database fault. Unable to add " + channel)
            await ctx.respond("Database fault!")
            return

        # Checks if the author is a moderator
        moderator = ctx.author.guild_permissions.administrator
        # Delete channel from db
        if moderator:
            db.deleteChannel(channel.name.replace(prefix, ''))
        else:
            await ctx.respond('You do not have permission to do this!')
            return
        if db.getError() != 0:
            await ctx.respond('A problem occurred! Please reboot and check logs.')
            return

        # Get channels and roles
        text_channels = ctx.guild.text_channels
        voice_channels = ctx.guild.voice_channels
        roles = ctx.guild.roles
        CHANNEL = channel.name.lower()

        # Find text channel
        for i in range(0, len(text_channels)):
            if CHANNEL == text_channels[i].name:
                CHANNEL = text_channels[i]
                break

        # Delete text channel
        await CHANNEL.delete()

        # Upper alpha characters

        CHANNEL = channel.name.upper()

        # Find voice channel
        for i in range(0, len(voice_channels)):
            if CHANNEL == voice_channels[i].name:
                CHANNEL = voice_channels[i]
                break

        # Delete voice channel
        await CHANNEL.delete()

        channel = channel.name.replace(prefix, '')

        # Find role
        for i in range(0, len(roles)):
            if channel == roles[i].name:
                CHANNEL = roles[i]
                break

        # Delete role
        await CHANNEL.delete()

        await ctx.respond('Channel has been deleted.')

        # Delete database
        del db

        # Command that enables/disables monitoring of a user

    @slash_command(description='Please refer to the wiki.')
    async def watch(
            self,
            ctx,
            user: Option(discord.Member, 'The user to watch'),
            status: Option(str, 'On or Off', choices = ['on', 'off'])
    ):
        # Check if valid user
        if user.bot:
            await ctx.respond('Cannot watch a bot')
            return
        # Check if command was ran inside server
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this command!')
            return

        # Check for permission
        moderator = ctx.author.guild_permissions.administrator
        if not moderator:
            await ctx.respond('You do not have permission to do this.')
            return

        db = EE_DB(self.database, ctx.guild.id)
        # Check if command is enabled
        if not db.checkCog(cogEnum.Moderator):
            await ctx.respond('This command has been disabled.')
            return
        # Check if status should be turned on or off and update the watch flag
        if status == 'on':
            db.insertIntoWatchlist(user.id)
            if db.getError() == 0:
                db.updateMonitor(user.id, True)
        elif status == 'off':
            db.insertIntoWatchlist(user.id)
            if db.getError() == 0:
                db.updateMonitor(user.id, False)
        # Check for errors
        if db.getError() != 0:
            cID = db.getLoggingID(channelType.log)
            if cID != 0:
                channel = self.client.get_channel(cID)
                await channel.send('An error occured when setting up monitoring')
            await ctx.respond('An error occurred!')
        else:
            # Update the override flag
            db.updateOverride(user.id, True)
            # Respond
            if db.getError() != 0:
                cID = db.getLoggingID(channelType.log)
                if cID != 0:
                    channel = self.client.get_channel(cID)
                    await channel.send('An error occured when setting up monitoring')
                await ctx.respond('An error occurred')
            else:
                cID = db.getLoggingID(channelType.log)
                if cID != 0:
                    channel = self.client.get_channel(cID)
                    await channel.send('Monitoring was set up for %s' % user.name)
                await ctx.respond('Status updated.')

    # Command that undoes the override after invoking the watch command
    @slash_command(description='See wiki')
    async def disable_watchlist_override(
            self,
            ctx,
            user: Option(discord.Member, 'This is self-explanatory')
    ):
        # Check if user is valid
        if user.bot:
            await ctx.respond('Cannot update flags for bot')
            return
        # Check if command was ran inside server
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this command!')
            return

        # Check for permission
        moderator = ctx.author.guild_permissions.administrator
        if not moderator:
            await ctx.respond('You do not have permission to do this!')
            return

        db = EE_DB(self.database, ctx.guild.id)
        # Check if command is enabled
        if not db.checkCog(cogEnum.Moderator):
            await ctx.respond('This command has been disabled')
            return

        # Check if user is in watchlist
        if db.inWatchlist(user.id):
            if db.getError() == 0:
                # Undo override
                db.updateOverride(user.id, False)

        # Respond
        if db.getError() != 0:
            cID = db.getLoggingID(channelType.log)
            if cID != 0:
                channel = self.client.get_channel(cID)
                await channel.send('An error occured with the database')
            await ctx.respond('An error occurred')
        else:
            await ctx.respond('Override disabled')

    # Command that lifts a political channel ban
    # noinspection SpellCheckingInspection
    @slash_command(description='See wiki')
    async def lift_poli_ban(
            self,
            ctx,
            user: Option(discord.Member, 'The member to ban from politics chat')
    ):
        # Check if valid user
        if user.bot:
            await ctx.respond('Cannot update flags for bot accounts.')
            return
        # Check if command was ran inside server
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this command!')
            return

        # Check for permission
        moderator = ctx.author.guild_permissions.administrator
        if not moderator:
            await ctx.respond('You do not have permission to do this!')
            return

        db = EE_DB(self.database, ctx.guild.id)
        # Check if command is enabled
        if not db.checkCog(cogEnum.Moderator):
            await ctx.respond('This command has been disabled.')
            return

        # Check if in watchlist
        if db.inWatchlist(user.id):
            if db.getError() == 0:
                # Update flag
                db.updatePoliBan(user.id, False)

        # Respond
        if db.getError() != 0:
            cID = db.getLoggingID(channelType.log)
            if cID != 0:
                channel = self.client.get_channel(cID)
                await channel.send('An error occured with the database')
            await ctx.respond('An error occurred')
        else:
            await ctx.respond('User flag updated')

    # TODO: Move this command into server configs
    @slash_command(description='Set class prefix for class specific channels.')
    async def set_channel_prefix(
            self,
            ctx,
            prefix: Option(str, 'Cannot exceed 5 letters')
    ):
        if prefix == '':
            await ctx.respond('Prefix cannot be empty')
            return
        # Check if command was ran inside server
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this command!')
            return
        if (0 >= len(prefix) > 5) or not prefix.isalpha():
            await ctx.respond('The prefix needs to be at least 1 character long and at most 5 characters long. The '
                              'prefix also must be alphabetical.')
            return
        # Checks if the author is a moderator
        moderator = ctx.author.guild_permissions.administrator

        # Open database
        db = EE_DB(self.database, ctx.guild.id)
        # Check if command is enabled
        if not db.checkCog(cogEnum.Moderator):
            await ctx.respond('This command has been disabled.')
            return

        # Respond
        if moderator:
            db.setChannelPrefix(prefix)
            cID = db.getLoggingID(channelType.log)
            if cID == 0:
                await ctx.channel.send('Please run `/configure` first.')
                await ctx.respond('Bad response: Check log channel.')
                return
            if db.getError() != 0:
                await self.client.get_channel(cID).send("Database fault. Unable to set channel prefix.")
                await ctx.respond('Bad response: Check log channel.')
            else:
                cID = db.getLoggingID(channelType.log)
                await self.client.get_channel(cID).send("Set the channel prefix to " + prefix)
                await ctx.respond('Prefix set')

    # TODO: Move this command into server configs
    @slash_command(description='Resets prefix back to default prefix: EE')
    async def clear_channel_prefix(self, ctx):
        # Check if command was ran inside server
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this command!')
            return
        # Checks if the author is a moderator
        moderator = ctx.author.guild_permissions.administrator

        # Open database
        db = EE_DB(self.database, ctx.guild.id)
        # Check if command is enabled
        if not db.checkCog(cogEnum.Moderator):
            await ctx.respond('This command has been disabled.')
            return

        # Respond
        if moderator:
            db.clearChannelPrefix()
            cID = db.getLoggingID(channelType.log)
            if cID == 0:
                await ctx.channel.send('Please run `/configure` first.')
                await ctx.respond('An error occurred.')
                return
            if db.getError() != 0:
                await self.client.get_channel(cID).send("Database fault. Unable to clear channel prefix.")
                await ctx.respond('An error occurred')
            else:
                await self.client.get_channel(cID).send("Cleared Channel Prefix")
                await ctx.respond('Prefix reset back to default')

    # Command that fetches the warning data and sends it as a csv
    @slash_command(description='Outputs csv file of warning data.')
    async def fetch_warnings(
            self,
            ctx,
            args: Option(str, 'The arguments provided. See /help for more info.', required = False, default = '')
    ):
        # Check if command was ran inside server
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this command!')
            return
        # Open database
        db = EE_DB(self.database, ctx.guild.id)
        # Check if command is enabled
        if not db.checkCog(cogEnum.Moderator):
            await ctx.respond('This command has been disabled')
            return
        # Parse the arguments
        args = tuple(filter(lambda a: a != '', args.split(' ')))
        search = searchWarnings.NONE
        values = []
        # Check the flags
        if len(args) == 0:
            search = search | searchWarnings.ALL
        else:
            if '--guild' in args and ctx.author.id == self.MASTER:
                values.append(args[args.index('--guild') + 1])
                search = search | searchWarnings.GUILD
            if '--channel' in args:
                values.append(args[args.index('--channel') + 1])
                search = search | searchWarnings.CHANNEL
            if '--userid' in args:
                values.append(args[args.index('--userid') + 1])
                search = search | searchWarnings.USER_ID
            if '--username' in args:
                values.append(args[args.index('--username') + 1])
                search = search | searchWarnings.USER_NAME
            if '--adminid' in args:
                values.append(args[args.index('--adminid') + 1])
                search = search | searchWarnings.ADMIN_ID
            if '--adminname' in args:
                values.append(args[args.index('--adminname') + 1])
                search = search | searchWarnings.ADMIN_NAME
            if '--rule' in args:
                values.append(str(args[args.index('--rule') + 1]))
                search = search | searchWarnings.RULE
            if '--reason' in args:
                values.append(args[args.index('--reason') + 1])
                search = search | searchWarnings.REASON
            if '--date' in args:
                values.append(args[args.index('--date') + 1])
                search = search | searchWarnings.DATE
        # Check permissions
        if ctx.author.guild_permissions.administrator:
            # Respond
            await ctx.respond('Fetching data. This may take a while.')
            # Fetch data
            serverData = db.getWarningData(search, ctx.guild.id, ctx.author.id == self.MASTER, values)
            # Put data into CSV file and send it
            buffer = io.StringIO()
            writer = csv.writer(buffer)
            writer.writerow(self.csvHeader)
            writer.writerows(serverData)
            buffer.seek(0)
            await ctx.channel.send(file=File(buffer, '%s-warning-data.csv' % ctx.guild.name))
        else:
            await ctx.respond('You do not have permission to access this data.')
        return

    # Command that demotes a user
    @slash_command(description='Demote a member')
    async def demote(
            self,
            ctx,
            user: Option(discord.User, 'User who gets demoted.'),
            levels: Option(int, 'The number of levels they get demoted by.'),
            reason: Option(str, 'The reason why they are getting demoted.')
    ):
        # Check if command was ran inside server
        if ctx.guild is None:
            await ctx.respond('You must be in a server to run this command!')
            return
        db = EE_DB(self.database, ctx.guild.id)
        # Check if command is enabled
        if not db.checkCog(cogEnum.Moderator):
            await ctx.respond('This command has been disabled.')
            return
        # Check permissions
        if not ctx.author.guild_permissions.administrator:
            await ctx.respond('You do not have permission to do this.')
            return
        # Validate user
        if user.bot:
            await ctx.respond('Cannot demote a bot.')
            return
        # Validate input
        if levels < 0:
            await ctx.respond('The number of levels must be 0 or greater!')
            return
        # Demote the member
        db.demoteMember(user.id, levels)
        # Respond
        if db.getError() == 0:
            level = db.checkUserRank(user.id)
            await user.send('You have been demoted to level %d in %s for %s.' % (level[0], ctx.guild.name, reason))
            await ctx.respond(f'{user.name} has been demoted by {levels}.')
        else:
            await ctx.respond('There was a database error.')
        return
