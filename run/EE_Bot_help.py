# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import discord
from YosysCommands import YS
from EE_Bot_DB import EE_DB
from EE_Bot_DB import cogEnum
from RFCommands import rfExists
from globals import embedColor as color


COMMANDS = [
    # Category
    "general",
    "moderator",
    "games",
    "hardware",
    "yosys",
    "serverConfig",
    "rf",
    "schedule",
    "power",

    #Uncategorized commands
    "kill",
    "reboot",
    "help",

    #General commands
    "status",
    "version",
    "rank",
    "notif",
    "compile",
    "leaderboard",
    "echo",
    "kicost",
    "mils_mm",

    #Moderator commands
    "createchannel",
    "clear",
    "clearchannel",
    "assignrole",
    "unassignrole",
    "archive",
    "ban",
    "create_poll",
    "delete_poll",
    "fetch_db",
    "fetch_logs",
    "edit_poll",
    "warn",
    "remove_warning",
    "clear_warnings",
    "clear_roles",
    "delete_channel",
    "watch",
    "disable_watchlist_override",
    "lift_poli_ban",
    "set_channel_prefix",
    "clear_channel_prefix",
    "fetch_warnings",
    "demote",

    #Games commands
    "quiz",
    "answer",
    "quit",
    "game",

    #Hardware commands
    "temp",
    "cool",
    "cmd",

    #Yosys
    "pre_synth",
    "synth",
    "gen_spice",
    "gen_json",

    #serverConfig commands
    "configure",
    "config_test",
    "set_poli_msg",
    "clear_poli_msg",
    "set_poli_rules",
    "clear_poli_rules",
    "level_system",
    "check_level_system",
    "enable_cog",
    "disable_cog",
    "check_cog_status",
    "set_ban_msg",
    "clear_ban_msg",
    "set_color",
    "role_color",
    "clear_color",

    #RF commands
    "dbm",
    "dms_to_deg",
    "deg_to_dms",
    "rayrand",
    "diffraction_gain",
    "erlangs",
    "erlangs_table",
    "exposure_radius",
    "cluster_sizes",
    "q",
    "qinv",

    #Schedule commands
    "create_task",
    "view_tasks",
    "remove_task",
    "clear_tasks",
    "schedule_tasks",
    "cancel_tasks",
    "schedule_announcement",
    "cancel_announcement",

    #Power commands
    "powercmd",
    "line_phase",
    "reactance"
]


#Function for autocompleting commands
async def get_commands(ctx: discord.AutocompleteContext):
    return [cmd for cmd in COMMANDS if cmd.startswith(ctx.value.lower())]


#Help command
async def help(ctx, avatar: str, categoryOrCmd: str, database: str):
    owner = False
    if ctx.guild is None:
        db = EE_DB(database, -1)
    else:
        db = EE_DB(database, ctx.guild.id)
        owner = (ctx.guild.owner_id == ctx.author.id)
    if len(categoryOrCmd) != 0:
        # Categories
        if categoryOrCmd == 'general':
            embed=discord.Embed(title="General", description="These are the general commands and are available for all users to use.", color=color)
            embed.add_field(name="/status", value="Displays status of bot. See `/help status` for more information.", inline=False)
            embed.add_field(name='/version', value="Displays the version of the bot. See `/help version` for more information.", inline=False)
            embed.add_field(name="/rank", value="Displays level and progress. See `/help rank` for more information.", inline=False)
            embed.add_field(name="/notif", value="Toggle on and off level up notifications. See `/help notif` for more information.", inline=False)
            embed.add_field(name="/compile", value="Compiles a single C or C++ file into assembly code. See `/help compile` for more information.", inline=False)
            embed.add_field(name="/echo", value="I don't know why you would want to run this command. See `/help echo` for more information.", inline=False)
            embed.add_field(name="/kicost", value="Processes BOM XML files from KiCAD, Altium, Proteus, Eagle, Upverter and hand made CSVs to generate a part cost spreadsheet. See `/help kicost` for more information.", inline=False)
            embed.add_field(name='/leaderboard', value="Display rank leaderboard for the server. See `/help leaderboard` for more information.", inline=False)
            embed.add_field(name="/radix", value="Converts a number with a given base to another base. See `/help radix` for more information.", inline=False)
            embed.add_field(name="/mils_mm", value="Converts between mils and mm. See `/help mils_mm` for more information.", inline=False)
        elif categoryOrCmd == 'moderator' and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="Moderator", description="These are the moderator/admin commands. The commands listed are the visble commands.", color=color)
            embed.add_field(name="/createchannel", value="This command creates a new class channel. See `/help createchannel` for more information.", inline=False)
            embed.add_field(name="/clear", value="This command deletes a given amount of messages from a channel. See `/help clear` for more information.", inline=False)
            embed.add_field(name="/assignrole", value="(DEPRECATED) This command assigns a given role to the user who used the command. See `/help assignrole` for more information.", inline=False)
            embed.add_field(name="/unassignrole", value="(DEPRECATED) This command unassigns a given role to the user who used the command. See `/help unassignrole` for more information.", inline=False)
            embed.add_field(name="/archive", value="This command archives messages that are not commands or bot messages. See `/help archive` for more information.", inline=False)
            embed.add_field(name="/create_poll", value="This command creates a poll. See `/help create_poll` for more information", inline=False)
            embed.add_field(name="/warn", value="This command warns a user. See `/help warn` for more information.", inline=False)
            embed.add_field(name="/remove_warning", value="This command removes a warning from a user. See `/help remove_warning` for more information.", inline=False)
            embed.add_field(name="/clear_warnings", value="This command clears all the warnings from a user. See `/help clear_warnings` for more information", inline=False)
            if ctx.guild is not None and ctx.author.guild_permissions.administrator:
                embed1=discord.Embed(title="Moderator", description="These are the moderator/admin commands. The commands listed are the hidden commands.", color=color)
                embed1.set_thumbnail(url=avatar)
                embed1.add_field(name="/clearchannel", value="This command deletes all the messages in a channel. See `/help clearchannel` for more information.", inline=False)
                embed1.add_field(name="/ban", value="This command bans a user from the server. See `/help ban` for more information.", inline=False)
                embed1.add_field(name="/fetch_warnings", value="This command returns warning records from the database. See `/help fetch_warnings` for more information.", inline=False)
                embed1.add_field(name="/delete_poll", value="This command deletes a role pole. See `/help delete_poll` for more information.", inline=False)
                embed1.add_field(name="/fetch_db", value="This command fetches the database. See `/help fetch_db` for more information.", inline=False)
                embed1.add_field(name="/fetch_logs", value="This command fetches the logs. See `/help fetch_logs` for more information.", inline=False)
                embed1.add_field(name="/edit_poll", value="This command edits role polls. See `/help edit_poll` for more information.", inline=False)
                embed1.add_field(name="/clear_roles", value="This command clears the class roles from everyone in the server. See `/help clear_roles` for more information.", inline=False)
                embed1.add_field(name="/delete_channel", value="This command deletes a channel. See `/help delete_channel` for more information.", inline=False)
                embed1.add_field(name="/watch", value="This command adds monitoring on a given user. See `/help watch` for more information.", inline=False)
                embed1.add_field(name="/disable_watchlist_override", value="This command disables the override flag for a given user. See `/help disable_watchlist_override` for more information.", inline=False)
                embed1.add_field(name="/lift_poli_ban", value="This command lifts a political channel ban on a given user. See `/help lift_poli_ban` for more information.", inline=False)
                embed1.add_field(name="/set_channel_prefix", value="This command overrides the default class prefix for a server. See `/help set_channel_prefix` for more information.", inline=False)
                embed1.add_field(name="/clear_channel_prefix", value="This command resets class channel prefix back to the default class prefix for a server. See `/help clear_channel_prefix` for more information.", inline=False)
                embed1.add_field(name="/demote", value="This command takes away xp from a user. See `/help demote` for more information.", inline=False)
                await ctx.respond(embed=embed1)
        elif categoryOrCmd == 'games' and db.checkCog(cogEnum.Games):
            embed=discord.Embed(title="Games", description="These are the game commands. The quiz game is the only game available at the moment.", color=color)
            embed.add_field(name="/quiz", value="This command starts a new quiz. See `/help quiz` for more details", inline=False)
            embed.add_field(name="/answer", value="This command submits an answer for the ongoing game. See `/help answer` for more information.", inline=False)
            embed.add_field(name="/quit", value="This command quits the ongoing game. See `/help quit` for more information.", inline=False)
            embed.add_field(name="/game", value="This command returns the current status of the game. See `/help game` for more information.", inline=False)
        elif categoryOrCmd == 'hardware' and db.checkCog(cogEnum.Hardware):
            embed=discord.Embed(title="Hardware", description="These are the hardware commands. Most of these commands are not functional at the moment.", color=color)
            embed.add_field(name="/temp", value="This command gets the temperature of the CPU. See `/help temp` for more information.", inline=False)
            embed.add_field(name="/cool", value="Not functional. See `/help cool` for more information.", inline=False)
            embed.add_field(name="/cmd", value="Allows Linux terminal commands to be ran from discord. See `/help cmd` for more information.", inline=False)
        elif categoryOrCmd == 'yosys' and YS and db.checkCog(cogEnum.Yosys):
            embed=discord.Embed(title="Yosys", url="http://www.clifford.at/yosys/", description="These commands use the Yosys library to evaluate Verilog HDL designs. Note that these commands accept 1 Verilog file at a time. VHDL is not functional at the moment.", color=color)
            embed.add_field(name="/pre_synth", value="This command runs RTL analysis and returns a picture of what the library sees. See `/help pre_synth` for more information.", inline=False)
            embed.add_field(name="/synth", value="This command runs generic synthesis on your design. See `/help synth` for more information.", inline=False)
            embed.add_field(name="/gen_spice", value="This command generates a spice file from your design. See `/help gen_spice` for more information.", inline=False)
            embed.add_field(name="/gen_json", value="This command generates a json file from your design. See `/help gen_json` for more information.", inline=False)
        elif categoryOrCmd == 'serverConfig' and owner:
            embed=discord.Embed(title="serverConfig", description="These are the server configuration commands. These commands are used for configuring the server and can only be used by the owner.", color=color)
            embed.add_field(name="/configure", value="This command configures the bot for logging channels. See `/help configure` for more information.", inline=False)
            embed.add_field(name="/config_test", value="This command checks the logging channels. See `/help config_test` for more information.", inline=False)
            embed.add_field(name="/set_poli_msg", value="This command links the bot to the political rules message. See `/help set_poli_msg` for more information.", inline=False)
            embed.add_field(name="/clear_poli_msg", value="This command unlinks the bot from the political channel rules message. See `/help clear_poli_msg` for more information.", inline=False)
            embed.add_field(name="/set_poli_rules", value="This command links the bot to certain rules that would get someone banned from the political channels. See `/help set_poli_rules` for more information.", inline=False)
            embed.add_field(name="/clear_poli_rules", value="This command unlinks the bot to certain rules that would get someone banned from the political channels. See `/help clear_poli_rules` for more information.", inline=False)
            embed.add_field(name="/level_system", value="Gives the owner the ability to turn on and off the level system for the server. See `/help level_system` for more information.", inline=False)
            embed.add_field(name="/check_level_system", value="Checks if the level system is on or off for the server. See `/help check_level_system` for more information.", inline=False)
            embed.add_field(name="/enable_cog", value="This command enables a given category. See `/help enable_cog` for more information.", inline=False)
            embed.add_field(name="/disable_cog", value="This command disables a given category. See `/help disable_cog` for more information.", inline=False)
            embed.add_field(name="/check_cog_status", value="This command shows if the command category is enabled or disabled. See `/help check_cog_status` for more information.", inline=False)
            embed.add_field(name="/set_ban_msg", value="This command sets a custom ban dm message. See `/help set_ban_msg` for more information.", inline=False)
            embed.add_field(name="/clear_ban_msg", value="This command clears the custom ban dm message. See `/help clear_ban_msg` for more information.", inline=False)
            embed.add_field(name="/set_color", value="Set the new class role color for the server. See `/help set_color` for more information.", inline=False)
            embed.add_field(name='/role_color', value="View the current class role color for the server. See `/help role_color` for more information", inline=False)
            embed.add_field(name="/clear_color", value="Reset the class role color to the bot's default color. See `/help clear_color` for more information.", inline=False)
        elif categoryOrCmd == 'rf' and db.checkCog(cogEnum.RF):
            embed=discord.Embed(title="RF", description="These are commands that perform common RF calculations.", color=color)
            embed.add_field(name="/dbm", value="Converts power from Watts to dbm. See `/help dbm` for more information.", inline=False)
            embed.add_field(name="/dms_to_deg", value="Converts degrees, minutes, seconds to degrees. See `/help dms_to_deg` for more information.", inline=False)
            embed.add_field(name="/deg_to_dms", value="Converts degrees to degrees, minutes, seconds. See `/help deg_to_dms` for more information.", inline=False)
            embed.add_field(name="/rayrand", value="This command generates a Rayleigh Random Variable. See `/help rayrand` for more information.", inline=False)
            embed.add_field(name="/diffraction_gain", value="This command calculates the diffraction gain. See `/help diffraction_gain` for more information.", inline=False)
            embed.add_field(name="/erlangs", value="This command calculates the number of erlangs. See `/help erlangs` for more information.", inline=False)
            embed.add_field(name="/erlangs_table", value="This command generates a table of erlang values. See `/help erlangs_table` for more information.", inline=False)
            embed.add_field(name="/exposure_radius", value="This command calculates the minimum exposure radii for controlled and uncontrolled environments according to FCC regulations. See `/help exposure_radius` for more information.", inline=False)
            embed.add_field(name="/cluster_sizes", value="This command generates a table of possible cluster sizes. See `/help cluster_sizes` for more information.", inline=False)
            embed.add_field(name="/q", value="Calculates the Q function. See `/help q` for more information.", inline=False)
            embed.add_field(name="/qinv", value="Approximates the inverse Q function. See `/help qinv` for more information.", inline=False)
        elif categoryOrCmd == 'schedule' and db.checkCog(cogEnum.Schedule):
            embed=discord.Embed(title="Schedule", description="These are commands that schedule server tasks to be executed for later.", color=color)
            embed.add_field(name="/create_task", value="Creates a task and adds it to the task queue. See `/help create_task` for more information.", inline=False)
            embed.add_field(name="/view_tasks", value="Displays the contents of the task queue. See `/help view_tasks` for more information.", inline=False)
            embed.add_field(name="/remove_task", value="Removes a task from the task queue. See `/help remove_task` for more information.", inline=False)
            embed.add_field(name="/clear_tasks", value="Clears the task queue. See `/help clear_tasks` for more information.", inline=False)
            embed.add_field(name="/schedules_tasks", value="Schedules the task queue for execution. See `/help schedule_tasks` for more information.", inline=False)
            embed.add_field(name="/cancel_tasks", value="Cancels the scheduled tasks. See `/help cancel_tasks` for more information.", inline=False)
            embed.add_field(name="/schedule_announcement", value="Schedules an announcement within a 24-hour period. See `/help schedule_announcement` for more information.", inline=False)
            embed.add_field(name="/cancel_announcement", value="Cancels a scheduled announcement. See `help cancel_announcement` for more information.", inline=False)
        elif categoryOrCmd == 'power' and db.checkCog(cogEnum.Power):
            embed=discord.Embed(title="Power", description='These are commands that perform power system related calculations.', color=color)
            embed.add_field(name='/power', value="Calculates the power consumption of a circuit given various parameters. See `/help powercmd` for more information.", inline=False)
            embed.add_field(name='/line_phase', value='Performs line phase calculations. See `/help line_phase` for more information.', inline=False)
            embed.add_field(name='/reactance', value='Calculates the reactance of a circuit. See `/help reactance` for more information.', inline=False)
        # Commands 
        elif ctx.guild is not None and categoryOrCmd == 'kill':
            embed=discord.Embed(title="/kill", description="This command stops the execution of the bot. It will also update the code from the remote repository.\n\n**Arguments**: None", color=color)
        elif ctx.guild is not None and categoryOrCmd == 'reboot' and ctx.author.guild_permissions.administrator:
            embed=discord.Embed(title="/reboot", description="This command stops the execution and restarts the bot. It will also update the code from the remote repository.\n\n**Arguments**: None", color=color)
        elif categoryOrCmd == 'status':
            embed=discord.Embed(title="/status", description="This command checks the status of the bot. It is primarly used to check if the bot is running.\n\n**Arguments**: None", color=color)
        elif categoryOrCmd == 'version':
            embed=discord.Embed(title="/version", description="This command makes the bot respond with the version of the bot and the bot's database code.\n\n**Arguments**: None", color=color)
        elif categoryOrCmd == 'rank':
            embed=discord.Embed(title="/rank", description="This command gets the current rank or level and the progress towards the next level or rank of the user who used this command.\n\n**Arguments**: None", color=color)
        elif categoryOrCmd == 'notif':
            embed=discord.Embed(title="\notif", description="This command can turn on or off the level up notifications for a user.\n\n**Arguments**:", color=color)
            embed.add_field(name="Notifications", value="**on**: Turns on level notifications\n**off**: Turns off level notifications", inline=False)
        elif categoryOrCmd == 'compile':
            embed=discord.Embed(title="/compile", description='This command compiles a given C or C++ file into assembly code.\n\n**Arguments**:', url="https://gitlab.com/ee_bot-developers/ee_bot/-/wikis/Compiler%20Options", color=color)
            embed.add_field(name="source_file", value="The C/C++ file. This file must be a .c or .cpp file.", inline=False)
            embed.add_field(name="architecture", value="The target architecture. If this parameter is left blank, the command will use the host machine's compiler.", inline=False)
            embed.add_field(name="endianness", value="The endianness of the target. If not given, the command will default to little endian.", inline=False)
            embed.add_field(name="flags", value="Add compiler flags for compilation.", inline=False)
            embed.set_footer(text="For this command to work, 1 file, and 1 file only must be passed in.")
        elif categoryOrCmd == 'echo':
            embed=discord.Embed(title="/echo", description="Really, why are you trying to figure out what this command does? I thought it would be obvious by now...", color=color)
        elif categoryOrCmd == 'createchannel' and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="/createchannel", description="This command creates a new channel in the class category. In order for a user to use this command, they must have admin privelages or they must be at least level 10.\n\n**Arguments**:", color=color)
            embed.add_field(name="Class Code", value="This must be a 3-digit number.", inline=False)
        elif categoryOrCmd == 'clear':
            embed=discord.Embed(title='/clear', description='This command clears a certain amount of messages from a channel.\n\n**Arguments**:', color=color)
            embed.add_field(name='Amount (Optional)', value='The number of messages to be deleted. This must be a number or "all." By default, it will delete all the messages.', inline=False)
            embed.set_footer(text='This command is only available for the server owner and will only work for messages that are less than 2 weeks old. To clear an entire channel, use /clearchannel.')
        elif categoryOrCmd == 'assignrole' and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="/assignrole", description="This command assigns the user a class role.\n\n**Arguments**:", color=color)
            embed.add_field(name="Role", value="This value should be a 3-digit number.", inline=False)
            embed.set_footer(text="The role opt in poll should be used if possible.")
        elif categoryOrCmd == 'unassignrole' and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="/unassignrole", description="This command unassigns a class role from the user.\n\n**Arguments**:", color=color)
            embed.add_field(name="Role", value="This value should be a 3-digit number.", inline=False)
            embed.set_footer(text="This command is deprecated and the role opt in poll should be used if possible.")
        elif categoryOrCmd == 'archive' and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="/archive", description="This command archives the class channel it was ran in. It will also create a new category and channel if necessary. If an archive category and archive channel for the class already exists, it will add all the user messages that are not commands and are not deleted to the existing channel. This command also prevents non-admin members from sending messages during the archive process.\n\n**Arguments**:", color=color)
            embed.add_field(name='Custom Role (Optional)', value="If a channel has a nonstandard name, then you must use this argument to specify the role that belongs to the channel. If the channel has a standard name, then this argument will be ignored.")
            embed.set_footer(text="This command takes a while to run and will not allow multiple channels to be archived at the same time on the same server. It will send a notification in the logging channel when it is done archiving.")
        elif categoryOrCmd == 'create_poll' and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="/create_poll", description="This command creates new poll or a new role poll.\n\n**Arguments**:", color=color)
            embed.add_field(name="For Roles", value="This parameter should either be yes or no. In most cases, this argument should be no. If this command is being used to create a role poll, then this argument should be yes.", inline=False)
            embed.add_field(name="Emojis", value="List of emojis that will be used for creating the poll. It is recommended that you use the `:` to select emojis. There is a limit of 20 emojis.", inline=False)
            embed.add_field(name="Arguments", value='List of arguments. For arguments with more than 1 word, there should be double quotes (") around the phrase/sentence. If this command is used for creating a role poll, then this list should be the names of the roles.', inline=False)
            embed.add_field(name="Custom Message", value="Use a custom message instead of the generic message. This does not affect role polls.", inline=False)
        elif categoryOrCmd == 'warn' and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="/warn", description="This command warns a user for their behavior in the server. If the user in question receives more than 5 warnings, they will get banned from the server. This command will also notify the user when they are warned.\n\n**Arguments**:", color=color)
            embed.add_field(name="User", value="This argument should be a user.", inline=False)
            embed.add_field(name="Rule Violated", value="This must be an integer (0, 1, 2, 3...)", inline=False)
            embed.add_field(name="Reason", value="This should be the reason why the user was warned so they know where the line is.", inline=False)
        elif categoryOrCmd == 'remove_warning' and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="/remove_warning", description="This command removes a warning from a given user.\n\n**Arguments**:", color=color)
            embed.add_field(name="User", value="Remove a warning from this user. This argument should be a user.", inline=False)
        elif categoryOrCmd == 'clear_warnings'and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="/clear_warnings", description="This command removes all the warnings from a given user.\n\n**Arguments**:", color=color)
            embed.add_field(name="User", value="Remove all warnings from this user. This must be a user.", inline=False)
        elif ctx.guild is not None and categoryOrCmd == 'clearchannel' and ctx.author.guild_permissions.administrator and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="/clearchannel", description="This command clears **all** messages from a given channel, regardless if the messages are older than 2 weeks or not. To clear an entire channel, use this command inside the one you want to clear.\n\n**Arguments**: None", color=color)
        elif ctx.guild is not None and categoryOrCmd == 'ban' and ctx.author.guild_permissions.administrator and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="/ban", description="This command bans a member from a discord server. It will not work on members with admin privileges, server owners, or bots. When a user is banned, the bot sends the reason to the user as well as a custom message set by the server owner.\n\n**Arguments**:", color=color)
            embed.add_field(name="Member", value="The user that is getting banned from the server. This argument must be a user.", inline=False)
            embed.add_field(name="Rule Violated", value="The rule that the user violated. This argument must be an integer.", inline=False)
            embed.add_field(name="Reason", value="This is the reason the user is banned. It is encouraged that you are as thorough as possible with your reason.", inline=False)
        elif ctx.guild is not None and categoryOrCmd == 'delete_poll' and ctx.author.guild_permissions.administrator and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="/delete_poll", description="This command deletes a given role pole.\n\n**Arguments**:", color=color)
            embed.add_field(name="msg_url", value="The url of the poll message.", inline=False)
        elif ctx.guild is not None and categoryOrCmd == 'fetch_db' and ctx.author.guild_permissions.administrator and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="/fetch_db", description="This command sends the database in a DM.\n\n**Arguments**: None", color=color)
            embed.set_footer(text="This command will ignore everybody except the bot owner.")
        elif ctx.guild is not None and categoryOrCmd == 'fetch_logs' and ctx.author.guild_permissions.administrator and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="/fetch_logs", description="This command sends the logs in a DM.\n\n**Arguments**: None", color=color)
            embed.set_footer(text="This command will ignore everybody except the bot owner.")
        elif ctx.guild is not None and categoryOrCmd == 'edit_poll' and ctx.author.guild_permissions.administrator and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="/edit_poll", description="This command edits a given role poll.\n\n**Arguments**:", color=color)
            embed.add_field(name="poll_url", value="This is the URL of the poll message.", inline=False)
            embed.add_field(name="Mode", value="**add**: Adds an entry to the given role pole.\n**remove**: Deletes an entry from the given role pole.", inline=False)
            embed.add_field(name="Role", value="The role being added or removed from the poll", inline=False)
            embed.add_field(name="Emoji", value="The emoji for adding a new entry for a poll. This option is ignored when deleting an entry.", inline=False)
        elif ctx.guild is not None and categoryOrCmd == 'clear_roles' and ctx.author.guild_permissions.administrator and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="/clear_roles", description="This command clears all the class roles from every user in the server.\n\nArguments: None", color=color)
        elif ctx.guild is not None and categoryOrCmd == 'delete_channel' and ctx.author.guild_permissions.administrator and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="/delete_channel", description="This command deletes a class channel, and it should be used whenever deleting class channels.\n\n**Arguments**:", color=color)
            embed.add_field(name="Channel", value="The class channel that is to be deleted. This must be a reference to a channel.", inline=False)
        elif categoryOrCmd == 'configure' and owner:
            embed=discord.Embed(title="/configure", description="This command sets up the logging channels for the server.\n\n**Arguments**:", color=color)
            embed.add_field(name="Logging", value="This must be a channel reference. This is where the bot is going to post general server logging messages.", inline=False)
            embed.add_field(name="roleOptIn", value="TThis must be a channel reference. This is where the bot posts the role polls.", inline=False)
            embed.add_field(name="levelNotifications", value="TThis must be a channel reference. This is where the bot posts level up notifications.", inline=False)
            embed.add_field(name="Shaming", value="This must be a channel reference. This is where the bot makes an example of users who got banned.", inline=False)
            embed.add_field(name="Monitoring", value="This must be a channel reference. This is where the bot posts the links to troublesome user's messages.", inline=False)
        elif ctx.guild is not None and categoryOrCmd == 'watch' and ctx.author.guild_permissions.administrator and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="/watch", description="This command overrides the bot and adds or removes the user from the watchlist.\n\n**Arguments**:", color=color)
            embed.add_field(name="User", value="The user who gets added to or removed from the watchlist.", inline=False)
            embed.add_field(name="Status", value="Add or remove the watch on the user.\n**on**: Monitor the user.\n**off**: Remove the monitor from the user.", inline=False)
            embed.set_footer(text='''When using this command, it overrides the bot's automatic monitoring. To re-enable automatic monitoring, see "/help disable_watchlist_override"''')
        elif ctx.guild is not None and categoryOrCmd == 'disable_watchlist_override' and ctx.author.guild_permissions.administrator and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="/disable_watchlist_override", description="This command enables the bot's automatic monitoring.\n\n**Arguments**:", color=color)
            embed.add_field(name="User", value="The user who gets automatic monitoring enabled for again.", inline=False)
            embed.set_footer(text="By default, automatic monitoring is enabled for all users.")
        elif ctx.guild is not None and categoryOrCmd == 'lift_poli_ban' and ctx.author.guild_permissions.administrator and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="/liftPoliBan", description="This command lifts the political channels ban from a given user.\n\n**Arguments**:", color=color)
            embed.add_field(name="User", value="The user who gets removed from the politics channel banned list.", inline=False)
            embed.set_footer(text="The political channel rules are set during server configuration. If a user gets warned about violating one of the political rules, then they get banned from the political channels. By default, there are no political rules.")
        elif categoryOrCmd == 'config_test' and owner:
            embed=discord.Embed(title="/config_test", description="This command checks all the logging channels by sending each one a message.\n\n**Arguments**: None", color=color)
        elif categoryOrCmd == 'set_poli_msg' and owner:
            embed=discord.Embed(title="/set_poli_msg", description="This command sets the political rules message for people to opt into the politics role.\n\n**Arguments**:", color=color)
            embed.add_field(name="msg_url", value="This is the URL of the political rules message.", inline=False)
            embed.add_field(name="emoji", value="The emoji used for opting into politics role.", inline=False)
        elif categoryOrCmd == 'clear_poli_msg' and owner:
            embed=discord.Embed(title="/clearPoliMsg", description="The command that clears the political channel opt in message.\n\n**Arguments**: None", color=color)
        elif categoryOrCmd == 'set_poli_rules' and owner:
            embed=discord.Embed(title="/set_poli_rules", description="The command that sets the political rules of the server.\n\n**Arguments**:", color=color)
            embed.add_field(name="Rules", value="Numerical list of rules. These are the rules that earn users a political ban if they violate them.", inline=False)
        elif categoryOrCmd == 'clear_poli_rules' and owner:
            embed=discord.Embed(title="/clear_poli_rules", description="The command that clears the server's political rules.\n\n**Arguments**: None", color=color)
        elif ctx.guild is not None and categoryOrCmd == 'set_channel_prefix' and ctx.author.guild_permissions.administrator:
            embed=discord.Embed(title="/set_channel_prefix", description="This command sets the class prefix for creating new class channels.\n\n**Arguments**:", color=color)
            embed.add_field(name="Prefix", value="The class prefix for the server's class channels.", inline=False)
            embed.set_footer(text="The default class prefix is 'ee'")
        elif ctx.guild is not None and categoryOrCmd == 'clear_channel_prefix' and ctx.author.guild_permissions.administrator and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="/clear_channel_prefix", description="This command resets the class prefix back to the default for creating new class channels.\n\n**Arguments**: None", color=color)
            embed.set_footer(text="The default class prefix is 'ee'")
        elif categoryOrCmd == 'quiz' and db.checkCog(cogEnum.Games):
            embed=discord.Embed(title="/quiz", description="This command starts a new quiz mini-game.\n\n**Arguments**:", color=color)
            embed.add_field(name="Difficulty", value="The difficulty of the quiz.\n**very easy**: Initiates a new quiz in very easy difficulty.\n**easy**: Initiates a new quiz in easy difficulty. (In testing)\n**normal**: Initiates a new quiz in normal difficulty. (Not available)\n**hard**: Initiates a new quiz in hard difficulty. (Not available)\n**very hard**: Initiates a new quiz in very hard difficulty. (Not available)", inline=False)
        elif categoryOrCmd == 'answer' and db.checkCog(cogEnum.Games):
            embed=discord.Embed(title="/answer", description="This command answers a quiz question. Make sure to include your units!\n\n**Arguments**:", color=color)
            embed.add_field(name="Answer", value="The answer to the quiz question. Don't forget units!", inline=False)
        elif categoryOrCmd == 'quit' and db.checkCog(cogEnum.Games):
            embed=discord.Embed(title="/quit", description="This command quits the user's game.\n\n**Arguments**: None", color=color)
        elif categoryOrCmd == 'game' and db.checkCog(cogEnum.Games):
            embed=discord.Embed(title="/game", description="This command gets the current status of the user's game.\n\n**Arguments**: None", color=color)
        elif categoryOrCmd == 'temp' and db.checkCog(cogEnum.Hardware):
            embed=discord.Embed(title="/temp", description="This command gets the current temperature of the CPU.\n\n**Arguments**: None", color=color)
        elif categoryOrCmd == 'cool' and db.checkCog(cogEnum.Hardware):
            embed=discord.Embed(title="/cool", description="Not operational at the moment.\n\n**Arguments**: None", color=color)
        elif categoryOrCmd == 'cmd' and db.checkCog(cogEnum.Hardware):
            embed=discord.Embed(title="/cmd", description="This command runs a linux command. Most users are limited to the cowsay command, while developers and the owner does not have limited access. To gain full access of the command, you must acquire the 'Bot Developer' role in https://discord.gg/58YvXfZpvN \n\n**Arguments**:", color=color)
            embed.add_field(name="Command", value="This can be any Linux command. Normal users are limited to cowsay.", inline=True)
        elif categoryOrCmd == 'pre_synth' and db.checkCog(cogEnum.Yosys):
            embed=discord.Embed(title="/pre_synth", description="This command helps you visualize your Verilog code, and is the same as Vivado's Elaborated Design tool.\n\n**Arguments**:", color=color)
            embed.add_field(name="attachment", value="The Verilog file (.v) that get converted into a block diagram.", inline=False)
            embed.add_field(name="flags", value="Flags to help build block diagrams. See website for flags.", inline=False)
        #    embed.add_field(name="-print", value="Prints the logging messages from Yosys", inline=False)
            embed.set_footer(text="This command requires one, and only one Verilog HDL file (.v) in order to run.")
        elif categoryOrCmd == 'synth' and db.checkCog(cogEnum.Yosys):
            embed=discord.Embed(title="/synth", description="This command that goes synthesizes design and generates a picture of what it sees.\n\n**Arguments**:", color=color)
            embed.add_field(name="attachment", value="The Verilog file (.v) that get converted into a block diagram.", inline=False)
            embed.add_field(name="synth_proccess", value="Specify a synthesis process for the command to follow. Leave blank to use generic synthesis process.", inline=False)
            embed.add_field(name="flags", value="Flags to help build block diagrams. See website for flags.", inline=False)
            embed.set_footer(text="This command requires one, and only one Verilog HDL file (.v) in order to run.")
        elif categoryOrCmd == 'gen_spice' and db.checkCog(cogEnum.Yosys):
            embed=discord.Embed(title="/gen_spice", description="This command generates a spice netlist.\n\n**Arguments**:", color=color)
            embed.add_field(name="attachment", value="The Verilog file (.v) that get converted into a block diagram.", inline=False)
            embed.add_field(name="flags", value="Flags to help build block diagrams. See website for flags.", inline=False)
            embed.set_footer(text="This command requires one, and only one Verilog HDL file (.v) in order to run.")
        elif categoryOrCmd == 'gen_json' and db.checkCog(cogEnum.Yosys):
            embed=discord.Embed(title="/gen_json", description="This command generates a JSON netlist.\n\n**Arguments**:", color=color)
            embed.add_field(name="attachment", value="The Verilog file (.v) that get converted into a block diagram.", inline=False)
            embed.add_field(name="flags", value="Flags to help build block diagrams. See website for flags.", inline=False)
            embed.set_footer(text="This command requires one, and only one Verilog HDL file (.v) in order to run.")
        elif categoryOrCmd == 'help':
            embed=discord.Embed(title="...", color=color)
            embed.set_image(url="https://cdn.discordapp.com/attachments/759942951001391154/872342158454194226/IMG_3335.jpg")
        elif categoryOrCmd == 'kicost':
            embed=discord.Embed(title="/kicost", description="This command uses the KiCost library to generate a parts cost spreadsheet. Must have 1 XML or CSV file attached. See this link to see what fields are needed in your BoM for kicost to work: https://hildogjr.github.io/KiCost/docs/_build/singlehtml/index.html#usage \n\n**Arguments**:\n\n", color=color, url='https://github.com/hildogjr/KiCost')
            embed.add_field(name="bom", value="The bill of materials. This must be an XML or a CSV file.", inline=False)
            embed.add_field(name="flags", value="Flags for KiCost to generate the spreadsheet. Below are the available flags:", inline=False)
            embed.add_field(name="-f NAME [NAME ...], --fields NAME [NAME ...]", value="Specify the names of additional part fields to extract and insert in the global data section of the spreadsheet.", inline=False)
            embed.add_field(name="--translate_fields NAME [NAME ...]", value="Specify or remove field translation (--translate X1 Y1 X2 Y2 X3 ~, translates X1 to Y1 and X2 to Y2 and remove X3 for the internal dictionary).", inline=False)
            embed.add_field(name="--variant VARIANT [VARIANT ...]", value="schematic variant name filter using regular expression.", inline=False)
            embed.add_field(name="-q, --quiet", value="Enable quiet mode with no warnings.", inline=False)
            embed.add_field(name="--ignore_fields NAME [NAME ...]", value="Declare part fields to ignore when reading the BoM file.", inline=False)
            embed.add_field(name="--group_fields NAME [NAME ...]", value="Declare part fields to merge when grouping parts.", inline=False)
            embed.add_field(name="--eda {kicad,altium,csv} [{kicad,altium,csv} ...]", value="Choose EDA tool from which the XML BOM file originated, or use csv for .CSV files.", inline=False)
            embed.add_field(name="--show_dist_list", value="Show list of distributors that can be scraped for cost data, then exit.", inline=False)
            embed.add_field(name="--show_eda_list", value="Show list of EDA tools whose files KiCost can read, then exit.", inline=False)
            embed.add_field(name="--no_collapse", value="Do not collapse the part references in the spreadsheet.", inline=False)
            embed.add_field(name="--show_cat_url", value="Do not suppress the catalogue links into the catalogue code in the spreadsheet.", inline=False)
            embed.add_field(name="-e DIST [DIST ...], --exclude DIST [DIST ...]", value="Excludes the given distributor(s) from the scraping process.", inline=False)
            embed.add_field(name="--include DIST [DIST ...]", value="Includes only the given distributor(s) in the scraping process.", inline=False)
            embed.add_field(name="--no_price", value="Create a spreadsheet without scraping part data from distributor websites.", inline=False)
            embed.add_field(name="--currency [CURRENCY]", value="Define the priority currency. Use the ISO4217 for currency (`USD`, `EUR`). Default: `USD`.", inline=False)
        elif categoryOrCmd == 'leaderboard':
            embed=discord.Embed(title="/leaderboard", description="This command shows who are the user with the highest ranks in the server. It will also tell you where you stand in the leaderboard as well.\n\n**Arguments**: None", color=color)
        elif categoryOrCmd == 'level_system' and owner:
            embed=discord.Embed(title="/level_system", description="This command can turn on or off the level system for the server.\n\n**Arguments**:", color=color)
            embed.add_field(name="Status", value="**on**: Turns on level system\n**off**: Turns off level system", inline=False)
        elif categoryOrCmd == 'check_level_system' and owner:
            embed=discord.Embed(title='/check_level_system', description="This command checks if the level system is enabled or disabled for the server.\n\n**Arguments**: None", color=color)
        elif categoryOrCmd == 'enable_cog' and owner:
            embed=discord.Embed(title="/enable_cog", description="This command allows the server owner to enable groups of commands for the server.\n\n**Arguments**:", color=color)
            embed.add_field(name="Category", value="The category of commands to be enabled.")
            embed.set_footer(text='Note: The Yosys category is also dependendent on the OS, so it might be disabled depending on what operating system the bot is running on.')
        elif categoryOrCmd == 'disable_cog' and owner:
            embed=discord.Embed(title="/disable_cog", description="This command allows the server owner to disable groups of commands for the server.\n\n**Arguments**:", color=color)
            embed.add_field(name="Category", value="The category of commands to be disabled.")
            embed.set_footer(text='Note: The Yosys category is also dependendent on the OS, so it might be disabled depending on what operating system the bot is running on.')
        elif categoryOrCmd == 'check_cog_status' and owner:
            embed=discord.Embed(title="/checkCogStatus", description="This command allows the server owner to check which categories are enabled and disabled.\n\n**Arguments**: None", color=color)
            embed.set_footer(text='Note: The Yosys category is also dependendent on the OS, so it might be disabled depending on what operating system the bot is running on.')
        elif categoryOrCmd == 'demote' and ctx.author.guild_permissions.administrator and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="/demote", description="This command takes xp away from a member. If the Levels argument is greater than the user's rank, then the user's rank is reset.\n\n**Arguments**:", color=color)
            embed.add_field(name="User", value="The user who gets demoted.", inline=False)
            embed.add_field(name="Levels", value="The number of levels to demote a user. This argument can be 0 to reset user's progress towards the next level.", inline=False)
            embed.add_field(name="Reason", value="The reason for the demotion.", inline=False)
        elif categoryOrCmd == 'set_ban_msg' and owner:
            embed=discord.Embed(title="/set_ban_msg", description="This command sets a custom ban message. When a user gets banned, the bot tells them they got banned and why. The ban message will get tacked onto the end of the default message. The ban message is commonly used for ban repeal forms.\n\n**Arguments**:", color=color)
            embed.add_field(name="Message", value="The ban message that gets tacked onto the end of the default ban message. This message cannot excede 1000 characters.", inline=False)
        elif categoryOrCmd == 'clear_ban_msg' and owner:
            embed=discord.Embed(title="/clear_ban_msg", description="This command deletes the server's ban message. The ban message is the message tacked onto the end of the default ban message.\n\n**Arguments**: None", color=color)
        elif categoryOrCmd == 'radix':
            embed=discord.Embed(title="/radix", description="Converts any number from any common base to another common base.\n\n**Arguments**:", color=color)
            embed.add_field(name="From", value="The base of the given number. This can be 2 (binary), 8 (octal), 10 (decimal), or 16 (hex).", inline=False)
            embed.add_field(name="To", value="The desired base. This can be 2 (binary), 8 (octal), 10 (decimal), or 16 (hex).", inline=False)
            embed.add_field(name="Number", value="The number that gets converted from the given base to the desired base.", inline=False)
        elif categoryOrCmd == 'dbm' and db.checkCog(cogEnum.RF):
            embed=discord.Embed(title="/dbm", description="Convert power in Watts to dBm.\n\n**Arguments**:", color=color)
            embed.add_field(name="Power", value="This argument can be a floating point number.", inline=False)
        elif categoryOrCmd == 'dms_to_deg' and db.checkCog(cogEnum.RF):
            embed=discord.Embed(title="/dms_to_deg", description="Command that converts degrees, minutes, seconds to degrees.\n\n**Arguments**:", color=color)
            embed.add_field(name="Degrees", value="The degrees parameter in degrees, minutes, seconds data. This argument must be an integer.", inline=False)
            embed.add_field(name="Minutes", value="The minutes parameter in degrees, minutes, seconds data. This argument must be an integer.", inline=False)
            embed.add_field(name="Seconds", value="The seconds parameter in degrees, minutes, seconds data. This argument must be an integer.", inline=False)
        elif categoryOrCmd == 'deg_to_dms' and db.checkCog(cogEnum.RF):
            embed=discord.Embed(title="/dms_to_deg", description="Command that converts degrees to degrees, minutes, seconds.\n\n**Arguments**:", color=color)
            embed.add_field(name="Degrees", value="The coordinate in degrees. This is a floating point number.", inline=False)
        elif categoryOrCmd == 'rayrand' and db.checkCog(cogEnum.RF):
            embed=discord.Embed(title="/rayrand", description="Command that returns a rayleigh random variable.\n\n**Arguments**: None", color=color)
        elif categoryOrCmd == 'diffraction_gain' and db.checkCog(cogEnum.RF):
            embed=discord.Embed(title="/diffraction_gain", description="This command takes in a file, transmitter height, receiver height, and the wavelength, and it calculates the diffraction gain from the data in the file and the user inputs.\n\n**Arguments**:", color=color)
            embed.add_field(name="landscape_data", value="The file that contains the landscape data. The file has a very specific format that it needs to follow: The first line must be the number of data entries, the second line must be the data header, and the x coordinates must be in ascending order. The data header must have the column definitions (x and y) and a delimeter. Here's an example of the file formatting: ```\n3\nx y\n0 0\n1000 21.76\n1100 0```", inline=False)
            embed.add_field(name="ht", value="The height of the transmitter in meters above the ground. The transmitter is assumed to be at the first data point in the file.", inline=False)
            embed.add_field(name="hr", value="The height of the receiver in meters above the ground. The receiver is assumed to be at the last point in the data.", inline=False)
            embed.add_field(name="wavelength", value="The wavelength of the carrier. This argument must be expressed in meters.", inline=False)
        elif categoryOrCmd == 'erlangs' and db.checkCog(cogEnum.RF):
            embed=discord.Embed(title="/erlangs", description="Calculates the number of erlangs given the number of channels per a cell and the blocking probability.\n\n**Arguments**:", color=color)
            embed.add_field(name="Channels", value="The number of channels per a cell. This argument must be an integer greater than 0 and is limited up to 100000.", inline=False)
            embed.add_field(name="PB", value="The probability of blocking. This argument must be between 0 and 1, but cannot be 0 or 1.", inline=False)
        elif categoryOrCmd == 'erlangs_table' and db.checkCog(cogEnum.RF):
            embed=discord.Embed(title="/erlangs_table", description="Generates a table of erlang calculations given the channel limits and blocking probability. By default, the range is 10 to 100 channels per a cell with a blocking probability of 0.01.\n\nFlags: --pb\n               --range\n\nNote that the Flags are optional and are non-positional. For them to work, their arguments have to be passed right after the flag.\n\nExample usage: `/erlang_table Flags:--pb 0.02 --range 1 50`\n\n", color=color)
            embed.add_field(name="--pb", value="This flag allows you to use a different blocking probability than the default (0.01).", inline=False)
            embed.add_field(name="--range", value="This flag allows you to set a different channels per a cell range. The first argument after this flag is the lower limit and the second argument is the upper limit. The limits cannot be 0 or less, and they cannot excede 100000 channels per a cell.", inline=False)
        elif categoryOrCmd == 'exposure_radius' and db.checkCog(cogEnum.RF):
            embed=discord.Embed(title="/exposure_radius", url="https://www.ecfr.gov/current/title-47/section-1.1310#p-1.1310(e)(1)", description="This command calculates the minumum radii for controlled and uncontrolled exposure according to FCC regulations.\n\n**Arguments**:", color=color)
            embed.add_field(name="Frequency", value="The frequency in MHz.", inline=False)
            embed.add_field(name="Gain", value="The gain in dB.", inline=False)
            embed.add_field(name="Power", value="The power in Watts.", inline=False)
        elif categoryOrCmd == 'cluster_sizes' and db.checkCog(cogEnum.RF):
            embed=discord.Embed(title="/cluster_sizes", description="This command generates a table of cluster sizes up to the given cluster size. The formula is `N = i^2 + j^2 + ij`.\n\n**Arguments**:", color=color)
            embed.add_field(name="Size", value="The maximum cluster size for table generation. This cannot excede 100000.", inline=False)
        elif ctx.guild is not None and categoryOrCmd == 'fetch_warnings'and ctx.author.guild_permissions.administrator and db.checkCog(cogEnum.Moderator):
            embed=discord.Embed(title="/fetch_warnings", description="This command returns warning records from the database in a csv file. The command can also be used to fetch records based on certain parameters by using the flags.\n\n**Arguments**:", color=color)
            embed.add_field(name='Args (Optional)', value='Optional argument for passing in certain **flags**:')
            embed.add_field(name="--guild", value="The argument after this flag must be a guild ID. This flag is only available to the bot owner.", inline=False)
            embed.add_field(name="--channel", value="The argument must be the channel name.", inline=False)
            embed.add_field(name="--userid", value="The argument is the user id of the warned user.", inline=False)
            embed.add_field(name="--username", value="The argument is the user name of the warned user.", inline=False)
            embed.add_field(name="--adminid", value="The argument is the user id of the admin who warned the user.", inline=False)
            embed.add_field(name="--adminname", value="The argument is the user name of the admin who warned the user.", inline=False)
            embed.add_field(name="--rule", value="The argument is the rule that was violated. This argument must be an integer. If a non-integer value is passed in, the command will return an empty csv file.", inline=False)
            embed.add_field(name="--reason", value='The argument is the reason that was provided when warning the user. If the reason has spaces, double quotes (") must be used around the reason.', inline=False)
            embed.add_field(name="--date", value="The argument is the date of the warning. Format: month/day/year", inline=False)
        elif categoryOrCmd == 'create_task' and db.checkCog(cogEnum.Schedule):
            embed=discord.Embed(title='/create_task', description="This command creates a new archive or clear roles task and adds it onto the task queue for the server. If the tasks have already been scheduled for execution, then `cancel_tasks` must be invoked in order to modify the queue.\n\n**Arguments**:", color=color)
            embed.add_field(name='Task Type', value="This must be either Archive or Clear Roles. This argument is required.", inline=False)
            embed.add_field(name='Channel', value='This argument specifies the channel that gets archived. It is required for the archive task type, but has no effect on the clear roles task type.', inline=False)
            embed.add_field(name='Role', value='This argument specifies the role that gets its members cleared from. If left blank, the bot will clear all the members from the standard class roles. This argument has no effect on the archive task type.', inline=False)
        elif categoryOrCmd == 'view_tasks' and db.checkCog(cogEnum.Schedule):
            embed=discord.Embed(title='/view_tasks', description="This command allows for the task queue to be viewed. It also shows the positions of each task in the queue. The task positions are important for removing tasks from the queue.\n\n**Arguments**: None", color=color)
        elif categoryOrCmd == 'remove_task' and db.checkCog(cogEnum.Schedule):
            embed=discord.Embed(title="/remove_task", description="This command removes a task given the position of the task in the task queue. This command cannot be ran after the tasks are scheduled for execution.\n\n**Arguments**:", color=color)
            embed.add_field(name="Task ID", value="The task's position in the queue.", inline=False)
            embed.set_footer(text="The `/view_tasks` command is useful for finding the Task ID.")
        elif categoryOrCmd == 'clear_tasks' and db.checkCog(cogEnum.Schedule):
            embed=discord.Embed(title='/clear_tasks', description="This command clears the task queue. If the tasks are scheduled for execution, this command will do nothing.\n\n**Arguments**: None", color=color)
        elif categoryOrCmd == 'schedule_tasks' and db.checkCog(cogEnum.Schedule):
            embed=discord.Embed(title='/schedule_tasks', description="This command schedules the commands in the queue for execution. After invoking this command, the task queue cannot be modified unless `/cancel_tasks` is invoked.\n\n**Arguments**:", color=color)
            embed.add_field(name='month', description="The month part of the scheduled execution date.", inline=False)
            embed.add_field(name="day", description="The day part of the scheduled execution date.", inline=False)
            embed.add_field(name="announce", description="Choose to announce the tasks' execution date to the server.", inline=False)
            embed.add_field(name="announcement_ch", description="Specify the channel the announcement goes in. If left blank, the bot will use either the first announcement channel it finds or the very first channel in the list. This argument has no effect if the announce argument is `no`.", inline=False)
        elif categoryOrCmd == 'cancel_taks' and db.checkCog(cogEnum.Schedule):
            embed=discord.Embed(title="/cancel_tasks", description="This command cancels tasks scheduled for execution and unlocks the queue for modification. It will also remove completed tasks from the queue if called while the tasks are running. It is not recommended to invoke this command while the bot is archiving!\n\n**Arguments**: None", color=color)
        elif categoryOrCmd == 'mils_mm':
            embed=discord.Embed(title="/mils_mm", description="Converts mils to millimeters and vice versa.\n\n**Arguments**:")
            embed.add_field(name='convert_to', value='The unit to convert to. If mm is given, then the command assumes that the value is in mils and vice versa.', inline=False)
            embed.add_field(name='value', value='The value that gets converted.', inline=False)
        elif categoryOrCmd == 'set_color' and owner:
            embed=discord.Embed(title="/set_color", description="Set the class role color of the server given the 8-bit unsigned decimal numbers of red, green, and blue.\n\n**Arguments**:", color=color)
            embed.add_field(name="red", value="The red component of the color. This must be an unsigned 8-bit number (0, 1, 2, ..., 10, 11, ..., 254, 255)", inline=False)
            embed.add_field(name="green", value="The greem component of the color. This must be an unsigned 8-bit number (0, 1, 2, ..., 10, 11, ..., 254, 255)", inline=False)
            embed.add_field(name="blue", value="The blue component of the color. This must be an unsigned 8-bit number (0, 1, 2, ..., 10, 11, ..., 254, 255)", inline=False)
            embed.set_footer(text="If a number is larger than 8 bits, then the bot will truncate the input and use only the least significant 8 bits.")
        elif categoryOrCmd == 'role_color' and owner:
            embed=discord.Embed(title="/role_color", description="Responds with a picture of the saved role color for the server. If no custom role color is available for the server, then it will respond with discord's dark teal.\n\n**Arguments**: None", color=color)
        elif categoryOrCmd == 'clear_color' and owner:
            embed=discord.Embed(title="/clear_color", description="Clears the saved class role color for the server, and makes the bot default to discord's dark teal.\n\n**Arguments**: None", color=color)
        elif categoryOrCmd == 'q' and db.checkCog(cogEnum.RF):
            embed=discord.Embed(title="/q", description="Calculates the area from a certain point to infinity underneath the Gaussian PDF Curve. The Q function is commonly used to model AWGN (White Gaussian Noise).\n\n**Arguments**:", color=color)
            embed.add_field(name='x', value='The lower bound of the integral for the area calculation (x_0 in the image).', inline=False)
            embed.set_image(url='https://cdn.discordapp.com/attachments/759942951001391154/956816767924183080/Probability-density-function-for-Gaussian-distribution-with-tails-shaded-Q-function-error-function.png')
        elif categoryOrCmd == 'qinv' and db.checkCog(cogEnum.RF):
            embed=discord.Embed(title="/qinv", description="Approximates the lower bound of the area integral given the area. This function is considered the inverse Q function.\n\n**Arguments**:", color=color)
            embed.add_field(name="x", value="The area between the lower bound and infinity below the Gaussian PDF Curve (The red area).", inline=False)
            embed.set_image(url='https://cdn.discordapp.com/attachments/759942951001391154/956816767924183080/Probability-density-function-for-Gaussian-distribution-with-tails-shaded-Q-function-error-function.png')
        elif categoryOrCmd == 'schedule_announcement' and db.checkCog(cogEnum.Schedule):
            embed = discord.Embed(title="/schedule_announcement", description="This command schedules an announcement within a 24-hour window. If there is an announcement already scheduled, this command will do nothing.\n\n**Arguments**:", color=color)
            embed.add_field(name="msg", value="The actual announcement message.", inline=False)
            embed.add_field(name="hour", value="The hour of the day that the bot should make the announcement. This can be any number between 0 and 23.", inlin=False)
            embed.add_field(name="minute", value="The minute of the hour that the bot should make the announcement. This can be any number between 0 and 59.", inline=False)
            embed.add_field(name="timezone", value="The timezone of the time you are entering. If left blank, it will default to UTC time.", inline=False)
            embed.add_field(name="channel", value="The channel where the bot should make the announcement. If left blank, it will default to the first news channel it finds. If it can't find a news channel, it will use the first text channel it can find.", inline=False)
        elif categoryOrCmd == 'cancel_announcement' and db.checkCog(cogEnum.Schedule):
            embed = discord.Embed(title="/cancel_announcement", description="This command will cancel the scheduled announcement for the server. If no announcements are scheduled, it will do nothing.\n\n**Arguments**: None", color=color)
        elif categoryOrCmd == 'powercmd' and db.checkCog(cogEnum.Power):
            embed = discord.Embed(title="/power", description="This command calculates the power consumption of a circuit given various parameters. This command can calculate real, reactive, and apparent power.\n\n**Arguments**:", color=color)
            embed.add_field(name='calc_type', value='The type of power calculation. The 4 possible choices are real, reactive, apparent, and power. The first 3 choices are pretty self explanatory, but the power choice will calculate real, reactive, and apparent power.', inline=False)
            embed.add_field(name='voltage', value='The voltage supplied to the circuit measured in volts. This parameter is optional.', inline=False)
            embed.add_field(name='current', value='The current supplied to the circuit measured in amps. This parameter is optional.', inline=False)
            embed.add_field(name='resistance', value='The resistance of the circuit measured in ohms. This is considered the real part of the impedance. This parameter is optional.', inline=False)
            embed.add_field(name='reactance', value='The reactance of the circuit measured in ohms. This is considered the imaginary part of the impedance. This parameter is optional.', inline=False)
        elif categoryOrCmd == 'line_phase' and db.checkCog(cogEnum.Power):
            embed = discord.Embed(title='/line_phase', description="This command calculates the line to phase voltage/current or vice versa given the circuit configuration.\n\n**Arguments**:", color=color)
            embed.add_field(name='configuration', value='The configuration of the load/generator. This can be delta, star, or wye (same as star).', inline=False)
            embed.add_field(name='line_voltage', value='The voltage in Volts measured between any two lines in a three-phase circuit. This parameter is optional.', inline=False)
            embed.add_field(name='phase_voltage', value='The voltage in Volts measured across a single component in a three-phase source or load. This parameter is optional.', inline=False)
            embed.add_field(name='line_current', value='The current in Amps through any one line between a three-phase source and load. This parameter is optional.', inline=False)
            embed.add_field(name='phase_current', value='The current in Amps through any one component comprising a three-phase source or load. This parameter is optional.', inline=False)
        elif categoryOrCmd == 'reactance' and db.checkCog(cogEnum.Power):
            embed = discord.Embed(title='/reactance', description="This command calculates the reactance given the frequency and other parameters.\n\n**Arguments**:", color=color)
            embed.add_field(name='frequency', value='The frequency of the source. By default, this parameter is measured in Hz.', inline=False)
            embed.add_field(name='frequency_units', value='The units of the frequency which can either be radians or Hz. This parameter is optional.', inline=False)
            embed.add_field(name='inductance', value='The inductance of the circuit measured in Henries. This parameter is optional.', inline=False)
            embed.add_field(name='capacitance', value='The capacitance of the circuit measured in Farads. This parameter is optional.', inline=False)
        # Put more elifs here for commands
        else:  #Command or category does not exist
            embed=discord.Embed(title="No Category or Command found", url="https://gitlab.com/ee_bot-developers/ee_bot/-/issues", description="The command %s was not found. Please check if the category or command exists or check your spelling/capitalization if you believe the command/category exists before submitting an issue." % (categoryOrCmd), color=color)
        embed.set_thumbnail(url=avatar)
        # Send message
        await ctx.respond(embed=embed)
        return
    embed=discord.Embed(title="EE_Bot Commands", description="Commands for EE_Bot. Please submit bugs or suggestions in the gitlab: https://gitlab.com/ee_bot-developers/ee_bot/-/issues", color=color)
    embed.set_thumbnail(url=avatar)
    embed.add_field(name="General", value="`/help general`", inline=True)
    if db.checkCog(cogEnum.Moderator):
        embed.add_field(name="Moderator", value="`/help moderator`", inline=True)
    if db.checkCog(cogEnum.Games):
        embed.add_field(name="Games", value="`/help games`", inline=True)
    if db.checkCog(cogEnum.Hardware):
        embed.add_field(name="Hardware", value="`/help hardware`", inline=True)
    if YS and db.checkCog(cogEnum.Yosys):
        embed.add_field(name="Yosys", value="`/help yosys`", inline=True)
    if owner:
        embed.add_field(name="serverConfig", value="`/help serverConfig`", inline=True)
    if db.checkCog(cogEnum.RF) and rfExists:
        embed.add_field(name="RF", value="`/help rf`", inline=True)
    if db.checkCog(cogEnum.Schedule):
        embed.add_field(name='Schedule', value="`/help schedule`", inline=True)
    if db.checkCog(cogEnum.Power):
        embed.add_field(name='Power', value="`/help power`", inline=True)
    await ctx.respond(embed=embed)
