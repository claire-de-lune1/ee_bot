# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import discord
from discord.ext import commands
from discord.commands import slash_command
from discord.commands import Option
from EE_Bot_DB import EE_DB
from EE_Bot_DB import cogEnum
from subprocess import PIPE
from subprocess import Popen
import threading
import asyncio
import os
import io
from random import randint

rfExists = os.path.isdir('RF')


class RF(commands.Cog):
    """This category is for RF commands"""

    def __init__(self, client, database, MASTER):
        self.client = client
        self.database = database
        self.MASTER = MASTER

    def runCommand(self, args, inputDict):
        # Create process to run command
        process = Popen(args, stdout=PIPE, stderr=PIPE, universal_newlines=True)
        # Run the command and capture output
        # Most commands have timeout implemented already
        inputDict['output'], inputDict['error'] = process.communicate()

        inputDict['done'] = True
        return

    @slash_command(description='Convert Watts to dBm')
    async def dbm(
            self,
            ctx,
            power: Option(str, 'The power in Watts')
    ):
        inGuild = True
        if ctx.guild is None:
            db = EE_DB(self.database, 0, False)
            inGuild = False
        else:
            db = EE_DB(self.database, ctx.guild.id, False)
        # check if Cog is enabled
        if not db.checkCog(cogEnum.RF) and inGuild:
            await ctx.respond('This command has been disabled.')
            return
        # create terminal statement
        L = ["RF/ee432.out", "dbm", power]
        # Create mutex
        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'done': False}
        # Create thread
        RFthread = threading.Thread(target=self.runCommand, args=(L, outputDict))
        # Run command
        RFthread.start()
        # Respond so discord doesn't freak out
        await ctx.respond('Calculating...')
        # acquire mutex
        mutex.acquire()
        while not outputDict['done']:
            # Release mutex
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()
        output = outputDict['output']
        _error = outputDict['error']
        # check if there's an output
        if len(output) != 0:
            await ctx.channel.send(f"```{output}```")
        if len(_error) != 0:
            await ctx.channel.send(f"```diff\n- {_error}```")
        return

    @slash_command(description='Convert dms coordinates to degrees coordinates')
    async def dms_to_deg(
            self,
            ctx,
            degrees: Option(int, 'degrees part of dms coordinates'),
            minutes: Option(int, 'minutes part of dms coordinates'),
            seconds: Option(int, 'seconds part of dms coordinates')
    ):
        inGuild = True
        if ctx.guild is None:
            db = EE_DB(self.database, 0, False)
            inGuild = False
        else:
            db = EE_DB(self.database, ctx.guild.id, False)
        # check if Cog is enabled
        if not db.checkCog(cogEnum.RF) and inGuild:
            await ctx.respond('This command has been disabled.')
            return
        # create terminal statement
        L = ["RF/ee432.out", "dms_to_deg", str(degrees), str(minutes), str(seconds)]
        # Create mutex
        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'done': False}
        # Create thread
        RFthread = threading.Thread(target=self.runCommand, args=(L, outputDict))
        # Run command
        RFthread.start()
        # Respond so discord doesn't freak out
        await ctx.respond('Calculating...')
        # acquire mutex
        mutex.acquire()
        while not outputDict['done']:
            # Release mutex
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()
        output = outputDict['output']
        _error = outputDict['error']
        # check if there's an output
        if len(output) != 0:
            await ctx.channel.send(f'```{output}```')
        if len(_error) != 0:
            await ctx.channel.send(f'```diff\n- {_error}```')
        return

    @slash_command(description='Convert degrees coordinates to dms coordinates')
    async def deg_to_dms(
            self,
            ctx,
            degrees: Option(float, 'The coordinate in degrees.')
    ):
        inGuild = True
        if ctx.guild is None:
            db = EE_DB(self.database, 0, False)
            inGuild = False
        else:
            db = EE_DB(self.database, ctx.guild.id, False)
        # check if Cog is enabled
        if not db.checkCog(cogEnum.RF) and inGuild:
            await ctx.respond('This command has been disabled.')
            return
        # create terminal statement
        L = ["RF/ee432.out", "deg_to_dms", str(degrees)]
        # Create mutex
        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'done': False}
        # Create thread
        RFthread = threading.Thread(target=self.runCommand, args=(L, outputDict))
        # Run command
        RFthread.start()
        # Respond so discord doesn't freak out
        await ctx.respond('Calculating...')
        # acquire mutex
        mutex.acquire()
        while not outputDict['done']:
            # Release mutex
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()
        output = outputDict['output']
        _error = outputDict['error']
        # Checki if there's an output
        if len(output) != 0:
            await ctx.channel.send(f"```{output}```")
        if len(_error) != 0:
            await ctx.channel.send(f'```diff\n- {_error}```')
        return

    @slash_command(description='Generate a rayleigh random variable.')
    async def rayrand(self, ctx):
        inGuild = True
        if ctx.guild is None:
            db = EE_DB(self.database, 0, False)
            inGuild = False
        else:
            db = EE_DB(self.database, ctx.guild.id, False)
        # check if Cog is enabled
        if not db.checkCog(cogEnum.RF) and inGuild:
            await ctx.respond('This command has been disabled.')
            return
        # create terminal statement
        L = ["RF/ee432.out", "rayrand", str(randint(1, 1000))]
        # Create mutex
        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'done': False}
        # Create thread
        RFthread = threading.Thread(target=self.runCommand, args=(L, outputDict))
        # Run command
        RFthread.start()
        # Respond so discord doesn't freak out
        await ctx.respond('Calculating...')
        # acquire mutex
        mutex.acquire()
        while not outputDict['done']:
            # Release mutex
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()
        output = outputDict['output']
        _error = outputDict['error']
        # Check if there's an output
        if len(output) != 0:
            await ctx.channel.send(f'```{output}```')
        if len(_error) != 0:
            await ctx.channel.send(f'```diff\n- {_error}```')
        return

    @slash_command(description='Calculates the diffraction gain given topography data.')
    async def diffraction_gain(
            self,
            ctx,
            landscape_data: Option(discord.Attachment,
                                   'The file that contains the topography data. See /help diffraction_gain for file '
                                   'format.'),
            ht: Option(float, 'The height of the transmitter antenna.'),
            hr: Option(float, 'The height of the receiver antenna.'),
            wavelength: Option(float, 'The wavelength of the signal. (c/f)')
    ):
        inGuild = True
        if ctx.guild is None:
            db = EE_DB(self.database, 0, False)
            inGuild = False
        else:
            db = EE_DB(self.database, ctx.guild.id, False)
        # check if Cog is enabled
        if not db.checkCog(cogEnum.RF) and inGuild:
            await ctx.respond('This command has been disabled.')
            return
        # Get filename
        filename = landscape_data.filename
        # save file
        await landscape_data.save(fp=f"./RF/{filename}")
        # Create terminal statement
        L = ["./RF-interface.out", "defractGain", filename, str(ht), str(hr), str(wavelength)]
        # Create mutex
        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'done': False}
        # Create thread
        RFthread = threading.Thread(target=self.runCommand, args=(L, outputDict))
        # Run command
        RFthread.start()
        # Respond so discord doesn't freak out
        await ctx.respond('Calculating...')
        # acquire mutex
        mutex.acquire()
        while not outputDict['done']:
            # Release mutex
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()
        output = outputDict['output']
        _error = outputDict['error']
        # Check for output
        if len(output) != 0:
            await ctx.channel.send(f"```{output}```")
        if len(_error) != 0:
            await ctx.channel.send(f"```diff\n- {_error}```")
        # Clean up afterwards
        L = ["rm", "-f", filename]
        self.runCommand(L, outputDict)
        return

    @slash_command(description='Calculate carrier capacity')
    async def erlangs(
            self,
            ctx,
            channels: Option(int, 'Number of channels available.'),
            pb: Option(float, 'The blocking probability.')
    ):
        inGuild = True
        if ctx.guild is None:
            db = EE_DB(self.database, 0, False)
            inGuild = False
        else:
            db = EE_DB(self.database, ctx.guild.id, False)
        # check if Cog is enabled
        if not db.checkCog(cogEnum.RF) and inGuild:
            await ctx.respond('This command has been disabled.')
            return
        # Create terminal statement
        L = ["RF/ee432.out", "erlangs", str(channels), str(pb)]
        # Create mutex
        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'done': False}
        # Create thread
        RFthread = threading.Thread(target=self.runCommand, args=(L, outputDict))
        # Run command
        RFthread.start()
        # Respond so discord doesn't freak out
        await ctx.respond('Calculating...')
        # acquire mutex
        mutex.acquire()
        while not outputDict['done']:
            # Release mutex
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()
        output = outputDict['output']
        _error = outputDict['error']
        # check for outputs
        if len(output) != 0:
            await ctx.channel.send(f"```{output}```")
        if len(_error) != 0:
            await ctx.channel.send(f"```diff\n- {_error}```")
        return

    @slash_command(description='Command that generates a table of erlangs.')
    async def erlangs_table(
            self,
            ctx,
            flags: Option(str, 'Flags for this command. See help menu for flags.', required = False, default = '')
    ):
        inGuild = True
        if ctx.guild is None:
            db = EE_DB(self.database, 0, False)
            inGuild = False
        else:
            db = EE_DB(self.database, ctx.guild.id, False)
        # check if Cog is enabled
        if not db.checkCog(cogEnum.RF) and inGuild:
            await ctx.respond('This command has been disabled.')
            return
        # Create terminal statement
        if len(flags) == 0:
            L = ["RF/ee432.out", "erlang_table"]
        else:
            L = ["RF/ee432.out", "erlang_table", flags]
        # Create mutex
        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'done': False}
        # Create thread
        RFthread = threading.Thread(target=self.runCommand, args=(L, outputDict))
        # Run command
        RFthread.start()
        # Respond so discord doesn't freak out
        await ctx.respond('Calculating...')
        # acquire mutex
        mutex.acquire()
        while not outputDict['done']:
            # Release mutex
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()
        output = outputDict['output']
        _error = outputDict['error']
        # Check outputs
        if len(output) != 0:
            buffer = io.StringIO(output)
            await ctx.channel.send(file=discord.File(buffer, 'erlangs_table.txt'))
        if len(_error) != 0:
            await ctx.channel.send(f"```diff\n- {_error}```")
        return

    @slash_command(description='Calculates the exposure radius for controlled and uncontrolled exposure.')
    async def exposure_radius(
            self,
            ctx,
            frequency: Option(float, 'The frequency of the basestation.'),
            gain: Option(float, 'The gain of the base station.'),
            power: Option(float, 'The transmitted power of the base station.')
    ):
        inGuild = True
        if ctx.guild is None:
            db = EE_DB(self.database, 0, False)
            inGuild = False
        else:
            db = EE_DB(self.database, ctx.guild.id, False)
        # check if Cog is enabled
        if not db.checkCog(cogEnum.RF) and inGuild:
            await ctx.respond('This command has been disabled.')
            return
        # Create terminal statement
        L = ["RF/ee432.out", "exposure-radius", str(frequency), str(gain), str(power)]
        # Create mutex
        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'done': False}
        # Create thread
        RFthread = threading.Thread(target=self.runCommand, args=(L, outputDict))
        # Run command
        RFthread.start()
        # Respond so discord doesn't freak out
        await ctx.respond('Calculating...')
        # acquire mutex
        mutex.acquire()
        while not outputDict['done']:
            # Release mutex
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()
        output = outputDict['output']
        _error = outputDict['error']
        # check outputs
        if len(output) != 0:
            await ctx.channel.send(f"```{output}```")
        if len(_error) != 0:
            await ctx.channel.send(f"```diff\n- {_error}```")
        return

    @slash_command(description='Generates a table of different cluster size combinations.')
    async def cluster_sizes(
            self,
            ctx,
            max_size: Option(int, 'The maximum cluster size')
    ):
        inGuild = True
        if ctx.guild is None:
            db = EE_DB(self.database, 0, False)
            inGuild = False
        else:
            db = EE_DB(self.database, ctx.guild.id, False)
        # check if Cog is enabled
        if not db.checkCog(cogEnum.RF) and inGuild:
            await ctx.respond('This command has been disabled.')
            return
        # Create terminal statement
        L = ["RF/ee432.out", "cluster-size", str(max_size)]
        # Create mutex
        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'done': False}
        # Create thread
        RFthread = threading.Thread(target=self.runCommand, args=(L, outputDict))
        # Run command
        RFthread.start()
        # Respond so discord doesn't freak out
        await ctx.respond('Calculating...')
        # acquire mutex
        mutex.acquire()
        while not outputDict['done']:
            # Release mutex
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()
        output = outputDict['output']
        _error = outputDict['error']
        # check outputs
        if len(output) != 0:
            buffer = io.StringIO(output)
            await ctx.channel.send(file=discord.File(buffer, 'cluster_sizes.txt'))
        if len(_error) != 0:
            await ctx.channel.send(f"```diff\n- {_error}```")
        return

    @slash_command(description="Calculates the output of the Q function")
    async def q(
            self,
            ctx,
            x: Option(float, 'Q(x)')
    ):
        inGuild = True
        if ctx.guild is None:
            db = EE_DB(self.database, 0, False)
            inGuild = False
        else:
            db = EE_DB(self.database, ctx.guild.id, False)
        # check if Cog is enabled
        if not db.checkCog(cogEnum.RF) and inGuild:
            await ctx.respond('This command has been disabled.')
            return
        # Create terminal statement
        L = ["RF/ee432.out", "Q", str(x)]
        # Create mutex
        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'done': False}
        # Create thread
        RFthread = threading.Thread(target=self.runCommand, args=(L, outputDict))
        # Run command
        RFthread.start()
        # Respond so discord doesn't freak out
        await ctx.respond('Calculating...')
        # acquire mutex
        mutex.acquire()
        while not outputDict['done']:
            # Release mutex
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()
        output = outputDict['output']
        _error = outputDict['error']
        # check outputs
        if output:
            await ctx.channel.send(f"```{output}```")
        if _error:
            await ctx.channel.send(f"```diff\n- {_error}```")
        return

    @slash_command(description='Calculates the output of the inverse Q function.')
    async def qinv(
            self,
            ctx,
            x: Option(float, 'Q^-1(x)')
    ):
        inGuild = True
        if ctx.guild is None:
            db = EE_DB(self.database, 0, False)
            inGuild = False
        else:
            db = EE_DB(self.database, ctx.guild.id, False)
        # check if Cog is enabled
        if not db.checkCog(cogEnum.RF) and inGuild:
            await ctx.respond('This command has been disabled.')
            return
        # Create terminal statement
        L = ["RF/ee432.out", "Qinv", str(x)]
        # Create mutex
        mutex = threading.Lock()
        outputDict = {'output': '', 'error': '', 'done': False}
        # Create thread
        RFthread = threading.Thread(target=self.runCommand, args=(L, outputDict))
        # Run command
        RFthread.start()
        # Respond so discord doesn't freak out
        await ctx.respond('Calculating...')
        # acquire mutex
        mutex.acquire()
        while not outputDict['done']:
            # Release mutex
            mutex.release()
            await asyncio.sleep(1)
            mutex.acquire()
        output = outputDict['output']
        _error = outputDict['error']
        # check outputs
        if output:
            await ctx.channel.send(f"```{output}```")
        if _error:
            await ctx.channel.send(f"```diff\n- {_error}```")
        return
