# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from random import randint
import numpy as np

__very_easy = []


def load_very_easy():
    #Open text file holding questions and filenames
    with open("veryEasyQuestions.txt", 'r', encoding='utf-8') as f:
        #Read file in with each line as an item 
        lines = f.read().splitlines()
    #iterate through list and format each line
    for i in range(0, len(lines)):
        #split string into 2 list items
        line = lines[i].split(';')
        #Check if 1st item is an empty string
        if line[1] == '':
            line[1] = None  #Set 1st item to None if empty
        #Append item into global list
        __very_easy.append(line)


def freeVeryEasy():
    #Clear global list
    __very_easy.clear()


#############################################
# Variants of question 1
def _ve_Q1_v1(R, V):
    file = None
    if __very_easy[0][1] is not None:
        file = "Veasy/" + __very_easy[0][1]
    Question = __very_easy[0][0] %(R, V)
    ans = V/R
    margin = 0.01
    return Question, file, ans, margin


def _ve_Q1_v2(R1, R2, R3, V):
    file = None
    if __very_easy[1][1] is not None:
        file = "Veasy/" + __very_easy[1][1]
    Question = __very_easy[1][0] %(R1, R2, R3, V)
    R = (R1 * R2)/(R1 + R2)
    R = (R * R3)/(R + R3)
    ans = V/R
    margin = 0.01
    return Question, file, ans, margin


def _ve_Q1_v3(R, I):
    file = None
    if __very_easy[2][1] is not None:
        file = "Veasy/" + __very_easy[2][1]
    Question = __very_easy[2][0] %(R, I)
    I = I/1000
    ans = I * R
    margin = 0.01
    return Question, file, ans, margin


def _ve_Q1_v4(R1, R2, R3, I):
    file = None
    if __very_easy[3][1] is not None:
        file = "Veasy/" + __very_easy[3][1]
    Question = __very_easy[3][0] %(R1, R2, R3, I)
    R = (R1 * R2)/(R1 + R2)
    R = (R * R3)/(R + R3)
    I = I/1000
    ans = R*I
    margin = 0.01
    return Question, file, ans, margin


def _ve_Q1_v5(V, I):
    file = None
    if __very_easy[4][1] is not None:
        file = "Veasy/" + __very_easy[4][1]
    Question = __very_easy[4][0] %(V, I)
    I = I/1000
    ans = V/I
    margin = 0.01
    return Question, file, ans, margin


def _ve_Q1_v6(V, I, R2, R3):
    file = None
    if __very_easy[5][1] is not None:
        file = "Veasy/" + __very_easy[5][1]
    Question = __very_easy[5][0] % (V, I, R2, R3)
    tmp1 = (V/R2) * 1000
    tmp2 = (V/R3) * 1000
    tmp = tmp1 + tmp2
    I -= tmp
    I /= 1000
    ans = V/I
    margin = 0.01
    return Question, file, ans, margin


def _ve_Q1_v7(V, I, R1, R3):
    file = None
    if __very_easy[6][1] is not None:
        file = "Veasy/" + __very_easy[6][1]
    Question = __very_easy[6][0] % (V, I, R1, R3)
    tmp1 = (V/R1) * 1000
    tmp2 = (V/R3) * 1000
    tmp = tmp1 + tmp2
    I -= tmp
    I /= 1000
    ans = V/I
    margin = 0.01
    return Question, file, ans, margin


def _ve_Q1_v8(V, I, R1, R2):
    file = None
    if __very_easy[7][1] is not None:
        file = "Veasy/" + __very_easy[7][1]
    Question = __very_easy[7][0] % (V, I, R1, R2)
    tmp1 = (V/R1) * 1000
    tmp2 = (V/R2) * 1000
    tmp = tmp1 + tmp2
    I -= tmp
    I /= 1000
    ans = V/I
    margin = 0.01
    return Question, file, ans, margin


#############################################
#Variants of question 2
def _ve_Q2_1(V, R1, R2, R3, R4, R5, version):
    file = None
    if __very_easy[7+version][1] is not None:
        file = "Veasy/" + __very_easy[7+version][1]
    Question = __very_easy[7+version][0] % (V, R1, R2, R3, R4, R5)
    X = np.array([[((1/R1) + (1/R3) + (1/R5)), -1/R3], [-1/R3, ((1/R2)+(1/R3)+(1/R4))]])
    Y = np.array([V/R1, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    ans = 0
    if version == 1:
        ans = abs(V-nodeVoltage[0])
    elif version == 2:
        ans = abs(nodeVoltage[1])
    elif version == 3:
        ans = abs(nodeVoltage[0] - nodeVoltage[1])
    elif version == 4:
        ans = abs(nodeVoltage[1])
    elif version == 5:
        ans = abs(nodeVoltage[0])
    margin = 0.01
    ans = float(ans)
    return Question, file, ans, margin


def _ve_Q2_2(V, R1, R2, R3, R4, R5, R6, version):
    file = None
    if __very_easy[12+version][1] is not None:
        file = "Veasy/" + __very_easy[12+version][1]
    Question = __very_easy[12+version][0] % (V, R1, R2, R3, R4, R5, R6)
    X = np.array([[((1/R1)+(1/R2)+(1/R5)), -1/R2, -1/R5], [-1/R2, ((1/R2) + (1/R3) + (1/R4)), -1/R4], [-1/R5, -1/R4, ((1/R4)+(1/R5)+(1/R6))]])
    Y = np.array([V/R1, 0, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    ans = 0
    if version == 1:
        ans = abs(V-nodeVoltage[0])
    elif version == 2:
        ans = abs(nodeVoltage[0]-nodeVoltage[1])
    elif version == 3:
        ans = abs(nodeVoltage[1])
    elif version == 4:
        ans = abs(nodeVoltage[1] - nodeVoltage[2])
    elif version == 5:
        ans = abs(nodeVoltage[0] - nodeVoltage[2])
    elif version == 6:
        ans = abs(nodeVoltage[2])
    margin = 0.01
    ans = float(ans)
    return Question, file, ans, margin


def _ve_Q2_3(V1, V2, R1, R2, R3, R4, R5, R6, version):
    file = None
    if __very_easy[18+version][1] is not None:
        file = "Veasy/" + __very_easy[18+version][1]
    Question = __very_easy[18+version][0] % (V1, V2, R1, R2, R3, R4, R5, R6)
    X = np.array([[((1/R1)+(1/R2)+(1/R4)+(1/R5)), ((-1/R2)+(-1/R4))], [((-1/R2)+(-1/R4)), ((1/R2)+(1/R3)+(1/R4))]])
    Y = np.array([((V1/R1)+(V2/R5)), 0])
    nodeVoltages = np.linalg.solve(X, Y)
    ans = 0
    if version == 1:
        ans = abs(V1-nodeVoltages[0])
    elif version == 2:
        ans = abs(nodeVoltages[0]-nodeVoltages[1])
    elif version == 3:
        ans = abs(nodeVoltages[1])
    elif version == 4:
        ans = abs(nodeVoltages[0]-nodeVoltages[1])
    elif version == 5:
        ans = abs(V2-nodeVoltages[0])
    elif version == 6:
        ans = abs(V2)
    margin = 0.01
    ans = float(ans)
    return Question, file, ans, margin


#############################################
#Variants of question 3
def _ve_Q3_v1(R1, R2, R3, R4, R5, R6, R7, R8, R9, R10):
    file = None
    if __very_easy[25][1] is not None:
        file = "Veasy/" + __very_easy[25][1]
    Question = __very_easy[25][0] % (R1, R2, R3, R4, R5, R6, R7, R8, R9, R10)
    tmp1 = R8 + R9 + R10
    tmp2 = (R7*tmp1)/(R7 + tmp1)
    tmp1 = (R6*tmp2)/(R6+tmp2)
    tmp2 = R4 + R5 + tmp1
    tmp1 = (R3*tmp2)/(R3+tmp2)
    ans = R1 + R2 + tmp1
    margin = 0.01
    return Question, file, ans, margin


def _ve_Q3_v2(R1, R2, R3, R4, R5, R6, R7, R8, R9, R10):
    file = None
    if __very_easy[26][1] is not None:
        file = "Veasy/" + __very_easy[26][1]
    Question = __very_easy[26][0] % (R1, R2, R3, R4, R5, R6, R7, R8, R9, R10)
    tmp1 = R9 + R10
    tmp1 = (tmp1*R8)/(tmp1+R8)
    tmp1 += R6 + R7
    tmp2 = R1 + R2 + R3
    tmp2 = (tmp2*R4)/(tmp2 + R4)
    tmp2 += R5
    ans = (tmp1*tmp2)/(tmp1 + tmp2)
    margin = 0.01
    return Question, file, ans, margin


def _ve_Q3_v3(R1, R2, R3, R4, R5, R6, R7):
    file = None
    if __very_easy[27][1] is not None:
        file = "Veasy/" + __very_easy[27][1]
    Question = __very_easy[27][0] % (R1, R2, R3, R4, R5, R6, R7)
    Ra = (R4*R5)/(R4 + R5 + R7)
    Rb = (R5*R7)/(R4 + R5 + R7)
    Rc = (R4*R7)/(R4 + R5 + R7)
    tmp = ((R3+Ra)*(R6+Rb))/(R3+Ra+R6+Rb)
    ans = R1 + tmp + Rc + R2
    margin = 0.01
    return Question, file, ans, margin


def _ve_Q3_v4(R1, R2, R3, R4, R5, R6, R7, R8, R9, R10, R11, R12, R13, R14):
    file = None
    if __very_easy[28][1] is not None:
        file = "Veasy/" + __very_easy[28][1]
    Question = __very_easy[28][0] % (R1, R2, R3, R4, R5, R6, R7, R8, R9, R10, R11, R12, R13, R14)
    Ra = (R4*R5)/(R4 + R5 + R7)
    Rb = (R5*R7)/(R4 + R5 + R7)
    Rc = (R4*R7)/(R4 + R5 + R7)
    tmp = ((R3+Ra)*(R6+Rb))/(R3+Ra+R6+Rb)
    tmp = ((tmp+Rc)*(R8+R9))/(tmp+R8+R9+Rc)
    ans = R1+tmp+R2
    margin = 0.01
    return Question, file, ans, margin


#############################################
#question 4 private function
def _ve_Q4_(version):
    file = None
    if __very_easy[28+version][1] is not None:
        file = "Veasy/" + __very_easy[28+version][1]
    Question = __very_easy[28+version][0]
    ans=None
    if version == 1:
        ans = 'NAND'
    elif version == 2:
        ans = 'AND'
    elif version == 3:
        ans = 'NOR'
    elif version == 4:
        ans = 'OR'
    elif version == 5:
        ans = 'XOR'
    elif version == 6:
        ans = 'XNOR'
    margin = 0
    return Question, file, ans, margin, None


#############################################
#variants of question 5
def _ve_Q5_v1(I, R1, R2):
    file = None
    if __very_easy[35][1] is not None:
        file = "Veasy/" + __very_easy[35][1]
    Question = __very_easy[35][0] % (I, R1, R2)
    I /= 1000
    ans = (R1 + R2)*I
    margin = 0.01
    return Question, file, ans, margin


def _ve_Q5_v2(I, R1, R2):
    file = None
    if __very_easy[36][1] is not None:
        file = "Veasy/" + __very_easy[36][1]
    Question = __very_easy[36][0] % (I, R1, R2)
    Req = (R1 * R2)/(R1 + R2)
    I /= 1000
    ans = Req * I
    margin = 0.1
    return Question, file, ans, margin


#############################################
#variants of question 6
def _ve_Q6_v1(V, R1, R2, R3, R4, R5):
    file = None
    if __very_easy[37][1] is not None:
        file = "Veasy/" + __very_easy[37][1]
    Question = __very_easy[37][0] % (V, R1, R2, R3, R4, R5)
    X = np.array([[((1/R1) + (1/R3) + (1/R5)), -1/R3], [-1/R3, ((1/R2)+(1/R3)+(1/R4))]])
    Y = np.array([V/R1, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    ans = pow((V-nodeVoltage[0]), 2)/R1
    ans += pow((nodeVoltage[0]-nodeVoltage[1]), 2)/R3
    ans += pow(nodeVoltage[0], 2)/R5
    ans += pow(nodeVoltage[1], 2)/R2
    ans += pow(nodeVoltage[1], 2)/R4
    margin = 0.01
    ans = float(ans)
    return Question, file, ans, margin


def _ve_Q6_v2(V, R1, R2, R3, R4, R5, R6):
    file = None
    if __very_easy[38][1] is not None:
        file = "Veasy/" + __very_easy[38][1]
    Question = __very_easy[38][0] % (V, R1, R2, R3, R4, R5, R6)
    X = np.array([[((1/R1)+(1/R2)+(1/R5)), -1/R2, -1/R5], [-1/R2, ((1/R2) + (1/R3) + (1/R4)), -1/R4], [-1/R5, -1/R4, ((1/R4)+(1/R5)+(1/R6))]])
    Y = np.array([V/R1, 0, 0])
    nodeVoltage = np.linalg.solve(X, Y)
    ans = pow((V-nodeVoltage[0]), 2)/R1
    ans += pow((nodeVoltage[0]-nodeVoltage[1]), 2)/R2
    ans += pow((nodeVoltage[0]-nodeVoltage[2]), 2)/R5
    ans += pow(nodeVoltage[1], 2)/R3
    ans += pow(nodeVoltage[2], 2)/R6
    ans += pow((nodeVoltage[1]-nodeVoltage[2]), 2)/R4
    margin = 0.01
    ans = float(ans)
    return Question, file, ans, margin


def _ve_Q6_v3(V1, V2, R1, R2, R3, R4, R5, R6):
    file = None
    if __very_easy[39][1] is not None:
        file = "Veasy/" + __very_easy[39][1]
    Question = __very_easy[39][0] % (V1, V2, R1, R2, R3, R4, R5, R6)
    X = np.array([[((1/R1)+(1/R2)+(1/R4)+(1/R5)), ((-1/R2)+(-1/R4))], [((-1/R2)+(-1/R4)), ((1/R2)+(1/R3)+(1/R4))]])
    Y = np.array([((V1/R1)+(V2/R5)), 0])
    nodeVoltage = np.linalg.solve(X, Y)
    ans = pow((V1-nodeVoltage[0]), 2)/R1
    ans += pow((nodeVoltage[0]-nodeVoltage[1]), 2)/R2
    ans += pow(nodeVoltage[1], 2)/R3
    ans += pow((nodeVoltage[0]-nodeVoltage[1]), 2)/R4
    ans += pow((nodeVoltage[0]-V2), 2)/R5
    ans += pow(V2, 2)/R6
    pV = -1*((V1 - nodeVoltage[0])/R1)*V1
    if pV > 0:
        ans += pV
    pV = -1*(((V2 - nodeVoltage[0])/R5) + (V2/R6))*V2
    if pV > 0:
        ans += pV
    margin = 0.01
    ans = float(ans)
    return Question, file, ans, margin


#############################################
#private function for question 7
def _ve_Q7_(A, version):
    file = None
    if __very_easy[39+version][1] is not None:
        file = "Veasy/" + __very_easy[version+39][1]
    Question = __very_easy[39+version][0] % (A)
    ans = A
    margin = 0
    return Question, file, ans, margin


#############################################
#Question 8 function
def Q8():
    file = None
    #Retrieve filename
    if __very_easy[42][1] is not None:
        file = "Veasy/" + __very_easy[42][1]
    #retrieve question
    Question = __very_easy[42][0]
    #calculate answer
    ans = 16
    #calculate margin
    margin = 0
    #return question, file, answer, margin, and units
    return Question, file, ans, margin, None


#############################################
#private function for question 9
def _ve_Q9_(A, version):
    file = None
    if __very_easy[42+version][1] is not None:
        file = "Veasy/" + __very_easy[42+version][1]
    Question = __very_easy[42+version][0] % (A, A)
    ans = None
    if version == 1:
        ans = ['cos(%dt)' % (A), 'cos%dt' % (A), 'cos[%dt]' % (A), 'cos{%dt}' % (A)]
    else:
        ans = ['cos(%dn)' % (A), 'cos%dn' % (A), 'cos[%dn]' % (A), 'cos{%dn}' % (A)]
    margin = 0
    return Question, file, ans, margin


#############################################
#private function for question 10
def _ve_Q10_(version):
    file = None
    if __very_easy[44+version][1] is not None:
        file = "Veasy/" + __very_easy[version+44][1]
    Question = __very_easy[version+44][0]
    if version == 1:
        ans = ['ohm', 'ohms', 'Ω']
    elif version == 2:
        ans = ['farad', 'farads', 'F']
    elif version == 3:
        ans = ['henry', 'henries', 'henrys', 'H']
    margin = 0
    return Question, file, ans, margin


#############################################
#variants of question 11
def _ve_Q11_v1(C1, C2, C3, C4, C5, C6):
    file = None
    if __very_easy[48][1] is not None:
        file = "Veasy/" + __very_easy[48][1]
    Question = __very_easy[48][0] % (C1, C2, C3, C4, C5, C6)
    tmp = C3 + C5
    tmp = (tmp*C4)/(tmp+C4)
    tmp += C6
    ans = pow((1/C1)+(1/tmp)+(1/C2), -1)
    margin = 0.01
    return Question, file, ans, margin


def _ve_Q11_v2(C1, C2, C3, C4, C5, C6, C7, C8, C9):
    file = None
    if __very_easy[49][1] is not None:
        file = "Veasy/" + __very_easy[49][1]
    Question = __very_easy[49][0] % (C1, C2, C3, C4, C5, C6, C7, C8, C9)
    tmp = (C8*C9)/(C8+C9)
    tmp += C7
    tmp += ((C3+C4+C6)*C5)/(C3+C4+C5+C6)
    ans = pow((1/C1) + (1/tmp) + (1/C2), -1)
    margin = 0.01
    return Question, file, ans, margin


#############################################
#variants of question 12
def _ve_Q12_v1(L1, L2, L3, L4, L5, L6, L7):
    file = None
    if __very_easy[50][1] is not None:
        file = "Veasy/" + __very_easy[50][1]
    Question = __very_easy[50][0] % (L1, L2, L3, L4, L5, L6, L7)
    tmp = (L6*L7)/(L6 + L7)
    tmp += L4 + L5
    tmp = (L3*tmp)/(L3+tmp)
    tmp = (L2*tmp)/(L2+tmp)
    ans = tmp + L1
    margin = 0.01
    return Question, file, ans, margin


def _ve_Q12_v2(L1, L2, L3, L4, L5, L6, L7, L8, L9, L10):
    file = None
    if __very_easy[51][1] is not None:
        file = "Veasy/" + __very_easy[51][1]
    Question = __very_easy[51][0] % (L1, L2, L3, L4, L5, L6, L7, L8, L9, L10)
    tmp = L9 + L10
    tmp = (tmp*L8)/(tmp+L8)
    tmp = (tmp*L7)/(tmp+L7)
    tmp += L4 + L5 + L6
    tmp = (tmp*L3)/(tmp+L3)
    ans = L1 + L2 + tmp
    margin = 0.01
    return Question, file, ans, margin


#############################################
#Question 13 function
def Q13():
    file = None
    #Retrieve filename
    if __very_easy[52][1] is not None:
        file = "Veasy/" + __very_easy[52][1]
    #Retrieve question
    Question = __very_easy[52][0]
    #Calculate answer
    ans = 2000
    #calculate margin
    margin = 0
    #return question, file, answer, answer margin, and units
    return Question, file, ans, margin, 7


#############################################

#Public function for question 1
def Q1():
    #randomly choose variant of question 1
    version = randint(1, 8)
    Question = None
    file = None
    ans = None
    margin = None
    units = None

    #Generate question
    if version == 1:
        V = randint(1, 25)
        R = randint(1, 1000)
        Question, file, ans, margin = _ve_Q1_v1(R, V)
        units = 1
    elif version == 2:
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        V = randint(1, 25)
        Question, file, ans, margin = _ve_Q1_v2(R1, R2, R3, V)
        units = 1
    elif version == 3:
        I = randint(1, 5000)
        R = randint(1, 1000)
        Question, file, ans, margin = _ve_Q1_v3(R, I)
        units = 2
    elif version == 4:
        I = randint(1, 5000)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        Question, file, ans, margin = _ve_Q1_v4(R1, R2, R3, I)
        units = 2
    elif version == 5:
        I = randint(1, 5000)
        V = randint(1, 25)
        Question, file, ans, margin = _ve_Q1_v5(V, I)
        units = 3
    elif version == 6:
        I = randint(1, 5000)
        V = randint(1, 25)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        Question, file, ans, margin = _ve_Q1_v6(V, I, R2, R3)
        units = 3
    elif version == 7:
        I = randint(1, 5000)
        V = randint(1, 25)
        R1 = randint(1, 1000)
        R3 = randint(1, 1000)
        Question, file, ans, margin = _ve_Q1_v7(V, I, R1, R3)
        units = 3
    elif version == 8:
        I = randint(1, 5000)
        V = randint(1, 25)
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        Question, file, ans, margin = _ve_Q1_v8(V, I, R1, R2)
        units = 3

    #Return question data
    return Question, file, ans, margin, units


#Public function for question 2
def Q2():
    #randomly choose variant of question 2
    version = randint(1, 17)
    Question = None
    file = None
    ans = None
    margin = None
    units = None

    #Generate question
    if version in range(1, 6):
        V = randint(1, 25)
        R1 = randint(1, 10000)
        R2 = randint(1, 10000)
        R3 = randint(1, 10000)
        R4 = randint(1, 10000)
        R5 = randint(1, 10000)
        Question, file, ans, margin = _ve_Q2_1(V, R1, R2, R3, R4, R5, version)
        units = 2
    elif version in range(6, 12):
        V = randint(1, 25)
        R1 = randint(1, 10000)
        R2 = randint(1, 10000)
        R3 = randint(1, 10000)
        R4 = randint(1, 10000)
        R5 = randint(1, 10000)
        R6 = randint(1, 10000)
        Question, file, ans, margin = _ve_Q2_2(V, R1, R2, R3, R4, R5, R6, (version-5))
        units = 2
    elif version in range(12, 18):
        V1 = randint(-25, 25)
        if V1 is 0:
            V1 += 1
        V2 = randint(-25, 25)
        if V2 is 0:
            V2 -= 1
        R1 = randint(1, 10000)
        R2 = randint(1, 10000)
        R3 = randint(1, 10000)
        R4 = randint(1, 10000)
        R5 = randint(1, 10000)
        R6 = randint(1, 10000)
        Question, file, ans, margin = _ve_Q2_3(V1, V2, R1, R2, R3, R4, R5, R6, (version-11))
        units = 2

    #Return question data
    return Question, file, ans, margin, units


#Public function for question 3
def Q3():
    #randomly choose variant of question 3
    version = randint(1, 4)
    question = None
    file = None
    ans = None
    margin = None
    units = None

    #Generate question
    if version == 1:
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        R8 = randint(1, 1000)
        R9 = randint(1, 1000)
        R10 = randint(1, 1000)
        question, file, ans, margin = _ve_Q3_v1(R1, R2, R3, R4, R5, R6, R7, R8, R9, R10)
        units = 3
    elif version == 2:
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        R8 = randint(1, 1000)
        R9 = randint(1, 1000)
        R10 = randint(1, 1000)
        question, file, ans, margin = _ve_Q3_v2(R1, R2, R3, R4, R5, R6, R7, R8, R9, R10)
        units = 3
    elif version == 3:
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        question, file, ans, margin = _ve_Q3_v3(R1, R2, R3, R4, R5, R6, R7)
        units = 3
    elif version == 4:
        R1 = randint(1, 1000)
        R2 = randint(1, 1000)
        R3 = randint(1, 1000)
        R4 = randint(1, 1000)
        R5 = randint(1, 1000)
        R6 = randint(1, 1000)
        R7 = randint(1, 1000)
        R8 = randint(1, 1000)
        R9 = randint(1, 1000)
        R10 = randint(1, 1000)
        R11 = randint(1, 1000)
        R12 = randint(1, 1000)
        R13 = randint(1, 1000)
        R14 = randint(1, 1000)
        question, file, ans, margin = _ve_Q3_v4(R1, R2, R3, R4, R5, R6, R7, R8, R9, R10, R11, R12, R13, R14)
        units = 3

    #Return question data
    return question, file, ans, margin, units


#Public function for question 4
def Q4():
    version = randint(1, 6)
    question, file, ans, margin, units = _ve_Q4_(version)
    return question, file, ans, margin, units


#Public function for question 5
def Q5():
    #randomly choose variant of question 5
    version = randint(1, 2)
    question = None
    file = None
    ans = None
    margin = None
    units = 2

    #Generate question
    I = randint(1, 1000)
    R1 = randint(1, 10000)
    R2 = randint(1, 10000)

    if version == 1:
        question, file, ans, margin = _ve_Q5_v1(I, R1, R2)
    else:
        question, file, ans, margin = _ve_Q5_v2(I, R1, R2)

    #Return question data
    return question, file, ans, margin, units


#Public function for question 6
def Q6():
    #randomly choose variant of question 6
    version = randint(1, 3)
    question = None
    file = None
    ans = None
    margin = None
    units = None

    #Generate question
    if version == 1:
        V = randint(1, 25)
        R1 = randint(1, 10000)
        R2 = randint(1, 10000)
        R3 = randint(1, 10000)
        R4 = randint(1, 10000)
        R5 = randint(1, 10000)
        question, file, ans, margin = _ve_Q6_v1(V, R1, R2, R3, R4, R5)
        units = 4
    elif version == 2:
        V = randint(1, 25)
        R1 = randint(1, 10000)
        R2 = randint(1, 10000)
        R3 = randint(1, 10000)
        R4 = randint(1, 10000)
        R5 = randint(1, 10000)
        R6 = randint(1, 10000)
        question, file, ans, margin = _ve_Q6_v2(V, R1, R2, R3, R4, R5, R6)
        units = 4
    elif version == 3:
        V1 = randint(1, 25)
        V2 = randint(1, 25)
        R1 = randint(1, 10000)
        R2 = randint(1, 10000)
        R3 = randint(1, 10000)
        R4 = randint(1, 10000)
        R5 = randint(1, 10000)
        R6 = randint(1, 10000)
        question, file, ans, margin = _ve_Q6_v3(V1, V2, R1, R2, R3, R4, R5, R6)
        units = 4

    #Return question data
    return question, file, ans, margin, units


#Public function for question 7
def Q7():
    #randomly choose variant of question 6
    version = randint(1, 2)
    #Generate question
    A = randint(-100000000000, 100000000000)
    if A == 0:
        A = 1
    question, file, ans, margin = _ve_Q7_(A, version)
    #Return question data
    return question, file, ans, margin, None


#Public function for question 9
def Q9():
    #randomly choose variant of question 9
    version = randint(1, 2)
    #Generate question
    A = randint(-100000, 100000)
    if A == 0:
        A = 1
    question, file, ans, margin = _ve_Q9_(A, version)
    #Return question data
    return question, file, ans, margin, None


#Public function for question 10
def Q10():
    #randomly choose variant of question 10
    version = randint(1, 3)
    #Generate question
    question, file, ans, margin = _ve_Q10_(version)
    #Return question data
    return question, file, ans, margin, None


#Public function for question 11
def Q11():
    #randomly choose variant of question 11
    version = randint(1, 2)
    question = None
    file = None
    ans = None
    margin = None
    units = None

    #Generate question
    if version == 1:
        C1 = float(randint(1, 100))/10
        C2 = float(randint(1, 100))/10
        C3 = float(randint(1, 100))/10
        C4 = float(randint(1, 100))/10
        C5 = float(randint(1, 100))/10
        C6 = float(randint(1, 100))/10
        question, file, ans, margin = _ve_Q11_v1(C1, C2, C3, C4, C5, C6)
        units = 5
    else:
        C1 = float(randint(1, 100))/10
        C2 = float(randint(1, 100))/10
        C3 = float(randint(1, 100))/10
        C4 = float(randint(1, 100))/10
        C5 = float(randint(1, 100))/10
        C6 = float(randint(1, 100))/10
        C7 = float(randint(1, 100))/10
        C8 = float(randint(1, 100))/10
        C9 = float(randint(1, 100))/10
        question, file, ans, margin = _ve_Q11_v2(C1, C2, C3, C4, C5, C6, C7, C8, C9)
        units = 5

    #Return question data
    return question, file, ans, margin, units


#Public function for question 12
def Q12():
    #randomly choose variant of question 12
    version = randint(1, 2)
    question = None
    file = None
    ans = None
    margin = None
    units = None

    #Generate question
    if version == 1:
        L1 = float(randint(1, 10000))/1000
        L2 = float(randint(1, 10000))/1000
        L3 = float(randint(1, 10000))/1000
        L4 = float(randint(1, 10000))/1000
        L5 = float(randint(1, 10000))/1000
        L6 = float(randint(1, 10000))/1000
        L7 = float(randint(1, 10000))/1000
        question, file, ans, margin = _ve_Q12_v1(L1, L2, L3, L4, L5, L6, L7)
        units = 6
    else:
        L1 = float(randint(1, 10000))/1000
        L2 = float(randint(1, 10000))/1000
        L3 = float(randint(1, 10000))/1000
        L4 = float(randint(1, 10000))/1000
        L5 = float(randint(1, 10000))/1000
        L6 = float(randint(1, 10000))/1000
        L7 = float(randint(1, 10000))/1000
        L8 = float(randint(1, 10000))/1000
        L9 = float(randint(1, 10000))/1000
        L10 = float(randint(1, 10000))/1000
        question, file, ans, margin = _ve_Q12_v2(L1, L2, L3, L4, L5, L6, L7, L8, L9, L10)
        units = 6

    #Return question data
    return question, file, ans, margin, units
