# BSD 3-Clause License
#
# Copyright (c) 2021, Tom Schmitz
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from random import randint
import _veryEasyQuiz as ve
import _EasyQuiz as Ez
import re
import globals
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import six

__normal = []

__hard = []

__very_hard = []


#Function that converts a str to a complex number
def str_2_complex(num: str):
    # support for i
    num = num.replace('i', 'j')
    #Remove spaces
    num = "".join(num.split(' '))
    #Remove leading '+'
    if num[0] is '+':
        num = num[1:]
    #Remove unnecessary '+'
    i = 0
    while i < len(num):
        if num[i] is '+' and num[i - 1] is 'e':
            num = num[:i] + num[i + 1:]
        i += 1
    #Standardize input
    if '+' not in num:
        for i in range(1, len(num)):
            if num[i] is '-' and num[i - 1] is not 'e':
                num = num[:i] + '+' + num[i:]
                break
    # Split into 2 numbers, gaurentees that one number is returned
    num = num.split('+')
    #Ensure that there are 2 numbers
    if len(num) < 2:
        num.append('')
    #Return nothing if too many numbers
    elif len(num) > 2:
        return None
    #Both inputs are imaginary
    if 'j' in num[0] and 'j' in num[1]:
        return complex(0, float(num[0].replace('j', '')) + float(num[1].replace('j', '')))
    #Both inputs are real
    if 'j' not in num[0] and 'j' not in num[1]:
        return complex(float(num[0]) + float(num[1]))
    #Half and Half
    a = None
    b = None
    if 'j' in num[0]:
        num[0] = num[0].replace('j', '')
        a = float(num[1])
        b = float(num[0])
    else:
        num[1] = num[1].replace('j', '')
        a = float(num[0])
        b = float(num[1])
    #Finish conversion
    try:
        return complex(a, b)
    except Exception:
        return None


#############################################

# Unit index:
# A = 1
# V = 2
# ohms = 3
# watts = 4
# F = 5
# H = 6
# Hz = 7
# V/V = 8
# s = 9
# No units = None
class QuizLogic:
    def __init__(self, difficulty, id):
        #Initialize data members
        self.userID = id
        self.__difficulty = difficulty
        self.__questions = []
        self.__files = []
        self.__answers = []
        self.__margin = []
        self.__units = []
        self.__userAnswers = []
        self.__correct = [False, False, False, False, False]
        self.__score = 0
        self.__index = 0

        #load questions, answers, and margins based on difficulty...
        if difficulty == 'very easy':
            #Load very easy questions
            ve.load_very_easy()
            #Initialize used array
            used = [False, False, False, False, False, False, False, False, False, False, False, False, False]
            #Loop while there are less than 5 questions selected
            while len(self.__questions) < 5:
                #Generate random integer between 0 and 12
                Q = randint(0, 12)

                #Check if question is used
                if used[Q] is False:
                    #Mark question used
                    used[Q] = True
                    question = None
                    file = None
                    ans = None
                    margin = None
                    unit = None

                    #Call function to load question, picturem, answer, answer margin, and units
                    if Q == 0:
                        question, file, ans, margin, unit = ve.Q1()
                    elif Q == 1:
                        question, file, ans, margin, unit = ve.Q2()
                    elif Q == 2:
                        question, file, ans, margin, unit = ve.Q3()
                    elif Q == 3:
                        question, file, ans, margin, unit = ve.Q4()
                    elif Q == 4:
                        question, file, ans, margin, unit = ve.Q5()
                    elif Q == 5:
                        question, file, ans, margin, unit = ve.Q6()
                    elif Q == 6:
                        question, file, ans, margin, unit = ve.Q7()
                    elif Q == 7:
                        question, file, ans, margin, unit = ve.Q8()
                    elif Q == 8:
                        question, file, ans, margin, unit = ve.Q9()
                    elif Q == 9:
                        question, file, ans, margin, unit = ve.Q10()
                    elif Q == 10:
                        question, file, ans, margin, unit = ve.Q11()
                    elif Q == 11:
                        question, file, ans, margin, unit = ve.Q12()
                    elif Q == 12:
                        question, file, ans, margin, unit = ve.Q13()

                    #Add question data to lists
                    if question is not None:
                        self.__questions.append(question)
                        self.__files.append(file)
                        self.__answers.append(ans)
                        self.__margin.append(margin)
                        self.__units.append(unit)
            #free memory allocated to very easy list
            ve.freeVeryEasy()
        elif difficulty == 'easy':
            #Load easy questions
            Ez.load_easy()
            #Initialize used array
            used = [False, False, False, False, False, False, False, False, False, False, False, False, False, False, False]
            #Loop while there are less than 5 questions selected
            while len(self.__questions) < 5:
                #Generate random integer between 0 and 14
                Q = randint(0, 14)

                #Check if question is used
                if used[Q] is False:
                    #Mark question used
                    used[Q] = True
                    question = None
                    file = None
                    ans = None
                    margin = None
                    units = None
                    #Call function to load question, picturem, answer, answer margin, and units
                    if Q == 0:
                        question, file, ans, margin, units = Ez.Q1()
                    elif Q == 1:
                        question, file, ans, margin, units = Ez.Q2()
                    elif Q == 2:
                        question, file, ans, margin, units = Ez.Q3()
                    elif Q == 3:
                        question, file, ans, margin, units = Ez.Q4()
                    elif Q == 4:
                        question, file, ans, margin, units = Ez.Q5()
                    elif Q == 5:
                        question, file, ans, margin, units = Ez.Q6()
                    elif Q == 6:
                        question, file, ans, margin, units = Ez.Q7()
                    elif Q == 7:
                        question, file, ans, margin, units = Ez.Q8()
                    elif Q == 8:
                        question, file, ans, margin, units = Ez.Q9()
                    elif Q == 9:
                        question, file, ans, margin, units = Ez.Q10()
                    elif Q == 10:
                        question, file, ans, margin, units = Ez.Q11()
                    elif Q == 11:
                        question, file, ans, margin, units = Ez.Q12()
                    elif Q == 12:
                        question, file, ans, margin, units = Ez.Q13()
                    elif Q == 13:
                        question, file, ans, margin, units = Ez.Q14()
                    elif Q == 14:
                        question, file, ans, margin, units = Ez.Q15()

                    #Add question data to lists
                    if question is not None:
                        self.__questions.append(question)
                        self.__files.append(file)
                        self.__answers.append(ans)
                        self.__margin.append(margin)
                        self.__units.append(units)
            #free memory allocated to easy list
            Ez.freeEasy()

    #Function that processes user's answer
    def ans(self, userAnswer: str):
        #Check answer type
        if (type(self.__answers[self.__index]) is int) or (type(self.__answers[self.__index]) is float):
            #Create regex statements to sort out numbers and units
            dec = re.compile(r'[^\d.-]+')
            units = re.compile(r'[0-9. ]+')
            #Get number as a string
            num = dec.sub('', userAnswer)

            #Convert number into float or integer
            if type(self.__answers[self.__index]) is int:
                num = int(num)
            else:
                # Check for multiple decimal points
                if (num.count(".") > 1) or (num.count('-') > 1):
                    #Return error if multiple decimal points are found
                    return False, "Unable to process answer, multiple instances of '.' or '-' occured."
                else:
                    #Convert into float
                    num = float(num)

            #Retrieve units from string
            userUnits = units.sub('', userAnswer)
            if userUnits == '-':
                #Get rid of '-' symbol
                userUnits = userUnits.strip('-')

            #Check prefix
            multiple = 1
            prefix = False

            #First check if units were given
            if len(userUnits) > 0:
                #adjust multiple based off of prefix and set prefix flag to true
                if userUnits[0] is 'p':
                    multiple = 1e-12
                    prefix = True
                elif userUnits[0] is 'n':
                    multiple = 1e-9
                    prefix = True
                elif userUnits[0] is 'u':
                    multiple = 1e-6
                    prefix = True
                elif userUnits[0] is 'm':
                    multiple = 1e-3
                    prefix = True
                elif userUnits[0] is 'k':
                    multiple = 1e3
                    prefix = True
                elif userUnits[0] is 'M':
                    multiple = 1e6
                    prefix = True
                elif userUnits[0] is 'G':
                    multiple = 1e9
                    prefix = True
                #Check prefix flag
                if prefix:
                    #strip prefix
                    userUnits = userUnits[1:]
                #Encode units passed in
                if 'A' == userUnits:
                    units = 1
                elif 'V/V' == userUnits:
                    units = 8
                elif 'V' == userUnits:
                    units = 2
                elif ('ohms' == userUnits.lower()) or ('ohm' == userUnits.lower()) or ('Ω' == userUnits):
                    units = 3
                elif 'W' == userUnits:
                    units = 4
                elif 'F' == userUnits:
                    units = 5
                elif 'Hz' == userUnits:
                    units = 7
                elif 'H' == userUnits:
                    units = 6
                elif 's' == userUnits:
                    units = 9
            else:
                units = None

            #Check if units are correct
            if units == self.__units[self.__index]:
                num *= multiple  #Adjust numerical part to fit the base
                #Check if numerical answer is in range
                if (self.__answers[self.__index] - self.__margin[self.__index]) <= num <= (self.__answers[self.__index] + self.__margin[self.__index]):
                    self.__score += 1  #Add to score
                    self.__correct[self.__index] = True  #Set correct flag
        #Check for string type answer
        elif type(self.__answers[self.__index]) is str:
            #Check string lengths
            if len(userAnswer) == len(self.__answers[self.__index]):
                #Check if user answer matches generated answer
                if userAnswer.lower() == self.__answers[self.__index].lower():
                    #Add to score
                    self.__score += 1
                    self.__correct[self.__index] = True
##################################################################################
        elif type(self.__answers[self.__index]) is complex:
            units = re.compile(r'[0-9.i\+j\- ]+')
            userUnits = units.sub('', userAnswer)
            userAnswer = userAnswer.replace(userUnits, '')
            num = str_2_complex(userAnswer)

            #Check prefix
            multiple = 1
            prefix = False

            #First check if units were given
            if len(userUnits) > 0:
                #adjust multiple based off of prefix and set prefix flag to true
                if userUnits[0] is 'p':
                    multiple = 1e-12
                    prefix = True
                elif userUnits[0] is 'n':
                    multiple = 1e-9
                    prefix = True
                elif userUnits[0] is 'u':
                    multiple = 1e-6
                    prefix = True
                elif userUnits[0] is 'm':
                    multiple = 1e-3
                    prefix = True
                elif userUnits[0] is 'k':
                    multiple = 1e3
                    prefix = True
                elif userUnits[0] is 'M':
                    multiple = 1e6
                    prefix = True
                elif userUnits[0] is 'G':
                    multiple = 1e9
                    prefix = True
                #Check prefix flag
                if prefix:
                    #strip prefix
                    userUnits = userUnits[1:]
                #Encode units passed in
                if 'A' == userUnits:
                    units = 1
                elif 'V/V' == userUnits:
                    units = 8
                elif 'V' == userUnits:
                    units = 2
                elif ('ohms' == userUnits.lower()) or ('ohm' == userUnits.lower()) or ('Ω' == userUnits):
                    units = 3
                elif 'W' == userUnits:
                    units = 4
                elif 'F' == userUnits:
                    units = 5
                elif 'Hz' == userUnits:
                    units = 7
                elif 'H' == userUnits:
                    units = 6
                elif 's' == userUnits:
                    units = 9
            else:
                units = None

            #Check if units are correct
            if units == self.__units[self.__index]:
                num *= multiple  #Adjust numerical part to fit the base
                #Check if numerical answer is in range
                if (self.__answers[self.__index].real - self.__margin[self.__index].real) <= num.real <= (self.__answers[self.__index].real + self.__margin[self.__index].real):
                    if (self.__answers[self.__index].imag - self.__margin[self.__index].imag) <= num.imag <= (self.__answers[self.__index].imag + self.__margin[self.__index].imag):
                        self.__score += 1  #Add to score
                        self.__correct[self.__index] = True  #Set correct flag

##################################################################################
        #Check if user answer is in list of answers
        else:
            #Check if exact user answer is in list or if lowercase user answer is in the list
            if (userAnswer in self.__answers[self.__index]) or (userAnswer.lower() in self.__answers[self.__index]):
                #Add to score
                self.__score += 1
                self.__correct[self.__index] = True
            #Set first item as correct answer
            self.__answers[self.__index] = self.__answers[self.__index][0]

        #append user answer to list
        self.__userAnswers.append(userAnswer)
        #adjust index
        self.__index += 1
        #return success
        return True, None

    #Function that retrieves the current question
    def nextQuestion(self):
        #Check if index is in range
        if self.__index < 5:
            #return the question, file, and question number
            return self.__questions[self.__index], self.__files[self.__index], (self.__index + 1)
        else:
            return None, None, None

    #Function that checks if results are ready to be shown
    def showResults(self):
        return self.__index == 5

    #Function that translates the units from numerical values to unit symbols
    def _translate_units(self):
        #Decode units
        for i in range(0, 5):
            if self.__units[i] == 1:
                self.__units[i] = 'A'
            elif self.__units[i] == 2:
                self.__units[i] = 'V'
            elif self.__units[i] == 3:
                self.__units[i] = 'Ω'
            elif self.__units[i] == 4:
                self.__units[i] = 'W'
            elif self.__units[i] == 5:
                self.__units[i] = 'F'
            elif self.__units[i] == 6:
                self.__units[i] = 'H'
            elif self.__units[i] == 7:
                self.__units[i] = 'Hz'
            elif self.__units[i] == 8:
                self.__units[i] = 'V/V'
            elif self.__units[i] == 9:
                self.__units[i] = 's'
        return

    def render_table(self, col_width=3.0, row_height=0.625, font_size=14,
                     header_color=globals.sEmbedColor, row_colors=['#f1f1f2', 'w'],
                     edge_color='w', bbox=[0, 0, 1, 1], header_columns=0, ax=None,
                     **kwargs):
        self._translate_units()
        # Loop through answers and limit them to 12 decimal places
        for i in range(0, len(self.__answers)):
            if type(self.__answers[i]) is float:
                self.__answers[i] = f'{self.__answers[i]:.12f}'.rstrip('0')
        # Create dataframe for image generation
        data = pd.DataFrame()
        data['Question'] = list(range(1, 6))
        data['User Answer'] = self.__userAnswers
        data['Answer'] = self.__answers
        data['Units'] = self.__units
        data['Error Allowed'] = self.__margin
        data['Correct'] = self.__correct

        # Generate the image
        if ax is None:
            size = (np.array(data.shape[::-1]) + np.array([0, 1])) * np.array([col_width, row_height])
            fig, ax = plt.subplots(figsize=size)
            ax.axis('off')

        mpl_table = ax.table(cellText=data.values, bbox=bbox, colLabels=data.columns, **kwargs)

        mpl_table.auto_set_font_size(False)
        mpl_table.set_fontsize(font_size)

        for k, cell in six.iteritems(mpl_table._cells):
            cell.set_edgecolor(edge_color)
            if k[0] == 0 or k[1] < header_columns:
                cell.set_text_props(weight='bold', color='w')
                cell.set_facecolor(header_color)
            else:
                cell.set_facecolor(row_colors[k[0] % len(row_colors)])

        return self.__score, fig
